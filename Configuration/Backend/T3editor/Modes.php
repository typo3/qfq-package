<?php
/**
 * Created by PhpStorm.
 * User: bbaer
 * Date: 5/3/19
 * Time: 6:30 PM
 */

return [
    'sql' => [
        'module' => 'cm/mode/sql/sql',
        'extensions' => ['sql'],
    ],
    'html' => [
        'module' => 'cm/mode/htmlmixed/htmlmixed',
        'extensions' => ['htm', 'html'],
    ],
];
