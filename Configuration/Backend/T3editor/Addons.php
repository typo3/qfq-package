<?php
/**
 * Created by PhpStorm.
 * User: bbaer
 * Date: 5/3/19
 * Time: 6:31 PM
 */

return [
    'edit/matchbrackets' => [
        'module' => 'cm/addon/edit/matchbrackets',
        'options' => [
            'matchBrackets' => true,
        ],
    ],
    'mode/overlay' => [
        'module' => 'cm/addon/mode/overlay',
        'modes' => ['sql'],
    ],
    'hint/sql-hint' => [
        'module' => 'cm/addon/hint/sql-hint',
        'modes' => ['sql'],
    ],
];
