<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 8/17/17
 * Time: 10:29 PM
 */

namespace IMATHUZH\Qfq\External;

require_once(__DIR__ . '/../../vendor/autoload.php');

use IMATHUZH\Qfq\External\AutoCron;

global $argv;

try {
    try {
        $verbose = false;

        if (isset($argv[1]) && $argv[1] == '-v') {
            $verbose = true;
        }

        // If this script is started via an absolute path: set CWD to the T3 installation directory
        if ($argv[0][0] == '/') {
            $baseDir='';
            // /var/www/html/typo3conf/ext/qfq/Classes/External/autocron.php
            $arr = explode('/', $argv[0]);
            $cnt = count($arr) - 6; // Strip the last '6' entries, these are below T3 basedir until this script.
            for ($ii = 0; $ii < $cnt; $ii++) {
                if($arr[$ii]!='') {
                    $baseDir .= '/' . $arr[$ii];
                }
            }
            chdir($baseDir);
        }

        $autoCron = new AutoCron($verbose);
        $autoCron->process();

    } catch (\UserFormException $e) {
        $answer[API_MESSAGE] = $e->formatMessage();
    } catch (\CodeException $e) {
        $answer[API_MESSAGE] = $e->formatMessage();
    } catch (\DbException $e) {
        $answer[API_MESSAGE] = $e->formatMessage();
    }
} catch (\Exception $e) {
    $answer[API_MESSAGE] = "Generic Exception: " . $e->getMessage();
}

if (!empty($answer[API_MESSAGE])) {
    echo $answer[API_MESSAGE];
}

