###
#
# GETFUNCTIONSHASH() is used for checking whether this file has been played properly in DatabaseUpdate.php
#
DROP FUNCTION IF EXISTS GETFUNCTIONSHASH;
CREATE FUNCTION GETFUNCTIONSHASH()
  RETURNS TEXT
  DETERMINISTIC
  SQL SECURITY INVOKER
BEGIN
  RETURN '%%FUNCTIONSHASH%%';
END;


###
#
# QMORE(input, maxlen)
# inserts a span into `input` after `maxlen` number of characters and returns it.
#
DROP FUNCTION IF EXISTS QMORE;
CREATE FUNCTION QMORE(input TEXT, maxlen INT)
  RETURNS TEXT
  DETERMINISTIC
  SQL SECURITY INVOKER
BEGIN
  DECLARE output TEXT;
  IF maxlen < 1 THEN
    SET maxlen = 1;
  END IF;
  IF CHAR_LENGTH(input) > maxlen THEN
    SET output = CONCAT(INSERT(input, maxlen, 0, '<span class="qfq-more-text">'), '</span>');
  ELSE
    SET output = input;
  END IF;
  RETURN output;
END;


###
#
# QBAR(input)
# replaces '|' in `input` with '\|'
#
DROP FUNCTION IF EXISTS QBAR;
CREATE FUNCTION QBAR(input TEXT)
  RETURNS TEXT
  DETERMINISTIC
  SQL SECURITY INVOKER
BEGIN
  DECLARE output TEXT;
  SET output = REPLACE(input, '|', '\\|');
  RETURN output;
END;

###
#
# QNL2BR(input)
# replaces '|' in `input` with '\|'
#
DROP FUNCTION IF EXISTS QNL2BR;
CREATE FUNCTION QNL2BR(input TEXT)
  RETURNS TEXT
  DETERMINISTIC
  SQL SECURITY INVOKER
BEGIN
  DECLARE output TEXT;
  SET output = REPLACE(REPLACE(input, CHAR(13), ''), CHAR(10), '<br>');
  RETURN output;
END;

###
#
# QIFEMPTY(input, token)
# If 'input' is empty, replaces by 'token'
#
DROP FUNCTION IF EXISTS QIFEMPTY;
CREATE FUNCTION QIFEMPTY(input TEXT, token TEXT)
  RETURNS TEXT
  DETERMINISTIC
  SQL SECURITY INVOKER
BEGIN
  DECLARE output TEXT;
  SET output = IF(input = '', token, input);
  RETURN output;
END;
