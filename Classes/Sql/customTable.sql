CREATE TABLE IF NOT EXISTS `Period`
(
  `id`       INT(11)      NOT NULL AUTO_INCREMENT,
  `start`    DATETIME     NOT NULL,
  `name`     VARCHAR(255) NOT NULL,
  `modified` TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP
    ON UPDATE CURRENT_TIMESTAMP,
  `created`  DATETIME     NOT NULL,
  PRIMARY KEY (`id`),
  KEY `start` (`start`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 0;

# Default record for table Period
INSERT INTO Period (start, name, created)
VALUES (NOW(), 'dummy', NOW());

