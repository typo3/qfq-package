<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 4/25/16
 * Time: 8:02 PM
 */

namespace IMATHUZH\Qfq\Api;

require_once(__DIR__ . '/../../vendor/autoload.php');

use IMATHUZH\Qfq\Core\File;
 

/**
 * Process File Upload - immediately when the the user selects a file.
 * Return JSON encoded answer
 *
 * status: success|error
 * message: <message>
 *
 * Description:
 *
 * Upload successful & File accepted by server.
 *  status = 'success'
 *  message = <message>
 *
 * Upload failed:
 *  status = 'error'
 *  message = <message>
 */

$answer = array();

$answer[API_STATUS] = API_ANSWER_STATUS_ERROR;
$answer[API_MESSAGE] = '';

try {
    try {

        $fileUpload = new File();

        $fileUpload->process();

        $answer[API_MESSAGE] = 'upload: success';
//    $answer[API_REDIRECT] = API_ANSWER_REDIRECT_NO;
        $answer[API_STATUS] = API_ANSWER_STATUS_SUCCESS;

    } catch (\UserFormException $e) {
        $answer[API_MESSAGE] = $e->formatMessage();
    } catch (\CodeException $e) {
        $answer[API_MESSAGE] = $e->formatMessage();
    }
} catch (\Exception $e) {
    $answer[API_MESSAGE] = "Generic Exception: " . $e->getMessage();
}

header("Content-Type: application/json");
echo json_encode($answer);

