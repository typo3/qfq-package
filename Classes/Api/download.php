<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 4/17/17
 * Time: 5:51 PM
 *
 * Check: CODING.md > Download
 */

namespace IMATHUZH\Qfq\Api;

require_once(__DIR__ . '/../../vendor/autoload.php');

use IMATHUZH\Qfq\Core\Report\Download;


set_error_handler("\\IMATHUZH\\Qfq\\Core\\Exception\\ErrorHandler::exception_error_handler");

$output = '';

try {
    try {
        $download = new Download();

        // If all is fine: process() will output file via print() !!
        $output = $download->process(STORE_SIP, OUTPUT_MODE_DIRECT);

    } catch (\CodeException $e) {
        $output = $e->formatMessage();
    } catch (\DbException $e) {
        $output = $e->formatMessage();
    } catch (\DownloadException $e) {
        $output = $e->formatMessage();
    }
} catch (\Exception $e) {
    $output = "Exception: " . $e->getMessage();
}

echo $output;
