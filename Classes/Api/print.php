<?php
/**
 * Created by PhpStorm.
 * User: ep
 * Date: 12/23/15
 * Time: 6:17 PM
 */


namespace IMATHUZH\Qfq\Api;

require_once(__DIR__ . '/../../vendor/autoload.php');

use IMATHUZH\Qfq\Core\Report\Html2Pdf;

/**
 * Main
 */
try {
    $html2pdf = new Html2Pdf();

    $html2pdf->outputHtml2Pdf();

} catch (\Exception $e) {
    echo "Exception: " . $e->getMessage();
}
