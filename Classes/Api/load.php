<?php
/**
 * Created by PhpStorm.
 * User: ep
 * Date: 12/23/15
 * Time: 6:17 PM
 */

namespace IMATHUZH\Qfq\Api;

require_once(__DIR__ . '/../../vendor/autoload.php');

use IMATHUZH\Qfq\Core\Store\Store;
use IMATHUZH\Qfq\Core\QuickFormQuery;
 


/**
 * Return JSON encoded answer
 *
 * status: success|error
 * message: <message>
 * redirect: client|url|no
 * redirect-url: <url>
 * field-name: <field name>
 * field-message: <message>
 * form-data: [ fieldname1 => value1, fieldname2 => value2, ... ]
 * form-control: [ fieldname1 => status1, fieldname2 => status2, ... ]  status: show|hide, enabled|disabled,
 * readonly|readwrite
 *
 * Description:
 *
 * Save successful. Button 'close', 'new'. Form.forward: 'auto'. Client logic decide to redirect or not. Show message
 * if no redirect. status = 'success' message = <message> redirect = 'client'
 *
 * Save successful. Button 'close': Form.forward: 'page'. Client redirect to url.
 *  status = 'success'
 *  message = <message>
 *  redirect = 'url'
 *  redirect-url = <URL>
 *
 * Save failed: Button: any. Show message and set 'alert' on _optional_ specified form element. Bring 'pill' of
 * specified form element to front. status = 'error' message = <message> redirect = 'no' Optional: field-name = <field
 * name> field-message = <message appearing as tooltip (or similar) near the form element>
 */

$answer = array();

$answer[API_REDIRECT] = API_ANSWER_REDIRECT_NO;
$answer[API_STATUS] = API_ANSWER_STATUS_ERROR;
$answer[API_MESSAGE] = '';

try {
    try {
        $qfq = new QuickFormQuery(['bodytext' => '']);

        $data = $qfq->updateForm();

//    $answer[API_REDIRECT] = $qfq->getForwardMode($answer[API_REDIRECT_URL]);
        $answer[API_STATUS] = API_ANSWER_STATUS_SUCCESS;
        $answer[API_MESSAGE] = 'load: success';
        $answer[API_FORM_UPDATE] = $data[API_FORM_UPDATE];
        $answer[API_ELEMENT_UPDATE] = $data[API_ELEMENT_UPDATE];
//    unset($answer[API_FORM_UPDATE][API_ELEMENT_UPDATE]);

    } catch (\UserFormException $e) {
        $answer[API_MESSAGE] = $e->formatMessage();

        $val = Store::getVar(SYSTEM_FORM_ELEMENT, STORE_SYSTEM);
        if ($val !== false)
            $answer[API_FIELD_NAME] = $val;

        $val = Store::getVar(SYSTEM_FORM_ELEMENT_MESSAGE, STORE_SYSTEM);
        if ($val !== false)
            $answer[API_FIELD_MESSAGE] = $val;

    } catch (\CodeException $e) {
        $answer[API_MESSAGE] = $e->formatMessage();
    } catch (\DbException $e) {
        $answer[API_MESSAGE] = $e->formatMessage();
    }

} catch (\Exception $e) {
    $answer[API_MESSAGE] = "Generic Exception: " . $e->getMessage();
}

header("Content-Type: application/json");
echo json_encode($answer);

