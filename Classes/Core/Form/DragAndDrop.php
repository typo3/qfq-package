<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 3/13/17
 * Time: 9:29 PM
 */

namespace IMATHUZH\Qfq\Core\Form;

use IMATHUZH\Qfq\Core\Store\Store;
use IMATHUZH\Qfq\Core\Database\Database;
use IMATHUZH\Qfq\Core\Evaluate;
 


/**
 * Class DragAndDrop
 * @package qfq
 */
class DragAndDrop {

    /**
     * @var Database instantiated class
     */
    private $db = null;

    /**
     * @var Store
     */
    private $store = null;

    /**
     * @var Evaluate instantiated class
     */
    protected $evaluate = null;  // copy of the loaded form

    /**
     * @var array
     */
    private $formSpec = null;

    /**
     * @param array $formSpec F_TABLE_NAME, F_DRAG_AND_DROP_ORDER_SQL, F_DRAG_AND_DROP_INTERVAL
     * @param bool|false $phpUnit
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function __construct(array $formSpec = array(), $phpUnit = false) {

        #TODO: rewrite $phpUnit to: "if (!defined('PHPUNIT_QFQ')) {...}"
        $this->formSpec = $formSpec;

        $dbIndex = DB_INDEX_DEFAULT;  //Hier muss noch die aktuelle DB ermittelt werden (kann im Form angegeben sein) - Gerade im Formular FORM Editor genau testen!
        $this->db = new Database($dbIndex);

        $this->store = Store::getInstance('', $phpUnit);
//        $this->evaluate = new Evaluate($this->store, $this->db);
    }

    /**
     * Reorder the elements according formSpec[F_DRAG_AND_DROP_ORDER_SQL]
     *
     * @return array|int
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function process() {

        if (!is_array($this->formSpec[F_DRAG_AND_DROP_ORDER_SQL]) || count($this->formSpec[F_DRAG_AND_DROP_ORDER_SQL]) == 0) {
            throw new \UserFormException('Reorder SQL failed - expect at least one record, but got nothing. Check: Form.parameter.' . F_DRAG_AND_DROP_ORDER_SQL, ERROR_DND_EMPTY_REORDER_SQL);
        }

        $dragId = $this->store->getVar(DND_DRAG_ID, STORE_CLIENT, SANITIZE_ALLOW_ALNUMX);
        $setTo = $this->store->getVar(DND_SET_TO, STORE_CLIENT, SANITIZE_ALLOW_ALNUMX);
        $hoverId = $this->store->getVar(DND_HOVER_ID, STORE_CLIENT, SANITIZE_ALLOW_ALNUMX);

        $orderInterval = empty($this->formSpec[F_ORDER_INTERVAL]) ? 1 : $this->formSpec[F_ORDER_INTERVAL];
        $orderColumn = empty($this->formSpec[F_ORDER_COLUMN]) ? F_ORDER_COLUMN_NAME : $this->formSpec[F_ORDER_COLUMN];

//        if (!is_array($this->formSpec[F_DRAG_AND_DROP_ORDER_SQL])) {
//            return [];
//        }

        $data = $this->reorder($this->formSpec[F_DRAG_AND_DROP_ORDER_SQL], $dragId, $setTo, $hoverId, $orderColumn,
            $orderInterval, $this->formSpec[F_TABLE_NAME]);

        return $data;
    }

    /**
     * Calculate new ord values. The array rows$ contains the old order with
     * [
     *   [
     *     [DND_COLUMN_ID] => 1,
     *     [DND_COLUMN_ORD] => 10
     *   ], [
     *     [DND_COLUMN_ID] => 2,
     *     [DND_COLUMN_ORD] => 20
     *   ],
     *   ...
     * ]
     *
     * @param array $rows Array with id/ord in the old order.
     * @param int $dragId Id of the element which has been drag'ed
     * @param string $setTo DND_SET_TO_BEFORE|DND_SET_TO_AFTER   Indicates if the drop zone is before or after the $hoverId
     * @param int $hoverId Id of element where the drag'ed element has been dropped on.
     * @param string $orderColumn Table column where to save the new calculated order.
     * @param int $orderInterval Order increment.
     * @param string $tableName Table name where to update the order records.
     * @return array          Array with html-id references to update order values in the browser. Check PROTOCOL.md for 'element-update'.
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    private function reorder(array $rows, $dragId, $setTo, $hoverId, $orderColumn, $orderInterval, $tableName) {
        $ord = $orderInterval;
        $ordDragOld = -1;
        $data = array();

        // Reorder. Get index for 'drag' and 'hover'
        foreach ($rows as $key => $row) {

            // the dragged element: skip old position.
            if ($row[DND_COLUMN_ID] == $dragId) {
                $ordDragOld = $row[DND_COLUMN_ORD];
                continue;
            }

            // the dragged element: new position.
            if ($row[DND_COLUMN_ID] == $hoverId) {

                switch ($setTo) {
                    case DND_SET_TO_BEFORE:
                        $data = $this->setNewOrder($tableName, $orderColumn, $dragId, $ordDragOld, $ord, $data);
                        $ord += $orderInterval;
                        $data = $this->setNewOrder($tableName, $orderColumn, $row[DND_COLUMN_ID], $row[DND_COLUMN_ORD], $ord, $data);
                        break;

                    case DND_SET_TO_AFTER:
                        $data = $this->setNewOrder($tableName, $orderColumn, $row[DND_COLUMN_ID], $row[DND_COLUMN_ORD], $ord, $data);
                        $ord += $orderInterval;
                        $data = $this->setNewOrder($tableName, $orderColumn, $dragId, $ordDragOld, $ord, $data);
                        break;

                    default:
                        throw new \CodeException(json_encode([ERROR_MESSAGE_TO_USER => 'Unknown "setTo" string', ERROR_MESSAGE_TO_DEVELOPER => "Token found: " . $setTo]), ERROR_UNKNOWN_TOKEN);
                }
            } else {
                $data = $this->setNewOrder($tableName, $orderColumn, $row[DND_COLUMN_ID], $row[DND_COLUMN_ORD], $ord, $data);
            }
            $ord += $orderInterval;
        }

        return $data;
    }

    /**
     * @param string $tableName
     * @param string $orderColumn
     * @param int $id
     * @param int $ordOld
     * @param int $ordNew
     * @param array $data
     * @return array
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    private function setNewOrder($tableName, $orderColumn, $id, $ordOld, $ordNew, array $data) {

        if ($ordNew == $ordOld) {
            return $data;
        }

        $this->db->sql("UPDATE $tableName SET $orderColumn=? WHERE id=?", ROW_REGULAR, [$ordNew, $id]);

        // Converting to string is necessary: JSON detects int else.
        $data[API_ELEMENT_UPDATE][DND_ORD_HTML_ID_PREFIX . $id][API_ELEMENT_CONTENT] = (string)$ordNew;

        return $data;
    }
}