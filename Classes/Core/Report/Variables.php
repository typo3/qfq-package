<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2010 Glowbase GmbH
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMATHUZH\Qfq\Core\Report;

use IMATHUZH\Qfq\Core\Evaluate;
 


/**
 * Class Variables
 * @package qfq
 */
class Variables {

    public $resultArray = array();

    // TODO to explain
    private $tt_content_uid = 0;
    /**
     * @var Evaluate
     */
    private $eval;

    /**
     * @param Evaluate $eval
     * @param int $tt_content_uid
     */
    public function __construct(Evaluate $eval, $tt_content_uid = 0) {

        $this->eval = $eval;

        // specified once during initialisation.
        $this->tt_content_uid = $tt_content_uid;

    }

    /**
     * Matches on the variables {{10.20.name}} and substitutes them with the values
     *
     * @param string $text
     *
     * @return mixed
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function doVariables($text) {

        if ($text == '') {
            return '';
        }

//        $str = preg_replace_callback("/{{(([a-zA-Z0-9.:_])*)}}/", array($this, 'replaceVariables'), $text);
        // Process all {{x[.x].name}}
        $str = preg_replace_callback('/{{\s*(([0-9]+.)+[a-zA-Z0-9_.]+)\s*}}/', array($this, 'replaceVariables'), $text);

        // Try the Stores
        $str = $this->eval->parse($str);

        return $str;
    }

    /**
     * Callbackfunction called by variableSQL()
     * Replaces the variablenames with the value from the resultArray
     *
     * @param $matches
     *
     * @return string The content that is displayed on the website
     * @internal param string $content : The PlugIn content
     * @internal param array $conf : The PlugIn configuration
     */
    public function replaceVariables($matches) {

        // $matches[0]: {{variablename: 10.20.<columnname>}}
        // $matches[1]: variablename: 10.20.<columnname>
        $data = $matches[0];

        // index of last '.'
        $pos = strrpos($matches[1], ".");
        if ($pos !== false) {
            $fullLevel = substr($matches[1], 0, $pos + 1);
            $varName = substr($matches[1], $pos + 1, strlen($matches[1]));

            if (isset($this->resultArray[$fullLevel][$varName])) {
                $data = $this->resultArray[$fullLevel][$varName];
            }
        }

        // If not replaced, try the Stores
//        if ($data === $matches[0]) {
//            $dataTmp = $this->eval->parse($data);
//            if ($dataTmp !== false)
//                $data = $dataTmp;
//        }

        return $data;
    }

    /**
     * Collect Global Variables
     *
     * @param    void
     *
     * @return    array with global variables which might be replaced
     */
    public function collectGlobalVariables() {
        $arr = array();

        if (isset($_SERVER["REMOTE_ADDR"])) {
            //TODO: Variablen sollten vom STORE_TYPO3 genommen werden
            $arr["REMOTE_ADDR"] = $_SERVER["REMOTE_ADDR"] ?? '';
            $arr["HTTP_HOST"] = $_SERVER["HTTP_HOST"] ?? '';
            $arr["REQUEST_URI"] = $_SERVER["REQUEST_URI"] ?? '';

            $protocol = 'http';
            if (isset($_SERVER['HTTPS'])) {
                if ($_SERVER["HTTPS"] != "off")
                    $protocol = 'https';
            }
            $arr["url"] = $protocol . "://" . $_SERVER["HTTP_HOST"];
            if ($_SERVER["SERVER_PORT"] != 80)
                $arr["url"] .= ":" . $_SERVER["SERVER_PORT"];
            $arr["url"] .= $arr["REQUEST_URI"];
        }

        if (isset($GLOBALS["TSFE"]->fe_user)) {
            $arr["fe_user_uid"] = $GLOBALS["TSFE"]->fe_user->user["uid"] ?? '-';
            $arr["fe_user"] = $GLOBALS["TSFE"]->fe_user->user["username"] ?? '-';
        } else {
            $arr["fe_user_uid"] = '-';
            $arr["fe_user"] = '-';
        }

        $arr["be_user_uid"] = (isset($GLOBALS['BE_USER'])) ? $GLOBALS['BE_USER']->user["uid"] : '-';
        $arr["ttcontent_uid"] = $this->tt_content_uid;
        if (isset($GLOBALS["TSFE"])) {
            $arr["page_id"] = $GLOBALS["TSFE"]->id;
            $arr["page_type"] = $GLOBALS["TSFE"]->type;
            $arr["page_language_uid"] = $GLOBALS["TSFE"]->sys_language_uid;
        }

        return ($arr);
    }

    /**
     * @param        $arr
     * @param        $return
     * @param string $keyPath
     */
    public function linearizeArray($arr, &$return, $keyPath = "") {
        if (is_array($arr)) {
            foreach ($arr as $key => $value) {
                $this->linearizeArray($value, $return, $keyPath . "_" . $key);
            }
        } else {
            $return[ltrim($keyPath, "_")] = $arr;
        }
    }
}
