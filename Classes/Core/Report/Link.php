<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2010 Glowbase GmbH
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMATHUZH\Qfq\Core\Report;


use IMATHUZH\Qfq\Core\Helper\KeyValueStringParser;
use IMATHUZH\Qfq\Core\Helper\OnArray;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Helper\Token;
use IMATHUZH\Qfq\Core\Store\Sip;
use IMATHUZH\Qfq\Core\Store\Store;

/*
 * a:AltText
 * A:Attribute
 * b:bootstrap [0|1|<button>]
 * B:bullet
 * c:class  [n|i|e|<class>]
 * C:checkbox    [name]
 * d:download
 * D:delete
 * e:encryption 0|1
 * E:edit
 * f:
 * F:File
 * g:target
 * G:Glyph
 * h:
 * H:Help
 * i:
 * I:information
 * j:
 * J:
 * k:
 * K:
 * l:
 * L:
 * m:mailto
 * M:Mode
 * n:
 * N:new
 * o:ToolTip
 * O:Monitor
 * p:page
 * P:picture       [file]
 * q:question  <text>
 * Q:
 * r:render
 * R:right
 * s:sip
 * S:Show
 * t:text
 * T:Thumbnail
 * u:url
 * U:URL Param
 * v:
 * V:
 * w:
 * W:Dimension
 * x:Delete
 * X:
 * y:Copy to clipboard
 * Y:
 * z:DropDown Menu
 * Z:
 *
 */

/**
 * Class Link
 * @package qfq
 */
class Link {

    /**
     * @var Sip
     */
    private $sip = null;

    /**
     * @var Store
     */
    private $store = null;

    /**
     * @var Thumbnail
     */
    private $thumbnail = null;

    private $dbIndexData = false;

    private $phpUnit;
    private $renderControl = array();
//    private $linkClassSelector = array(TOKEN_CLASS_INTERNAL => "internal ", TOKEN_CLASS_EXTERNAL => "external ");
//    private $cssLinkClassInternal = '';
//    private $cssLinkClassExternal = '';
    private $ttContentUid = '';

    private $callTable = [
        TOKEN_URL => 'buildUrl',
        TOKEN_MAIL => 'buildMail',
        TOKEN_PAGE => 'buildPage',
        TOKEN_COPY_TO_CLIPBOARD => 'buildCopyToClipboard',
        TOKEN_DOWNLOAD => 'buildDownload',
        TOKEN_DROPDOWN => 'buildDropdown',
        TOKEN_TOOL_TIP => 'buildToolTip',
        TOKEN_PICTURE => 'buildPicture',
        TOKEN_BULLET => 'buildBullet',
        TOKEN_CHECK => 'buildCheck',
        TOKEN_DELETE => 'buildDeleteIcon',
        TOKEN_ACTION_DELETE => 'buildActionDelete',
        TOKEN_EDIT => 'buildEdit',
        TOKEN_HELP => 'buildHelp',
        TOKEN_INFO => 'buildInfo',
        TOKEN_NEW => 'buildNew',
        TOKEN_SHOW => 'buildShow',
        TOKEN_FILE => 'buildFile',
        TOKEN_FILE_DEPRECATED => 'buildFile',
        TOKEN_GLYPH => 'buildGlyph',
        TOKEN_BOOTSTRAP_BUTTON => 'buildBootstrapButton',
    ];

    private $tableVarName = [
        TOKEN_URL => NAME_URL,
        TOKEN_MAIL => NAME_MAIL,
        TOKEN_PAGE => NAME_PAGE,
        TOKEN_UID => NAME_UID,
        TOKEN_DROPDOWN => NAME_DROPDOWN,
        TOKEN_DOWNLOAD => NAME_DOWNLOAD,
        TOKEN_DOWNLOAD_MODE => NAME_DOWNLOAD_MODE,
        TOKEN_TEXT => NAME_TEXT,
        TOKEN_ALT_TEXT => NAME_ALT_TEXT,
        TOKEN_BOOTSTRAP_BUTTON => NAME_BOOTSTRAP_BUTTON,
        TOKEN_TOOL_TIP => NAME_TOOL_TIP,
        TOKEN_PICTURE => NAME_IMAGE,
        TOKEN_BULLET => NAME_IMAGE,
        TOKEN_CHECK => NAME_IMAGE,
        TOKEN_DELETE => NAME_IMAGE,
        TOKEN_EDIT => NAME_IMAGE,
        TOKEN_HELP => NAME_IMAGE,
        TOKEN_INFO => NAME_IMAGE,
        TOKEN_NEW => NAME_IMAGE,
        TOKEN_SHOW => NAME_IMAGE,
        TOKEN_GLYPH => NAME_IMAGE,
        TOKEN_RENDER => NAME_RENDER,
        TOKEN_TARGET => NAME_TARGET,
        TOKEN_CLASS => NAME_LINK_CLASS,
        TOKEN_QUESTION => NAME_QUESTION,
        TOKEN_ENCRYPTION => NAME_ENCRYPTION,
        TOKEN_SIP => NAME_SIP,
        TOKEN_URL_PARAM => NAME_URL_PARAM,
        TOKEN_RIGHT => NAME_RIGHT,
        TOKEN_ACTION_DELETE => NAME_ACTION_DELETE,
        TOKEN_FILE => NAME_FILE,
        TOKEN_FILE_DEPRECATED => NAME_FILE,
        TOKEN_THUMBNAIL => NAME_THUMBNAIL,
        TOKEN_THUMBNAIL_DIMENSION => NAME_THUMBNAIL_DIMENSION,
        TOKEN_COPY_TO_CLIPBOARD => NAME_COPY_TO_CLIPBOARD,
        TOKEN_ATTRIBUTE => NAME_ATTRIBUTE,

        TOKEN_MONITOR => NAME_MONITOR,
        // The following don't need a renaming: already 'long'
        TOKEN_L_FILE => TOKEN_L_FILE,
        TOKEN_L_TAIL => TOKEN_L_TAIL,
        TOKEN_L_APPEND => TOKEN_L_APPEND,
        TOKEN_L_INTERVAL => TOKEN_L_INTERVAL,
        TOKEN_L_HTML_ID => TOKEN_L_HTML_ID,
    ];

    // Used to find double definitions.
    private $tokenMapping = [
        TOKEN_URL => LINK_ANCHOR,
        TOKEN_MAIL => LINK_ANCHOR,
        TOKEN_PAGE => LINK_ANCHOR,
        TOKEN_UID => LINK_ANCHOR,
        TOKEN_DOWNLOAD => LINK_ANCHOR,
        TOKEN_FILE => NAME_FILE,
        TOKEN_COPY_TO_CLIPBOARD => LINK_ANCHOR,

        TOKEN_PICTURE => LINK_PICTURE,
        TOKEN_BULLET => LINK_PICTURE,
        TOKEN_CHECK => LINK_PICTURE,
        TOKEN_DELETE => LINK_PICTURE,
        TOKEN_EDIT => LINK_PICTURE,
        TOKEN_HELP => LINK_PICTURE,
        TOKEN_INFO => LINK_PICTURE,
        TOKEN_NEW => LINK_PICTURE,
        TOKEN_SHOW => LINK_PICTURE,
        TOKEN_GLYPH => LINK_PICTURE,
    ];

    /**
     * __construct
     *
     * @param Sip $sip
     * @param string $dbIndexData
     * @param bool $phpUnit
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function __construct(Sip $sip, $dbIndexData = DB_INDEX_DEFAULT, $phpUnit = false) {

        #TODO: rewrite $phpUnit to: "if (!defined('PHPUNIT_QFQ')) {...}"
        $this->phpUnit = $phpUnit;

        if ($phpUnit) {
            $_SERVER['REQUEST_URI'] = 'localhost';
        }

        $this->sip = $sip;
        $this->store = Store::getInstance('', $phpUnit);
//        $this->cssLinkClassInternal = $this->store->getVar(SYSTEM_CSS_LINK_CLASS_INTERNAL, STORE_SYSTEM);
//        $this->cssLinkClassExternal = $this->store->getVar(SYSTEM_CSS_LINK_CLASS_EXTERNAL, STORE_SYSTEM);
        $this->ttContentUid = $this->store->getVar(TYPO3_TT_CONTENT_UID, STORE_TYPO3);
        $this->dbIndexData = $dbIndexData;
        /*
         * mode:
         * 0: no output
         * 1: <span title='...'>text</span>   (no href)
         * 2: <span title='...'>url</span>    (no href)
         * 3: <a href=url>url</a>
         * 4: <a href=url>Text</a>
         * 5: text
         * 6: url
         * 8: SIP only - 's=badcaffee1234'
         *
         *  r=render mode, u=url, t:text and/or image.
         *
         *                  [r][u][t] = mode
         */

        $this->renderControl[0][0][0] = 0;
        $this->renderControl[0][0][1] = 0;
        $this->renderControl[0][1][0] = 3;
        $this->renderControl[0][1][1] = 4;

        $this->renderControl[1][0][0] = 0;
        $this->renderControl[1][0][1] = 1;
        $this->renderControl[1][1][0] = 3;
        $this->renderControl[1][1][1] = 4;

        $this->renderControl[2][0][0] = 0;
        $this->renderControl[2][0][1] = 0;
        $this->renderControl[2][1][0] = 0;
        $this->renderControl[2][1][1] = 4;

        $this->renderControl[3][0][0] = 0;
        $this->renderControl[3][0][1] = 1;
        $this->renderControl[3][1][0] = 2;
        $this->renderControl[3][1][1] = 1;

        $this->renderControl[4][0][0] = 0;
        $this->renderControl[4][0][1] = 1;
        $this->renderControl[4][1][0] = 2;
        $this->renderControl[4][1][1] = 2;

        $this->renderControl[5][0][0] = 0;
        $this->renderControl[5][0][1] = 0;
        $this->renderControl[5][1][0] = 0;
        $this->renderControl[5][1][1] = 0;

        $this->renderControl[6][0][0] = 0;
        $this->renderControl[6][0][1] = 5;
        $this->renderControl[6][1][0] = 0;
        $this->renderControl[6][1][1] = 5;

        $this->renderControl[7][0][0] = 0;
        $this->renderControl[7][0][1] = 0;
        $this->renderControl[7][1][0] = 6;
        $this->renderControl[7][1][1] = 6;

        $this->renderControl[8][0][0] = 0;
        $this->renderControl[8][0][1] = 0;
        $this->renderControl[8][1][0] = 8;
        $this->renderControl[8][1][1] = 8;
    }

    /**
     * In render mode 3,4,5 there is no '<a href ...>'. Nevertheless, tooltip and BS Button should be displayed.
     * Do this by applying a '<span>' attribute around the text.
     *
     * @param array $vars
     * @param       $keyName
     *
     * @return string
     * @throws \CodeException
     */
    private function wrapLinkTextOnly(array $vars, $keyName) {

        $text = $vars[$keyName];
        if ($vars[NAME_BOOTSTRAP_BUTTON] == '' && $vars[FINAL_TOOL_TIP] == '' && $vars[NAME_ATTRIBUTE] == '') {
            return $text;
        }

        $attributes = Support::doAttribute('title', $vars[FINAL_TOOL_TIP]);
        if ($vars[NAME_ATTRIBUTE] != '') {
            $attributes .= $vars[NAME_ATTRIBUTE] . ' ';
        }

        if ($vars[NAME_BOOTSTRAP_BUTTON] != '') {
            $attributes .= Support::doAttribute('class', [$vars[NAME_BOOTSTRAP_BUTTON], 'disabled']);
        }

        return Support::wrapTag("<span $attributes>", $text);

    }

    /**
     * Renders a BS-dropdown menu.
     *
     * @param string $str
     *      'z|t:menu|b|o:click me'  - the menu button to click on. A text 'menu', a BS button, a tooltip 'click me'.
     *      '||p:detail&pId=1&s|t:Person 1'     - Page id=detail with pId=1 will be opened in the browser.
     *      '||d:file.pdf|p:detail&pId=1&_sip=1||t:Person as PDF' - Page id=detail with pId=1 will downloaded as a PDF.
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function processDropdown($str) {

        $menuEntryStrArr = array();
        $menuEntryLinkArr = array();
        $tokenCollect = array();

        $htmlId = Support::uniqIdQfq('dd_');

        $paramArr = KeyValueStringParser::explodeEscape(PARAM_DELIMITER, $str);

        // Iterate over token. Find delimiter to separate dropdown definition and all menu entries.
        foreach ($paramArr as $tokenStr) {
            $tokenArr = explode(PARAM_TOKEN_DELIMITER, $tokenStr, 2);
            switch ($tokenArr[0] ?? '') {

                case TOKEN_DROPDOWN:
                    $tokenCollect[] = ''; // In case there are no further options given, it's necessary to have at least one
                    break;

                // Indicator to start menu entry: force a flush of existing token and start a new round.
                case '':
                    // New menu entry.
                    if (!empty($tokenCollect)) {
                        $menuEntryStrArr[] = implode(PARAM_DELIMITER, $tokenCollect);
                    }
                    $tokenCollect = array();
//                    $tokenCollect[] = $tokenStr;
                    break;

                default:
                    $tokenCollect[] = $tokenStr;
            }
        }

        // Flush remaining element.
        $menuEntryStrArr[] = implode(PARAM_DELIMITER, $tokenCollect);

        // Remove first array element - that's the menu, not a menu entry. Render the menu.
        $flagMenuEnabled = false;
        $dropdownSymbol = $this->renderDropdownSymbol(array_shift($menuEntryStrArr), $htmlId, $flagMenuEnabled);

        if ($flagMenuEnabled) {
            // For each menu entry get the link
            foreach ($menuEntryStrArr as $str) {
                $menuEntryLinkArr[] = $this->renderLink($str);
            }
        }

        return $this->renderDropdown($dropdownSymbol, $menuEntryLinkArr, $htmlId, $flagMenuEnabled);

    }

    /**
     * https://getbootstrap.com/docs/3.4/components/#dropdowns
     *
     * Start
     * <span class="dropdown">
     *   <span class="glyphicon glyphicon-option-vertical dropdown-toggle" id="dropdownMenu11" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
     *   </span>
     *   <ul class="dropdown-menu" aria-labelledby="dropdownMenu11">
     *    <li><a href="#">Action</a></li>
     *    <li><a href="#">Another action</a></li>
     *    <li><a href="#">Something else here</a></li>
     *    <li role="separator" class="divider"></li>
     *    <li><a href="#">Separated link</a></li>
     *   </ul>
     * </span>
     * End
     *
     * @param string $dropdownSymbol
     * @param array $menuEntryLinkArr
     * @param string $htmlId
     * @param bool $flagMenuEnabled
     * @return string
     */
    private function renderDropdown(string $dropdownSymbol, array $menuEntryLinkArr, $htmlId, $flagMenuEnabled) {
        $ul = '';
        $li = '';
        $attribute = '';

        if ($flagMenuEnabled) {
            $tmp = '';
            foreach ($menuEntryLinkArr as $link) {
                $attribute = '';

                switch (substr($link, 0, 3)) {
                    case '---':
                        $link = substr($link, 3);
                        if ($link == '') {
                            # Separator
                            $attribute = ' role="separator" class="divider"';
                        } else {
                            # Disabled
                            $attribute = ' class="disabled"';
                            if (false === strstr($link, '<a ')) {
                                # If there is no '<a>'-tag, the 'disabled' class is broken - set a fake one.
                                $link = Support::wrapTag('<a href="#">', $link);
                            }
                        }
                        break;

                    case '===':
                        // Header
                        $link = substr($link, 3);
                        $attribute = ' class="dropdown-header"';
                        break;

                    default:
                        break;
                }
                $li .= '<li' . $attribute . '>' . $link . '</li>';
            }

            $ul = Support::wrapTag('<ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="' . $htmlId . '">', $li);
        }

        return Support::wrapTag('<span class="dropdown">', $dropdownSymbol . $ul);
    }

    /**
     * @param string $dropdownStr
     * @param string $htmlId
     * @param $flagMenuEnabled
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function renderDropdownSymbol($dropdownStr, $htmlId, &$flagMenuEnabled) {
        $class = '';

        $paramArr = KeyValueStringParser::parse($dropdownStr, PARAM_TOKEN_DELIMITER, PARAM_DELIMITER);
        foreach ([TOKEN_URL, TOKEN_MAIL, TOKEN_PAGE, TOKEN_DOWNLOAD, TOKEN_COPY_TO_CLIPBOARD] as $token) {
            if (isset($paramArr[$token])) {
                throw new \UserReportException("Dropdown / broken definition: Token '$token' can't be part of " . TOKEN_DROPDOWN, ERROR_INVALID_VALUE);
            }
        }

        // All render mode <3 means: enabled
        $flagMenuEnabled = ($paramArr[TOKEN_RENDER] ?? 0) < 3;

        // Misuse the link class to render the menu symbol: this gives tooltip, glyph, button, text ... - set render mode to '3' = 'no link'
        $paramArr[TOKEN_RENDER] = '3';

        if (!isset($paramArr[TOKEN_GLYPH])) {
            $paramArr[TOKEN_GLYPH] = 'glyphicon-option-vertical';
        }

        if (isset($paramArr[TOKEN_BOOTSTRAP_BUTTON])) {
            $vars = $this->buildBootstrapButton(array(), $paramArr[TOKEN_BOOTSTRAP_BUTTON]);
            $class = ' ' . ($vars[NAME_BOOTSTRAP_BUTTON] ?? '');
        }

        if (!isset($paramArr[TOKEN_ATTRIBUTE])) {
            $paramArr[TOKEN_ATTRIBUTE] = 'data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"';
        }

        if (!$flagMenuEnabled) {
            $class .= ' disabled';
            // In case there is no button: set the text an glyphicon to 'muted'
            if (($vars[NAME_BOOTSTRAP_BUTTON] ?? '') === '') {

                if (($paramArr[TOKEN_TEXT] ?? '') != '') {
                    $paramArr[TOKEN_TEXT] = Support::wrapTag('<span class="text-muted">', $paramArr[TOKEN_TEXT]);
                }

                if (($paramArr[TOKEN_GLYPH] ?? '') != '') {
                    $paramArr[TOKEN_GLYPH] .= ' text-muted';
                }
            }
        }

        $paramArr[TOKEN_ATTRIBUTE] .= ' class="dropdown-toggle' . $class . '"';
        $paramArr[TOKEN_ATTRIBUTE] .= ' id="' . $htmlId . '"';

        return $this->renderLink(KeyValueStringParser::unparse($paramArr, PARAM_TOKEN_DELIMITER, PARAM_DELIMITER));
    }

    /**
     * Build the whole link.
     *
     * @param string $str Qualifier with params. 'report'-syntax. F.e.:  u:www.example.com|P:home.gif|t:Home"
     *
     * @return string The complete HTML encoded Link like
     *           <a href='http://example.com' class='external'><img src='iconf.gif' title='help text'>Description</a>
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function renderLink($str) {

        $tokenGiven = array();
        $link = "";

        if (empty($str)) {
            return '';
        }

        // Check for dropdown menu
        if (($str[0] ?? '') == TOKEN_DROPDOWN) {
            return $this->processDropdown($str);
        }

        $vars = $this->fillParameter($str, $tokenGiven);
        $vars = $this->processParameter($vars, $tokenGiven);
        $mode = $this->getModeRender($vars, $tokenGiven);

        if (isset($tokenGiven[TOKEN_DOWNLOAD]) && $tokenGiven[TOKEN_DOWNLOAD] === true) {
            $this->store->setVar(SYSTEM_DOWNLOAD_POPUP, DOWNLOAD_POPUP_REQUEST, STORE_SYSTEM);
        }

        // 0-6 URL, plain email
        // 10-14 encrypted email
        // 20-24 delete / ajax
        switch ($mode) {
            // 0: No Output
            case '0':
            case '10':
            case '20':
                break;

            // 1: 'text'
            case '1':
            case '11':
                $link = $this->wrapLinkTextOnly($vars, FINAL_CONTENT);
                break;

            // 2: 'url'
            case '2':
            case '12':
                $link = $this->wrapLinkTextOnly($vars, FINAL_HREF);
                break;

            // 3: <a href=url ...>url</a>
            case '3':
            case '13':
                $link = Support::wrapTag($vars[FINAL_ANCHOR], $vars[FINAL_HREF]);
                break;

            // 4: <a href=url ...>Text</a>
            case '4':
            case '14':
                $link = Support::wrapTag($vars[FINAL_ANCHOR], $vars[FINAL_CONTENT]);
                break;

            case '21':
            case '22':
            case '23':
            case '24':
                //TODO: Alter Code, umstellen auf JS Client von RO. Vorlage koennte 'Delete' in Subrecord sein.
                $link = "<a href=\"javascript: void(0);\" onClick=\"var del = new FR.Delete({recordId:'',sip:'',forward:'" .
                    $vars[NAME_PAGE] . "'});\" " . $vars[NAME_LINK_CLASS] . ">" . $vars[NAME_TEXT] . "</a>";
                break;

            // 5: plain text, no <span> around
            case '5':
                $link = $vars[NAME_TEXT];
                break;
            case '15':
            case '25':
                throw new \UserReportException ("Mode not implemented. internal render mode=$mode", ERROR_UNKNOWN_MODE);

            // 6: plain url, no <span> around
            case '6':
                $link = $vars[FINAL_HREF];
                break;
            case '16':
            case '26':
                throw new \UserReportException ("Mode not implemented. internal render mode=$mode", ERROR_UNKNOWN_MODE);
                break;
            case '8':
                $link = substr($vars[FINAL_HREF], 12); // strip 'index.php?s='
                break;

            default:
                throw new \UserReportException ("Mode not implemented. internal render mode=$mode", ERROR_UNKNOWN_MODE);
        }

        return $link;
    }

    /**
     * Order $param. Parameter with priority are hardcoded at the moment.
     *
     * @param array $param
     *
     * @return array
     */
    private function paramPriority(array $param) {
        $prio = array();
        $regular = array();

        foreach ($param as $value) {

            if ($value == '') {
                continue;
            }

            $key = substr($value, 0, 2);

            if (strlen($key) == 1) {
                $key .= PARAM_TOKEN_DELIMITER;
            }

            if ($key == TOKEN_DOWNLOAD . PARAM_TOKEN_DELIMITER) {
                $prio[] = $value;
            } else {
                $regular[] = $value;
            }
        }

        return array_merge($prio, $regular);
    }

    /**
     * Iterate over all given token. Check for double definition.
     *
     * @param string $str
     * @param array $rcTokenGiven - return an array with found token.
     *
     * @return array
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function fillParameter($str, array &$rcTokenGiven) {

        // Define all possible vars: no more isset().
        $vars = $this->initVars();
        $flagArray = array();

        // str="u:http://www.example.com|c:i|t:Hello World|q:Do you really want to delete the record 25:warn:yes:no"
//        $param = explode(PARAM_DELIMITER, $str);
        $param = KeyValueStringParser::explodeEscape(PARAM_DELIMITER, $str);

        $param = $this->paramPriority($param);

        // Parse all parameter, fill variables.
        foreach ($param as $item) {

            // Skip empty entries
            if ($item === '') {
                continue;
            }

            // u:www.example.com
            $arr = explode(":", $item, 2);
            $key = isset($arr[0]) ? $arr[0] : '';
            $value = isset($arr[1]) ? $arr[1] : '';

            // Bookkeeping defined parameter.
            if (isset($rcTokenGiven[$key])) {
                throw new \UserReportException ("Multiple definitions for key '$key'", ERROR_MULTIPLE_DEFINITION);
            }
            $rcTokenGiven[$key] = true;

            if (!isset($this->tableVarName[$key])) {
                $msg[ERROR_MESSAGE_TO_USER] = "Unknown link qualifier: '$key' - did you forget the one character qualifier?";
                $msg[ERROR_MESSAGE_TO_DEVELOPER] = $str;
                throw new \UserReportException (json_encode($msg), ERROR_UNKNOWN_LINK_QUALIFIER);
            }

            $keyName = $this->tableVarName[$key]; // convert token to name

            if ($key == TOKEN_PAGE && $value == '') {
                $value = $this->store->getVar(TYPO3_PAGE_ID, STORE_TYPO3); // If no pageid|pagealias is defined, take current page
            }
            $value = Token::checkForEmptyValue($key, $value);

            $value = $this->checkValue($key, $value);

            // Store value
            if ((isset($rcTokenGiven[TOKEN_DOWNLOAD]) || isset($rcTokenGiven[TOKEN_COPY_TO_CLIPBOARD])) &&
                ($key == TOKEN_PAGE || $key == TOKEN_URL || $key == TOKEN_URL_PARAM || $key == TOKEN_UID ||
                    $key == TOKEN_FILE || $key == TOKEN_FILE_DEPRECATED)) {

                $vars[NAME_COLLECT_ELEMENTS][] = $key . ':' . $value;

                unset($rcTokenGiven[$key]); // Skip Bookkeeping for TOKEN_URL_PARAM | TOKEN_FILE | TOKEN_URL.
                continue;
            } else {
                // TOKEN_GLYPH should not be treated as an regular image. Same applies to the other Glyph symbols, but those don't have a value, and therefore do not fill $vars['image'].
                if ($key != TOKEN_GLYPH) {
                    $vars[$keyName] = $value;
                }
            }

            // Check for double anchor or picture definition.
            if (isset($this->tokenMapping[$key])) {
                $type = $this->tokenMapping[$key];

                if (isset($flagArray[$type])) {
                    throw new \UserReportException ("Multiple definitions of url/mail/page/download or picture", ERROR_MULTIPLE_DEFINITION);
                }
                $flagArray[$type] = true;

//                if ($type === LINK_PICTURE) {
//                    $build = 'build' . strtoupper($keyName[0]) . substr($keyName, 1);
//                    $this->$build($vars, $value);
//                }
            }

            if (isset($this->callTable[$key])) {
                $build = $this->callTable[$key];
                $vars = $this->$build($vars, $value);
            }
        }

        // Download Link needs some extra work
        if (isset($rcTokenGiven[TOKEN_DOWNLOAD]) && $rcTokenGiven[TOKEN_DOWNLOAD]) {
            $vars = $this->buildDownloadLate($vars);
        }

        // CopyToClipboard (Download) Link needs some extra work
        if (isset($rcTokenGiven[TOKEN_COPY_TO_CLIPBOARD]) && $rcTokenGiven[TOKEN_COPY_TO_CLIPBOARD]) {
            $vars = $this->buildCopyToClipboardLate($vars);
        }

        // Check for special default setting.
        if ($vars[NAME_SIP] === false) {
            $vars[NAME_SIP] = "0";
        }

        // Final Checks
        $this->checkParam($rcTokenGiven, $vars);

        return $vars;
    }

    /**
     * Cleans and make existing the standard vars used every time to render a link.
     *
     * @return array
     */
    private function initVars() {

        return [
            NAME_MAIL => '',
            NAME_URL => '',
            NAME_PAGE => '',

            NAME_TEXT => '',
            NAME_ALT_TEXT => '',
            NAME_BOOTSTRAP_BUTTON => '',
            NAME_IMAGE => '',
            NAME_IMAGE_TITLE => '',
            NAME_GLYPH => '',
            NAME_GLYPH_TITLE => '',
            NAME_QUESTION => '',
            NAME_TARGET => '',
            NAME_TOOL_TIP => '',
            NAME_TOOL_TIP_JS => '',
            NAME_URL_PARAM => '',
            NAME_EXTRA_CONTENT_WRAP => '',
            NAME_DOWNLOAD_MODE => '',
            NAME_COLLECT_ELEMENTS => array(),
            NAME_COPY_TO_CLIPBOARD => '',
            NAME_ATTRIBUTE => '',

            NAME_RENDER => '0',
            NAME_RIGHT => 'l',
            NAME_SIP => false,
            NAME_ENCRYPTION => '0',
            NAME_DELETE => '',

            NAME_MONITOR => '0',
            NAME_COPY_TO_CLIPBOARD => '',

            NAME_LINK_CLASS => '', // class name
            NAME_LINK_CLASS_DEFAULT => '', // Depending of 'as page' or 'as url'. Only used if class is not explicit set.

            NAME_ACTION_DELETE => '',

            FINAL_HREF => '',
            FINAL_CONTENT => '',
            FINAL_SYMBOL => '',
            FINAL_TOOL_TIP => '',
            FINAL_CLASS => '',
            FINAL_QUESTION => '',
            FINAL_THUMBNAIL => '',
        ];

    }

    /**
     * Validate value for token
     *
     * @param $key
     * @param $value
     *
     * @return mixed
     * @throws \UserReportException
     */
    private function checkValue($key, $value) {

        switch ($key) {
            case TOKEN_ENCRYPTION:
            case TOKEN_SIP:
                if ($value !== '0' && $value !== '1') {
                    throw new \UserReportException ("Invalid value for token '$key': '$value''", ERROR_INVALID_VALUE);
                }
                break;
            case TOKEN_ACTION_DELETE:
                switch ($value) {
                    case TOKEN_ACTION_DELETE_AJAX:
                    case TOKEN_ACTION_DELETE_REPORT:
                    case TOKEN_ACTION_DELETE_CLOSE:
                        break;
                    default:
                        throw new \UserReportException ("Invalid value for token '$key': '$value''", ERROR_INVALID_VALUE);
                }
                break;
            case TOKEN_BOOTSTRAP_BUTTON:
                if ($value == '1') {
                    $value = 'btn-default';
                }
                break;
            default:
        }

        return $value;
    }

    /**
     * Check for double definition.
     *
     * @param array $tokenGiven
     *
     * @param array $vars
     * @throws \UserReportException
     */
    private function checkParam(array $tokenGiven, array $vars) {
        $countLinkAnchor = 0;
        $countLinkPicture = 0;
        $countSources = 0;

        foreach ($tokenGiven as $token => $value) {
            if (isset($this->tokenMapping[$token])) {
                switch ($this->tokenMapping[$token]) {
                    case LINK_ANCHOR:
                        $countLinkAnchor++;
                        break;
                    case LINK_PICTURE:
                        $countLinkPicture++;
                        break;
                    case NAME_FILE:
                    case NAME_URL:
                    case NAME_PAGE:
                        $countSources++;
                        break;
                    default:
                        break;
                }
            }
        }

        if ($countLinkAnchor > 1) {
            throw new \UserReportException ("Multiple URL / PAGE / MAILTO or COPY_TO_CLIPBOARD definition", ERROR_MULTIPLE_URL_PAGE_MAILTO_DEFINITION);
        }

        if ($countLinkPicture > 1) {
            throw new \UserReportException ("Multiple definitions for token picture/bullet/check/edit...delete'" . TOKEN_PAGE . "'", ERROR_MULTIPLE_DEFINITION);
        }

        if (isset($tokenGiven[TOKEN_MAIL]) && isset($tokenGiven[TOKEN_TARGET])) {
            throw new \UserReportException ("Token Mail and Target at the same time not possible'" . TOKEN_PAGE . "'", ERROR_MULTIPLE_DEFINITION);
        }

        if (isset($tokenGiven[TOKEN_DOWNLOAD]) && count($vars[NAME_COLLECT_ELEMENTS]) == 0 && $countSources == 0) {
            throw new \UserReportException ("Missing element sources for download", ERROR_MISSING_REQUIRED_PARAMETER);
        }
    }

    /**
     * Compute final link parameter.
     *
     * @param array $vars
     * @param array $tokenGiven
     *
     * @return array
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function processParameter(array $vars, array $tokenGiven) {

        $vars[FINAL_HREF] = $this->doHref($vars, $tokenGiven); // must be called before doToolTip()
        $vars[FINAL_TOOL_TIP] = $this->doToolTip($vars);
        $vars[FINAL_CLASS] = $this->doCssClass($vars);
        $vars[FINAL_SYMBOL] = $this->doSymbol($vars);
        $vars[FINAL_THUMBNAIL] = $this->doThumbnail($vars);
        $vars[FINAL_CONTENT] = $this->doContent($vars, $vars[FINAL_CONTENT_PURE]); // must be called after doSymbol()
        $vars[FINAL_QUESTION] = $this->doQuestion($vars);
        $vars[FINAL_ANCHOR] = $this->doAnchor($vars);

        return $vars;
    }

    /**
     * Determine DownloadMode: explicit given or detected by given download sources.
     * Do some basic checks if parameter are correct.
     *
     * @param array $vars
     *
     * @return string - DOWNLOAD_MODE_PDF | DOWNLOAD_MODE_ZIP | DOWNLOAD_MODE_FILE | DOWNLOAD_MODE_EXCEL
     * @throws \UserFormException
     */
    private function getDownloadModeNCheck(array $vars) {

        $cnt = count($vars[NAME_COLLECT_ELEMENTS]);
        $mode = $vars[NAME_DOWNLOAD_MODE];

        // Determine default.
        if ($mode == '') {
            if ($cnt == 1) {
                $mode = (substr($vars[NAME_COLLECT_ELEMENTS][0], 0, 1) == TOKEN_FILE_DEPRECATED) ? DOWNLOAD_MODE_FILE : DOWNLOAD_MODE_PDF;
            } else {
                $mode = DOWNLOAD_MODE_PDF;
            }
        }

        // Do some checks.
        switch ($mode) {
            case DOWNLOAD_MODE_PDF:
            case DOWNLOAD_MODE_SAVE_PDF:
            case DOWNLOAD_MODE_ZIP:
            case DOWNLOAD_MODE_EXCEL:
                break;
            case DOWNLOAD_MODE_FILE:
                if ($cnt > 1) {
                    throw new \UserFormException("With 'downloadMode' = 'file' only one element source is allowed.", ERROR_DOWNLOAD_UNEXPECTED_NUMBER_OF_SOURCES);
                }
                break;
            default:
                throw new \UserFormException("Unknown mode: $mode", ERROR_UNKNOWN_MODE);
                break;

        }

        return $mode;
    }

    /**
     * Concat final HREF string. Might be used directly (load new page) or as an AJAX call (e.g. Download).
     *
     * @param array $vars
     * @param array $tokenGiven
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function doHref(array &$vars, array $tokenGiven) {
        $urlNParam = '';

        // Download
        if (isset($tokenGiven[TOKEN_DOWNLOAD])) {
            // Message in download popup.
            $altText = ($vars[NAME_ALT_TEXT] == '') ? 'Please wait' : addslashes($vars[NAME_ALT_TEXT]);
            $vars[NAME_EXTRA_CONTENT_WRAP] = str_replace(DOWNLOAD_POPUP_REPLACE_TEXT, $altText, $vars[NAME_EXTRA_CONTENT_WRAP]);
            $vars[NAME_EXTRA_CONTENT_WRAP] = str_replace(DOWNLOAD_POPUP_REPLACE_TITLE, 'Download: ' . addslashes($vars[NAME_DOWNLOAD]), $vars[NAME_EXTRA_CONTENT_WRAP]);

            $tmpUrlParam = array();
            $tmpUrlParam[DOWNLOAD_MODE] = $this->getDownloadModeNCheck($vars);
            $tmpUrlParam[DOWNLOAD_EXPORT_FILENAME] = $vars[NAME_DOWNLOAD];
            $tmpUrlParam[SIP_DOWNLOAD_PARAMETER] = base64_encode(implode(PARAM_DELIMITER, $vars[NAME_COLLECT_ELEMENTS]));
            $vars[NAME_URL_PARAM] = KeyValueStringParser::unparse($tmpUrlParam, '=', '&');
        }

        // CopyToClipboard
        if (isset($tokenGiven[TOKEN_COPY_TO_CLIPBOARD]) && $vars[NAME_COPY_TO_CLIPBOARD] === '') {

//            $tmpUrlParam = array();
//            $tmpUrlParam[DOWNLOAD_MODE] = DOWNLOAD_MODE_FILE;
//            $tmpUrlParam[DOWNLOAD_OUTPUT_FORMAT] = DOWNLOAD_OUTPUT_FORMAT_JSON;
//            $tmpUrlParam[SIP_DOWNLOAD_PARAMETER] = base64_encode(implode(PARAM_DELIMITER, $vars[NAME_COLLECT_ELEMENTS]));
//            $vars[NAME_URL_PARAM] = KeyValueStringParser::unparse($tmpUrlParam, '=', '&');
            return '';
        }

        // Delete
        if ($vars[NAME_ACTION_DELETE] !== '') {
            $vars[NAME_URL_PARAM] = $this->adjustDeleteParameter($vars[NAME_ACTION_DELETE], $vars[NAME_URL_PARAM]);
        }

        // Regular Link
        if ($vars[NAME_MAIL] === '') {

            if ($vars[NAME_SIP] === "1") {
                // Only for SIP encoded Links: append the current $dbIndexData to make record locking work even for records in dbIndexQfq
                if ($vars[NAME_URL_PARAM] != '') {
                    $vars[NAME_URL_PARAM] .= '&';
                }

                $vars[NAME_URL_PARAM] .= PARAM_DB_INDEX_DATA . '=' . $this->dbIndexData;
            }

            // Normalize '?pageAlias' to 'index.php?pageAlias'
            if (substr($vars[NAME_URL], 0, 1) === '?') {
                $vars[NAME_URL] = INDEX_PHP . $vars[NAME_URL];
            }

            // Either NAME_URL is empty or NAME_PAGE is empty
            $urlNParam = Support::concatUrlParam($vars[NAME_URL] . $vars[NAME_PAGE], $vars[NAME_URL_PARAM]);

            if ($vars[NAME_SIP] === "1") {

                $paramArray = $this->sip->queryStringToSip($urlNParam, RETURN_ARRAY);
                $urlNParam = $paramArray[SIP_SIP_URL];

                if (Support::findInSet(SYSTEM_SHOW_DEBUG_INFO_YES, $this->store->getVar(SYSTEM_SHOW_DEBUG_INFO, STORE_SYSTEM))) {
                    $vars[NAME_TOOL_TIP] .= PHP_EOL . PHP_EOL . $this->sip->debugSip($paramArray);
                }
            }
        } else {
            // Link: MAILTO

            // If there is no encryption: handle the mailto as an ordinary URL
            if ($vars[NAME_ENCRYPTION] === '1') {
                throw new \UserReportException ("Oops, sorry: encrypted mail not implemented ...", ERROR_NOT_IMPLEMENTED);
            } else {
                $urlNParam = "mailto:" . $vars[NAME_MAIL];
            }
        }

        return $urlNParam;
    }


    /**
     * @param $tokenActionDelete
     * @param $nameUrlParam
     *
     * @return string
     * @throws \UserReportException
     */
    private function adjustDeleteParameter($tokenActionDelete, $nameUrlParam) {

        $kvp = new KeyValueStringParser();

        // Split in: [p => 'r=100&table=note&..', 'D' => ''... ],
//        $param = $kvp->parse($nameUrlParam, ':', '|');

//        Support::setIfNotSet($param, TOKEN_URL_PARAM);

        switch ($tokenActionDelete) {
            case TOKEN_ACTION_DELETE_AJAX:
                // TODO: Implement for AJAX (subrecord)
                throw new \UserReportException ("Not implemented!", ERROR_NOT_IMPLEMENTED);
                break;
            case TOKEN_ACTION_DELETE_REPORT:
                $nameUrlParam .= '&' . SIP_MODE_ANSWER . '=' . MODE_HTML;
                // Encode '&' in url to preserve parameters (#4545) - need to decode on use.
                $nameUrlParam .= '&' . SIP_TARGET_URL . '=' . str_replace('&', '--amp--', $_SERVER['REQUEST_URI'] ?? '');
                break;
            case TOKEN_ACTION_DELETE_CLOSE:
                // TODO: Implement for Form (primary Record wird geloescht)
                throw new \UserReportException ("Not implemented!", ERROR_NOT_IMPLEMENTED);
                break;
            default:
                throw new \UserReportException ("Invalid value for token '" . TOKEN_ACTION_DELETE . "': '$tokenActionDelete''", ERROR_INVALID_VALUE);
        }

        return $nameUrlParam;
    }

    /**
     * Return $vars[NAME_TOOL_TIP]. If $vars[NAME_TOOL_TIP] is empty, set $vars[NAME_GLYPH_TITLE] as tooltip.
     *
     * @param array $vars
     *
     * @return mixed
     */
    private function doToolTip(array $vars) {

        // Set default tooltip
        if ($vars[NAME_TOOL_TIP] == '') {
            $vars[NAME_TOOL_TIP] = $vars[NAME_GLYPH_TITLE];
        }

        return $vars[NAME_TOOL_TIP];
    }

    /**
     * Parse CSS Class Settings
     *
     * @param array $vars
     *
     * @return    string
     */
    private function doCssClass(array $vars) {

//        $class = ($vars[NAME_LINK_CLASS] === '') ? $vars[NAME_LINK_CLASS_DEFAULT] : $vars[NAME_LINK_CLASS];
        $class = $vars[NAME_LINK_CLASS];

        switch ($class) {
            case TOKEN_CLASS_NONE:
                $class = '';
                break;
// #5302
//            case TOKEN_CLASS_INTERNAL:
//            case TOKEN_CLASS_EXTERNAL:
//                $class = $this->linkClassSelector[$vars[NAME_LINK_CLASS]] . ' ';
//                break;
            default:
                break;
        }

        if ($class === NO_CLASS) {
            $class = '';
        }

        //TODO: Old way to detect if BS Button should be rendered - should be replaced by 'b:'
        if ($vars[NAME_GLYPH] !== '' && $vars[NAME_EXTRA_CONTENT_WRAP] == '' && $vars[NAME_BOOTSTRAP_BUTTON] == '') {
            $class = 'btn btn-default ' . $class;
        }

        return implode(' ', [$vars[NAME_BOOTSTRAP_BUTTON], $class]);
    }

    /**
     * Create Image HTML Tag
     *
     * @param array $vars
     *
     * @return string
     * @throws \CodeException
     */
    private function doSymbol(array $vars) {
        $tags = '';
        $html = '';

        // Build Image
        if ($vars[NAME_IMAGE] !== '') {
            $tags .= Support::doAttribute('alt', $vars[NAME_ALT_TEXT]);
            $tags .= Support::doAttribute('src', $vars[NAME_IMAGE]);
            $tags .= Support::doAttribute('title', $vars[FINAL_TOOL_TIP] === '' ? $vars[NAME_IMAGE_TITLE] : $vars[FINAL_TOOL_TIP]);
            $html .= '<img ' . $tags . '>';
        }

        if ($vars[NAME_GLYPH] !== '' && $vars[NAME_GLYPH] !== '0') {
            $tags = Support::doAttribute('class', 'glyphicon ' . $vars[NAME_GLYPH]);
            $html .= Support::wrapTag('<span ' . $tags . '>', '', false);
        }

        return $html;
    }

    /**
     * Concat Text, Image, Glyph and/or Thumbnail.
     * Depending of $vars[NAME_RIGHT_PICTURE_POSITION], swap the position,
     *
     * @param array $vars
     *
     * @param $contentPure
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function doContent(array $vars, &$contentPure) {
        $arr = array();

        if ($vars[NAME_MONITOR] == '1') {
            $monitor = new Monitor();
            return $monitor->process($vars);
        }

        $order = ($vars[NAME_RIGHT] === "l") ? [FINAL_SYMBOL, FINAL_THUMBNAIL, NAME_TEXT] : [NAME_TEXT, FINAL_THUMBNAIL, FINAL_SYMBOL];
        foreach ($order as $key) {
            if (isset($vars[$key]) && $vars[$key] != '') { // empty() is wrong here, cause '0' would be recognized as empty.
                $arr[] = $vars[$key];
            }
        }

        $contentPure = implode(' ', $arr);
        return Support::wrapTag($vars[NAME_EXTRA_CONTENT_WRAP], $contentPure);
    }

    /**
     * Build 'alert' JS. $vars['question'] = '<text>:<color>:<button ok>:<button fail>'
     * Return JS Code to place in '<a>' tag. Be carefull: function creates a uniqe 'id' tag.
     *
     * @param array $vars
     *
     * @return string
     * @throws \CodeException
     * @throws \UserReportException
     */
    private function doQuestion(array $vars) {

        if ($vars[NAME_QUESTION] === '') {
            return '';
        }

        $arr = OnArray::explodeWithoutEscaped(':', $vars[NAME_QUESTION]);
        $arr = array_merge($arr, ['', '', '', '', '', '']);

        $id = Support::uniqIdQfq('a_');
        $content = Support::doAttribute('id', $id);

        $text = $arr[QUESTION_INDEX_TEXT] === '' ? DEFAULT_QUESTION_TEXT : $arr[QUESTION_INDEX_TEXT];
        $level = ($arr[QUESTION_INDEX_LEVEL] === '') ? DEFAULT_QUESTION_LEVEL : $arr[QUESTION_INDEX_LEVEL];
        $ok = ($arr[QUESTION_INDEX_BUTTON_OK] === '') ? 'Ok' : $arr[QUESTION_INDEX_BUTTON_OK];
        $cancel = ($arr[QUESTION_INDEX_BUTTON_FALSE] === '') ? 'Cancel' : $arr[QUESTION_INDEX_BUTTON_FALSE];
        $cancel = ($cancel == '-') ? '' : ", { label: '$cancel',eventName: 'cancel'}";

        $timeout = ($arr[QUESTION_INDEX_TIMEOUT] === '') ? '0' : $arr[QUESTION_INDEX_TIMEOUT] * 1000;
        $flagModalStatus = ($arr[QUESTION_INDEX_FLAG_MODAL] === '') ? '1' : $arr[QUESTION_INDEX_FLAG_MODAL];
        $flagModal = ($flagModalStatus === "1") ? 'true' : 'false';

        $js = <<<EOF
var alert = new QfqNS.Alert({ message: '$text', type: '$level', modal: $flagModal, timeout: $timeout, buttons: [
    { label: '$ok', eventName: 'ok' }
    $cancel
] } );
alert.on('alert.ok', function() {
  window.location = $('#$id').attr('href');
});

alert.show();
return false;
EOF;

        $content .= Support::doAttribute('onClick', $js);


        return $content;
    }

    /**
     * Create the HTML anchor.
     * - <a href="mailto:info@example.com" title=".." class="..">
     * - <a href="http://example.com" title=".." class="..">
     *
     * @param array $vars
     *
     * @return string
     * @throws \CodeException
     */
    private function doAnchor(array $vars) {

        $attributes = '';

        $attributes .= Support::doAttribute('href', $vars[FINAL_HREF]);
        $attributes .= Support::doAttribute('class', $vars[FINAL_CLASS]);
        $attributes .= Support::doAttribute('target', $vars[NAME_TARGET]);
        $attributes .= Support::doAttribute('title', $vars[FINAL_TOOL_TIP]);
        if ($vars[NAME_ATTRIBUTE] != '') {
            $attributes .= $vars[NAME_ATTRIBUTE] . ' ';
        }
        $attributes .= $vars[FINAL_QUESTION];

        $anchor = '<a ' . $attributes . '>';

        return $anchor;
    }

    /**
     *
     *
     * @param array $vars
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function doThumbnail(array $vars) {

        // Check if there is a thumbnail and if it should be rendered
        if (empty($vars[NAME_THUMBNAIL]) || $vars[NAME_RENDER] == RENDER_MODE_7) {
            return '';
        }

        $arr = array();
        $arr[] = TOKEN_THUMBNAIL . ':' . $vars[NAME_THUMBNAIL];
        $arr[] = TOKEN_THUMBNAIL_DIMENSION . ':' . $vars[NAME_THUMBNAIL_DIMENSION];
        $arr[] = TOKEN_SIP . ':' . $vars[NAME_SIP];

        $str = implode('|', $arr);

        if ($this->thumbnail === null) {
            $this->thumbnail = new Thumbnail();
        }

        return $this->thumbnail->process($str);
    }

    /**
     * Get mode from table $this->renderControl, depending on $modeRender, $modeUrl, $modeText
     *
     * @param array $vars
     * @param array $tokenGiven
     * @return string
     * @throws \UserReportException
     */
    private function getModeRender(array $vars, array $tokenGiven) {

        if (isset($tokenGiven[TOKEN_COPY_TO_CLIPBOARD]) && $tokenGiven[TOKEN_COPY_TO_CLIPBOARD]) {
            if ($vars[NAME_RENDER] == '0') {
                $vars[NAME_RENDER] = RENDER_MODE_1;
            }
        }

        if ($vars[NAME_MONITOR] == '1') {
            return 1;
        }

        $modeRender = $vars[NAME_RENDER];
        $modeUrl = ($vars[FINAL_HREF] === '') ? 0 : 1;
        $modeText = ($vars[FINAL_CONTENT] === '') ? 0 : 1;

        // Create 'fake' modes for encrypted 'mailto'
        $prefix = "";
        if ($vars[NAME_MAIL] != '') {
            $prefix = "1";
//            $vars[NAME_URL] = "dummy";
        }

        // Create 'fake' mode for ajax/html delete
        if ($vars[NAME_DELETE]) {
            $prefix = "2";
        }

        if (isset($this->renderControl[$modeRender][$modeUrl][$modeText])) {
            $mode = $prefix . $this->renderControl[$modeRender][$modeUrl][$modeText];
        } else {
            throw new \UserReportException ("Undefined combination of 'rendering mode' / 'url' / 'text': " .
                $modeRender . ' / ' . $modeUrl . ' / ' . $modeText, ERROR_UNDEFINED_RENDER_CONTROL_COMBINATION);
        }

        return $mode;
    }

    /**
     *  Encrypt the mailto address via JS.
     *  Email address protected against email crawler (as long as they don't interpret JS).
     *
     *    <script language=javascript><!--
     *    var mm1 = name
     *    var mm2 = @domain.
     *    var ... = tld
     *    document.write("<a href=" + "mail" + "to:" + mm1 + mm2 + ... + ">" + name + "</a>")
     *    document.write("<a href=" + "mail" + "to:" + mm1 + mm2 + ... + ">" + @domain. + "</a>")
     *    document.write("<a href=" + "mail" + "to:" + mm1 + mm2 + ... + ">" + tld + "</a>")
     *    //--></script>';
     *
     * @param array $vars
     * @param bool|TRUE $href TRUE: create a '<a>',   false: just encrypt or show the email, no link.
     *
     * @return string
     */
    private function encryptMailtoJS(array $vars, $href = true) {

        // Split $mailto
        $tmp = $this->splitAndAddDelimter($vars[NAME_MAIL], "@");
        $arr = array_merge($this->splitAndAddDelimter($tmp[0], "."), $this->splitAndAddDelimter($tmp[1], "."));

        $tt = "<script language=javascript><!--" . chr(10);
        $ii = 0;
        if ($href) {
            $dw = 'document.write("<a href=" + "mail" + "to:"';
            foreach ($arr as $value) {
                // Setup JS Variables
                $tt .= 'var mm' . $ii . '= "' . $value . '"' . chr(10);
                // Compose $dw (documentwrite statement)
                $dw .= ' + mm' . $ii++;
            }
            $dw .= ' + "' . $vars[NAME_LINK_CLASS] . str_replace('"', '\\"', $vars[NAME_TOOL_TIP_JS][0]) . '>"';
            $closeDw = '"</a>")';
        } else {
            $dw = 'document.write(';
            $closeDw = ')';
        }

        // Wrap mailto around text
        if ($vars[NAME_MAIL] == $vars[NAME_TEXT]) {
            // Text is an email: wrap every single part of mailto.
            $ii = 0;
            foreach ($arr as $value) {
                $tt .= $dw . " + mm" . $ii++ . ' + ' . $closeDw . chr(10);
            }
        } else {
            // Single wrap text
            $tt .= $dw . ' + "' . $vars[NAME_TEXT] . '" + ' . $closeDw . chr(10);
        }

        $tt .= '//--></script>';
        if ($href) {
            $tt .= $vars[NAME_TOOL_TIP_JS][1];
        }

        return ($tt);
    }

    /**
     * Split a string around the $delimiter.
     *
     * Append the delimiter to each part except the last one.
     *
     * @param $mailto
     * @param $delimiter
     *
     * @return array
     */
    private function splitAndAddDelimter($mailto, $delimiter) {
        //TODO: Ich verstehe die Funktion nicht - funktioniert das hier wirklich?
        $value = '';

        $arr = explode($delimiter, $mailto);            // split string

        foreach ($arr as $key => $value) {
            $arr[$key] = $value . $delimiter;
        }

        if (isset($key)) {
            $arr[$key] = $value;
        }

        return ($arr);
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildUrl($vars, $value) {
//        $vars[NAME_LINK_CLASS_DEFAULT] = $this->cssLinkClassExternal;
        $vars[NAME_LINK_CLASS_DEFAULT] = '';

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildMail($vars, $value) {
//        $vars[NAME_LINK_CLASS_DEFAULT] = $this->cssLinkClassExternal;
        $vars[NAME_LINK_CLASS_DEFAULT] = '';

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildPage($vars, $value) {

        if (substr($value, 0, 3) !== 'id=') {
            $value = 'id=' . $value;
        }

        $vars[NAME_PAGE] = Support::concatUrlParam('', $value);
        $vars[NAME_LINK_CLASS_DEFAULT] = '';
//        $vars[NAME_LINK_CLASS_DEFAULT] = $this->cssLinkClassInternal;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildDownload($vars, $value) {

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildCopyToClipboard($vars, $value) {

        return $vars;
    }

    /**
     * @param $vars
     * @return array
     * @throws \CodeException
     */
    private function buildDownloadLate($vars) {

        if ($vars[NAME_BOOTSTRAP_BUTTON] == '') {
            $vars = $this->buildBootstrapButton($vars, '');
        }

        $bootstrapButton = $vars[NAME_BOOTSTRAP_BUTTON];
        if ($vars[NAME_BOOTSTRAP_BUTTON] == '0') {
            $bootstrapButton = '';
            $vars[NAME_GLYPH] = '';
            $vars[NAME_EXTRA_CONTENT_WRAP] = '';
        } else {
            $vars[NAME_GLYPH] = GLYPH_ICON_FILE;
            $vars[NAME_GLYPH_TITLE] = "Download";
        }

        // No download link! Only text/button
        if ($vars[NAME_RENDER] == '3') {
            return $vars;
        }

        if ($vars[NAME_LINK_CLASS] !== '') { // custom class
            $bootstrapButton = $vars[NAME_LINK_CLASS];
            $vars[NAME_LINK_CLASS] = ''; // prevent this class from also being used in the wrapping element
        }

        $text = DOWNLOAD_POPUP_REPLACE_TEXT;
        $title = DOWNLOAD_POPUP_REPLACE_TITLE;

        $attributes = Support::doAttribute('class', $bootstrapButton, true);
        $attributes .= ' data-toggle="modal" data-target="#qfqModal101" data-title="' . $title .
            '" data-text="' . $text . '" data-backdrop="static" data-keyboard="false" ';

        $onClick = <<<EOF
           onclick="$('#qfqModalTitle101').text($(this).data('title')); $('#qfqModalText101').text($(this).data('text'));"
EOF;

        $vars[NAME_URL] = API_DIR . '/' . API_DOWNLOAD_PHP;
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        $vars[NAME_EXTRA_CONTENT_WRAP] = '<span ' . $attributes . $onClick . '>';
        $vars[NAME_BOOTSTRAP_BUTTON] = '0';

        // Download links should be by default SIP encoded.
        if (($vars[NAME_SIP]) === false) {
            $vars[NAME_SIP] = "1";
        }

        return $vars;
    }

    /**
     * @param $vars
     * @return array
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function buildCopyToClipboardLate($vars) {

        $jsAction = '';
        $source = '';

        if ($vars[NAME_BOOTSTRAP_BUTTON] == '') {
            $vars = $this->buildBootstrapButton($vars, '');
        }

        $bootstrapButton = $vars[NAME_BOOTSTRAP_BUTTON];
        if ($vars[NAME_BOOTSTRAP_BUTTON] == '0') {
            $bootstrapButton = '';
            $vars[NAME_GLYPH] = '';
            $vars[NAME_EXTRA_CONTENT_WRAP] = '';
        } else {
            $vars[NAME_GLYPH] = GLYPH_ICON_COPY;
            $vars[NAME_GLYPH_TITLE] = "Copy to clipboard";
        }

        $attributes = Support::doAttribute('class', $bootstrapButton, true);

        // Clipboard Source
        if ($vars[NAME_COPY_TO_CLIPBOARD] !== '') {
            $jsAction = 'onClick';
            // Take care that ' and " are properly escaped
            $source = json_encode(["text" => $vars[NAME_COPY_TO_CLIPBOARD]], JSON_HEX_QUOT | JSON_HEX_APOS);
        } elseif (isset($vars[NAME_COLLECT_ELEMENTS][0])) {

            $jsAction = 'onmousedown';

            $tmpUrlParam = array();
            $tmpUrlParam[DOWNLOAD_MODE] = DOWNLOAD_MODE_FILE;
            $tmpUrlParam[DOWNLOAD_OUTPUT_FORMAT] = DOWNLOAD_OUTPUT_FORMAT_JSON;
            $tmpUrlParam[SIP_DOWNLOAD_PARAMETER] = base64_encode(implode(PARAM_DELIMITER, $vars[NAME_COLLECT_ELEMENTS]));
            $urlNParam = KeyValueStringParser::unparse($tmpUrlParam, '=', '&');
            $paramArray = $this->sip->queryStringToSip($urlNParam, RETURN_ARRAY);

            if (Support::findInSet(SYSTEM_SHOW_DEBUG_INFO_YES, $this->store->getVar(SYSTEM_SHOW_DEBUG_INFO, STORE_SYSTEM))) {
                $vars[NAME_TOOL_TIP] .= PHP_EOL . PHP_EOL . $this->sip->debugSip($paramArray);
            }

            $source = json_encode(['uri' => API_DIR . '/' . API_DOWNLOAD_PHP . '?s=' . $paramArray[SIP_SIP]]);
        } else {
            throw new \UserReportException("Missing content for 'copy to clipboard'", ERROR_MISSING_CONTENT);
        }

        // Depending on 'text' or 'file' different JS class are necessary.
        //   onClick="new QfqNS.Clipboard({text: 'test clipboard'});">
        //   onmousedown="new QfqNS.Clipboard({uri: 'mockData/downloadResponse.json'});">
        $onClick = "$jsAction='new QfqNS.Clipboard($source);'";

        $vars[NAME_EXTRA_CONTENT_WRAP] = '<button ' . $attributes . $onClick . '>';
        $vars[NAME_BOOTSTRAP_BUTTON] = '';

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildPicture($vars, $value) {
        if ($vars[NAME_ALT_TEXT] == '') {
            $vars[NAME_ALT_TEXT] = $value;
        }

        $vars[NAME_IMAGE_TITLE] = $value;
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildBullet($vars, $value) {
        if ($value == '') {
            $value = DEFAULT_BULLET_COLOR;
        }

        if ($vars[NAME_ALT_TEXT] == '') {
            $vars[NAME_ALT_TEXT] = "Bullet " . $value;
        }

        $vars[NAME_IMAGE] = PATH_ICONS . "/bullet-" . $value . '.gif';
        $vars[NAME_IMAGE_TITLE] = $value;
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildCheck($vars, $value) {

        if ($value == '') {
            $value = DEFAULT_CHECK_COLOR;
        }

        if ($vars[NAME_ALT_TEXT] == '') {
            $vars[NAME_ALT_TEXT] = "Checked " . $value;
        }

        $vars[NAME_IMAGE] = PATH_ICONS . "/checked-" . $value . '.gif';
        $vars[NAME_IMAGE_TITLE] = $value;
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     *
     * @return array
     */
    private function buildDeleteIcon($vars) {

        $vars[NAME_GLYPH] = GLYPH_ICON_DELETE;
        $vars[NAME_GLYPH_TITLE] = "Delete";
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function buildActionDelete($vars, $value) {

        // Minimal check for required parameter.
        $vars[NAME_URL_PARAM] = $this->checkDeleteParam($vars[NAME_URL_PARAM]);

        if ($vars[NAME_URL_PARAM] == '') {
            $vars[NAME_RENDER] = RENDER_MODE_5;
            return $vars;
        }

        if ($vars[NAME_URL] == '') {
            $vars[NAME_URL] = API_DIR . '/' . API_DELETE_PHP;
        }

        if (!isset($vars[NAME_LINK_CLASS])) {
            // no_class: By default a button will be rendered.
            $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;
        }

        $vars[NAME_SIP] = "1";

        return $vars;
    }

    /**
     * Check that at least SIP_RECORD_ID is given and SIP_TABLE or SIP_FORM.
     * This check is only processed for COLUMN_PAGED & COLUMN_PPAGED. Not for COLOUMN_LINK, cause it's not known there.
     * In case of missing parameter, throw an exception.
     *
     * @param $urlParam
     * @return string
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function checkDeleteParam($urlParam) {

        // Fill array 'found' with every given token
        $found = KeyValueStringParser::parse($urlParam, '=', '&');

        // If there is no 'r' or 'r'=0: no delete button will be rendered
        if (!isset($found[SIP_RECORD_ID]) || $found[SIP_RECORD_ID] == '' || $found[SIP_RECORD_ID] == 0) {
            return '';
        }

        $flagRecordId = isset($found[SIP_RECORD_ID]);
        $flagTable = isset($found[SIP_TABLE]) && $found[SIP_TABLE] != '';
        $flagForm = isset($found[SIP_FORM]) && $found[SIP_FORM] != '';

        if ($flagRecordId && ($flagTable || $flagForm)) {
            return $urlParam;
        }

        throw new \UserReportException ("Missing some qualifier/value for column " . COLUMN_PAGED . '/' . COLUMN_PPAGED . ": " .
            SIP_RECORD_ID . ", " . SIP_FORM . " or " . SIP_TABLE, ERROR_MISSING_REQUIRED_DELETE_QUALIFIER);

    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildEdit($vars, $value) {

        $vars[NAME_GLYPH] = GLYPH_ICON_EDIT;
        $vars[NAME_GLYPH_TITLE] = "Edit";
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildHelp($vars, $value) {

        $vars[NAME_GLYPH] = GLYPH_ICON_HELP;
        $vars[NAME_GLYPH_TITLE] = "Help";
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildInfo($vars, $value) {

        $vars[NAME_GLYPH] = GLYPH_ICON_INFO;
        $vars[NAME_GLYPH_TITLE] = "Information";
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildNew($vars, $value) {

        $vars[NAME_GLYPH] = GLYPH_ICON_NEW;
        $vars[NAME_GLYPH_TITLE] = "New";
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildShow($vars, $value) {

        $vars[NAME_GLYPH] = GLYPH_ICON_SHOW;
        $vars[NAME_GLYPH_TITLE] = "Details";
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildGlyph($vars, $value) {

        $vars[NAME_GLYPH] = $value;
        $vars[NAME_GLYPH_TITLE] = "Details";
        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Called by $this->callTable
     * Parameter: b:?
     *
     * 0 : no button, do nothing, return $vars unchanged
     * '': default
     * 'success', 'btn-success': 'btn btn-success'
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildBootstrapButton($vars, $value) {

        $value = trim($value);

        if ($value === '0') {
            return $vars;
        }

        if ($value === '') {
            $value = 'default';
        }

        // Just in case the user forgot 'btn-' in front of btn-default, btn-primary, btn-su...
        if (substr($value, 0, 4) != 'btn-') {
            $value = 'btn-' . $value;
        }

        $vars[NAME_BOOTSTRAP_BUTTON] = 'btn ' . $value;

        return $vars;
    }

    /**
     * Called by $this->callTable
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildFile($vars, $value) {

        // Don't know what to do for file - seems to missing.
//        $vars[NAME_GLYPH] = 'glyphicon ' . $value;
//        $vars[NAME_GLYPH_TITLE] = "Details";
//        $vars[NAME_LINK_CLASS_DEFAULT] = NO_CLASS;

        return $vars;
    }

    /**
     * Create a ToolTip: $toolTip[0] and $toolTip[1] have to inserted in HTML code accordingly.
     * $vars[NAME_TOOL_TIP_JS][0]: JS to show '$toolTip[1]'.
     * $vars[NAME_TOOL_TIP_JS][1]: '<span>...</span>' with the tooltip text.
     *
     * @param $vars
     * @param $value
     *
     * @return array
     */
    private function buildToolTip($vars, $value) {
        static $count = 0;

//        $toolTipIndex = 'tooltip.' . $GLOBALS["TSFE"]->currentRecord . '.' . ++$count;
        $toolTipIndex = 'fake';

        $vars[NAME_TOOL_TIP_JS] = array();

        // Expample: <img src="fileadmin/icons/bullet-gray.gif" onmouseover="document.getElementById('gm167979').style.
        // display='block';" onmouseout="document.getElementById('gm167979').style.display='none';" />
        $vars[NAME_TOOL_TIP_JS][0] = " onmouseover=\"document.getElementById('" . $toolTipIndex .
            "').style.display='block';\" onmouseout=\"document.getElementById('" . $toolTipIndex . "').style.display='none';\"";

        // Example: <span id="gm167979" style="display:none; position:absolute; border:solid 1px black; background-color:#F9F3D0;
        // padding:3px;">My pesonal tooltip</span>
        $vars[NAME_TOOL_TIP_JS][1] = '<span id="' . $toolTipIndex .
            '" style="display:none; position:absolute; border:solid 1px black; background-color:#F9F3D0; padding:3px;">' .
            $value . '</span>';

        return $vars;
    }

}