<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/6/16
 * Time: 8:02 PM
 */

namespace IMATHUZH\Qfq\Core;

use IMATHUZH\Qfq\Core\Database\Database;
use IMATHUZH\Qfq\Core\Helper\HelperFile;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\KeyValueStringParser;
use IMATHUZH\Qfq\Core\Helper\Ldap;
use IMATHUZH\Qfq\Core\Helper\Logger;
use IMATHUZH\Qfq\Core\Helper\OnArray;
use IMATHUZH\Qfq\Core\Helper\Sanitize;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Report\Link;
use IMATHUZH\Qfq\Core\Report\Report;
use IMATHUZH\Qfq\Core\Store\Sip;
use IMATHUZH\Qfq\Core\Store\Store;

/**
 * Class AbstractBuildForm
 * @package qfq
 */
abstract class AbstractBuildForm {
    /**
     * @var array
     */
    protected $formSpec = array();  // copy of the loaded form
    /**
     * @var array
     */
    protected $feSpecAction = array(); // copy of all formElement.class='action' of the loaded form
    /**
     * @var array
     */
    protected $feSpecNative = array(); // copy of all formElement.class='native' of the loaded form
    /**
     * @var array
     */
    protected $buildElementFunctionName = array();
    /**
     * @var array
     */
    protected $pattern = array();
    /**
     * @var array
     */
    protected $wrap = array();
    /**
     * @var array
     */
    protected $symbol = array();
    /**
     * @var bool
     */
    protected $showDebugInfoFlag = false;

//    protected $feDivClass = array(); // Wrap FormElements in <div class="$feDivClass[type]">

    /**
     * @var Store
     */
    protected $store = null;
    /**
     * @var Evaluate
     */
    protected $evaluate = null;
    /**
     * @var string
     */
    private $formId = null;
    /**
     * @var Sip
     */
    private $sip = null;
    /**
     * @var Link
     */
    protected $link = null;
    /**
     * @var Report
     */
    private $report = null;
    /**
     * @var BodytextParser
     */
    private $bodytextParser = null;

    /**
     * @var Database[] Array of Database instantiated class
     */
    protected $dbArray = array();

    /**
     * @var bool|mixed
     */
    protected $dbIndexData = false;
    /**
     * @var bool|string
     */
    protected $dbIndexQfq = false;

    /**
     * AbstractBuildForm constructor.
     *
     * @param array $formSpec
     * @param array $feSpecAction
     * @param array $feSpecNative
     * @param array Database $db
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function __construct(array $formSpec, array $feSpecAction, array $feSpecNative, array $db = null) {
        $this->formSpec = $formSpec;
        $this->feSpecAction = $feSpecAction;
        $this->feSpecNative = $feSpecNative;
        $this->store = Store::getInstance();
//        $this->dbIndexData = $this->store->getVar(SYSTEM_DB_INDEX_DATA, STORE_SYSTEM);
        $this->dbIndexData = $formSpec[F_DB_INDEX];
        $this->dbIndexQfq = $this->store->getVar(SYSTEM_DB_INDEX_QFQ, STORE_SYSTEM);

        $this->dbArray = $db;
        $this->evaluate = new Evaluate($this->store, $this->dbArray[$this->dbIndexData]);
        $this->showDebugInfoFlag = Support::findInSet(SYSTEM_SHOW_DEBUG_INFO_YES, $this->store->getVar(SYSTEM_SHOW_DEBUG_INFO, STORE_SYSTEM));

        $this->sip = $this->store->getSipInstance();

        $this->link = new Link($this->sip, $this->dbIndexData);

        // render mode specific
        $this->fillWrap();

        $this->buildElementFunctionName = [
            FE_TYPE_CHECKBOX => 'Checkbox',
            FE_TYPE_DATE => 'DateTime',
            FE_TYPE_DATETIME => 'DateTime',
            'dateJQW' => 'DateJQW',
            'datetimeJQW' => 'DateJQW',
            'email' => 'Input',
            'gridJQW' => 'GridJQW',
            FE_TYPE_EXTRA => 'Extra',
            FE_TYPE_TEXT => 'Input',
            FE_TYPE_EDITOR => 'Editor',
            FE_TYPE_TIME => 'DateTime',
            FE_TYPE_NOTE => 'Note',
            FE_TYPE_PASSWORD => 'Input',
            FE_TYPE_RADIO => 'Radio',
            FE_TYPE_SELECT => 'Select',
            FE_TYPE_SUBRECORD => 'Subrecord',
            FE_TYPE_UPLOAD => 'File',
            FE_TYPE_ANNOTATE => 'Annotate',
            FE_TYPE_IMAGE_CUT => 'ImageCut',
            'fieldset' => 'Fieldset',
            'pill' => 'Pill',
            'templateGroup' => 'TemplateGroup',
        ];

        $this->buildRowName = [
            FE_TYPE_CHECKBOX => 'Native',
            FE_TYPE_DATE => 'Native',
            FE_TYPE_DATETIME => 'Native',
            'dateJQW' => 'Native',
            'datetimeJQW' => 'Native',
            'email' => 'Native',
            'gridJQW' => 'Native',
            FE_TYPE_EXTRA => 'Native',
            FE_TYPE_TEXT => 'Native',
            FE_TYPE_EDITOR => 'Native',
            FE_TYPE_TIME => 'Native',
            FE_TYPE_NOTE => 'Native',
            FE_TYPE_PASSWORD => 'Native',
            FE_TYPE_RADIO => 'Native',
            FE_TYPE_SELECT => 'Native',
            FE_TYPE_SUBRECORD => 'Subrecord',
            FE_TYPE_UPLOAD => 'Native',
            FE_TYPE_ANNOTATE => 'Native',
            FE_TYPE_IMAGE_CUT => 'Native',
            'fieldset' => 'Fieldset',
            'pill' => 'Pill',
            'templateGroup' => 'TemplateGroup',
        ];

        $this->symbol[SYMBOL_EDIT] = "<span class='glyphicon " . GLYPH_ICON_EDIT . "'></span>";
        $this->symbol[SYMBOL_SHOW] = "<span class='glyphicon " . GLYPH_ICON_SHOW . "'></span>";
        $this->symbol[SYMBOL_NEW] = "<span class='glyphicon " . GLYPH_ICON_NEW . "'></span>";
        $this->symbol[SYMBOL_DELETE] = "<span class='glyphicon " . GLYPH_ICON_DELETE . "'></span>";
    }

    abstract public function fillWrap();

    /**
     * Builds complete 'form'. Depending of form specification, the layout will be 'plain' / 'table' / 'bootstrap'.
     *
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @param bool $htmlElementNameIdZero
     * @param array $latestFeSpecNative
     * @return array|string $mode=LOAD_FORM: The whole form as HTML, $mode=FORM_UPDATE: array of all
     *                        formElement.dynamicUpdate-yes  values/states
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function process($mode, $htmlElementNameIdZero = false, $latestFeSpecNative = array()) {
        $htmlHead = '';
        $htmlTail = '';
        $htmlT3vars = '';
        $htmlSubrecords = '';
        $htmlElements = '';
        $json = array();

        // After action 'afterSave', it's necessary to reinitialize the FeSpecNative
        if (!empty($latestFeSpecNative)) {
            $this->feSpecNative = $latestFeSpecNative;
        }

        $modeCollectFe = FLAG_DYNAMIC_UPDATE;
        $storeUse = STORE_USE_DEFAULT;

        if ($mode === FORM_SAVE) {
            $modeCollectFe = FLAG_ALL;
            $storeUse = STORE_RECORD . STORE_TABLE_DEFAULT;
        }

        // <form>
        if ($mode === FORM_LOAD) {
            $htmlHead = $this->head();
        }

        $filter = $this->getProcessFilter();

        if ($this->formSpec['multiMode'] !== 'none') {

            $parentRecords = $this->dbArray[$this->dbIndexQfq]->sql($this->formSpec['multiSql']);
            foreach ($parentRecords as $row) {
                $this->store->setStore($row, STORE_PARENT_RECORD, true);
                $jsonTmp = array();
                $htmlElements = $this->elements($row['_id'], $filter, 0, $jsonTmp, $modeCollectFe);
                $json[] = $jsonTmp;
            }

        } else {

            $recordId = $this->store->getVar(SIP_RECORD_ID, STORE_SIP);
            if (!($recordId == '' || is_numeric($recordId))) {
                throw new \UserFormException(
                    json_encode([ERROR_MESSAGE_TO_USER => 'Invalid record ID', ERROR_MESSAGE_TO_DEVELOPER => 'Invalid record ID: r="' . $recordId]),
                    ERROR_INVALID_VALUE);
            }

            // Build FormElements
            $htmlElements = $this->elements($recordId, $filter, 0, $json, $modeCollectFe, $htmlElementNameIdZero, $storeUse, $mode);

            if ($mode === FORM_SAVE && $recordId != 0) {

                // element-update: with 'value'
                $recordId = $this->store->getVar(SIP_RECORD_ID, STORE_SIP . STORE_ZERO);
                $md5 = $this->buildRecordHashMd5($this->formSpec[F_TABLE_NAME], $recordId, $this->formSpec[F_PRIMARY_KEY]);

                // Via 'element-update'
                $json[][API_ELEMENT_UPDATE][DIRTY_RECORD_HASH_MD5][API_ELEMENT_ATTRIBUTE]['value'] = $md5;
            }
        }

        // <form>
        if ($mode === FORM_LOAD) {
            $htmlT3vars = $this->prepareT3VarsForSave();
            $htmlTail = $this->tail();
            $htmlSubrecords = $this->doSubrecords();
        }
        $htmlHidden = $this->buildAdditionalFormElements();

        $htmlSip = $this->buildHiddenSip($json);

        return ($mode === FORM_LOAD) ? $htmlHead . $htmlHidden . $htmlElements . $htmlSip . $htmlT3vars . $htmlTail . $htmlSubrecords : $json;
    }

    /**
     * Builds the head area of the form.
     *
     * @param string $mode
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function head($mode = FORM_LOAD) {
        $html = '';

        $html .= '<div ' . Support::doAttribute('class', $this->formSpec[F_CLASS], true) . '>'; // main <div class=...> around everything

        // Logged in BE User will see a FormEdit Link
        $sipParamString = OnArray::toString($this->store->getStore(STORE_SIP), ':', ', ', "'");
        $formEditUrl = $this->createFormEditorUrl(FORM_NAME_FORM, $this->formSpec[F_ID]);

        $html .= "<p><a " . Support::doAttribute('href', $formEditUrl) . ">Edit</a> <small>[$sipParamString]</small></p>";

        $html .= $this->wrapItem(WRAP_SETUP_TITLE, $this->formSpec[F_TITLE], true);

        $html .= $this->getFormTag();

        return $html;
    }

    /**
     * If SHOW_DEBUG_INFO=yes: create a link (incl. SIP) to edit the current form. Show also the hidden content of
     * the SIP.
     *
     * @param string $form FORM_NAME_FORM | FORM_NAME_FORM_ELEMENT
     * @param int $recordId id of form or formElement
     * @param array $param
     *
     * @return string String: <a href="?pageId&sip=....">Edit</a> <small>[sip:..., r:..., urlparam:...,
     *                ...]</small>
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function createFormEditorUrl($form, $recordId, array $param = array()) {

        if (!$this->showDebugInfoFlag) {
            return '';
        }

        $queryStringArray = [
            'id' => $this->store->getVar(SYSTEM_EDIT_FORM_PAGE, STORE_SYSTEM),
            'form' => $form,
            'r' => $recordId,
            PARAM_DB_INDEX_DATA => $this->dbIndexQfq,
        ];
        $queryStringArray = array_merge($queryStringArray, $param);

        $queryString = Support::arrayToQueryString($queryStringArray);

        $sip = $this->store->getSipInstance();
        $url = $sip->queryStringToSip($queryString);

        return $url;
    }

    /**
     * Wrap's $this->wrap[$item][WRAP_SETUP_START] around $value. If $flagOmitEmpty==true && $value=='': return ''.
     *
     * @param string $item
     * @param string $value
     * @param bool|false $flagOmitEmpty
     *
     * @return string
     */
    public function wrapItem($item, $value, $flagOmitEmpty = false) {

        if ($flagOmitEmpty && $value === "") {
            return '';
        }

        return $this->wrap[$item][WRAP_SETUP_START] . $value . $this->wrap[$item][WRAP_SETUP_END];
    }

    /**
     * Returns '<form ...>'-tag with various attributes.
     *
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function getFormTag() {
        $md5 = '';

        $attribute = $this->getFormTagAttributes();

        $honeypot = $this->getHoneypotVars();

        $md5 = $this->buildInputRecordHashMd5();

        return '<form ' . OnArray::toString($attribute, '=', ' ', "'") . '>' . $honeypot . $md5;
    }

    /**
     * Build MD5 from the current record. Return HTML Input element.
     *
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function buildInputRecordHashMd5() {

        $recordId = $this->store->getVar(SIP_RECORD_ID, STORE_SIP . STORE_ZERO);
        $md5 = $this->buildRecordHashMd5($this->formSpec[F_TABLE_NAME], $recordId, $this->formSpec[F_PRIMARY_KEY]);

        $data = "<input id='" . DIRTY_RECORD_HASH_MD5 . "' name='" . DIRTY_RECORD_HASH_MD5 . "' type='hidden' value='$md5'>";

//        $data = "<input id='" . DIRTY_RECORD_HASH_MD5 . "' name='" . DIRTY_RECORD_HASH_MD5 . "' type='text' value='$md5'>";

        return $data;
    }


    /**
     * @param $tableName
     * @param $recordId
     * @param string $primaryKey
     *
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function buildRecordHashMd5($tableName, $recordId, $primaryKey = F_PRIMARY_KEY_DEFAULT) {
        $record = array();

        if ($recordId != 0) {
            $record = $this->dbArray[$this->dbIndexData]->sql("SELECT * FROM $tableName WHERE $primaryKey=?", ROW_EXPECT_1, [$recordId], "Record to load not found.");
        }

        return OnArray::getMd5($record);
    }

    /**
     * Create HTML Input vars to detect bot automatic filling of forms.
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function getHoneypotVars() {
        $html = '';

        $vars = $this->store->getVar(SYSTEM_SECURITY_VARS_HONEYPOT, STORE_SYSTEM);

        // Iterate over all fake vars
        $arr = explode(',', $vars);
        foreach ($arr as $name) {
            $name = trim($name);
            if ($name === '') {
                continue;
            }
            $html .= "<input name='$name' type='hidden' value='' readonly>";
        }

        return $html;
    }


    /**
     * Build an assoc array with standard form attributes.
     *
     * @return array
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function getFormTagAttributes() {

        $attribute['id'] = $this->getFormId();
        $attribute['method'] = 'post';
        $attribute['action'] = $this->getActionUrl();
        $attribute['target'] = '_top';
        $attribute['accept-charset'] = 'UTF-8';
        $attribute[FE_INPUT_AUTOCOMPLETE] = 'on';
        $attribute['enctype'] = $this->getEncType();
        $attribute['data-disable-return-key-submit'] = $this->formSpec[F_ENTER_AS_SUBMIT] == '1' ? "false" : "true"; // attribute meaning is inverted

        return $attribute;
    }

    /**
     * Return a uniq form id
     *
     * @return string
     */
    public function getFormId() {
        if ($this->formId === null) {
            $this->formId = uniqid('qfq-form-');
        }

        return $this->formId;
    }

    /**
     * Builds the HTML 'form'-tag inlcuding all attributes and target.
     *
     * Notice: the SIP will be transferred as POST Parameter.
     *
     * @return string
     */
    public function getActionUrl() {

        return API_DIR . '/save.php';
    }

    /**
     * Determines the enctype.
     *
     * See: https://www.w3.org/wiki/HTML/Elements/form#HTML_Attributes
     *
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function getEncType() {

        $result = $this->dbArray[$this->dbIndexQfq]->sql("SELECT id FROM FormElement AS fe WHERE fe.formId=? AND fe.type='upload' LIMIT 1", ROW_REGULAR, [$this->formSpec['id']], 'Look for Formelement.type="upload"');

        return (count($result) === 1) ? 'multipart/form-data' : 'application/x-www-form-urlencoded';

    }

    abstract public function getProcessFilter();

    /**
     * @param array|string $value
     *
     * @return array|string
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    private function processReportSyntax($value) {

        if (is_array($value)) {
            $new = array();

            //might happen for e.g Template Groups
            foreach ($value as $item) {
                $new[] = $this->processReportSyntax($item);
            }

            return $new;
        }

        $value = trim($value);
        if (substr($value, 0, 8) == SHEBANG_REPORT) {
            if ($this->report === null) {
                $this->report = new Report(array(), $this->evaluate, false);
            }

            if ($this->bodytextParser === null) {
                $this->bodytextParser = new BodytextParser();
            }

            $storeRecord = $this->store->getStore(STORE_RECORD);
            $value = $this->report->process($this->bodytextParser->process($value));
            $this->store->setStore($storeRecord, STORE_RECORD, true);
            $this->store->setVar(SYSTEM_REPORT_FULL_LEVEL, '', STORE_SYSTEM); // debug
        }

        return $value;
    }

    /**
     * Process all FormElements in $this->feSpecNative: Collect and return all HTML code & JSON.
     *
     * @param int $recordId
     * @param string $filter FORM_ELEMENTS_NATIVE | FORM_ELEMENTS_SUBRECORD | FORM_ELEMENTS_NATIVE_SUBRECORD
     * @param int $feIdContainer
     * @param array $json
     * @param string $modeCollectFe
     * @param bool $htmlElementNameIdZero
     * @param string $storeUseDefault
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function elements($recordId, $filter, $feIdContainer, array &$json,
                             $modeCollectFe = FLAG_DYNAMIC_UPDATE, $htmlElementNameIdZero = false,
                             $storeUseDefault = STORE_USE_DEFAULT, $mode = FORM_LOAD) {
        $html = '';

        // The following 'FormElement.parameter' will never be used during load (fe.type='upload'). FE_PARAMETER has been already expanded.
        $skip = [FE_SQL_UPDATE, FE_SQL_INSERT, FE_SQL_DELETE, FE_SQL_AFTER, FE_SQL_BEFORE, FE_PARAMETER, FE_FILL_STORE_VAR, FE_FILE_DOWNLOAD_BUTTON];

        // get current data record
        $primaryKey = $this->formSpec[F_PRIMARY_KEY];
        if ($recordId > 0 && $this->store->getVar($primaryKey, STORE_RECORD) === false) {
            $tableName = $this->formSpec[F_TABLE_NAME];
            $row = $this->dbArray[$this->dbIndexData]->sql("SELECT * FROM $tableName WHERE $primaryKey = ?", ROW_EXPECT_1,
                array($recordId), "Form '" . $this->formSpec[F_NAME] . "' failed to load record '$primaryKey'='$recordId' from table '" .
                $this->formSpec[F_TABLE_NAME] . "'.");
            $this->store->setStore($row, STORE_RECORD);
        }

        $this->checkAutoFocus();

        $parameterLanguageFieldName = $this->store->getVar(SYSTEM_PARAMETER_LANGUAGE_FIELD_NAME, STORE_SYSTEM);

        // Iterate over all FormElements
        foreach ($this->feSpecNative as $fe) {
            $storeUse = $storeUseDefault;

            if (($filter === FORM_ELEMENTS_NATIVE && $fe[FE_TYPE] === 'subrecord')
                || ($filter === FORM_ELEMENTS_SUBRECORD && $fe[FE_TYPE] !== 'subrecord')
//                || ($filter === FORM_ELEMENTS_DYNAMIC_UPDATE && $fe[FE_DYNAMIC_UPDATE] === 'no')
            ) {
                continue; // skip this FE
            }

            $flagOutput = ($fe[FE_TYPE] !== FE_TYPE_EXTRA); // type='extra' will not displayed and not transmitted to the form.

            $debugStack = array();

            // Preparation for Log, Debug
            $this->store->setVar(SYSTEM_FORM_ELEMENT, Logger::formatFormElementName($fe), STORE_SYSTEM);
            $this->store->setVar(SYSTEM_FORM_ELEMENT_ID, $fe[FE_ID], STORE_SYSTEM);

            // Fill STORE_LDAP
            $fe = $this->prepareFillStoreFireLdap($fe);

            if (isset($fe[FE_FILL_STORE_VAR])) {
                $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, FE_FILL_STORE_VAR, STORE_SYSTEM); // debug
                $fe[FE_FILL_STORE_VAR] = $this->evaluate->parse($fe[FE_FILL_STORE_VAR], ROW_EXPECT_0_1);
                $this->store->appendToStore($fe[FE_FILL_STORE_VAR], STORE_VAR);
            }

            // for Upload FormElements, it's necessary to pre-calculate an optional given 'slaveId'.
            if ($fe[FE_TYPE] === FE_TYPE_UPLOAD) {
                Support::setIfNotSet($fe, FE_SLAVE_ID);
                $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, FE_SLAVE_ID, STORE_SYSTEM); // debug
                $slaveId = Support::falseEmptyToZero($this->evaluate->parse($fe[FE_SLAVE_ID]));
                $this->store->setVar(VAR_SLAVE_ID, $slaveId, STORE_VAR);
            }

            $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, FE_VALUE, STORE_SYSTEM); // debug
            $fe[FE_VALUE] = $this->processReportSyntax($fe[FE_VALUE]);

            $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, FE_NOTE, STORE_SYSTEM); // debug
            $fe[FE_NOTE] = $this->processReportSyntax($fe[FE_NOTE]);

            // ** evaluate current FormElement **
            $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, 'Some of the columns of current FormElement', STORE_SYSTEM); // debug
            $formElement = $this->evaluate->parseArray($fe, $skip, $debugStack);
            $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, 'Set language', STORE_SYSTEM); // debug
            $formElement = HelperFormElement::setLanguage($formElement, $parameterLanguageFieldName);

            // Some Defaults
            $formElement = Support::setFeDefaults($formElement, $this->formSpec);

//            // Copy global readonly mode.
//            if ($this->formSpec[F_MODE] == F_MODE_READONLY) {
//                $fe[FE_MODE] = FE_MODE_READONLY;
//                $fe[FE_MODE_SQL] = '';
//            }

            if ($flagOutput === true) {
                $this->fillWrapLabelInputNote($formElement[FE_BS_LABEL_COLUMNS], $formElement[FE_BS_INPUT_COLUMNS], $formElement[FE_BS_NOTE_COLUMNS]);
            }

            $value = '';
            Support::setIfNotSet($formElement, FE_VALUE);

            if (is_array($formElement[FE_VALUE])) {
                $formElement[FE_VALUE] = (count($formElement[FE_VALUE])) > 0 ? current($formElement[FE_VALUE][0]) : '';
            }

            $value = $formElement[FE_VALUE];

            if ($value === '') {
                // #2064 / Only take the default, if the FE is a real tablecolumn.
                // #3426 / Dynamic Update: Inputs loose the new content and shows the old value.
                if ($storeUse == STORE_USE_DEFAULT && $this->store->getVar($formElement[FE_NAME], STORE_TABLE_COLUMN_TYPES) === false) {
                    $storeUse = str_replace(STORE_TABLE_DEFAULT, '', $storeUse); // Remove STORE_DEFAULT
                }

                // In case the current element is a 'RETYPE' element: take the element name of the source FormElement. Needed to retrieve the default value later.
                $name = (isset($formElement[FE_RETYPE_SOURCE_NAME])) ? $formElement[FE_RETYPE_SOURCE_NAME] : $formElement[FE_NAME];

                // Retrieve value via FSRVD
                $sanitizeClass = ($mode == FORM_UPDATE) ? SANITIZE_ALLOW_ALL : $formElement[FE_CHECK_TYPE];
                $value = $this->store->getVar($name, $storeUse, $sanitizeClass, $foundInStore);
            }

            if ($formElement[FE_ENCODE] === FE_ENCODE_SPECIALCHAR) {
//                $value = htmlspecialchars_decode($value, ENT_QUOTES);
                $value = Support::htmlEntityEncodeDecode(MODE_DECODE, $value);
            }

            // Typically: $htmlElementNameIdZero = true
            // After Saving a record, staying on the form, the FormElements on the Client are still known as '<feName>:0'.
            $htmlFormElementName = HelperFormElement::buildFormElementName($formElement, ($htmlElementNameIdZero) ? 0 : $recordId);
            $formElement[FE_HTML_ID] = HelperFormElement::buildFormElementId($this->formSpec[F_ID], $formElement[FE_ID],
                ($htmlElementNameIdZero) ? 0 : $recordId,
                $formElement[FE_TG_INDEX]);

            // Construct Marshaller Name: buildElement
            $buildElementFunctionName = 'build' . $this->buildElementFunctionName[$formElement[FE_TYPE]];

            $jsonElement = array();
            $elementExtra = '';
            // Render pure element
            $elementHtml = $this->$buildElementFunctionName($formElement, $htmlFormElementName, $value, $jsonElement, $mode);

            // container elements do not have dynamicUpdate='yes'. Instead they deliver nested elements.
            if ($formElement[FE_CLASS] == FE_CLASS_CONTAINER) {
                if (count($jsonElement) > 0) {
                    $json = array_merge($json, $jsonElement);
                }
            } else {
                // for non-container elements: just add the current json status
                if ($modeCollectFe === FLAG_ALL || ($modeCollectFe == FLAG_DYNAMIC_UPDATE && $fe[FE_DYNAMIC_UPDATE] === 'yes')) {
                    if (isset($jsonElement[0]) && is_array($jsonElement[0])) {
                        // Checkboxes are delivered as array of arrays: unnest them and append them to the existing json array.
                        $json = array_merge($json, $jsonElement);
                    } else {
                        $json[] = $jsonElement;
                    }
                }
            }

            if ($flagOutput) {
                // debugStack as Tooltip
                if ($this->showDebugInfoFlag) {
                    if (count($debugStack) > 0) {
                        $elementHtml .= Support::doTooltip($formElement[FE_HTML_ID] . HTML_ID_EXTENSION_TOOLTIP, implode("\n", $debugStack));
                    }

                    // Build 'FormElement' Edit symbol
                    $feEditUrl = $this->createFormEditorUrl(FORM_NAME_FORM_ELEMENT, $formElement[FE_ID], ['formId' => $formElement[FE_FORM_ID]]);
                    $titleAttr = Support::doAttribute('title', $this->formSpec[FE_NAME] . ' / ' . $formElement[FE_NAME] . ' [' . $formElement[FE_ID] . ']');
                    $icon = Support::wrapTag('<span class="' . GLYPH_ICON . ' ' . GLYPH_ICON_EDIT . '">', '');
                    $elementHtml .= Support::wrapTag("<a class='hidden " . CLASS_FORM_ELEMENT_EDIT . "' href='$feEditUrl' $titleAttr>", $icon);
                }

                // Construct Marshaller Name: buildRow
                $buildRowName = 'buildRow' . $this->buildRowName[$formElement[FE_TYPE]];

                $html .= $formElement[FE_HTML_BEFORE] . $this->$buildRowName($formElement, $elementHtml, $htmlFormElementName) . $formElement[FE_HTML_AFTER];
            }
        }

        $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, '', STORE_SYSTEM); // debug

        // Log / Debug: Last FormElement has been processed.
        $this->store->setVar(SYSTEM_FORM_ELEMENT, '', STORE_SYSTEM);

        return $html;
    }

    /**
     * Checks if LDAP search is requested.
     * Yes: prepare configuration and fire the query.
     * No: do nothing.
     *
     * @param array $formElement
     *
     * @return array
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function prepareFillStoreFireLdap(array $formElement) {

        if (isset($formElement[FE_FILL_STORE_LDAP]) || isset($formElement[FE_TYPEAHEAD_LDAP])) {
            $keyNames = [F_LDAP_SERVER, F_LDAP_BASE_DN, F_LDAP_ATTRIBUTES,
                F_LDAP_SEARCH, F_TYPEAHEAD_LDAP_SEARCH, F_TYPEAHEAD_LDAP_SEARCH_PER_TOKEN, F_TYPEAHEAD_LDAP_SEARCH_PREFETCH,
                F_TYPEAHEAD_LIMIT, F_TYPEAHEAD_MINLENGTH, F_TYPEAHEAD_LDAP_VALUE_PRINTF,
                F_TYPEAHEAD_LDAP_ID_PRINTF, F_LDAP_TIME_LIMIT, F_LDAP_USE_BIND_CREDENTIALS];
            $formElement = OnArray::copyArrayItemsIfNotAlreadyExist($this->formSpec, $formElement, $keyNames);
        } else {
            return $formElement; // nothing to do.
        }

        if (isset($formElement[FE_FILL_STORE_LDAP])) {

            // Extract necessary elements
            $config = OnArray::getArrayItems($formElement, [FE_LDAP_SERVER, FE_LDAP_BASE_DN, FE_LDAP_SEARCH, FE_LDAP_ATTRIBUTES]);

            $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN,
                FE_LDAP_SERVER . ',' . FE_LDAP_BASE_DN . ',' . FE_LDAP_SEARCH . ',' . FE_LDAP_ATTRIBUTES, STORE_SYSTEM);
            $config = $this->evaluate->parseArray($config);

            if ($formElement[FE_LDAP_USE_BIND_CREDENTIALS] == 1) {
                $config[SYSTEM_LDAP_1_RDN] = $this->store->getVar(SYSTEM_LDAP_1_RDN, STORE_SYSTEM);
                $config[SYSTEM_LDAP_1_PASSWORD] = $this->store->getVar(SYSTEM_LDAP_1_PASSWORD, STORE_SYSTEM);
            }

            $ldap = new Ldap();
            $arr = $ldap->process($config, '', MODE_LDAP_SINGLE);
            $this->store->setStore($arr, STORE_LDAP, true);
        }

        return $formElement;
    }

    /**
     * Check if there is an explicit 'autofocus' definition in at least one FE.
     * Found: do nothing, it will be rendered at the correct position.
     * Not found: set 'autofocus' on the first FE.
     *
     * Accepted misbehaviour on forms with pills: if there is at least one editable element on the first pill,
     *   the other pills are not checked - independent if there was a definition on the first pill or not.
     *   Reason: checks happens per pill - if there is no explizit definition on the first pill, take the first
     *   editable element of that pill.
     */
    private function checkAutoFocus() {
        static $found = false;
        $idx = false;

        if ($found) {
            return;
        }

        // Search if there is an explicit autofocus definition.
        for ($i = 0; $i < count($this->feSpecNative); ++$i) {
            // Only check native elements which will be shown
            if ($this->feSpecNative[$i][FE_CLASS] == FE_CLASS_NATIVE &&
                ($this->feSpecNative[$i][FE_MODE] == FE_MODE_SHOW || $this->feSpecNative[$i][FE_MODE] == FE_MODE_REQUIRED ||
                    $this->feSpecNative[$i][FE_MODE] == FE_MODE_SHOW_REQUIRED)
            ) {
                // Check if there is an explicit definition.
                if (isset($this->feSpecNative[$i][FE_AUTOFOCUS])) {
                    if ($this->feSpecNative[$i][FE_AUTOFOCUS] == '' || $this->feSpecNative[$i][FE_AUTOFOCUS] == '1') {
                        $this->feSpecNative[$i][FE_AUTOFOCUS] = '1'; // fix to '=1'
                    } else {
                        unset($this->feSpecNative[$i][FE_AUTOFOCUS]);
                    }
                    $found = true;

                    return;
                }

                if ($idx === false) {
                    $idx = $i;
                }
            }
        }

        // No explicit definition found: take the first found editable element.
        if ($idx !== false) {
            $found = true;
            // No explicit definition found: set autofocus.
            $this->feSpecNative[$idx][FE_AUTOFOCUS] = '1';
        }
    }

    /**
     * @param $label
     * @param $input
     * @param $note
     */
    abstract public function fillWrapLabelInputNote($label, $input, $note);

    /**
     * Copy a subset of current STORE_TYPO3 variables to SIP. Set a hidden form field to submit the assigned SIP to
     * save/update.
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function prepareT3VarsForSave() {

        $t3VarsSip = $this->store->copyT3VarsToSip();

        return $this->buildNativeHidden(CLIENT_TYPO3VARS, $t3VarsSip);

    }

    /**
     * Builds a real HTML hidden form element. Useful for checkboxes, Multiple-Select and Radios.
     *
     * @param        $htmlFormElementName
     * @param string $value
     *
     * @return string
     */
    public function buildNativeHidden($htmlFormElementName, $value) {
        return '<input type="hidden" name="' . $htmlFormElementName . '" value="' . htmlentities($value) . '">';
    }

    /**
     *
     */
    abstract public function tail();

    /**
     *
     */
    abstract public function doSubrecords();

    /**
     * Get all elements from STORE_ADDITIONAL_FORM_ELEMENTS and return them as a string.
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function buildAdditionalFormElements() {

        $data = $this->store->getStore(STORE_ADDITIONAL_FORM_ELEMENTS);

        return is_array($data) ? implode('', $data) : '';
    }

    /**
     * Create a hidden sip, based on latest STORE_SIP Values. Return complete HTML 'hidden' element.
     *
     * @param array $json
     *
     * @return string  <input type='hidden' name='s' value='<sip>'>
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function buildHiddenSip(array &$json) {

        $sipArray = $this->store->getStore(STORE_SIP);

        // do not include system vars
        unset($sipArray[SIP_SIP]);
        unset($sipArray[SIP_URLPARAM]);

        $queryString = Support::arrayToQueryString($sipArray);
        $sip = $this->store->getSipInstance();

        $sipValue = $sip->queryStringToSip($queryString, RETURN_SIP);

        $json[] = $this->getFormElementForJson(CLIENT_SIP, $sipValue, [FE_MODE => FE_MODE_SHOW]);

        return $this->buildNativeHidden(CLIENT_SIP, $sipValue);
    }

    /**
     * Create an array with standard elements for 'mode' (hidden, disabled, required) and add 'form-element',
     * 'value'.
     * 'Generic Element Update': add via API_ELEMENT_UPDATE 'label' and 'note'.
     * All collected data as array - will be later converted to JSON.
     *
     * @param string $htmlFormElementName
     * @param string|array $value
     * @param array $formElement
     *
     * @return array
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function getFormElementForJson($htmlFormElementName, $value, array $formElement) {
        $addClassRequired = array();

        $json = $this->getJsonFeMode($formElement[FE_MODE]); // disabled, required

        $json[API_FORM_UPDATE_FORM_ELEMENT] = $htmlFormElementName;

        if (isset($formElement[FE_FLAG_ROW_OPEN_TAG]) && isset($formElement[FE_FLAG_ROW_CLOSE_TAG])) {
            $flagRowUpdate = ($formElement[FE_FLAG_ROW_OPEN_TAG] && $formElement[FE_FLAG_ROW_CLOSE_TAG]);
        } else {
            $flagRowUpdate = true;
        }

        $statusHidden = ($formElement[FE_MODE] == 'hidden');
        $pattern = null;
        if (isset($formElement[FE_CHECK_PATTERN]) && $formElement[FE_CHECK_PATTERN] != '') {
            $pattern = $statusHidden ? false : $formElement[FE_CHECK_PATTERN];
        }

        // 'value' update via 'form-update' on the full row: only if there is no other FE in that row
        if ($flagRowUpdate) {
            $json[API_FORM_UPDATE_VALUE] = $value;

//CR            if ($pattern !== null) {
//                $json['pattern'] = $pattern;
//            }
        }

        if ($formElement[FE_MODE] == FE_MODE_REQUIRED || $formElement[FE_MODE] == FE_MODE_SHOW_REQUIRED) {
            $addClassRequired = HelperFormElement::getRequiredPositionClass($formElement[F_FE_REQUIRED_POSITION]);
        }

        if (isset($formElement[FE_LABEL])) {
            $key = $formElement[FE_HTML_ID] . HTML_ID_EXTENSION_LABEL;
            $json[API_ELEMENT_UPDATE][$key][API_ELEMENT_CONTENT] = $this->buildLabel($htmlFormElementName, $formElement[FE_LABEL], $addClassRequired[FE_LABEL] ?? '');
        }

        if (isset($formElement[FE_NOTE])) {
            $key = $formElement[FE_HTML_ID] . HTML_ID_EXTENSION_NOTE;
            if (!empty($addClassRequired[FE_NOTE])) {
                $formElement[FE_NOTE] = Support::wrapTag('<span class="' . $addClassRequired[FE_NOTE] . '">', $formElement[FE_NOTE]);
            }
            $json[API_ELEMENT_UPDATE][$key][API_ELEMENT_CONTENT] = $formElement[FE_NOTE];
        }

        if (isset($formElement[FE_TYPE])) {
            $key = $formElement[FE_HTML_ID] . HTML_ID_EXTENSION_INPUT;

            // For FE.type='note': update the column 'input'
            if ($formElement[FE_TYPE] === FE_TYPE_NOTE) {
                $json[API_ELEMENT_UPDATE][$key][API_ELEMENT_CONTENT] = $value;
            }

            // Check show/hide: only FE with FE_MODE_SQL given, might change.
            if (!empty($formElement[FE_MODE_SQL])) {
                $class = is_numeric($formElement[FE_BS_INPUT_COLUMNS]) ? ('col-md-' . $formElement[FE_BS_INPUT_COLUMNS]) : $formElement[FE_BS_INPUT_COLUMNS];
//                $class = 'col-md-' . $formElement[FE_BS_INPUT_COLUMNS] . ' ';
                $class .= ($formElement[FE_MODE] == FE_MODE_HIDDEN) ? ' hidden' : '';
                $json[API_ELEMENT_UPDATE][$key][API_ELEMENT_ATTRIBUTE]['class'] = $class;
                $json[API_ELEMENT_UPDATE][$key][API_ELEMENT_ATTRIBUTE]['required'] = ($formElement[FE_MODE] == 'required');
                $json[API_ELEMENT_UPDATE][$key][API_ELEMENT_ATTRIBUTE]['hidden'] = $statusHidden;

                if ($pattern !== null) {
                    $json[API_ELEMENT_UPDATE][$key][API_ELEMENT_ATTRIBUTE]['pattern'] = $pattern;
                }
            }

            // #3647
//            if (!$flagRowUpdate) {

            // #4771 - temporary workaround: SELECT in 'multi FE row' won't updated after 'save' or with dynamic update.
            //TODO #5016 - exception for FE_TYPE_CHECKBOX should be removed ASAP
            if ($formElement[FE_TYPE] != FE_TYPE_SELECT && $formElement[FE_TYPE] != FE_TYPE_UPLOAD && $formElement[FE_TYPE] != FE_TYPE_CHECKBOX) {
                $json[API_ELEMENT_UPDATE][$formElement[FE_HTML_ID]][API_ELEMENT_ATTRIBUTE]['value'] = $value;
                $json[API_ELEMENT_UPDATE][$formElement[FE_HTML_ID]][API_ELEMENT_ATTRIBUTE]['required'] = ($formElement[FE_MODE] == 'required');
                $json[API_ELEMENT_UPDATE][$formElement[FE_HTML_ID]][API_ELEMENT_ATTRIBUTE]['hidden'] = $statusHidden;

                if ($pattern !== null) {
                    $json[API_ELEMENT_UPDATE][$formElement[FE_HTML_ID]][API_ELEMENT_ATTRIBUTE]['pattern'] = $pattern;
                }
            }
        }

        // Show / Hide the complete FormElement Row.
        if (isset($formElement[FE_HTML_ID])) { // HIDDEN_SIP comes without a real existing FE structure.
            // Activate 'show' or 'hidden' on the current FormElement via JSON 'API_ELEMENT_UPDATE'
            $class = $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_CLASS];
            if ($formElement[FE_MODE] == FE_MODE_HIDDEN) {
                $class .= ' hidden';
            }

            if (!empty($addClassRequired[FE_INPUT])) {
                $class .= ' ' . $addClassRequired[FE_INPUT];
            }

            $key = $formElement[FE_HTML_ID] . HTML_ID_EXTENSION_ROW;
            $json[API_ELEMENT_UPDATE][$key][API_ELEMENT_ATTRIBUTE]['class'] = $class;
        }

        return $json;
    }

    /**
     * Set corresponding JSON attributes readonly/required/disabled, based on $formElement[FE_MODE].
     *
     * @param string $feMode
     *
     * @return array
     * @throws \UserFormException
     */
    private function getJsonFeMode($feMode) {

        $this->getFeMode($feMode, $dummy, $disabled, $required);

        return [API_FORM_UPDATE_DISABLED => ($disabled === 'yes'), API_FORM_UPDATE_REQUIRED => ($required === 'yes')];
    }

    /**
     * Depending of $feMode set variables $hidden, $disabled, $required to 'yes' or 'no'.
     *
     * @param string $feMode
     * @param string $hidden
     * @param string $disabled
     * @param string $required
     *
     * @throws \UserFormException
     */
    private function getFeMode($feMode, &$hidden, &$disabled, &$required) {
        $hidden = 'no';
        $disabled = 'no';
        $required = 'no';

        switch ($feMode) {
            case FE_MODE_SHOW:
            case FE_MODE_SHOW_REQUIRED:
                break;
            case FE_MODE_REQUIRED:
                $required = 'yes';
                break;
            case FE_MODE_READONLY:
                $disabled = 'yes';  // convert the UI status 'readonly' to the HTML/CSS status 'disabled'.
                break;
            case FE_MODE_HIDDEN:
                $hidden = 'yes';
                break;
            default:
                throw new \UserFormException("Unknown mode '$feMode'", ERROR_UNKNOWN_MODE);
                break;
        }
    }

    /**
     * Builds a label, typically for an html-'<input>'-element.
     *
     * @param string $htmlFormElementName
     * @param string $label
     * @param string $addClass
     *
     * @return string
     * @throws \CodeException
     */
    public function buildLabel($htmlFormElementName, $label, $addClass = '') {
        $attributes = Support::doAttribute('for', $htmlFormElementName);
        $attributes .= Support::doAttribute('class', ['control-label', $addClass]);

        $html = Support::wrapTag("<label $attributes>", $label);

        return $html;
    }

    /**
     * Takes the current SIP ('form' and additional parameter), set SIP_RECORD_ID=0 and create a new 'NewRecordUrl'.
     *
     * @param $toolTipNew
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function deriveNewRecordUrlFromExistingSip(&$toolTipNew) {

        $urlParam = $this->store->getNonSystemSipParam();
        $urlParam[SIP_RECORD_ID] = 0;
        $urlParam[SIP_FORM] = $this->formSpec[F_NAME];

        Support::appendTypo3ParameterToArray($urlParam);

        $sip = $this->store->getSipInstance();

        $url = $sip->queryStringToSip(OnArray::toString($urlParam));

        if ($this->showDebugInfoFlag) {
            //TODO: missing decoding of SIP
            $toolTipNew .= PHP_EOL . PHP_EOL . OnArray::toString($urlParam, ' = ', PHP_EOL, "'");
        }

        return $url;
    }

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed
     */
    abstract public function buildRowPill(array $formElement, $elementHtml);

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed
     */
    abstract public function buildRowFieldset(array $formElement, $elementHtml);

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed
     */
    abstract public function buildRowTemplateGroup(array $formElement, $elementHtml);

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed
     */
    abstract public function buildRowSubrecord(array $formElement, $elementHtml);

    /**
     * Builds HTML 'input' element.
     * Format: <input name="$htmlFormElementName" <type="email|input|password|url" [autocomplete="autocomplete"]
     * [autofocus="autofocus"]
     *           [maxlength="$maxLength"] [placeholder="$placeholder"] [size="$size"] [min="$min"] [max="$max"]
     *           [pattern="$pattern"] [required="required"] [disabled="disabled"] value="$value">
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json Return updates in this array - will be later converted to JSON.
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string complete rendered HTML input element.
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function buildInput(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $textarea = '';
        $attribute = '';
        $class = 'form-control';
        $elementCharacterCount = '';

        $typeAheadUrlParam = $this->typeAheadBuildParam($formElement);
        if ($typeAheadUrlParam != '') {

            if (empty($formElement[FE_INPUT_TYPE])) {
                $formElement[FE_INPUT_TYPE] = FE_TYPE_SEARCH; // typeahead behaves better with 'search' instead of 'text'
            }

            if (empty($formElement[FE_INPUT_AUTOCOMPLETE])) {
                $formElement[FE_INPUT_AUTOCOMPLETE] = 'off'; // typeahead behaves better with 'autocomplete'='off'
            }

            $class .= ' ' . CLASS_TYPEAHEAD;
            $dataSip = $this->sip->queryStringToSip($typeAheadUrlParam, RETURN_SIP);
            $attribute .= Support::doAttribute(DATA_TYPEAHEAD_SIP, $dataSip);
            $attribute .= Support::doAttribute(DATA_TYPEAHEAD_LIMIT, $formElement[FE_TYPEAHEAD_LIMIT]);
            $attribute .= Support::doAttribute(DATA_TYPEAHEAD_MINLENGTH, $formElement[FE_TYPEAHEAD_MINLENGTH]);
            if (isset($formElement[FE_TYPEAHEAD_PEDANTIC]) && $formElement[FE_TYPEAHEAD_PEDANTIC] === '1') {
                $attribute .= Support::doAttribute(DATA_TYPEAHEAD_PEDANTIC, 'true');
            }
        }

        if (isset($formElement[FE_CHARACTER_COUNT_WRAP])) {
            $class .= ' ' . CLASS_CHARACTER_COUNT;
            $attribute .= Support::doAttribute(DATA_CHARACTER_COUNT_ID, $formElement[FE_HTML_ID] . HTML_ID_EXTENSION_CHARACTER_COUNT);
            $attributeCC = Support::doAttribute('id', $formElement[FE_HTML_ID] . HTML_ID_EXTENSION_CHARACTER_COUNT);

            $classCC = ($formElement[FE_CHARACTER_COUNT_WRAP] == '') ? Support::doAttribute('class', 'qfq-cc-style') : '';
            $elementCharacterCount = "<span $attributeCC $classCC></span>";

            if ($formElement[FE_CHARACTER_COUNT_WRAP] != '') {
                $arr = explode('|', $formElement[FE_CHARACTER_COUNT_WRAP], 2);
                $arr[] = '';
                $arr[] = ''; //skip check that at least 2 elements exist
                $elementCharacterCount = $arr[0] . $elementCharacterCount . $arr[1];
            }
        }

        $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attribute .= Support::doAttribute('name', $htmlFormElementName);
        $attribute .= Support::doAttribute('class', $class);
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        if (isset($formElement[FE_RETYPE_SOURCE_NAME])) {
            $htmlFormElementNamePrimary = str_replace(RETYPE_FE_NAME_EXTENSION, '', $htmlFormElementName);
            $attribute .= Support::doAttribute('data-match', '[name=' . str_replace(':', '\\:', $htmlFormElementNamePrimary) . ']');
        }

        if ($formElement[FE_MAX_LENGTH] > 0 && $value !== '') {
            // crop string only if it's not empty (substr returns false on empty strings)
            $value = mb_substr($value, 0, $formElement[FE_MAX_LENGTH]);
        }
        // 'maxLength' needs an upper 'L': naming convention for DB tables!
        if ($formElement[FE_MAX_LENGTH] > 0) {
            $attribute .= Support::doAttribute('maxlength', $formElement[FE_MAX_LENGTH], false);
        }

        if ($formElement[FE_HIDE_ZERO] != '0' && $value == '0') {
            $value = '';
        }

        if ($formElement[FE_DECIMAL_FORMAT] !== '') {
            if ($value !== '') { // empty string causes exception in number_format()
                $decimalScale = explode(',', $formElement[FE_DECIMAL_FORMAT])[1]; // scale: Nachkommastellen
                $value = number_format($value, $decimalScale, '.', '');
            }
        }

        // In case the user specifies MIN & MAX with numbers, the html tag 'type' has to be 'number', to make the range check work in the browser.
        if (empty($formElement[FE_INPUT_TYPE]) && !empty($formElement[FE_MIN]) && !empty($formElement[FE_MAX]) &&
            is_numeric($formElement[FE_MIN]) && is_numeric($formElement[FE_MAX])
        ) {
            $formElement[FE_INPUT_TYPE] = 'number';
        }

        // Check for input type 'textarea'. Possible notation: a) '', b) '<width>', c) '<width>,<height>', d) '<width>,<min-height>,<max-height>'
        $colsRows = explode(',', $formElement[FE_SIZE], 3);

        // AutoHeight
        $colsRows[1] = HelperFormElement::textareaAutoHeight($colsRows, $value);
        $flagTextarea = ($colsRows[1] > 1);

        $formElement = HelperFormElement::prepareExtraButton($formElement, !$flagTextarea);


        if ($flagTextarea) {
            $htmlTag = '<textarea';

            $attribute .= Support::doAttribute('cols', $colsRows[0]);
            $attribute .= Support::doAttribute('rows', $colsRows[1]);
            $textarea = htmlentities($value) . '</textarea>';
        } else {
            $htmlTag = '<input';
            if (!empty($formElement[FE_INPUT_TYPE])) {
                $formElement[FE_TYPE] = $formElement[FE_INPUT_TYPE];
            }
            $attribute .= $this->getAttributeList($formElement, [FE_TYPE, 'size']);
            $attribute .= Support::doAttribute('value', htmlentities($value), false);
        }

        $attribute .= $this->getAttributeList($formElement, [FE_INPUT_AUTOCOMPLETE, 'autofocus', 'placeholder']);

        $formElement[FE_CHECK_PATTERN] = Sanitize::getInputCheckPattern($formElement[FE_CHECK_TYPE], $formElement[FE_CHECK_PATTERN]
            , $formElement[FE_DECIMAL_FORMAT], $sanitizeMessage);
        if ($formElement[FE_CHECK_PATTERN] !== '') {
            $attribute .= Support::doAttribute('pattern', $formElement[FE_CHECK_PATTERN], true, ESCAPE_WITH_BACKSLASH);

            // Use system specific message only,if no custom one is set.
            if ($formElement[F_FE_DATA_PATTERN_ERROR] == $formElement[F_FE_DATA_PATTERN_ERROR_SYSTEM]) {
                $formElement[F_FE_DATA_PATTERN_ERROR] = $sanitizeMessage;
            }
            $attribute .= Support::doAttribute(F_FE_DATA_PATTERN_ERROR, $formElement[F_FE_DATA_PATTERN_ERROR], true, ESCAPE_WITH_BACKSLASH);
        }

        if ($formElement[FE_MODE] == FE_MODE_REQUIRED) {
            $attribute .= Support::doAttribute(F_FE_DATA_REQUIRED_ERROR, $formElement[F_FE_DATA_REQUIRED_ERROR]);
        }

        if (empty($formElement[F_FE_DATA_MATCH_ERROR])) {
            $formElement[F_FE_DATA_REQUIRED_ERROR] = F_FE_DATA_REQUIRED_ERROR_DEFAULT;
        }

        $attribute .= $this->getAttributeList($formElement, [F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR, FE_MIN, FE_MAX, FE_STEP]);
        $attribute .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attribute .= Support::doAttribute('title', $formElement[FE_TOOLTIP]);


        $attribute .= $this->getAttributeFeMode($formElement[FE_MODE], false);

        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement);

        $input = "$htmlTag $attribute>$textarea";

        if ($formElement[FE_TMP_EXTRA_BUTTON_HTML] !== '') {

            if ($flagTextarea) {
                $input .= $formElement[FE_TMP_EXTRA_BUTTON_HTML];
            } else {
                $input = Support::wrapTag('<div class="input-group">', $input . $formElement[FE_TMP_EXTRA_BUTTON_HTML]);
            }
        }

        $input .= $this->getHelpBlock() . $elementCharacterCount;

        if (isset($formElement[FE_INPUT_EXTRA_BUTTON_INFO])) {
            $input .= $formElement[FE_INPUT_EXTRA_BUTTON_INFO];
        }

        return $input;
    }

    /**
     * Check $formElement for FE_TYPE_AHEAD_SQL or FE_TYPE_AHEAD_LDAP_SERVER.
     * If one of them is given: build $urlParam with typeAhead Params.
     * Additionally set some parameter for later outside use, especially FE_TYPEAHEAD_LIMIT, FE_TYPEAHEAD_MINLENGTH
     *
     * @param array $formElement
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function typeAheadBuildParam(array &$formElement) {

        $arr = [];

        $formElement[FE_TYPEAHEAD_LIMIT] = Support::setIfNotSet($formElement, FE_TYPEAHEAD_LIMIT, TYPEAHEAD_DEFAULT_LIMIT);
        $formElement[FE_TYPEAHEAD_MINLENGTH] = Support::setIfNotSet($formElement, FE_TYPEAHEAD_MINLENGTH, 2);

        if (isset($formElement[FE_TYPEAHEAD_SQL])) {
            $sql = $this->checkSqlAppendLimit($formElement[FE_TYPEAHEAD_SQL], $formElement[FE_TYPEAHEAD_LIMIT]);
            $formElement[FE_TYPEAHEAD_SQL_PREFETCH] = Support::setIfNotSet($formElement, FE_TYPEAHEAD_SQL_PREFETCH);

            $arr = [
                FE_TYPEAHEAD_SQL => $sql,
                FE_TYPEAHEAD_SQL_PREFETCH => $formElement[FE_TYPEAHEAD_SQL_PREFETCH]
            ];
        } elseif (isset($formElement[FE_TYPEAHEAD_LDAP])) {
            $formElement[FE_LDAP_SERVER] = Support::setIfNotSet($formElement, FE_LDAP_SERVER);
            $formElement[FE_LDAP_BASE_DN] = Support::setIfNotSet($formElement, FE_LDAP_BASE_DN);
            $formElement[FE_TYPEAHEAD_LDAP_SEARCH] = Support::setIfNotSet($formElement, FE_TYPEAHEAD_LDAP_SEARCH);
            $formElement[FE_TYPEAHEAD_LDAP_SEARCH_PREFETCH] = Support::setIfNotSet($formElement, FE_TYPEAHEAD_LDAP_SEARCH_PREFETCH);
            $formElement[FE_TYPEAHEAD_LDAP_VALUE_PRINTF] = Support::setIfNotSet($formElement, FE_TYPEAHEAD_LDAP_VALUE_PRINTF);
            $formElement[FE_TYPEAHEAD_LDAP_ID_PRINTF] = Support::setIfNotSet($formElement, FE_TYPEAHEAD_LDAP_ID_PRINTF);
            $formElement[FE_LDAP_USE_BIND_CREDENTIALS] = Support::setIfNotSet($formElement, FE_LDAP_USE_BIND_CREDENTIALS);

            foreach ([FE_LDAP_SERVER, FE_LDAP_BASE_DN, FE_TYPEAHEAD_LDAP_SEARCH] as $key) {
                if ($formElement[$key] == '') {
                    throw new \UserFormException('Missing definition: ' . $key, ERROR_MISSING_DEFINITON);
                }
            }

            if ($formElement[FE_TYPEAHEAD_LDAP_VALUE_PRINTF] . $formElement[FE_TYPEAHEAD_LDAP_ID_PRINTF] == '') {
                throw new \UserFormException('Missing definition: ' . FE_TYPEAHEAD_LDAP_VALUE_PRINTF . ' or ' . FE_TYPEAHEAD_LDAP_ID_PRINTF, ERROR_MISSING_DEFINITON);
            }

            $arr = [
                FE_LDAP_SERVER => $formElement[FE_LDAP_SERVER],
                FE_LDAP_BASE_DN => $formElement[FE_LDAP_BASE_DN],
                FE_TYPEAHEAD_LDAP_SEARCH => $formElement[FE_TYPEAHEAD_LDAP_SEARCH],
                FE_TYPEAHEAD_LDAP_SEARCH_PREFETCH => $formElement[FE_TYPEAHEAD_LDAP_SEARCH_PREFETCH],
                FE_TYPEAHEAD_LDAP_VALUE_PRINTF => $formElement[FE_TYPEAHEAD_LDAP_VALUE_PRINTF],
                FE_TYPEAHEAD_LDAP_ID_PRINTF => $formElement[FE_TYPEAHEAD_LDAP_ID_PRINTF],
                FE_TYPEAHEAD_LIMIT => $formElement[FE_TYPEAHEAD_LIMIT],
            ];

            if (isset($formElement[FE_TYPEAHEAD_LDAP_SEARCH_PER_TOKEN])) {
                $arr[FE_TYPEAHEAD_LDAP_SEARCH_PER_TOKEN] = '1';
            }

            if ($formElement[FE_LDAP_USE_BIND_CREDENTIALS] == '1') {
                $arr[SYSTEM_LDAP_1_RDN] = $this->store->getVar(SYSTEM_LDAP_1_RDN, STORE_SYSTEM);
                $arr[SYSTEM_LDAP_1_PASSWORD] = $this->store->getVar(SYSTEM_LDAP_1_PASSWORD, STORE_SYSTEM);
            }

        }

        return OnArray::toString($arr);
    }

    /**
     * Checks if $sql contains a SELECT statement.
     * Check for existence of a LIMIT Parameter. If not found add one.
     *
     * @param string $sql
     * @param int $limit
     *
     * @return string   Checked and maybe extended $sql statement.
     * @throws \UserFormException
     */
    private function checkSqlAppendLimit($sql, $limit) {
        $sqlTest = '';

        $sql = trim($sql);

        if ($sql[0] == '[') {
            // Remove optional existing dbIndex token.
            $pos = strpos($sql, ']');
            $sqlTest = substr($sql, $pos + 1);
        } else {
            $sqlTest = $sql;
        }

        if (false === stristr(substr($sqlTest, 0, 7), 'SELECT ')) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => '"Expect a SELECT statement', ERROR_MESSAGE_TO_DEVELOPER => "Expect a SELECT statement in " . FE_TYPEAHEAD_SQL . " - got: " . $sqlTest]),
                ERROR_BROKEN_PARAMETER);

        }

        if (false === stristr($sql, ' LIMIT ')) {
            $sql .= " LIMIT $limit";
        }

        return $sql;
    }

    /**
     * Builds a HTML attribute list, based on  $attributeList.
     *
     * E.g.: attributeList: [ 'type', 'autofocus' ]
     *       generates: 'type="$formElement[FE_TYPE]" autofocus="$formElement[FE_AUTOFOCUS]" '
     *
     * @param array $formElement
     * @param array $attributeList
     * @param bool $flagOmitEmpty
     *
     * @return string
     * @throws \CodeException
     */
    private function getAttributeList(array $formElement, array $attributeList, $flagOmitEmpty = true) {
        $attribute = '';
        foreach ($attributeList as $item) {
            if (isset($formElement[$item]))
                $attribute .= Support::doAttribute(strtolower($item), $formElement[$item], $flagOmitEmpty);
        }

        return $attribute;
    }

    /**
     * Set corresponding html attributes readonly/required/disabled, based on $formElement[FE_MODE].
     *
     * @param string $feMode
     *
     * @param bool $cssDisable
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function getAttributeFeMode($feMode, $cssDisable = true) {
        $attribute = '';

        $this->getFeMode($feMode, $hidden, $disabled, $required);

        switch ($feMode) {
            case FE_MODE_HIDDEN:
            case FE_MODE_SHOW:
            case FE_MODE_SHOW_REQUIRED:
                break;
            case FE_MODE_REQUIRED:
            case FE_MODE_READONLY:
                $attribute .= Support::doAttribute($feMode, $feMode);
                break;
            default:
                throw new \UserFormException("Unknown mode '$feMode'", ERROR_UNKNOWN_MODE);
                break;
        }

        // Attributes: data-...
        $attribute .= Support::doAttribute(DATA_HIDDEN, $hidden);
        if ($cssDisable) {
            $attribute .= Support::doAttribute(DATA_DISABLED, $disabled);
        }
        $attribute .= Support::doAttribute(DATA_REQUIRED, $required);

        return $attribute;
    }

    /**
     * Build HelpBlock
     *
     * @return string
     */
    private function getHelpBlock() {
        return '<div class="help-block with-errors hidden"></div>';
    }

    /**
     * Builds HTML 'checkbox' element.
     *
     * Checkboxes will only be submitted, if they are checked. Therefore, a hidden element with the unchecked value
     * will be transferred first.
     *
     * Format: <input type="hidden" name="$htmlFormElementName" value="$valueUnChecked">
     *         <input name="$htmlFormElementName" type="checkbox" [autofocus="autofocus"]
     *            [required="required"] [disabled="disabled"] value="<value>" [checked="checked"] >
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE*
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function buildCheckbox(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $itemKey = array();
        $itemValue = array();

        // Fill $itemKey & $itemValue
        $this->getKeyValueListFromSqlEnumSpec($formElement, $itemKey, $itemValue);

        // Get fallback, if 'checkBoxMode' is not defined:
        if (!isset($formElement['checkBoxMode'])) {
            // This fallback is problematic if 'set' or 'enum' has 2 : defaults to single but maybe multi is meant.
            $formElement['checkBoxMode'] = (count($itemKey) > 2) ? 'multi' : 'single';
        }

        if ($formElement['checkBoxMode'] === 'multi') {

        } else {
            // Fill meaningful defaults to parameter: checked|unchecked  (CHECKBOX_VALUE_CHECKED|CHECKBOX_VALUE_UNCHECKED)
            $this->prepareCheckboxCheckedUncheckedValue($itemKey, $formElement);
        }

        $attributeBase = $this->getAttributeFeMode($formElement[FE_MODE]);
        $attributeBase .= Support::doAttribute('type', $formElement[FE_TYPE]);
        $attributeBase .= $this->getAttributeList($formElement, [F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR, F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR]);

        switch ($formElement['checkBoxMode']) {
            case 'single':
                $html = $this->buildCheckboxSingle($formElement, $htmlFormElementName, $attributeBase, $value, $json);
                break;
            case 'multi';
                $html = $this->buildCheckboxMulti($formElement, $htmlFormElementName, $attributeBase, $value, $itemKey, $itemValue, $json);
                break;
            default:
                throw new \UserFormException('checkBoxMode: \'' . $formElement['checkBoxMode'] . '\' is unknown.', ERROR_CHECKBOXMODE_UNKNOWN);
        }

        $formElement = HelperFormElement::prepareExtraButton($formElement, false);

        return $html . $formElement[FE_TMP_EXTRA_BUTTON_HTML] . $this->getHelpBlock() . $formElement[FE_INPUT_EXTRA_BUTTON_INFO];
    }

    /**
     * Look for key/value list (in this order, first match counts) in
     *  a) `sql1`
     *  b) `parameter:itemList`
     *  c) table.column definition
     *
     * Copies the found keys to &$itemKey and the values to &$itemValue
     * If there are no &$itemKey, copy &$itemValue to &$itemKey.
     *
     * @param array $formElement
     * @param array $itemKey
     * @param array $itemValue
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function getKeyValueListFromSqlEnumSpec(array $formElement, array &$itemKey, array &$itemValue) {
        $fieldType = '';
        $itemKey = array();
        $itemValue = array();

        // Call getItemsForEnumOrSet() only if there a corresponding column really exist.
        if (false !== $this->store->getVar($formElement[FE_NAME], STORE_TABLE_COLUMN_TYPES)) {
            $itemValue = $this->getItemsForEnumOrSet($formElement[FE_NAME], $fieldType);
        }

        if (is_array($formElement[FE_SQL1])) {
            if (count($formElement[FE_SQL1]) > 0) {
                $keys = array_keys($formElement[FE_SQL1][0]);
                $itemKey = array_column($formElement[FE_SQL1], 'id');

                // If there is no column 'id' and at least two columns in total
                if (count($itemKey) === 0 && count($keys) >= 2) {
                    $itemKey = array_column($formElement[FE_SQL1], $keys[0]);
                }

                $itemValue = array_column($formElement[FE_SQL1], 'label');
                // If there is no column 'label' (e.g.: SHOW tables)
                if (count($itemValue) === 0) {
                    $idx = count($keys) == 1 ? 0 : 1;
                    $itemValue = array_column($formElement[FE_SQL1], $keys[$idx]);
                }
            }
        } elseif (isset($formElement['itemList']) && strlen($formElement['itemList']) > 0) {
            $arr = KeyValueStringParser::parse($formElement['itemList'], ':', ',', KVP_IF_VALUE_EMPTY_COPY_KEY);
            $itemValue = array_values($arr);
            $itemKey = array_keys($arr);
        } elseif ($fieldType === 'enum' || $fieldType === 'set') {
            // already done at the beginning with '$this->getItemsForEnumOrSet($formElement[FE_NAME], $fieldType);'
        } elseif (isset($formElement[FE_CHECKBOX_CHECKED]) && $formElement[FE_TYPE] == FE_TYPE_CHECKBOX) {
            // Nothing to do here.
        } else {
            throw new \UserFormException("Missing definition (- nothing found in 'sql1', 'parameter:itemList', 'enum-' or 'set-definition'", ERROR_MISSING_ITEM_LIST);
        }

        if (count($itemKey) === 0) {
            $itemKey = $itemValue;
        }

        // Process 'emptyHide'  before 'emptyItemAtStart' / 'emptyItemAtEnd': than 'emptyItem*' are still possible.
        if (isset($formElement['emptyHide'])) {
            $itemKey = OnArray::removeEmptyElementsFromArray($itemKey);
            $itemValue = OnArray::removeEmptyElementsFromArray($itemValue);
        }

        if (isset($formElement[FE_EMPTY_ITEM_AT_START])) {
            $placeholder = isset($formElement[FE_PLACEHOLDER]) ? $formElement[FE_PLACEHOLDER] : '';
            array_unshift($itemKey, '');
            array_unshift($itemValue, $placeholder);
        }

        if (isset($formElement[FE_EMPTY_ITEM_AT_END])) {
            $itemKey[] = '';
            $itemValue[] = '';
        }

    }

    /**
     * Get the attribute definition list of an enum or set column. For strings, get the default value. Return
     * elements as an array.
     *
     * @param string $column
     * @param string $fieldType
     *
     * @return array
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function getItemsForEnumOrSet($column, &$fieldType) {

        // Get column definition
        $fieldTypeDefinition = $this->store->getVar($column, STORE_TABLE_COLUMN_TYPES);

        if ($fieldTypeDefinition === false) {
            throw new \UserFormException("Column '$column' unknown in table '" . $this->formSpec[F_TABLE_NAME] . "'", ERROR_DB_UNKNOWN_COLUMN);
        }

        $length = strlen($fieldTypeDefinition);

        // enum('...   set('
        switch (substr($fieldTypeDefinition, 0, 4)) {
            case 'enum':
                $startPosition = 5;
                break;
            case 'set(':
                $startPosition = 4;
                break;
            default:
                $fieldType = 'string';

                return array();
        }

        // enum('a','b','c', ...)   >> [ 'a', 'b', 'c', ... ]
        // set('a','b','c', ...)   >> [ 'a', 'b', 'c', ... ]
        $values = substr($fieldTypeDefinition, $startPosition, $length - $startPosition - 1);
        $items = OnArray::trimArray(explode(',', $values), "'");
        $fieldType = substr($fieldTypeDefinition, 0, $startPosition - 1);

        return $items;
    }

    /**
     * For CheckBox's with only one checkbox: if no parameter:checked|unchecked is defined, take defaults:
     *
     *    checked: first Element in $itemKey
     *  unchecked: ''
     *
     * @param array $itemKey
     * @param array $formElement
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function prepareCheckboxCheckedUncheckedValue(array $itemKey, array &$formElement) {

        if (!isset($formElement[FE_CHECKBOX_CHECKED])) {
            if (isset($itemKey[0])) {
                // First element in $itemKey list
                $formElement[FE_CHECKBOX_CHECKED] = $itemKey[0];
            } else {
                // Take column default value
                $formElement[FE_CHECKBOX_CHECKED] = $this->store->getVar($formElement[FE_NAME], STORE_TABLE_DEFAULT);
            }
        }

        // unchecked
        if (!isset($formElement[FE_CHECKBOX_UNCHECKED])) {
            if (isset($itemKey[1])) {
                $formElement[FE_CHECKBOX_UNCHECKED] = ($itemKey[0] === $formElement[FE_CHECKBOX_CHECKED]) ? $itemKey[1] : $itemKey[0];
            } else {
                $formElement[FE_CHECKBOX_UNCHECKED] = '';
            }
        }

        if ($formElement[FE_CHECKBOX_CHECKED] === $formElement[FE_CHECKBOX_UNCHECKED]) {
            throw new \UserFormException('FormElement: type=checkbox - checked and unchecked can\'t be the same: ' . $formElement[FE_CHECKBOX_CHECKED], ERROR_CHECKBOX_EQUAL);
        }

    }

    /**
     * Build a Checkbox based on two values. Either in HTML plain layout or with Bootstrap Button class.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $attribute
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function buildCheckboxSingle(array $formElement, $htmlFormElementName, $attribute, $value, array &$json, $mode = FORM_LOAD) {

        if (isset($formElement[FE_BUTTON_CLASS])) {

            if ($formElement[FE_BUTTON_CLASS] == '') {
                $formElement[FE_BUTTON_CLASS] = 'btn-default';
            }

            if ($formElement[FE_MODE] == FE_MODE_READONLY) {
                $formElement[FE_BUTTON_CLASS] .= ' disabled';
            }

            return $this->constructCheckboxSingleButton($formElement, $htmlFormElementName, $attribute, $value, $json);
        } else {
            return $this->constructCheckboxSinglePlain($formElement, $htmlFormElementName, $attribute, $value, $json);
        }
    }

    /**
     * Build a Checkbox based on two values with Bootstrap Button class.
     *
     * <div class="btn-group" data-toggle="buttons">
     *    <input type="hidden" name="$htmlFormElementName" value="$valueUnChecked">
     *    <label class="btn btn-primary active">
     *       <input type="checkbox" autocomplete="off" name="$htmlFormElementName" value="$valueChecked"checked>
     *       Checkbox 1 (pre-checked)
     *    </label>
     * </div>
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $attribute
     * @param string $value
     * @param array $json
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function constructCheckboxSingleButton(array $formElement, $htmlFormElementName, $attribute, $value, array &$json) {
        $html = '';
        $valueJson = false;

        $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attribute .= Support::doAttribute('name', $htmlFormElementName);
        $attribute .= Support::doAttribute('value', $formElement[FE_CHECKBOX_CHECKED], false);
        $attribute .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attribute .= Support::doAttribute(FE_INPUT_AUTOCOMPLETE, 'off');
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        $classActive = '';
        if ($formElement[FE_CHECKBOX_CHECKED] === $value) {
            $attribute .= Support::doAttribute('checked', 'checked');
            $valueJson = true;
            $classActive = ' active';
        }

        $attribute .= $this->getAttributeList($formElement, ['autofocus']);
        $attribute .= $this->getAttributeList($formElement, [F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR, F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR]);

        $htmlHidden = $this->buildNativeHidden($htmlFormElementName, $formElement[FE_CHECKBOX_UNCHECKED]);
        $this->store->setVar($htmlFormElementName, $htmlHidden, STORE_ADDITIONAL_FORM_ELEMENTS, false);
        $html = '';

        $htmlElement = '<input ' . $attribute . '>';
        if (isset($formElement['label2'])) {
            $htmlElement .= $formElement['label2'];
        } else {
            $htmlElement .= $formElement['checked'];
        }

        $labelAttribute = Support::doAttribute('title', $formElement[FE_TOOLTIP]);
        $labelAttribute .= Support::doAttribute('class', 'btn ' . $formElement[FE_BUTTON_CLASS] . $classActive);
        $html .= Support::wrapTag("<label $labelAttribute>", $htmlElement, true);
        $html = Support::wrapTag('<div class="btn-group" data-toggle="buttons">', $html);

        $json = $this->getFormElementForJson($htmlFormElementName, $valueJson, $formElement);

        return $html;
    }

    /**
     * Build a single HTML plain checkbox based on two values.
     * Create a 'hidden' input field and a 'checkbox' input field - both with the same HTML 'name'.
     * HTML does not submit an unchecked checkbox. Then only the 'hidden' input field is submitted.
     *
     * Format: <input type="hidden" name="$htmlFormElementName" value="$valueUnChecked">
     *         <input name="$htmlFormElementName" type="radio" [autofocus="autofocus"]
     *            [required="required"] [disabled="disabled"] value="<value>" [checked="checked"] >
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $attribute
     * @param string $value
     * @param array $json
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function constructCheckboxSinglePlain(array $formElement, $htmlFormElementName, $attribute, $value, array &$json) {
        $html = '';
        $valueJson = false;

        $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attribute .= Support::doAttribute('name', $htmlFormElementName);
        $attribute .= Support::doAttribute('value', $formElement[FE_CHECKBOX_CHECKED], false);
        $attribute .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        if ($formElement[FE_CHECKBOX_CHECKED] === $value) {
            $attribute .= Support::doAttribute('checked', 'checked');
//            $valueJson = true;
            $valueJson = $value;
        }

        $attribute .= $this->getAttributeList($formElement, ['autofocus']);
        $attribute .= $this->getAttributeList($formElement, [F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR, F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR]);

        $htmlHidden = $this->buildNativeHidden($htmlFormElementName, $formElement[FE_CHECKBOX_UNCHECKED]);
        $this->store->setVar($htmlFormElementName, $htmlHidden, STORE_ADDITIONAL_FORM_ELEMENTS, false);
        $html = '';

        $html .= '<input ' . $attribute . '>';
        if (isset($formElement['label2'])) {
            $html .= $formElement['label2'];
        }

        $labelAttribute = Support::doAttribute('title', $formElement[FE_TOOLTIP]);
        $html = Support::wrapTag("<label $labelAttribute>", $html, true);
        $html = Support::wrapTag("<div class='checkbox'>", $html, true);

        $json = $this->getFormElementForJson($htmlFormElementName, $valueJson, $formElement);

        return $html;
    }

    /**
     * Build a Checkbox based on two values. Either in HTML plain layout or with Bootstrap Button class.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param $attributeBase
     * @param string $value
     * @param array $itemKey
     * @param array $itemValue
     * @param array $json
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function buildCheckboxMulti(array $formElement, $htmlFormElementName, $attributeBase, $value, array $itemKey, array $itemValue, array &$json) {

        if (isset($formElement[FE_BUTTON_CLASS])) {

            if ($formElement[FE_BUTTON_CLASS] == '') {
                $formElement[FE_BUTTON_CLASS] = 'btn-default';
            }

            if ($formElement[FE_MODE] == FE_MODE_READONLY) {
                $formElement[FE_BUTTON_CLASS] .= ' disabled';
            }

            return $this->constructCheckboxMultiButton($formElement, $htmlFormElementName, $attributeBase, $value, $itemKey, $itemValue, $json);
        } else {
            return $this->constructCheckboxMultiPlain($formElement, $htmlFormElementName, $attributeBase, $value, $itemKey, $itemValue, $json);
        }
    }

    /**
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $htmlHidden
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function fillStoreAdditionalFormElementsCheckboxHidden(array $formElement, $htmlFormElementName, $htmlHidden) {

        if (isset($formElement[NAME_TG_COPIES]) && $formElement[NAME_TG_COPIES] > 0) {
            for ($ii = 1; $ii <= $formElement[NAME_TG_COPIES]; $ii++) {
                $key = str_replace('%d', $ii, $htmlFormElementName);
                $value = str_replace('%d', $ii, $htmlHidden);
                $this->store->setVar($key, $value, STORE_ADDITIONAL_FORM_ELEMENTS, true);
            }
        } else {
            $this->store->setVar($htmlFormElementName, $htmlHidden, STORE_ADDITIONAL_FORM_ELEMENTS, false);
        }
    }

    /**
     * Build as many Checkboxes as items.
     *
     * Layout: The Bootstrap Layout needs very special setup, the checkboxes are wrapped differently with <div
     * class=checkbox> depending of if they aligned horizontal or vertical.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $attributeBase
     * @param string $value
     * @param array $itemKey
     * @param array $itemValue
     * @param array $json
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function constructCheckboxMultiButton(array $formElement, $htmlFormElementName, $attributeBase, $value, array $itemKey, array $itemValue, array &$json) {
        $json = array();

        // Defines which of the checkboxes will be checked.
        $values = explode(',', $value);

        $attributeBase .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attributeBase .= $this->getAttributeList($formElement, [F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR, F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR]);

        $key = HelperFormElement::prependFormElementNameCheckBoxMulti($htmlFormElementName, 'h');
        $htmlHidden = $this->buildNativeHidden($key, '');
        $this->fillStoreAdditionalFormElementsCheckboxHidden($formElement, $htmlFormElementName, $htmlHidden);

        $html = '';

        $attribute = $attributeBase;
        if (isset($formElement[FE_AUTOFOCUS])) {
            $attribute .= Support::doAttribute('autofocus', $formElement[FE_AUTOFOCUS]);
        }

        for ($ii = 0, $jj = 1; $ii < count($itemKey); $ii++, $jj++) {
            $jsonValue = false;
            $classActive = '';
            $htmlFormElementNameUniq = HelperFormElement::prependFormElementNameCheckBoxMulti($htmlFormElementName, $ii);
            $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID] . '-' . $ii);
            $attribute .= Support::doAttribute('name', $htmlFormElementNameUniq);
            $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE] . '-' . $ii);


            $attribute .= Support::doAttribute('value', $itemKey[$ii]);

            // Check if the given key is found in field.
            if (false !== array_search($itemKey[$ii], $values)) {
                $attribute .= Support::doAttribute('checked', 'checked');
                $jsonValue = true;
                $classActive = ' active';
            }

            // '&nbsp;' - This is necessary to correctly align an empty input.
            $value = ($itemValue[$ii] === '') ? '&nbsp;' : $itemValue[$ii];

            $htmlElement = '<input ' . $attribute . '>' . $value;

            $html .= Support::wrapTag("<label class='btn " . $formElement[FE_BUTTON_CLASS] . "$classActive'>",
                $htmlElement, true);

            $json[] = $this->getFormElementForJson($htmlFormElementNameUniq, $jsonValue, $formElement);

            // Init for the next checkbox
            $attribute = $attributeBase;
        }

        $html = Support::wrapTag('<div class="btn-group" data-toggle="buttons">', $html);

        return $html;
    }

    /**
     * Build as many Checkboxes as items.
     *
     * Layout: The Bootstrap Layout needs very special setup, the checkboxes are wrapped differently with <div
     * class=checkbox> depending of if they aligned horizontal or vertical.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $attributeBase
     * @param string $value
     * @param array $itemKey
     * @param array $itemValue
     * @param array $json
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function constructCheckboxMultiPlain(array $formElement, $htmlFormElementName, $attributeBase, $value, array $itemKey, array $itemValue, array &$json) {
        $json = array();

        // Defines which of the checkboxes will be checked.
        $values = explode(',', $value);

        $attributeBase .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attributeBase .= $this->getAttributeList($formElement, [F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR, F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR]);

        $key = HelperFormElement::prependFormElementNameCheckBoxMulti($htmlFormElementName, 'h');
        $htmlHidden = $this->buildNativeHidden($key, '');

        $this->fillStoreAdditionalFormElementsCheckboxHidden($formElement, $htmlFormElementName, $htmlHidden);

        $html = '';

        $orientation = ($formElement[FE_MAX_LENGTH] > 1) ? ALIGN_HORIZONTAL : ALIGN_VERTICAL;
        $checkboxClass = ($orientation === ALIGN_HORIZONTAL) ? 'checkbox-inline' : 'checkbox';
        $br = '';

        $flagFirst = true;
        for ($ii = 0, $jj = 1; $ii < count($itemKey); $ii++, $jj++) {
            $jsonValue = false;
            $attribute = $attributeBase;
            $htmlFormElementNameUniq = HelperFormElement::prependFormElementNameCheckBoxMulti($htmlFormElementName, $ii);
            $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID] . '-' . $ii);
            $attribute .= Support::doAttribute('name', $htmlFormElementNameUniq);
            $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE] . '-' . $ii);

            // Do this only the first round.
            if ($flagFirst) {
                $flagFirst = false;
                if (isset($formElement[FE_AUTOFOCUS]))
                    $attribute .= Support::doAttribute('autofocus', $formElement[FE_AUTOFOCUS]);
            }

            $attribute .= Support::doAttribute('value', $itemKey[$ii]);

            // Check if the given key is found in field.
            if (false !== array_search($itemKey[$ii], $values)) {
                $attribute .= Support::doAttribute('checked', 'checked');
                $jsonValue = true;
            }

            // '&nbsp;' - This is necessary to correctly align an empty input.
            $value = ($itemValue[$ii] === '') ? '&nbsp;' : $itemValue[$ii];

            $htmlElement = '<input ' . $attribute . '>' . $value;

            // With ALIGN_HORIZONTAL: the label causes some trouble: skip it
            if (($orientation === ALIGN_VERTICAL)) {
                $htmlElement = Support::wrapTag('<label>', $htmlElement);
            }

            $htmlElement = Support::wrapTag("<div class='$checkboxClass'>", $htmlElement, true);

            // control orientation
            if ($formElement[FE_MAX_LENGTH] > 1) {

                if ($jj == $formElement[FE_MAX_LENGTH]) {
                    $jj = 0;
                    $br = '<br>';
                } else {
                    $br = '';
                }
            }

            $html .= $htmlElement . $br;
            $json[] = $this->getFormElementForJson($htmlFormElementNameUniq, $jsonValue, $formElement);

        }

        return $html;
    }

    /**
     * Submit extra (hidden) values by SIP.
     *
     * Sometimes, it's usefull to precalculate values during formload and to submit them as hidden fields.
     * To avoid any manipulation on those fields, the values will be transferred by SIP.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function buildExtra(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {

        if ($mode === FORM_LOAD) {
            $this->store->setVar($formElement[FE_NAME], $value, STORE_SIP, true);
        }

        return;
    }

    /**
     * Build HTML 'radio' element.
     *
     * Format: <input type="hidden" name="$htmlFormElementName" value="$valueUnChecked">
     *         <input name="$htmlFormElementName" type="radio" [autofocus="autofocus"]
     *            [required="required"] [disabled="disabled"] value="<value>" [checked="checked"] >
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function buildRadio(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {

        if (isset($formElement[FE_BUTTON_CLASS])) {

            if ($formElement[FE_BUTTON_CLASS] == '') {
                $formElement[FE_BUTTON_CLASS] = 'btn-default';
            }

            $html = $this->constructRadioButton($formElement, $htmlFormElementName, $value, $json, $mode);
        } else {
            $html = $this->constructRadioPlain($formElement, $htmlFormElementName, $value, $json, $mode);
        }

        $formElement = HelperFormElement::prepareExtraButton($formElement, false);

        return $html . $formElement[FE_TMP_EXTRA_BUTTON_HTML] . $this->getHelpBlock() . $formElement[FE_INPUT_EXTRA_BUTTON_INFO];

    }

    /**
     * Build Bootstrap Button 'radio' element.
     *
     * <div class="btn-group" data-toggle="buttons">
     *
     *   <label class="btn btn-primary active">
     *     <input type="radio" name="options" id="option1" autocomplete="off" checked> Radio 1 (preselected)
     *   </label>
     *
     *   <label class="btn btn-primary">
     *     <input type="radio" name="options" id="option2" autocomplete="off"> Radio 2
     *   </label>
     *
     *   <label class="btn btn-primary">
     *     <input type="radio" name="options" id="option3" autocomplete="off"> Radio 3
     *   </label>
     *
     * </div>
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function constructRadioButton(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $itemKey = array();
        $itemValue = array();

        // Fill $itemKey & $itemValue
        $this->getKeyValueListFromSqlEnumSpec($formElement, $itemKey, $itemValue);

        $attributeBase = $this->getAttributeFeMode($formElement[FE_MODE]);
        $attributeBase .= $this->getAttributeList($formElement, [F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR, F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR]);
//        $attributeBase .= Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attributeBase .= Support::doAttribute('name', $htmlFormElementName);
        $attributeBase .= Support::doAttribute('type', $formElement[FE_TYPE]);
        $attributeBase .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attributeBase .= Support::doAttribute(FE_INPUT_AUTOCOMPLETE, 'off');

        $attribute = $attributeBase;
        if (isset($formElement[FE_AUTOFOCUS])) {
            $attribute .= Support::doAttribute('autofocus', $formElement[FE_AUTOFOCUS]);
        }

        $htmlHidden = $this->buildNativeHidden($htmlFormElementName, $value);
        $this->store->setVar($htmlFormElementName, $htmlHidden, STORE_ADDITIONAL_FORM_ELEMENTS, false);

        $html = '';
        for ($ii = 0; $ii < count($itemValue); $ii++) {
            $classActive = '';
            $classReadonly = '';

            $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID] . '-' . $ii);
            $attribute .= Support::doAttribute('value', $itemKey[$ii], false); // Always set value, even to '' - #3832
            $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE] . '-' . $ii);

            if ($itemKey[$ii] == $value) {
                $attribute .= Support::doAttribute('checked', 'checked');
                $classActive = ' active';
            }

            if ($formElement[FE_MODE] == FE_MODE_READONLY) {
                $classReadonly = ' noclick';
                if ($itemKey[$ii] != $value) {
                    $classReadonly .= ' disabled';
                }
            }

            if ($itemValue[$ii] == '') { // In case the value is empty, the rendered button looks bad. Set '&nbsp;'.
                $itemValue[$ii] = '&nbsp;';
            }

            $htmlElement = '<input ' . $attribute . '>' . $itemValue[$ii];

            $labelAttribute = Support::doAttribute('title', $formElement[FE_TOOLTIP]);
            $labelAttribute .= Support::doAttribute('class', 'btn ' . $formElement[FE_BUTTON_CLASS] . $classReadonly . $classActive);
            $htmlElement = Support::wrapTag("<label $labelAttribute>", $htmlElement);

            $html .= $htmlElement;

            // Init for the next round
            $attribute = $attributeBase;
        }

        $html = Support::wrapTag('<div class="btn-group" data-toggle="buttons">', $html);

        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement);

        return $html;
    }

    /**
     * Build plain HTML 'radio' element.
     *
     * Format: <input type="hidden" name="$htmlFormElementName" value="$valueUnChecked">
     *         <input name="$htmlFormElementName" type="radio" [autofocus="autofocus"]
     *            [required="required"] [disabled="disabled"] value="<value>" [checked="checked"] >
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function constructRadioPlain(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $attributeBase = '';

        if (isset($formElement[FE_BUTTON_CLASS])) {
            return $this->constructRadioButton($formElement, $htmlFormElementName, $value, $json, $mode);
        }


        $itemKey = array();
        $itemValue = array();

        // Fill $itemKey & $itemValue
        $this->getKeyValueListFromSqlEnumSpec($formElement, $itemKey, $itemValue);

        $attributeBase .= $this->getAttributeFeMode($formElement[FE_MODE]);
        $attributeBase .= $this->getAttributeList($formElement, [F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR, F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR]);
        $attributeBase .= Support::doAttribute('name', $htmlFormElementName);
        $attributeBase .= Support::doAttribute('type', $formElement[FE_TYPE]);
        $attributeBase .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');

        $jj = 0;

        $orientation = ($formElement[FE_MAX_LENGTH] > 1) ? ALIGN_HORIZONTAL : ALIGN_VERTICAL;
        $radioClass = ($orientation === ALIGN_HORIZONTAL) ? 'radio-inline' : 'radio';
        $radioOuterTag = ($orientation === ALIGN_HORIZONTAL) ? 'label' : 'div';
        $br = '';

        $attribute = $attributeBase;
        if (isset($formElement[FE_AUTOFOCUS])) {
            $attribute .= Support::doAttribute('autofocus', $formElement[FE_AUTOFOCUS]);
        }


        $html = $this->buildNativeHidden($htmlFormElementName, $value);

        for ($ii = 0; $ii < count($itemValue); $ii++) {
            $jj++;

            $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID] . '-' . $ii);
            $attribute .= Support::doAttribute('value', $itemKey[$ii], false); // Always set value, even to '' - #3832
            $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE] . '-' . $ii);

            if ($itemKey[$ii] == $value) {
                $attribute .= Support::doAttribute('checked', 'checked');
            }

            // '&nbsp;' - This is necessary to correctly align an empty input.
            $tmpValue = ($itemValue[$ii] === '') ? '&nbsp;' : $itemValue[$ii];

            $htmlElement = '<input ' . $attribute . '>' . $tmpValue;

            // With ALIGN_HORIZONTAL: the label causes some trouble: skip it
            if (($orientation === ALIGN_VERTICAL)) {
                $htmlElement = Support::wrapTag('<label>', $htmlElement);
            }

            if ($formElement[FE_MAX_LENGTH] > 1) {

                if ($jj == $formElement[FE_MAX_LENGTH]) {
                    $jj = 0;
                    $br = '<br>';
                } else {
                    $br = '';
                }
            }

            $wrapAttribute = Support::doAttribute('title', $formElement[FE_TOOLTIP]);
            $wrapAttribute .= Support::doAttribute('class', $radioClass);
            $htmlElement = Support::wrapTag("<$radioOuterTag $wrapAttribute>", $htmlElement) . $br;

            $html .= $htmlElement;

            $attribute = $attributeBase;
        }

        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement);

        return $html;
    }

    /**
     * Builds a Selct (Dropdown) Box.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return mixed
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function buildSelect(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $itemKey = array();
        $itemValue = array();
        $attribute = '';

        if (!empty($formElement[FE_PLACEHOLDER])) {
            $formElement[FE_EMPTY_ITEM_AT_START] = '';
            if (isset($formElement[FE_EMPTY_ITEM_AT_END])) {
                unset($formElement[FE_EMPTY_ITEM_AT_END]);
            }
        }

        // Fill $itemKey & $itemValue
        $this->getKeyValueListFromSqlEnumSpec($formElement, $itemKey, $itemValue);

        $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attribute .= Support::doAttribute('name', $htmlFormElementName);
        $attribute .= Support::doAttribute('class', 'form-control');
        $attribute .= Support::doAttribute('title', $formElement[FE_TOOLTIP]);
        $attribute .= $this->getAttributeList($formElement, ['autofocus', F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR]);
        $attribute .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        if (isset($formElement[FE_SIZE]) && $formElement[FE_SIZE] > 1) {
            $attribute .= Support::doAttribute('size', $formElement[FE_SIZE]);
            $attribute .= Support::doAttribute('multiple', 'multiple');
        }

        $option = '';
        $firstSelect = true;
        $jsonValues = array();
        $total = count($itemValue);
        for ($ii = 0; $ii < $total; $ii++) {
            $option .= '<option ';

            $option .= Support::doAttribute('value', $itemKey[$ii], false);

            if ($ii == 0 && $formElement[FE_PLACEHOLDER] != '') {
                $option .= 'disabled hidden ';
            }

            $jsonValues[] = [
                'value' => $itemKey[$ii],
                'text' => $itemValue[$ii],
                'selected' => ($itemKey[$ii] == $value && $firstSelect),
            ];

            if ($itemKey[$ii] == $value && $firstSelect) {
                $option .= 'selected';
                $firstSelect = false;
            }

            $option .= '>' . $itemValue[$ii] . '</option>';
        }

        $json = $this->getFormElementForJson($htmlFormElementName, $jsonValues, $formElement);

        $formElement = HelperFormElement::prepareExtraButton($formElement, false);
        $attribute .= $this->getAttributeFeMode($formElement[FE_MODE]);
        if (isset($formElement["datalist"])) {
            if ($formElement[FE_DYNAMIC_UPDATE] === 'yes') {
                throw new \UserFormException("Datalist funktionert nicht mit dynamic update", ERROR_NOT_IMPLEMENTED);
            }
            $datalistId = $formElement[FE_HTML_ID] . '-datalist';
            $html = '<input ' . Support::doAttribute('list', $datalistId) . $attribute . '><datalist '
                . Support::doAttribute('id', $datalistId) . '>' . $option . '</datalist>';
        } else {
            $html = '<select ' . $attribute . '>' . $option . '</select>';
        }
        $html = $html . $this->getHelpBlock() . $formElement[FE_TMP_EXTRA_BUTTON_HTML];
        return $html . $formElement[FE_INPUT_EXTRA_BUTTON_INFO];
    }

    /**
     * @param string $linkNew Complete Button, incl. SIP href
     * @param $flagDelete
     * @param $deleteTitle
     * @param array $firstRow First row of all subrecords to extract columntitles
     * @param array $control Array with <th> column names / format.
     *
     * @return string
     * @throws \UserFormException
     */
    private function subrecordHead($linkNew, $flagDelete, $deleteTitle, array $firstRow, array &$control) {

        $columns = $linkNew;

        if (!empty($firstRow)) {
            // construct column attributes
            $control = $this->getSubrecordColumnControl(array_keys($firstRow));

            // Subrecord: Column titles
            $columns .= '<th>' . implode('</th><th>', $control[SUBRECORD_COLUMN_TITLE]) . '</th>';
        }

        if ($flagDelete) {
            $columns .= "<th data-sorter='false' class='filter-false' data-priority='always'>$deleteTitle</th>";
        }

        return Support::wrapTag('<thead><tr>', $columns);
    }

    /**
     * Construct a HTML table of the subrecord data.
     * Column syntax
     * definition:https://docs.typo3.org/p/IMATHUZH/qfq/master/en-us/Manual.html#type-subrecord
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function buildSubrecord(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $rcText = false;
        $nameColumnId = 'id';
//        $targetTableName = '';
        $flagNew = false;
        $flagEdit = false;
        $flagDelete = false;
        $linkNew = '';
        $control = array();

        $primaryRecord = $this->store->getStore(STORE_RECORD);
        $cssClassColumnId = $this->store->getVar(SYSTEM_CSS_CLASS_COLUMN_ID, STORE_SYSTEM . STORE_EMPTY);

        if (!$this->prepareSubrecord($formElement, $primaryRecord, $rcText, $nameColumnId)) {
            return $rcText;
        }

        if (isset($formElement[SUBRECORD_PARAMETER_FORM])) {

            Support::setIfNotSet($formElement, F_EXTRA_DELETE_FORM, '');
            $formElement[F_FINAL_DELETE_FORM] = $formElement[F_EXTRA_DELETE_FORM] != '' ? $formElement[F_EXTRA_DELETE_FORM] : $formElement[SUBRECORD_PARAMETER_FORM];

            // Decode settings in subrecordOption
            $flagNew = Support::findInSet(SUBRECORD_NEW, $formElement[FE_SUBRECORD_OPTION]) && ($formElement[FE_MODE] != FE_MODE_READONLY);

            $flagEdit = Support::findInSet(SUBRECORD_EDIT, $formElement[FE_SUBRECORD_OPTION]);
            $flagDelete = Support::findInSet(SUBRECORD_DELETE, $formElement[FE_SUBRECORD_OPTION]) && ($formElement[FE_MODE] != FE_MODE_READONLY);

            $editColumnTitle = $flagNew ? $this->createFormLink($formElement, 0, $primaryRecord, $this->symbol[SYMBOL_NEW], 'New') : '';
            $editColumnTitle .= ' ' . ($formElement[SUBRECORD_COLUMN_TITLE_EDIT] ?? '');

            if ($flagNew || $flagEdit) {
                $linkNew = Support::wrapTag('<th data-sorter="false" class="filter-false" data-priority="always">', $editColumnTitle);
            }
        }

        // Determine if DragAndDrop is active
        $hasDragAndDrop = false;
        $orderColumn = $formElement[FE_ORDER_COLUMN] ?? DND_COLUMN_ORD;
        $dndTable = '';

        if (isset($formElement[FE_DND_TABLE])) {
            // Table is specified in parameter field
            $dndTable = $formElement[FE_DND_TABLE];
        } elseif (!empty($formElement[SUBRECORD_PARAMETER_FORM])) {
            // Read table from form specified in subrecord
            $formName = $formElement[SUBRECORD_PARAMETER_FORM];
            $form = $this->dbArray[$this->dbIndexQfq]->sql("SELECT * FROM Form AS f WHERE f." . F_NAME . " LIKE ? AND f.deleted='no'",
                ROW_REGULAR, [$formName]);
            if (count($form) > 0) {
                $dndTable = $form[0][F_TABLE_NAME];
            }
        } else {
            throw new \UserFormException(json_encode(
                [ERROR_MESSAGE_TO_USER => 'Missing Parameter',
                    ERROR_MESSAGE_TO_DEVELOPER => FE_DND_TABLE . ' or ' . SUBRECORD_PARAMETER_FORM]),
                ERROR_MISSING_TABLE_NAME);
        }

        if ($dndTable) {
            $columns = $this->dbArray[$this->dbIndexData]->sql("SHOW COLUMNS FROM $dndTable");
            foreach ($columns as $column) {
                if ($column['Field'] === $orderColumn) {
                    // DragAndDrop is active if the dndTable has the orderColumn
                    $hasDragAndDrop = true;
                    break;
                }
            }
        }

        $firstRow = isset($formElement[FE_SQL1][0]) ? $formElement[FE_SQL1][0] : array();
        $htmlHead = $this->subrecordHead($linkNew, $flagDelete, ($formElement[SUBRECORD_COLUMN_TITLE_DELETE] ?? ''), $firstRow, $control);
        $htmlBody = '';
        foreach ($formElement[FE_SQL1] as $row) {
            $rowHtml = '';

            if ($flagEdit) {
                $toolTip = ($formElement[FE_MODE] == FE_MODE_READONLY) ? 'Show' : 'Edit';
                $symbol = ($formElement[FE_MODE] == FE_MODE_READONLY) ? $this->symbol[SYMBOL_SHOW] : $this->symbol[SYMBOL_EDIT];
                $rowHtml .= Support::wrapTag('<td>', $this->createFormLink($formElement, $row[$nameColumnId], $primaryRecord, $symbol, $toolTip, $row));
            } elseif ($flagNew) {
                $rowHtml .= Support::wrapTag('<td>', $rowHtml, false);
            }

            // All columns
            foreach ($row as $columnName => $value) {
                if (isset($control[SUBRECORD_COLUMN_TITLE][$columnName])) {
                    $rowHtml .= Support::wrapTag("<td>", $this->renderCell($control, $columnName, $value, $cssClassColumnId));
                }
            }

            if ($flagDelete) {
                $toolTip = 'Delete';

                if ($this->showDebugInfoFlag) {
                    $toolTip .= PHP_EOL . "form = '" . $formElement[F_FINAL_DELETE_FORM] . "'" . PHP_EOL . "r = '" . $row[$nameColumnId] . "'";
                }

                $s = $this->createDeleteUrl($formElement[F_FINAL_DELETE_FORM], $row[$nameColumnId], RETURN_SIP);
                $rowHtml .= Support::wrapTag('<td>', Support::wrapTag("<button type='button' class='record-delete btn btn-default' data-sip='$s' " .
                    Support::doAttribute('title', $toolTip) . ">", '<span class="glyphicon ' . GLYPH_ICON_DELETE . '"></span>'));
            }


            Support::setIfNotSet($row, SUBRECORD_COLUMN_ROW_CLASS);
            $rowClass = 'record ';
            $rowClass .= $row[SUBRECORD_COLUMN_ROW_CLASS];

            $rowTooltip = '';
            if (isset($row[SUBRECORD_COLUMN_ROW_TOOLTIP])) {
                $rowTooltip = $row[SUBRECORD_COLUMN_ROW_TOOLTIP];
            } elseif (isset($row[SUBRECORD_COLUMN_ROW_TITLE])) { // backward compatibility
                $rowTooltip = $row[SUBRECORD_COLUMN_ROW_TITLE];
            }
            $rowAttribute = Support::doAttribute('class', $rowClass);
            $rowAttribute .= Support::doAttribute('title', $rowTooltip);
            if ($hasDragAndDrop) {
                $rowAttribute .= Support::doAttribute('id', $formElement[FE_HTML_ID] . '-' . $row[$nameColumnId]);
                $rowAttribute .= Support::doAttribute('data-dnd-id', $row[$nameColumnId]);
            }
            $htmlBody .= Support::wrapTag("<tr $rowAttribute>", $rowHtml, true);
        }

        // Handle DragAndDrop
        $dndAttributes = '';
        if ($hasDragAndDrop) {
            $orderInterval = 10;
            $numColumns = 0;
            if (count($formElement[FE_SQL1]) > 0) {
                $numColumns = count($formElement[FE_SQL1][0]) + (int)$flagDelete + (int)($flagNew || $flagEdit);
            }

            $dataDndApi = DND_SUBRECORD_ID . '=' . $formElement[FE_ID];
            $dataDndApi .= '&' . DND_SUBRECORD_FORM_ID . '=' . $this->store->getVar('id', STORE_RECORD);
            $dataDndApi .= '&' . FE_ORDER_INTERVAL . '=' . $orderInterval;
            $dataDndApi .= '&' . FE_ORDER_COLUMN . '=' . $orderColumn;
            $dataDndApi .= '&' . FE_DND_TABLE . '=' . $dndTable;

            $dndAttributes = Support::doAttribute('class', 'qfq-dnd-sort');
            $dndAttributes .= $this->evaluate->parse("{{ '$dataDndApi' AS _data-dnd-api }}") . ' ';
            $dndAttributes .= Support::doAttribute('data-columns', $numColumns);
        }

        $tableSorterHtmlId = $this->store::getVar(TYPO3_PAGE_ALIAS, STORE_TYPO3) . '-' . $formElement[FE_ID];
        $attribute = Support::doAttribute('class', $formElement[FE_SUBRECORD_TABLE_CLASS]);
        $attribute .= Support::doAttribute('id', $tableSorterHtmlId);


        return Support::wrapTag("<table $attribute>",
            $htmlHead . Support::wrapTag("<tbody $dndAttributes>", $htmlBody), true);
    }

    /**
     * Prepare Subrecord:
     * - check if the current record has an recordId>0. If not, subrecord can't be edited. Return a message.
     * - check if there is an SELECT statement for the subrecords.
     * - determine &$nameColumnId
     *
     * @param array $formElement
     * @param array $primaryRecord
     * @param string $rcText
     * @param string $nameColumnId
     *
     * @return bool
     * @throws \UserFormException
     */
    private function prepareSubrecord(array $formElement, array $primaryRecord, &$rcText, &$nameColumnId) {

        if (!isset($primaryRecord['id'])) {
            $rcText = 'Please save this record first.';

            return false;
        }

        if (!is_array($formElement[FE_SQL1])) {
            throw new \UserFormException('Missing \'sql1\' Query', ERROR_MISSING_SQL1);
        }

        // No records?
        if (count($formElement[FE_SQL1]) == 0) {
            $rcText = '';

            return true;
        }

        // Check if $nameColumnId column exist.
        if (!isset($formElement[FE_SQL1][0][$nameColumnId])) {
            // no: try fallback.
            $nameColumnId = '_id';

            if (!isset($formElement[FE_SQL1][0][$nameColumnId])) {
                throw new \UserFormException('Missing column \'id\' or \'_id\' in subrecord query', ERROR_SUBRECORD_MISSING_COLUMN_ID);
            }
        }

        if (!isset($formElement[FE_SQL1][0][$nameColumnId])) {
            throw new \UserFormException('Missing column \'id\' (or "_id") in  \'sql1\' Query', ERROR_DB_MISSING_COLUMN_ID);
        }

        return true;
    }

    /**
     * Renders an Link with a symbol (edit/new) and register a new SIP to grant permission to the link..
     *
     * Returns <a href="<Link>">[icon]</a>
     *
     * Link: <page>?s=<SIP>&<standard typo3 params>
     * SIP: form = $formElement['form'] (provided via formElement[FE_PARAMETER])
     *      r = $targetRecordId
     *      Parse  $formElement['detail'] with possible key/value pairs. E.g.: detail=id:gr_id,#{{a}}:p_id,#12:x_id
     *        gr_id = <<primarytable.id>>
     *        p_id = <<variable defined in SIP or Client>>
     *        x_id = 12 (constant)
     *
     *
     * @param array $formElement
     * @param string $targetRecordId
     * @param array $record
     *
     * @param $symbol
     * @param $toolTip
     * @param array $currentRow
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function createFormLink(array $formElement, $targetRecordId, array $record, $symbol, $toolTip, $currentRow = array()) {

        //TODO: Umstellen auf Benutzung der Link Klasse.

        $queryStringArray = [
            SIP_FORM => $formElement[SUBRECORD_PARAMETER_FORM],
            SIP_RECORD_ID => $targetRecordId,
            PARAM_DB_INDEX_DATA => $this->formSpec[F_DB_INDEX],
        ];

        // Inherit current F_MODE
        if ($this->formSpec[F_MODE] != '') {
            $queryStringArray[F_MODE_GLOBAL] = $this->formSpec[F_MODE];
        }

        // In case the subrecord FE is set to 'readonly': subforms will be called with formModeGlobal=readonly
        if ($formElement[FE_MODE] == FE_MODE_READONLY) {
            $queryStringArray[F_MODE_GLOBAL] = FE_MODE_READONLY;
        }
        // Add custom query parameter
        if (isset($formElement[SUBRECORD_PARAMETER_DETAIL])) {

            $detailParam = KeyValueStringParser::parse($formElement[SUBRECORD_PARAMETER_DETAIL]);

            foreach ($detailParam as $src => $dest) {
                // Constants
                if ($src[0] == '&') {
                    $queryStringArray[$dest] = substr($src, 1);
                    continue;
                }
                // Form record values or parameter
                if (isset($record[$src])) {
                    $queryStringArray[$dest] = $record[$src];
                    continue;
                }

                // Current row - check '$src' and  '_$src' )
                foreach (['', '_'] as $pre) {
                    if (isset($currentRow[$pre . $src])) {
                        $queryStringArray[$dest] = $currentRow[$pre . $src];
                        continue 2;
                    }
                }

                $queryStringArray[$dest] = ERROR_SUBRECORD_DETAIL_COLUMN_NOT_FOUND;
            }
        }

        if ($this->showDebugInfoFlag)
            $toolTip .= PHP_EOL . OnArray::toString($queryStringArray, ' = ', PHP_EOL, "'");

        Support::appendTypo3ParameterToArray($queryStringArray);
        // If there is a specific targetpage defined, take it.
        if (isset($formElement[SUBRECORD_PARAMETER_PAGE]) && $formElement[SUBRECORD_PARAMETER_PAGE] !== '') {
            $queryStringArray['id'] = $formElement[SUBRECORD_PARAMETER_PAGE];
        }

        //-----------------
        $queryString = Support::arrayToQueryString($queryStringArray);

        $sip = $this->store->getSipInstance();
        $url = $sip->queryStringToSip($queryString);

        return Support::wrapTag('<a class="btn btn-default" ' . Support::doAttribute('href', $url) . ' title="' . $toolTip . '">', $symbol);
    }

    /**
     * Get the name for the given form $formName. If not found, return ''.
     *
     * @param string $formName
     *
     * @return string   tableName for $formName
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    private function getFormTable($formName) {
        $row = $this->dbArray[$this->dbIndexQfq]->sql("SELECT " . F_TABLE_NAME . " FROM Form AS f WHERE f.name = ?", ROW_EXPECT_0_1, [$formName]);
        if (isset($row[F_TABLE_NAME])) {
            return $row[F_TABLE_NAME];
        }

        return '';
    }

    /**
     * Get various column format information based on the 'raw' column title. The attributes are separated by '|'
     * and specified as 'key' or 'key=value'.
     *
     * - Return all parsed values as an assoc array.
     * - For regular columns: If there is no 'width' specified, take the default 'SUBRECORD_COLUMN_WIDTH'
     * - For 'icon /  url / mailto': no width limit.
     *
     * Returned assoc array:
     *  title      Only key. Element is non numeric, which is not a keyword 'width/nostrip/icon/url/mailto'
     *  maxLength  Key/Value Pair. Not provided for 'icon/url/mailto'.
     *  nostrip    Only key. Do not strip HTML Tags from the content.
     *  icon       Only key. Value will rendered (later) as an image.
     *  url        Only key. Value will rendered (later) as a 'href'
     *  mailto     Only key. Value will rendered (later) as a 'href mailto'
     *
     *
     * @param array $titleRaw
     *
     * @return array
     * @throws \UserFormException
     */
    private function getSubrecordColumnControl(array $titleRaw) {
        $control = array();

        foreach ($titleRaw AS $columnName) {

            switch ($columnName) {
                case SUBRECORD_COLUMN_ROW_CLASS:
                case SUBRECORD_COLUMN_ROW_TOOLTIP:
                case SUBRECORD_COLUMN_ROW_TITLE: // Backward compatibility
                    continue 2;
                default:
                    break;
            }

            $flagWidthLimit = true;
            $control[SUBRECORD_COLUMN_MAX_LENGTH][$columnName] = SUBRECORD_COLUMN_DEFAULT_MAX_LENGTH;

            // a) 'City@maxLength=40', b) 'Status@icon', c) 'Mailto@maxLength=80@nostrip'
            $arr = KeyValueStringParser::parse($columnName, '=', '|', KVP_IF_VALUE_EMPTY_COPY_KEY);
            foreach ($arr as $attribute => $value) {
                switch ($attribute) {
                    case SUBRECORD_COLUMN_MAX_LENGTH:
                    case SUBRECORD_COLUMN_NO_STRIP:
                    case SUBRECORD_COLUMN_TITLE:
                    case SUBRECORD_COLUMN_LINK:
                        break;
                    case SUBRECORD_COLUMN_ICON:
                    case SUBRECORD_COLUMN_URL:
                    case SUBRECORD_COLUMN_MAILTO:
                        $flagWidthLimit = false;
                        break;
                    default:
                        $attribute = is_numeric($value) ? SUBRECORD_COLUMN_MAX_LENGTH : SUBRECORD_COLUMN_TITLE;
                        break;
                }
                $control[$attribute][$columnName] = $value;
            }

            if (!isset($control[SUBRECORD_COLUMN_TITLE][$columnName])) {
                $control[SUBRECORD_COLUMN_TITLE][$columnName] = ''; // Fallback:  Might be wrong, but better than nothing.
            }

            // Don't render Columns starting with '_...'.
            if (substr($control[SUBRECORD_COLUMN_TITLE][$columnName], 0, 1) === '_') {
                unset($control[SUBRECORD_COLUMN_TITLE][$columnName]); // Do not render column later.
                continue;
            }

            // Limit title length
            $control[SUBRECORD_COLUMN_TITLE][$columnName] = mb_substr($control[SUBRECORD_COLUMN_TITLE][$columnName], 0, $control[SUBRECORD_COLUMN_MAX_LENGTH][$columnName]);

            if (!$flagWidthLimit) {
                $control[SUBRECORD_COLUMN_MAX_LENGTH][$columnName] = false;
            }
        }

        return $control;
    }

    /**
     * Renders $value as specified in array $control
     *
     * nostrip: by default, HTML tags are removed. With this attribute, the value will be delivered as it is.
     * width: if there is a size limit - apply it.
     * icon: The cell will be rendered as an image. $value should contain the name of an image in
     * 'fileadmin/icons/'
     * mailto: The cell will be rendered as an <a> tag with the 'mailto' attribute.
     * url:  The cell will be rendered as an <a> tag. The value will be exploded by '|'. $value[0] = href, value[1]
     * = text. E.g. $value = 'www.math.uzh.ch/?id=45&v=234|Show details for Modul 123' >> <a
     * href="www.math.uzh.ch/?id=45&v=234">Show details for Modul 123</a>
     *
     * @param array $control
     * @param string $columnName
     * @param string $columnValue
     * @param string $cssClassColumnId
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function renderCell(array $control, $columnName, $columnValue, $cssClassColumnId) {

        switch ($columnName) {
            case SUBRECORD_COLUMN_ROW_CLASS:
            case SUBRECORD_COLUMN_ROW_TITLE:
                return '';
            default:
                break;
        }

        $arr = explode('|', $columnValue);
        if (count($arr) == 1) {
            $arr[1] = $arr[0];
        }

        if (isset($control[SUBRECORD_COLUMN_NO_STRIP][$columnName])) {
            $cell = $columnValue;
            $control[SUBRECORD_COLUMN_MAX_LENGTH][$columnName] = false;
        } else {
            $cell = strip_tags($columnValue);
        }

        if ($control[SUBRECORD_COLUMN_MAX_LENGTH][$columnName] !== false && $control[SUBRECORD_COLUMN_MAX_LENGTH][$columnName] != 0) {
            $cell = mb_substr($cell, 0, $control[SUBRECORD_COLUMN_MAX_LENGTH][$columnName]);
        }

        if (isset($control[SUBRECORD_COLUMN_ICON][$columnName])) {
            $cell = ($cell === '') ? '' : "<image src='" . PATH_ICONS . "/$cell'>";
        }

        if (isset($control[SUBRECORD_COLUMN_MAILTO][$columnName])) {
            $cell = "<a " . Support::doAttribute('href', "mailto:$arr[0]") . ">$arr[1]</a>";
        }

        if (isset($control[SUBRECORD_COLUMN_URL][$columnName])) {
            $cell = "<a " . Support::doAttribute('href', $arr[0]) . ">$arr[1]</a>";
        }

        if (isset($control[SUBRECORD_COLUMN_LINK][$columnName])) {
            $cell = $this->link->renderLink($columnValue);
        }

        if (strcasecmp($columnName, 'id') == 0) {
            $cell = Support::wrapTag('<span class="' . $cssClassColumnId . '">', $cell, true);
        }
        return $cell;
    }

    /**
     * Create a link (incl. SIP) to delete the current record.
     *
     * @param string $formName if there is a form, specify that
     * @param int $recordId record to delete
     * @param string $mode
     *                          * mode=RETURN_URL: return complete URL
     *                          * mode=RETURN_SIP: returns only the sip
     *                          * mode=RETURN_ARRAY: returns array with url ('sipUrl') and all decoded and created
     *                          parameters.
     *
     * @return string String: "API_DIR/delete.php?sip=...."
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function createDeleteUrl($formName, $recordId, $mode = RETURN_URL) {

        $urlParam = $this->store->getNonSystemSipParam(); // especially 'periodId' from calling URL should be passed to delete.php to evaluate it later.
        Support::appendTypo3ParameterToArray($urlParam);
        $urlParam[SIP_RECORD_ID] = $recordId;
        $urlParam[SIP_FORM] = $formName;
        $urlParam[SIP_MODE_ANSWER] = MODE_JSON;
//        $urlParam[PARAM_DB_INDEX_DATA] = $this->formSpec[F_DB_INDEX];

        $queryString = Support::arrayToQueryString($urlParam);

        $sip = $this->store->getSipInstance();

        return $sip->queryStringToSip($queryString, $mode, API_DIR . '/' . API_DELETE_PHP);
    }

    /**
     * Build an Upload (File) Button.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function buildFile(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $attribute = '';

        $this->store->appendToStore(HelperFile::pathinfo($value), STORE_VAR);

        if (empty($formElement[FE_FILE_MIME_TYPE_ACCEPT])) {
            $formElement[FE_FILE_MIME_TYPE_ACCEPT] = UPLOAD_DEFAULT_MIME_TYPE;
        }

        Support::setIfNotSet($formElement, FE_FILE_CAPTURE);
        if ($formElement[FE_FILE_CAPTURE] == FE_FILE_CAPTURE_CAMERA) {
            $formElement[FE_FILE_MIME_TYPE_ACCEPT] = 'image/*';
        }

        if ($formElement[FE_FILE_MIME_TYPE_ACCEPT] == '*' || $formElement[FE_FILE_MIME_TYPE_ACCEPT] == '*.*' || $formElement[FE_FILE_MIME_TYPE_ACCEPT] == '*/*') {
            $formElement[FE_FILE_MIME_TYPE_ACCEPT] = '';
        }

        # Build param array for uniq SIP
        $arr = array();
        $arr['fake_uniq_never_use_this'] = uniqid(); // make sure we get a new SIP. This is needed for multiple forms (same user) with r=0
        $arr[CLIENT_SIP_FOR_FORM] = $this->store->getVar(SIP_SIP, STORE_SIP);
        $arr[CLIENT_FE_NAME] = $formElement[FE_NAME];
        $arr[CLIENT_FORM] = $this->formSpec[F_NAME];
        $arr[CLIENT_RECORD_ID] = $this->store->getVar(SIP_RECORD_ID, STORE_SIP);
        $arr[CLIENT_PAGE_ID] = 'fake';
        $arr[EXISTING_PATH_FILE_NAME] = $value;
        $arr[FE_FILE_MIME_TYPE_ACCEPT] = $formElement[FE_FILE_MIME_TYPE_ACCEPT];

        // Check Safari Bug #5578: in case Safari (Mac OS X or iOS) loads an 'upload element' with more than one file type, fall back to 'no preselection'.
        // Still do the file type check on the server side!
        if (strpos($formElement[FE_FILE_MIME_TYPE_ACCEPT], ',') !== false) {
            $ua = $this->store->getVar('HTTP_USER_AGENT', STORE_CLIENT, SANITIZE_ALLOW_ALNUMX);
            // Look for " Version/11.0 Mobile/15A5370a Safari/" or  " Version/9.0.2 Safari/"
            $rc = preg_match('; Version/.*Safari/;', $ua, $matches);
            // But not like " Version/4.0 Chrome/52.0.2743.98 Safari/"
            if ($rc == 1 && false === strpos($matches[0], ' Chrome/')) {
                $formElement[FE_FILE_MIME_TYPE_ACCEPT] = ''; // This only fakes the upload dialog. But the server file type check is still active due to $arr[FE_FILE_MIME_TYPE_ACCEPT]
            }
        }

        $arr[FE_FILE_MAX_FILE_SIZE] = empty($formElement[FE_FILE_MAX_FILE_SIZE]) ? $this->formSpec[FE_FILE_MAX_FILE_SIZE] : $formElement[FE_FILE_MAX_FILE_SIZE];
        $maxFileSize = Support::returnBytes($arr[FE_FILE_MAX_FILE_SIZE]);
        if ((Support::returnBytes(ini_get('post_max_size')) < $maxFileSize) ||
            (Support::returnBytes(ini_get('upload_max_filesize')) < $maxFileSize)
        ) {
            throw new \UserFormException("Configured 'maxFileSize'=" . $arr[FE_FILE_MAX_FILE_SIZE] .
                " bigger than at least of one of the php.ini setttings 'post_max_size'=" . ini_get('post_max_size') .
                " or 'upload_max_filesize'=" . ini_get('upload_max_filesize'), ERROR_MAX_FILE_SIZE_TOO_BIG);
        }
        $arr[FE_FILE_MAX_FILE_SIZE] = $maxFileSize;

        $sipUpload = $this->sip->queryStringToSip(OnArray::toString($arr), RETURN_SIP);

        $hiddenSipUpload = $this->buildNativeHidden($htmlFormElementName, $sipUpload);

        $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attribute .= Support::doAttribute('name', $htmlFormElementName);
//        $attribute .= Support::doAttribute('class', 'form-control');
        $attribute .= Support::doAttribute('type', 'file');
        $attribute .= Support::doAttribute('title', $formElement[FE_TOOLTIP]);
        $attribute .= Support::doAttribute(FE_FILE_CAPTURE, $formElement[FE_FILE_CAPTURE], true);
        $attribute .= $this->getAttributeList($formElement, [FE_AUTOFOCUS, FE_FILE_MIME_TYPE_ACCEPT]);
        $attribute .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attribute .= Support::doAttribute('data-sip', $sipUpload);
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);


        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement); // Below, $formElement[FE_MODE]=FE_MODE_REQUIRED will be changed. Get the JSON unchanged

        if ($value === '' || $value === false) {
            $textDeleteClass = 'hidden';
            $uploadClass = '';
        } else {

            $textDeleteClass = '';
            $uploadClass = 'hidden';
            if ($formElement[FE_MODE] == FE_MODE_REQUIRED) {
                $formElement[FE_MODE] = FE_MODE_SHOW; // #4495 - Upload, which already has been uploaded should not marked as required
                $attribute .= Support::doAttribute(DATA_REQUIRED, FE_MODE_REQUIRED);
            }
//            $formElement[FE_MODE] = FE_MODE_HIDDEN; // #3876, CR did not understand why we need this here. Comment. If active, this element will be hide on each dynamic update.
        }

        $attribute .= $this->getAttributeFeMode($formElement[FE_MODE]);
        $attribute .= Support::doAttribute('class', $uploadClass, true);

//        $htmlInputFile = '<input ' . $attribute . '>' . $this->getHelpBlock();

        // <input type="file"> with BS3: https://stackoverflow.com/questions/11235206/twitter-bootstrap-form-file-element-upload-button/25053973#25053973
        $attribute .= Support::doAttribute('style', "display:none;");
        $htmlInputFile = '<input ' . $attribute . '>';
        $attributeFileLabel = Support::doAttribute('for', $formElement[FE_HTML_ID]);
        $attributeFileLabel .= Support::doAttribute('class', 'btn btn-default ' . $uploadClass);

        Support::setIfNotSet($formElement, FE_FILE_BUTTON_TEXT, FE_FILE_BUTTON_TEXT_DEFAULT);
        $htmlInputFile = Support::wrapTag("<label $attributeFileLabel>", $htmlInputFile . $formElement[FE_FILE_BUTTON_TEXT]);

        $disabled = ($formElement[FE_MODE] == FE_MODE_READONLY) ? 'disabled' : '';

        // Check if a custom text right beside the trash symbol is given.
        $trashText = '';
        if (!empty($formElement[FE_FILE_TRASH_TEXT])) {
            $trashText = ' ' . $formElement[FE_FILE_TRASH_TEXT];
        }

        if (!empty($value) && Support::isEnabled($formElement, FE_FILE_DOWNLOAD_BUTTON)) {
            if (is_readable($value)) {
                $link = new Link($this->sip, $this->dbIndexData);
                $value = $link->renderLink('s|M:file|d|F:' . $value . '|' . $this->evaluate->parse($formElement[FE_FILE_DOWNLOAD_BUTTON]));
            } else {

                $msg = "Already uploaded file not found.";
                // In case debugging is off: showing download button means 'never show the real pathfilename'
                if ($this->showDebugInfoFlag) {
                    $msg .= ' ShowDebugInfo=on >> Missing file is: ' . $value;
                }
                $value = $msg;
            }
        }

        $deleteButton = '';
        $htmlFilename = Support::wrapTag("<span class='uploaded-file-name'>", $value, false);

        if (($formElement[FE_FILE_TRASH] ?? '1') == '1') {
            $deleteButton = Support::wrapTag("<button type='button' class='btn btn-default delete-file $disabled' $disabled data-sip='$sipUpload' name='delete-$htmlFormElementName'>", $this->symbol[SYMBOL_DELETE] . $trashText);
        }
        $htmlTextDelete = Support::wrapTag("<div class='uploaded-file $textDeleteClass'>", $htmlFilename . ' ' . $deleteButton);

//        <button type="button" class="file-delete" data-sip="571d1fc9e6974"><span class="glyphicon glyphicon-trash"></span></button>

        $formElement = HelperFormElement::prepareExtraButton($formElement, false);

        return $htmlTextDelete . $htmlInputFile . $hiddenSipUpload . $formElement[FE_TMP_EXTRA_BUTTON_HTML] . $formElement[FE_INPUT_EXTRA_BUTTON_INFO];
    }

    /**
     * @param array $formElement
     * @param $htmlFormElementName
     * @param $value
     * @param array $json
     * @param string $mode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function buildAnnotate(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {

        switch ($formElement[FE_ANNOTATE_TYPE] ?? FE_ANNOTATE_TYPE_GRAPHIC) {
            case FE_ANNOTATE_TYPE_GRAPHIC:
                $html = $this->buildAnnotateGraphic($formElement, $htmlFormElementName, $value, $json, $mode);
                break;

            case FE_ANNOTATE_TYPE_TEXT:
                $html = $this->buildAnnotateCode($formElement, $htmlFormElementName, $value, $json, $mode);
                break;
            default:
                throw new \UserFormException("Unkown " . FE_ANNOTATE_TYPE . ": '" . $formElement[FE_ANNOTATE_TYPE] . "'", ERROR_UNKNOWN_MODE);
        }
        return $html;
    }

    /**
     * @param array $formElement
     * @param $htmlFormElementName
     * @param $value
     * @param array $json
     * @param string $mode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function buildAnnotateCode(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {

        //TODO: implement code annotation if there i s no file, but text, save in a specific column
        if (!isset($formElement[FE_TEXT_SOURCE])) {
            throw new \UserFormException("Missing parameter '" . FE_TEXT_SOURCE . "'", ERROR_IO_READ_FILE);
        }

#        if ($formElement[FE_TEXT_SOURCE] != '' && !is_readable($formElement[FE_TEXT_SOURCE])) {
#            throw new \UserFormException("Error reading file: " . $formElement[FE_TEXT_SOURCE], ERROR_IO_READ_FILE);
#        }

        if (!empty($value) && json_decode($value) === null) {
            throw new \UserFormException("Annotate data: JSON structure not valid", ERROR_BROKEN_JSON_STRUCTURE);
        }

        Support::setIfNotSet($formElement, FE_ANNOTATE_USER_UID);
        Support::setIfNotSet($formElement, FE_ANNOTATE_USER_NAME);
        Support::setIfNotSet($formElement, FE_ANNOTATE_USER_AVATAR);

        $dataHighlight = HelperFile::getFileTypeHighlight($formElement[FE_HIGHLIGHT] ?? '', $formElement[FE_TEXT_SOURCE]);


//        <div class="codeCorrection" data-uid='{"uid": 1, "name": "Reginald Commenter", "avatar": "http://www"}' data-file="../javascript/src/CodeCorrection.js" data-target="codeCorrection-output1">
//            </div>
//            <input id="codeCorrection-output1" name="correction-data" type="hidden"
//                   value='{ ... }'>

        $jsonDataUid = json_encode(['uid' => $formElement[FE_ANNOTATE_USER_UID]
            , 'name' => $formElement[FE_ANNOTATE_USER_NAME]
            , 'avatar' => $formElement[FE_ANNOTATE_USER_AVATAR]], JSON_UNESCAPED_SLASHES);

        $attributeDiv = Support::doAttribute('class', ANNOTATE_TEXT_CSS_CLASS);
//        $attributeDiv .= Support::doAttribute('data-uid', $jsonDataUid, true,ESCAPE_WITH_BACKSLASH);
        $attributeDiv .= Support::doAttribute('data-file', $this->fileToSipUrl($formElement[FE_TEXT_SOURCE]));
        $attributeDiv .= Support::doAttribute('data-target', $formElement[FE_HTML_ID]);
        $attributeDiv .= Support::doAttribute('data-highlight', $dataHighlight);
        $attributeDiv .= $this->getAttributeFeMode($formElement[FE_MODE]);
        if ($formElement[FE_MODE] == FE_MODE_READONLY) {
            $attributeDiv .= Support::doAttribute('data-view-only', 'true');
        }

        $htmlAnnotate = Support::wrapTag('<div ' . $attributeDiv . ' data-uid=\'' . $jsonDataUid . '\' >', '', false);
//        $htmlAnnotate = Support::wrapTag('<div ' . $attributeDiv .'>', '', false);

        $attributeInput = Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attributeInput .= Support::doAttribute('name', $htmlFormElementName);
        $attributeInput .= Support::doAttribute('type', 'hidden');
        $attributeInput .= Support::doAttribute('value', htmlentities($value), false);
        $attributeInput .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        $htmlInput = Support::wrapTag('<input ' . $attributeInput . ' >', '', false);

        $html = $htmlAnnotate . $htmlInput . $this->getHelpBlock();

//        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement);

        return $html;
    }

    /**
     * Build a Fabric Annotate Element
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function buildAnnotateGraphic(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {

        if ($mode == FORM_LOAD) {
            if (!isset($formElement[FE_IMAGE_SOURCE])) {
                throw new \UserFormException("Missing parameter '" . FE_IMAGE_SOURCE . "'", ERROR_IO_READ_FILE);
            }

            if ($formElement[FE_IMAGE_SOURCE] != '' && !is_readable($formElement[FE_IMAGE_SOURCE])) {
                throw new \UserFormException("Error reading file: " . $formElement[FE_IMAGE_SOURCE], ERROR_IO_READ_FILE);
            }
        }

        if (!empty($value) && json_decode($value) === null) {
            throw new \UserFormException("Fabric data: JSON structure not valid", ERROR_BROKEN_JSON_STRUCTURE);
        }

//        $attributeImage = Support::doAttribute('id', 'qfq-fabric-image-1');
//        $attributeImage .= Support::doAttribute('src', $this->fileToSipUrl($formElement[FE_IMAGE_SOURCE]));
//        $attributeImage .= Support::doAttribute('class', 'qfq-fabric-image');
//        $htmlImage = Support::wrapTag('<img ' . $attributeImage . '>', '', false);

//        $attributeFabric = Support::doAttribute('id', 'fabric');

        $attributeFabric = Support::doAttribute('class', ANNOTATE_GRAPHIC_CSS_CLASS);
        $attributeFabric .= Support::doAttribute('data-background-image', $this->fileToSipUrl($formElement[FE_IMAGE_SOURCE]));
        $attributeFabric .= Support::doAttribute('data-control-name', $formElement[FE_HTML_ID]);
        $attributeFabric .= Support::doAttribute('data-buttons', 'typo3conf/ext/qfq/Resources/Public/Json/fabric.buttons.json');
        $attributeFabric .= Support::doAttribute('data-emojis', 'typo3conf/ext/qfq/Resources/Public/Json/qfq.emoji.json');
        $attributeFabric .= Support::doAttribute('data-fabric-color', HelperFormElement::penColorToHex($formElement));
        $attributeFabric .= $this->getAttributeFeMode($formElement[FE_MODE]);
        if ($formElement[FE_MODE] == FE_MODE_READONLY) {
            $attributeFabric .= Support::doAttribute('data-view-only', 'true');
        }
        $htmlFabric = Support::wrapTag('<div ' . $attributeFabric . ' >', '', false);

        $attributeInput = Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attributeInput .= Support::doAttribute('name', $htmlFormElementName);
        $attributeInput .= Support::doAttribute('type', 'hidden');
        $attributeInput .= Support::doAttribute('value', htmlentities($value), false);
        $attributeInput .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        $htmlInput = Support::wrapTag('<input ' . $attributeInput . ' >', '', false);

        $html = $htmlFabric . $htmlInput . $this->getHelpBlock();

//        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement);

        return $html;
    }

    /**
     * Build a Fabric ImageCut Element
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function buildImageCut(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {

        $htmlImage = '';
        if ($mode == FORM_LOAD && !empty($formElement[FE_IMAGE_SOURCE])) {
            if (!is_readable($formElement[FE_IMAGE_SOURCE])) {
                throw new \UserFormException("Error reading file: " . $formElement[FE_IMAGE_SOURCE], ERROR_IO_READ_FILE);
            }
        }

        if ($mode === FORM_LOAD) {
            // Store source filename in SIP
            $field = HelperFormElement::AppendFormElementNameImageCut($formElement);
            $this->store->setVar($field, $value, STORE_SIP, false);
        }

        $htmlFabricId = $formElement[FE_HTML_ID];
        $htmlFabricImageId = $formElement[FE_HTML_ID] . '-image';

        $imageFileName = $this->fileToSipUrl($value);

        // <div class="fabric"
        //     data-buttons="mockData/fabric.editor.buttons.json"
        //     data-edit-image="true"
        //     data-background-image="mockData/Scan2a.jpeg"
        //     data-control-name='fabric-output'
        //     data-image-resize-width="1200"
        //     data-image-output="target-png">
        // </div>
        $attributeFabric = Support::doAttribute('class', ANNOTATE_GRAPHIC_CSS_CLASS);
        $attributeFabric .= Support::doAttribute('data-buttons', 'typo3conf/ext/qfq/Resources/Public/Json/fabric.editor.buttons.json');
        $attributeFabric .= Support::doAttribute('data-edit-image', 'true');
        $attributeFabric .= Support::doAttribute('data-background-image', $imageFileName);
        $attributeFabric .= Support::doAttribute('data-control-name', $htmlFabricId);
        if (!empty($formElement[FE_IMAGE_CUT_RESIZE_WIDTH]) && ctype_digit($formElement[FE_IMAGE_CUT_RESIZE_WIDTH])) {
            $attributeFabric .= Support::doAttribute('data-image-resize-width', $formElement[FE_IMAGE_CUT_RESIZE_WIDTH]);
        }
        $attributeFabric .= Support::doAttribute('data-image-output', $htmlFabricImageId);
        $attributeFabric .= $this->getAttributeFeMode($formElement[FE_MODE]);

        $htmlFabric = Support::wrapTag('<div ' . $attributeFabric . ' >', '', false);

        // <input id="fabric-output" name="fabric-data" type="hidden">
        $attributeInput = Support::doAttribute('id', $htmlFabricId);
        $attributeInput .= Support::doAttribute('name', $htmlFormElementName);
        $attributeInput .= Support::doAttribute('type', 'hidden');
        $attributeInput .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        $htmlInput = Support::wrapTag('<input ' . $attributeInput . ' >', '', false);

        // <img id="target-png"> - this element is for future use. It's not necessary for the function and is not used yet.
        $attributeImage = Support::doAttribute('id', $htmlFabricImageId);
        $htmlImage = Support::wrapTag('<img ' . $attributeImage . '>', '', false);

        $html = $htmlFabric . $this->getHelpBlock() . $htmlInput . $htmlImage;

//        $tmpUrlParam[DOWNLOAD_MODE] = $this->getDownloadModeNCheck($vars);
//        $tmpUrlParam[DOWNLOAD_EXPORT_FILENAME] = $vars[NAME_DOWNLOAD];
//        $tmpUrlParam[SIP_DOWNLOAD_PARAMETER] = base64_encode(implode(PARAM_DELIMITER, $vars[NAME_DOWNLOAD_ELEMENTS]));
//        $vars[NAME_URL_PARAM] = KeyValueStringParser::unparse($tmpUrlParam, '=', '&');

//        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement);

        return $html;
    }

    /**
     * @param string $pathFileName
     * @return string SIP encoded URL
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function fileToSipUrl($pathFileName) {
        $param[DOWNLOAD_MODE] = DOWNLOAD_MODE_FILE;
        $param[SIP_DOWNLOAD_PARAMETER] = base64_encode(TOKEN_FILE . PARAM_TOKEN_DELIMITER . $pathFileName);

        $url = $this->sip->queryStringToSip(API_DIR . '/' . API_DOWNLOAD_PHP . '?' . KeyValueStringParser::unparse($param, '=', '&'), RETURN_URL);

        return $url;
    }

    /**
     * Builds HTML 'input' element.
     * Format: <input name="$htmlFormElementName" <type="date" [autocomplete="autocomplete"] [autofocus="autofocus"]
     *           [maxlength="$maxLength"] [placeholder="$placeholder"] [size="$size"] [min="$min"] [max="$max"]
     *           [pattern="$pattern"] [required="required"] [disabled="disabled"] value="$value">
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function buildDateTime(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $attribute = '';
        $placeholder = '';

        $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attribute .= Support::doAttribute('name', $htmlFormElementName);
        $attribute .= Support::doAttribute('class', 'form-control');
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        $showTime = ($formElement[FE_TYPE] == 'time' || $formElement[FE_TYPE] == 'datetime') ? 1 : 0;
        if ($value == 'CURRENT_TIMESTAMP') {
            $value = date('Y-m-d H:i:s');
        }
        $value = Support::convertDateTime($value, $formElement[FE_DATE_FORMAT], $formElement[FE_SHOW_ZERO], $showTime, $formElement[FE_SHOW_SECONDS]);

        $tmpPattern = $formElement[FE_CHECK_PATTERN];
        $formElement[FE_CHECK_PATTERN] = Support::dateTimeRegexp($formElement[FE_TYPE], $formElement[FE_DATE_FORMAT], $formElement[FE_TIME_IS_OPTIONAL]);

        switch ($formElement[FE_CHECK_TYPE]) {
            case  SANITIZE_ALLOW_PATTERN:
                $formElement[FE_CHECK_PATTERN] = $tmpPattern;
                break;
            case SANITIZE_ALLOW_ALL:
            case SANITIZE_ALLOW_ALNUMX:
            case SANITIZE_ALLOW_ALLBUT:
                $formElement[FE_CHECK_TYPE] = SANITIZE_ALLOW_PATTERN;
                break;
            default:
                throw new \UserFormException("Checktype not applicable for date/time: '" . $formElement[FE_CHECK_TYPE] . "'", ERROR_NOT_APPLICABLE);
        }

        // truncate if necessary
        if ($value != '' && $formElement[FE_MAX_LENGTH] > 0) {
            $value = substr($value, 0, $formElement[FE_MAX_LENGTH]);
        }

        // See: https://project.math.uzh.ch/issues/3536
//        $type = $formElement[FE_TYPE];
//        if ($type === 'datetime') {
//            $type = 'datetime-local';
//        }
        $type = 'text'; // date|time|datetime|datetime-local are not appropriate - only I18n representation possible.

        $attribute .= Support::doAttribute('type', $type);


        // 'maxLength' needs an upper 'L': naming convention for DB tables!
        $attribute .= $this->getAttributeList($formElement, ['size', 'maxLength']);
        $attribute .= Support::doAttribute('value', htmlentities($value), false);

        if ($formElement[FE_PLACEHOLDER] == '') {
            $timePattern = ($formElement[FE_SHOW_SECONDS] == 1) ? 'hh:mm:ss' : 'hh:mm';
            switch ($formElement[FE_TYPE]) {
                case 'date':
                    $placeholder = $formElement[FE_DATE_FORMAT];
                    break;
                case 'datetime':
                    if ($formElement[FE_TIME_IS_OPTIONAL] == 1) $timePattern = "[$timePattern]";
                    $placeholder = $formElement[FE_DATE_FORMAT] . ' ' . $timePattern;
                    break;
                case 'time':
                    $placeholder = $timePattern;
                    break;
                default:
                    throw new \UserFormException("Unexpected Formelement type: '" . $formElement[FE_TYPE] . "'", ERROR_FORMELEMENT_TYPE);
            }
            $formElement[FE_PLACEHOLDER] = $placeholder;
        }

        if ($formElement[F_FE_DATA_PATTERN_ERROR] == '') {

            $formElement[F_FE_DATA_PATTERN_ERROR] = "Please match the format: " . $placeholder;
        }
        $attribute .= $this->getAttributeList($formElement, [F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR, F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR]);

        $attribute .= $this->getAttributeList($formElement, [FE_INPUT_AUTOCOMPLETE, 'autofocus', 'placeholder']);
        $attribute .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attribute .= Support::doAttribute('title', $formElement[FE_TOOLTIP]);

        $pattern = Sanitize::getInputCheckPattern($formElement[FE_CHECK_TYPE], $formElement[FE_CHECK_PATTERN], '', $sanitizeMessage);
        $attribute .= ($pattern === '') ? '' : 'pattern="' . $pattern . '" ';

        // Use system message only,if no custom one is set.
        if ($formElement[F_FE_DATA_PATTERN_ERROR] == $formElement[F_FE_DATA_PATTERN_ERROR_SYSTEM]) {
            $formElement[F_FE_DATA_PATTERN_ERROR] = $sanitizeMessage;
        }

        $attribute .= $this->getAttributeList($formElement, [FE_MIN, FE_MAX]);

        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement);

        $formElement = HelperFormElement::prepareExtraButton($formElement, true);

        $attribute .= $this->getAttributeFeMode($formElement[FE_MODE]);

        $input = "<input $attribute>" . $this->getHelpBlock() . $formElement[FE_TMP_EXTRA_BUTTON_HTML];

        if ($formElement[FE_TMP_EXTRA_BUTTON_HTML] !== '') {
            $input = Support::wrapTag('<div class="input-group">', $input);
        }

        return $input . $formElement[FE_INPUT_EXTRA_BUTTON_INFO];

    }

    /**
     * Builds HTML 'input' element.
     * Format: <input name="$htmlFormElementName" <type="date" [autocomplete="autocomplete"] [autofocus="autofocus"]
     *           [maxlength="$maxLength"] [placeholder="$placeholder"] [size="$size"] [min="$min"] [max="$max"]
     *           [pattern="$pattern"] [required="required"] [disabled="disabled"] value="$value">
     *
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function buildDateJQW(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $arrMinMax = null;

//        if ($formElement[FE_PLACEHOLDER] == '') {
//            $timePattern = ($formElement[FE_SHOW_SECONDS] == 1) ? 'hh:mm:ss' : 'hh:mm';
//            switch ($formElement[FE_TYPE]) {
//                case 'date':
//                    $placeholder = $formElement[FE_DATE_FORMAT];
//                    break;
//                case 'datetime':
//                    $placeholder = $formElement[FE_DATE_FORMAT] . ' ' . $timePattern;
//                    break;
//                case 'time':
//                    $placeholder = $timePattern;
//                    break;
//                default:
//                    throw new \UserFormException("Unexpected Formelement type: '" . $formElement[FE_TYPE] . "'", ERROR_FORMELEMENT_TYPE);
//            }
//            $formElement[FE_PLACEHOLDER] = $placeholder;
//        }

//        switch ($formElement['checkType']) {
////            case  SANITIZE_ALLOW_PATTERN:
////                $formElement['checkPattern'] = $tmpPattern;
////                break;
//            case SANITIZE_ALLOW_MIN_MAX_DATE:
//                $arrMinMax = explode('|', $formElement['checkPattern'], 2);
//                if (count($arrMinMax) != 2) {
//                    throw new \UserFormException('Missing min|max definition', ERROR_MISSING_MIN_MAX);
//                }
//                break;
////            case SANITIZE_ALLOW_ALL:
////            case SANITIZE_ALLOW_ALNUMX:
////            case SANITIZE_ALLOW_ALLBUT:
////                $formElement['checkType'] = SANITIZE_ALLOW_PATTERN;
////                break;
//            default:
//                throw new \UserFormException("Checktype not applicable for date/time: '" . $formElement['checkType'] . "'", ERROR_NOT_APPLICABLE);
//        }

        $showTime = ($formElement[FE_TYPE] == 'time' || $formElement[FE_TYPE] == 'datetime') ? 1 : 0;
        $value = Support::convertDateTime($value, $formElement[FE_DATE_FORMAT], $formElement[FE_SHOW_ZERO], $showTime, $formElement[FE_SHOW_SECONDS]);

//        $formElement[FE_DATE_FORMAT]

        $attribute = Support::doAttribute('id', $htmlFormElementName);
        $attribute .= Support::doAttribute('class', 'jqw-datetimepicker');
        $attribute .= Support::doAttribute('data-control-name', "$htmlFormElementName");
        $attribute .= Support::doAttribute('data-format-string', "dd.MM.yyyy HH:mm");
        $attribute .= Support::doAttribute('data-show-time-button', "true");
//        $attribute .= Support::doAttribute('data-placeholder', $formElement[FE_PLACEHOLDER]);
        $attribute .= Support::doAttribute('data-value', htmlentities($value), false);
//        $attribute .= Support::doAttribute('data-autofocus', $formElement[FE_AUTOFOCUS]);
        $attribute .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attribute .= Support::doAttribute('data-title', $formElement[FE_TOOLTIP]);

//        if (is_array($arrMinMax)) {
//            $attribute .= Support::doAttribute('data-min', $arrMinMax[0]);
//            $attribute .= Support::doAttribute('data-max', $arrMinMax[1]);
//        }

        $attribute .= $this->getAttributeFeMode($formElement[FE_MODE]);

//        $json = $this->getJsonElementUpdate($htmlFormElementName, $value, $formElement[FE_MODE]);

        $element = Support::wrapTag("<div $attribute>", '', false);

        return $element . $this->getHelpBlock();
    }

    /**
     * Build a HTML 'textarea' element which becomes a TinyMCE Editor.
     * List of possible plugins: https://www.tinymce.com/docs/plugins/
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function buildEditor(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $attribute = '';

        //TODO plugin autoresize nutzen um Editorgroesse anzugeben

        $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attribute .= Support::doAttribute('name', $htmlFormElementName);
//        $attribute .= Support::doAttribute('id', $htmlFormElementName);
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        $attribute .= Support::doAttribute('class', 'qfq-tinymce');
        $attribute .= Support::doAttribute('data-control-name', "$htmlFormElementName");
        $attribute .= Support::doAttribute('data-placeholder', $formElement[FE_PLACEHOLDER]);
//        $attribute .= Support::doAttribute('data-autofocus', $formElement[FE_AUTOFOCUS]);
        $attribute .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attribute .= Support::doAttribute('data-title', $formElement[FE_TOOLTIP]);

        $formElement = $this->setEditorConfig($formElement, $htmlFormElementName);
        // $formElement['editor-plugins']='autoresize code'
        // $formElement['editor-contextmenu']='link image | cell row column'
        $json = $this->getPrefixedElementsAsJSON(FE_EDITOR_PREFIX, $formElement);
        $attribute .= Support::doAttribute('data-config', $json, true);


        $attribute .= $this->getAttributeFeMode($formElement[FE_MODE]);
        $attribute .= $this->getAttributeList($formElement, [F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR, F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR]);

        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement);

        $html = Support::wrapTag("<textarea $attribute>", htmlentities($value), false);
        $formElement = HelperFormElement::prepareExtraButton($formElement, false);

        return $html . $this->getHelpBlock() . $formElement[FE_TMP_EXTRA_BUTTON_HTML] . $formElement[FE_INPUT_EXTRA_BUTTON_INFO];
    }

    /**
     * Parse $formElement[FE_EDITOR_*] settings and build editor settings.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     *
     * @return array
     */
    private function setEditorConfig(array $formElement, $htmlFormElementName) {
        $flagMaxHeight = false;

        // plugins
        if (!isset($formElement[FE_EDITOR_PREFIX . 'plugins'])) {
            $formElement[FE_EDITOR_PREFIX . 'plugins'] = 'code link lists searchreplace table textcolor textpattern visualchars';
        }

        // toolbar: https://www.tinymce.com/docs/advanced/editor-control-identifiers/#toolbarcontrols
        if (!isset($formElement[FE_EDITOR_PREFIX . 'toolbar'])) {
            $formElement[FE_EDITOR_PREFIX . 'toolbar'] = 'code searchreplace undo redo | ' .
                'styleselect link table | fontselect fontsizeselect | ' .
                'bullist numlist outdent indent | forecolor backcolor bold italic';
        }

        // menubar
        if (!isset($formElement[FE_EDITOR_PREFIX . 'menubar'])) {
            $formElement[FE_EDITOR_PREFIX . 'menubar'] = 'false';
        }

        // autofocus
        if (isset($formElement[FE_AUTOFOCUS]) && $formElement[FE_AUTOFOCUS] == 'yes') {
            $formElement[FE_EDITOR_PREFIX . 'auto_focus'] = $htmlFormElementName;
        }

        // Check for min_height, max_height
        $minMax = explode(',', $formElement[FE_SIZE], 2);
        if (isset($minMax[0]) && ctype_digit($minMax[0]) && !isset($formElement[FE_EDITOR_PREFIX . 'min_height'])) {
            $formElement[FE_EDITOR_PREFIX . 'min_height'] = $minMax[0];
        }
        if (isset($minMax[1]) && ctype_digit($minMax[1]) && !isset($formElement[FE_EDITOR_PREFIX . 'max_height'])) {
            $formElement[FE_EDITOR_PREFIX . 'max_height'] = $minMax[1];
            $flagMaxHeight = true;
        }

        // statusbar: disable if not user defined and if no max_height is given.
        if (!$flagMaxHeight && !isset($formElement[FE_EDITOR_PREFIX . 'statusbar'])) {
            $formElement[FE_EDITOR_PREFIX . 'statusbar'] = 'false';
        }

        return $formElement;
    }

    /**
     * Searches for '$prefix*' elements in $formElement. Collect all found elements, strip $prefix (=$keyName) and
     *   returns keys/values JSON encoded.
     * Only 'alpha' chars are allowed as keyName.
     * Empty $settings are ok.
     *
     * @param string $prefix
     * @param array $formElement
     *
     * @return string
     * @throws \UserFormException
     */
    private function getPrefixedElementsAsJSON($prefix, array $formElement) {
        $settings = array();

        // E.g.: $key = editor-plugins
        foreach ($formElement as $key => $value) {
            if (substr($key, 0, strlen($prefix)) == $prefix) {

                $keyName = substr($key, strlen($prefix));

                if ($keyName == '') {
                    throw new \UserFormException("Empty '" . $prefix . "*' keyname: '" . $keyName . "'", ERROR_INVALID_EDITOR_PROPERTY_NAME);
                }

                // $value might be boolean false, which should be used! Do not compare with ''.
                if (isset($value)) {
                    // real boolean are important for TinyMCE config 'statusbar', 'menubar', ...
                    if ($value === 'false') $value = false;
                    if ($value === 'true') $value = true;
                    $settings[$keyName] = $value;
                }
            }
        }

        return json_encode($settings);
    }

    /**
     * Build Grid JQW element.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param string $fake
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @throws \UserFormException
     */
    public function buildGridJQW(array $formElement, $htmlFormElementName, $value, $fake, $mode = FORM_LOAD) {
        // TODO: implement
        throw new \UserFormException("Not implemented yet: buildGridJQW()", ERROR_NOT_IMPLEMENTED);
    }

    /**
     * Build Note.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     *
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     * @return mixed
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function buildNote(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {

        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement);

        $attribute = Support::doAttribute('id', $formElement[FE_HTML_ID] . HTML_ID_EXTENSION_INPUT);
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        return Support::wrapTag("<div $attribute class='" . CLASS_NOTE . "'>", $value);
    }

    /**
     * Build Pill.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     *
     * @return mixed
     */
    public function buildPill(array $formElement, $htmlFormElementName, $value, array &$json) {
        return $value;
    }

    /**
     * Build a HTML fieldset. Renders all assigned FormElements inside the fieldset.
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return mixed
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function buildFieldset(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $attribute = '';

        // save parent processed FE's
        $tmpStore = $this->feSpecNative;

        $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attribute .= Support::doAttribute('name', $htmlFormElementName);
        $attribute .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attribute .= Support::doAttribute('class', 'qfq-fieldset');
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        // <fieldset>
        $html = '<fieldset ' . $attribute . '>';


        if ($formElement[FE_LABEL] !== '') {
            $html .= '<legend>' . $formElement[FE_LABEL] . '</legend>';
        }

        $html .= $this->wrap[WRAP_SETUP_IN_FIELDSET][WRAP_SETUP_START];

        // child FE's
        $this->feSpecNative = $this->dbArray[$this->dbIndexQfq]->getNativeFormElements(SQL_FORM_ELEMENT_SPECIFIC_CONTAINER,
            ['yes', $this->formSpec["id"], 'native,container', $formElement[FE_ID]], $this->formSpec);

        $html .= $this->elements($this->store->getVar(SIP_RECORD_ID, STORE_SIP), FORM_ELEMENTS_NATIVE_SUBRECORD, 0, $json);
        $html .= $this->wrap[WRAP_SETUP_IN_FIELDSET][WRAP_SETUP_END];

        $html .= '</fieldset>';

        // restore parent processed FE's
        $this->feSpecNative = $tmpStore;

        $json = $this->getFormElementForJson($htmlFormElementName, $value, $formElement);

        return $html;
    }

    /**
     * @param array $formElementArr
     * @param int $tgMaxCopies
     *
     * @return array
     */
    private function fillFeSpecNativeCheckboxWithTgMax(array $formElementArr, $tgMaxCopies) {

        foreach ($formElementArr as $key => $formElement) {
            if ($formElement[FE_TYPE] == FE_TYPE_CHECKBOX) {
                $formElementArr[$key][NAME_TG_COPIES] = $tgMaxCopies;
            }
        }

        return $formElementArr;
    }

    /**
     * Build a 'templateGroup'. Renders all assigned FormElements of the templateGroup.
     * If there are already vlaues for the formElements, fill as much copies as values exist
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return mixed
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function buildTemplateGroup(array $formElement, $htmlFormElementName, $value, array &$json, $mode = FORM_LOAD) {
        $attribute = '';
        $html = '';

        $addClass = Support::setIfNotSet($formElement, FE_TEMPLATE_GROUP_ADD_CLASS, 'btn btn-default');
        $addText = Support::setIfNotSet($formElement, FE_TEMPLATE_GROUP_ADD_TEXT, 'Add');
        $removeClass = Support::setIfNotSet($formElement, FE_TEMPLATE_GROUP_REMOVE_CLASS, 'btn btn-default');
        $removeText = Support::setIfNotSet($formElement, FE_TEMPLATE_GROUP_REMOVE_TEXT, 'Remove');
        $classCustom = Support::setIfNotSet($formElement, FE_TEMPLATE_GROUP_CLASS);

        $disabled = ($formElement[FE_MODE] == FE_MODE_READONLY) ? "disabled='disabled'" : '';

        // save parent processed FE's
        $feSpecNativeSave = $this->feSpecNative;

        $addButtonId = 'add_button_' . $formElement[FE_ID];
        $qfqFieldsName = 'qfq_fields_' . $formElement[FE_ID]; // ='qfq-fields'
        $templateName = 'template_' . $formElement[FE_ID]; // ='template'
        $targetName = 'target_' . $formElement[FE_ID]; // ='template'
        Support::setIfNotSet($formElement, FE_MAX_LENGTH, '5', '');
        $max = $formElement[FE_MAX_LENGTH];

        $codeJs = <<<EOT
<script type="text/javascript">
    $(function () {
        $(".$qfqFieldsName").each(
            function () {
                QfqNS.initializeFields(this);
            }
        );
    });
</script>
EOT;

        $htmlAdd = <<<EOT
<button type="button" id="$addButtonId" class="$addClass" $disabled onclick="QfqNS.addFields('#$templateName', '#$targetName', $max)">$addText</button>
EOT;

        $htmlDelete = <<<EOT
<div class="qfq-note-no-padding">
    <button type="button"  class="$removeClass" $disabled onclick="QfqNS.removeFields(this)">$removeText</button>
</div>
EOT;

        // child FE's
        $this->feSpecNative = $this->dbArray[$this->dbIndexQfq]->getNativeFormElements(SQL_FORM_ELEMENT_SPECIFIC_CONTAINER,
            ['yes', $this->formSpec[F_ID], 'native,container', $formElement[FE_ID]], $this->formSpec);

        // Count defined FormElements in the current templateGroup
        $elementsTotal = count($this->feSpecNative);
        if ($elementsTotal < 1) {
            // Nothing to do: return.
            $this->feSpecNative = $feSpecNativeSave;

            return '';
        }

        $this->feSpecNative = $this->fillFeSpecNativeCheckboxWithTgMax($this->feSpecNative, $max);

        // If there are already elements filled, take them.
        $html = $this->templateGroupCollectFilledElements($max, $htmlDelete, $json);

        $attribute = Support::doAttribute('class', $qfqFieldsName);
        $attribute .= Support::doAttribute('id', $targetName);
        $attribute .= Support::doAttribute('data-qfq-line-template', '#' . $templateName);
        $attribute .= Support::doAttribute('data-qfq-line-add-button', '#' . $addButtonId);
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        // Element where the effective FormElements will be copied to. The BS 'col-md-* Classes are inside the template, not here. This here should be pure data without wrapping.
        $html = Support::wrapTag("<div $attribute>", $html, false);

        // Add button, row below: The label & note of the FormElement 'templateGroup' will be used for the add button row.
        $tmpFe = $formElement;
        $tmpFe[FE_NAME] = '_add';
        $tmpFe[FE_LABEL] = '';
        $tmpFe[FE_NOTE] = '';
        $this->fillWrapLabelInputNote($tmpFe[FE_BS_LABEL_COLUMNS], $tmpFe[FE_BS_INPUT_COLUMNS], $tmpFe[FE_BS_NOTE_COLUMNS]);
        $html .= $this->buildRowNative($tmpFe, $htmlAdd, $htmlFormElementName);

        $html = $this->wrap[WRAP_SETUP_IN_TEMPLATE_GROUP][WRAP_SETUP_START] . $html . $this->wrap[WRAP_SETUP_IN_TEMPLATE_GROUP][WRAP_SETUP_END];

        // Append 'delete' Button after 'note' of the templateGroups last element.
        $this->feSpecNative[$elementsTotal - 1][FE_NOTE] .= $htmlDelete;

        // Get FE natives
        $template = $this->elements($this->store->getVar(SIP_RECORD_ID, STORE_SIP), FORM_ELEMENTS_NATIVE, 0, $json);

        // Wrap the main html code
        $attribute = Support::doAttribute('class', ['qfq-line', $classCustom]);
        $template = Support::wrapTag("<div $attribute>", $template);
        $template = Support::wrapTag('<script id="' . $templateName . '" type="text/' . $templateName . '">', $template);

        $html .= $template . $codeJs;

        // restore parent processed FE's
        $this->feSpecNative = $feSpecNativeSave;

        //TODO: nicht klar ob das hier noetig ist.
//        $json = $this->getJsonElementUpdate($htmlFormElementName, $value, $formElement[FE_MODE]);

        return $html;

    }

    /**
     * Build real FormElements based on the current templateGroup FormElements in $this->feSpecNative.
     * Take a templateGroup 'copy', if at least one of the elements is filled with a non default value.
     * Stop filling elements if there are no further elements filled.
     *
     * @param int $max
     * @param string $htmlDelete
     * @param array $json
     *
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    private function templateGroupCollectFilledElements($max, $htmlDelete, array &$json) {

        $record = $this->store->getStore(STORE_RECORD); // current values
        if ($record === false || count($record) === 0) {
            return '';
        }
        $default = $this->store->getStore(STORE_TABLE_DEFAULT); // current defaults

        // evaluate FE_VALUE on all templateGroup FormElements: After this call, in case of non-primary FEs, FE_VALUE
        // might contain an array of values for all copies of the current FE.
        $maxForeignRecords = $this->templateGroupDoValue();

        $lastFilled = 0; // Marker if there is at least one element per copy who is filled.
        $feSpecNativeCopy = array();
        for ($ii = 1; $ii <= $max; $ii++) {

            // Per copy, iterate over all templateGroup FormElements.
            foreach ($this->feSpecNative as $fe) {

                $fe[FE_NAME] = str_replace(FE_TEMPLATE_GROUP_NAME_PATTERN, $ii, $fe[FE_NAME]);
                $fe[FE_LABEL] = str_replace(FE_TEMPLATE_GROUP_NAME_PATTERN, $ii, $fe[FE_LABEL]);
                $fe[FE_NOTE] = str_replace(FE_TEMPLATE_GROUP_NAME_PATTERN, $ii, $fe[FE_NOTE]);
                $fe[FE_TG_INDEX] = $ii;
                $columnName = $fe[FE_NAME];

                // Column of primary table?
                if (isset($record[$columnName])) {
                    if ($record[$columnName] != $default[$columnName]) {
                        $lastFilled = max($ii, $lastFilled);
                    }

                } else {
                    $lastFilled = max($maxForeignRecords, $lastFilled);
                    if (is_array($fe[FE_VALUE]) && isset($fe[FE_VALUE][$ii - 1])) {
                        $fe[FE_VALUE] = current($fe[FE_VALUE][$ii - 1]); // replace array with current value
                    }
//                    $fe[FE_TEMPLATE_GROUP_CURRENT_IDX] = $ii;
                }

                $feSpecNativeCopy[$ii - 1][] = $fe; // Build array with current copy of templateGroup.
            }

            // Append $htmlDelete on the last element of all copies, but not the first.
            if ($ii > 1) {
                // Count defined FormElements in the current templateGroup
                $last = count($feSpecNativeCopy[$ii - 1]) - 1;
                // Append 'delete' Button at the note of the last element
                $feSpecNativeCopy[$ii - 1][$last][FE_NOTE] .= $htmlDelete;
            }
        }

        // Nothing found: return
        if (count($feSpecNativeCopy) == 0) {
            return '';
        }

        $feSpecNativeSave = $this->feSpecNative;

        $lastFilled = min($lastFilled, $max); // It's possible (external records) that there are more fetched values than the maximum allows - skip those.
        $html = '';
        for ($ii = 0; $ii < $lastFilled; $ii++) {
            $this->feSpecNative = $feSpecNativeCopy[$ii];
            $htmlCopy = $this->elements($this->store->getVar(SIP_RECORD_ID, STORE_SIP), FORM_ELEMENTS_NATIVE, 0, $json);
            $htmlCopy = Support::wrapTag('<div class="qfq-line">', $htmlCopy);
            $html .= $htmlCopy;
        }

        $this->feSpecNative = $feSpecNativeSave;

        return $html;
    }

    /**
     * Evaluate for all FormElements of the current templateGroup the field FE_VALUE.
     * If the specific FormElement is not a real column of the primary table, than the value is probably a
     * '{{!SELECT ...' statement, that one will be fired. In case of an non-primary FE, the result array are the
     * values for the copies of the specific FE.
     *
     * Additional the maximum count of all select rows will be determined and returned.
     *
     * @return int   max number of records in FormElement[FE_VALUE] over all FormElements.
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function templateGroupDoValue() {

        // Fire 'value' statement
        $tgMax = 0;
        foreach ($this->feSpecNative as $key => $arr) {
            $this->feSpecNative[$key][FE_VALUE] = $this->evaluate->parse($arr[FE_VALUE]);

            if (is_array($this->feSpecNative[$key][FE_VALUE])) {
                $cnt = count($this->feSpecNative[$key][FE_VALUE]);

                $tgMax = max($cnt, $tgMax);
            }
        }

        return $tgMax;
    }

    /**
     * @param array $formElement
     * @param $htmlElement
     * @param $htmlFormElementName
     * @return mixed
     */
    abstract public function buildRowNative(array $formElement, $htmlElement, $htmlFormElementName);

}