<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 5/9/17
 * Time: 9:38 AM
 */

// Always put the latest updates to the end!!!
$UPDATE_ARRAY = array(

    '0.12.0' => [
        "ALTER TABLE  `FormElement` ADD `rowLabelInputNote` SET('row','label','/label','input','/input','note','/note','/row') NOT NULL DEFAULT 'row,label,/label,input,/input,note,/note,/row' AFTER  `bsNoteColumns` ",
        "ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra', 'gridJQW',  'text',  'editor',  'time',  'note',  'password',  'radio',  'select',  'subrecord',  'upload',  'fieldset', 'pill',     'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete',  'afterLoad',  'afterSave', 'afterInsert',  'afterUpdate',  'afterDelete',  'sendMail' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text' ",
    ],

    '0.13.0' => [
        "ALTER TABLE FormElement MODIFY COLUMN checkType ENUM('alnumx','digit','numerical','email','min|max','min|max date', 'pattern','allbut','all') NOT NULL DEFAULT 'alnumx' ",
    ],

    '0.14.0' => [
        "ALTER TABLE `FormElement` CHANGE `placeholder` `placeholder` VARCHAR(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ",
    ],

    '0.15.0' => [
        "UPDATE FormElement SET parameter = REPLACE(parameter, 'typeAheadLdapKeyPrintf', 'typeAheadLdapIdPrintf')",
        "ALTER TABLE  `FormElement` CHANGE  `placeholder`  `placeholder` VARCHAR( 2048 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ",
    ],

    '0.16.0' => [
        "ALTER TABLE `FormElement` ADD INDEX `feIdContainer` (`feIdContainer`)",
        "ALTER TABLE `FormElement` ADD INDEX `ord` (`ord`)",
        "ALTER TABLE `FormElement` ADD INDEX `feGroup` (`feGroup`)",
        "ALTER TABLE `FormElement` ADD `adminNote` TEXT NOT NULL AFTER `note`",
    ],

    '0.17.0' => [
        "ALTER TABLE  `FormElement` ADD  `encode` ENUM(  'none',  'specialchar' ) NOT NULL DEFAULT  'specialchar' AFTER  `subrecordOption`",
        "UPDATE `FormElement` SET encode='none' WHERE class='native' AND type='editor'",

        "ALTER TABLE  `Form` ADD  `escapeTypeDefault` VARCHAR(32) NOT NULL DEFAULT  'c' AFTER  `permitEdit`",
        "UPDATE `Form` SET `escapeTypeDefault`='-'",
    ],

    '0.18.0' => [
        "ALTER TABLE  `Form` CHANGE  `forwardMode`  `forwardMode` ENUM(  'client',  'no',  'page',  'url',  'url-skip-history' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'client'",
        "UPDATE Form SET forwardMode='url' WHERE forwardMode='page'",
        "ALTER TABLE  `Form` CHANGE  `forwardMode`  `forwardMode` ENUM(  'client',  'no',  'url',  'url-skip-history' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'client'",
    ],

    '0.18.3' => [
        "ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra',  'gridJQW',  'text',  'editor',  'time', 'note',  'password',  'radio',  'select',  'subrecord',  'upload',  'fieldset',  'pill',  'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete', 'afterLoad',  'afterSave',  'afterInsert',  'afterUpdate',  'afterDelete',  'sendMail',  'paste' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text';",
        "ALTER TABLE  `Form` CHANGE  `forwardMode`  `forwardMode` ENUM(  'client',  'no',  'url',  'url-skip-history',  'url-sip' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'client';",
    ],

    '0.19.0' => [
        "ALTER TABLE  `Form` ADD  `dirtyMode` ENUM( 'exclusive', 'advisory', 'none' ) NOT NULL DEFAULT  'exclusive' AFTER  `requiredParameter`",
        "ALTER TABLE  `Form` ADD  `recordLockTimeoutSeconds` INT NOT NULL DEFAULT  '900' AFTER  `parameter`",
        "CREATE TABLE IF NOT EXISTS `Period` (`id` INT(11) NOT NULL AUTO_INCREMENT, `start` DATETIME NOT NULL, `name` VARCHAR(255) NOT NULL, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `created` DATETIME NOT NULL, PRIMARY KEY (`id`), KEY `start` (`start`)) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 0;",
        "INSERT INTO Period (start, name, created) VALUES (NOW(), 'dummy', NOW());"
    ],

    '0.19.2' => [
        "CREATE TABLE IF NOT EXISTS `Cron` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `grId` INT(11) NOT NULL, `type` ENUM('mail', 'website') NOT NULL DEFAULT 'website', `lastRun` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', `lastStatus` TEXT NOT NULL, `nextRun` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', `frequency` VARCHAR(32) NOT NULL, `inProgress` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', `status` ENUM('enable', 'disable') NOT NULL DEFAULT 'enable', `content` TEXT NOT NULL, `comment` TEXT NOT NULL, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', PRIMARY KEY (`id`)) ENGINE = InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8;",
    ],

    '0.19.5' => [
        "ALTER TABLE `Form` ADD `parameterLanguageA` TEXT NOT NULL AFTER `parameter`",
        "ALTER TABLE `Form` ADD `parameterLanguageB` TEXT NOT NULL AFTER `parameterLanguageA`",
        "ALTER TABLE `Form` ADD `parameterLanguageC` TEXT NOT NULL AFTER `parameterLanguageB`",
        "ALTER TABLE `Form` ADD `parameterLanguageD` TEXT NOT NULL AFTER `parameterLanguageC`",

        "ALTER TABLE `FormElement` ADD `parameterLanguageA` TEXT NOT NULL AFTER `parameter`",
        "ALTER TABLE `FormElement` ADD `parameterLanguageB` TEXT NOT NULL AFTER `parameterLanguageA`",
        "ALTER TABLE `FormElement` ADD `parameterLanguageC` TEXT NOT NULL AFTER `parameterLanguageB`",
        "ALTER TABLE `FormElement` ADD `parameterLanguageD` TEXT NOT NULL AFTER `parameterLanguageC`",
    ],

    '0.21.0' => [
        "ALTER TABLE  `Form` CHANGE  `requiredParameter`  `requiredParameterNew` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  ''",
        "ALTER TABLE  `Form` ADD  `requiredParameterEdit` VARCHAR( 255 ) NOT NULL AFTER  `requiredParameterNew`",
        "UPDATE Form SET requiredParameterEdit=requiredParameterNew",
    ],

    '0.24.0' => [
        "ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra',  'gridJQW',  'text',  'editor',  'time',  'note',  'password',  'radio',  'select',  'subrecord',  'upload', 'annotate',  'fieldset',  'pill',  'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete',  'afterLoad',  'afterSave',  'afterInsert',  'afterUpdate',  'afterDelete',  'sendMail',  'paste' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text';",
    ],

    '0.25.0' => [
        "ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra',  'gridJQW',  'text',  'editor',  'annotate',  'time',  'note',  'password',  'radio',  'select', 'subrecord',  'upload',  'annotate',  'fieldset',  'pill',  'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete',  'afterLoad',  'afterSave',  'afterInsert',  'afterUpdate',  'afterDelete', 'sendMail',  'paste' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text';",
    ],

    '0.25.3' => [
        "ALTER TABLE `MailLog` ADD `xId2` INT NOT NULL AFTER `xId`, ADD `xId3` INT NOT NULL AFTER `xId2`",
    ],

    '0.25.7' => [
        "ALTER TABLE `MailLog` CHANGE `attach` `attach` VARCHAR(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''",
    ],

    '0.25.10' => [
        "ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra',  'gridJQW',  'text',  'editor',  'annotate',  'imageCut', 'time',  'note',  'password',  'radio',  'select', 'subrecord',  'upload',  'fieldset',  'pill',  'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete',  'afterLoad',  'afterSave',  'afterInsert',  'afterUpdate',  'afterDelete', 'sendMail',  'paste' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text';",
    ],

    '0.25.11' => [
        "UPDATE FormElement SET checkType = 'alnumx', checkPattern = '', parameter = CONCAT(parameter, '\nmin = ', SUBSTRING_INDEX(checkPattern, '|', 1), '\nmax = ', SUBSTRING_INDEX(checkPattern, '|', -1)) WHERE checkType LIKE 'min|max%' AND checkPattern <> ''",
        "ALTER TABLE `FormElement` CHANGE `checkType` `checkType` ENUM('alnumx','digit','numerical','email','pattern','allbut','all') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'alnumx';",
    ],

    '0.25.12' => [
        "ALTER TABLE `FormElement` CHANGE `checkType` `checkType` ENUM('auto','alnumx','digit','numerical','email','pattern','allbut','all') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'auto';",
        "ALTER TABLE `FormElement` CHANGE `label` `label` VARCHAR(511) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''",
    ],

    '18.04.0' => [
        "ALTER TABLE `Cron` ADD `outputFile` VARCHAR(255) NOT NULL AFTER `comment`",
        "ALTER TABLE `Cron` ADD `outputMode` ENUM('overwrite','append') NOT NULL DEFAULT 'append' AFTER `outputFile`",
        "ALTER TABLE `Cron` ADD `outputPattern` VARCHAR(255) NOT NULL AFTER `outputMode`",
    ],

    '18.6.0' => [
        "ALTER TABLE `Form` CHANGE `forwardMode` `forwardMode` ENUM('auto', 'client','no','url','url-skip-history','url-sip') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'client'",
        "UPDATE `Form` SET forwardMode='auto' WHERE forwardMode='client'",
        "ALTER TABLE `Form` CHANGE `forwardMode` `forwardMode` ENUM('auto', 'close', 'no','url','url-skip-history','url-sip') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'auto';",
    ],

    '18.8.2' => [
        "ALTER TABLE `Form` CHANGE `title` `title` VARCHAR(511) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''",
    ],

    '18.9.1' => [
        "ALTER TABLE `Form` ADD `primaryKey` VARCHAR(255) NOT NULL DEFAULT '' AFTER `tableName`",
    ],

    '18.10.2' => [
        "ALTER TABLE `MailLog` ADD `cc` TEXT NOT NULL AFTER `receiver`, ADD `bcc` TEXT NOT NULL AFTER `cc`;",
    ],

    '19.1.2' => [
        "ALTER TABLE `Form` ADD `labelAlign` ENUM('default','left','center','right') NOT NULL DEFAULT 'default' AFTER `forwardPage`;",
        "ALTER TABLE `FormElement` ADD `labelAlign` ENUM('default','left','center','right') NOT NULL DEFAULT 'default' AFTER `maxLength`;",
    ],

    '19.2.3' => [
        "ALTER TABLE `Form` ADD `restMethod` SET('get','post','put','delete') NOT NULL DEFAULT '' AFTER `permitEdit`; ",
    ],

    '19.3.2' => [
        "ALTER TABLE  `Form` CHANGE  `forwardMode`  `forwardMode` ENUM('auto','close','no','url','url-skip-history','url-sip','url-sip-skip-history' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'auto';",
        "UPDATE `Form` SET forwardMode='url-sip-skip-history' WHERE forwardMode='url-sip'",
    ],

    '19.7.2' => [
        "ALTER TABLE `Form` CHANGE `forwardPage` `forwardPage` VARCHAR(511) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';",
    ],

    '19.9.1' => [
        "ALTER TABLE `Cron` ADD `xId` INT(11) NOT NULL DEFAULT 0 AFTER `grId`; ",
        "ALTER TABLE `Cron` ADD `autoGenerated` ENUM('yes','no') NOT NULL DEFAULT 'no' AFTER `outputPattern`; ",
    ],

);



