<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 4/25/16
 * Time: 10:39 PM
 */

namespace IMATHUZH\Qfq\Core;

 
use IMATHUZH\Qfq\Core\Helper\Logger;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Store\Session;
use IMATHUZH\Qfq\Core\Store\Store;
use IMATHUZH\Qfq\Core\Helper\HelperFile;

/**
 * Class File
 * @package qfq
 */
class File {

    private $uploadErrMsg = array();

    /**
     * @var Store
     */
    private $store = null;

    /**
     * @var Session
     */
    private $session = null;

    private $qfqLogFilename = '';

    /**
     * @param bool|false $phpUnit
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function __construct($phpUnit = false) {

        #TODO: rewrite $phpUnit to: "if (!defined('PHPUNIT_QFQ')) {...}"
        $this->session = Session::getInstance($phpUnit);

        $this->store = Store::getInstance('', $phpUnit);
        $this->qfqLogFilename = $this->store->getVar(SYSTEM_QFQ_LOG, STORE_SYSTEM);

        $this->uploadErrMsg = [
            UPLOAD_ERR_INI_SIZE => "The uploaded file exceeds the upload_max_filesize directive in php.ini",
            UPLOAD_ERR_FORM_SIZE => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
            UPLOAD_ERR_PARTIAL => "The uploaded file was only partially uploaded",
            UPLOAD_ERR_NO_FILE => "No file was uploaded",
            UPLOAD_ERR_NO_TMP_DIR => "Missing a temporary folder",
            UPLOAD_ERR_CANT_WRITE => "Failed to write file to disk",
            UPLOAD_ERR_EXTENSION => "File upload stopped by extension",
        ];
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function process() {

        $sipUpload = $this->store->getVar(SIP_SIP, STORE_SIP);
        if ($sipUpload === false) {

            if (empty($_FILES)) {
                throw new \UserFormException('Missing $_FILES[] - the whole request seems broken', ERROR_UPLOAD_FILES_BROKEN);
            }

            throw new \UserFormException('SIP invalid: ' . $sipUpload, ERROR_SIP_INVALID);
        }

        // Throws an exception if content is too big - if content is bigger than 'post_max_size', the POST is lost together with the PHP Upload error message.
        $size = $_SERVER['CONTENT_LENGTH'] ?? 0;
        $this->checkMaxFileSize($size);

        $statusUpload = $this->store->getVar($sipUpload, STORE_EXTRA, SANITIZE_ALLOW_ALL);
        if ($statusUpload === false) {
            $statusUpload = array();
        }

        $action = $this->store->getVar(FILE_ACTION, STORE_CLIENT, SANITIZE_ALLOW_ALNUMX);
        switch ($action) {
            case FILE_ACTION_UPLOAD:
                $this->doUpload($sipUpload, $statusUpload);
                break;
            case FILE_ACTION_DELETE:
                $this->doDelete($sipUpload, $statusUpload);
                break;
            default:
                throw new \UserFormException("Unknown FILE_ACTION: $action", ERROR_UPLOAD_UNKNOWN_ACTION);
        }
    }

    /**
     * Checks the max file size, defined per FormElement.
     * Only possible if there is a valid SIP Store.
     *
     * @param $size
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function checkMaxFileSize($size) {

        // Checked and set during form build.
        $maxFileSize = $this->store->getVar(FE_FILE_MAX_FILE_SIZE, STORE_SIP . STORE_ZERO);

        if ($size >= $maxFileSize) {
            throw new \UserFormException('File too big. Max allowed size: ' . $maxFileSize . ' Bytes', ERROR_UPLOAD_TOO_BIG);
        }
    }

    /**
     * doUpload
     *
     * @param string $sipUpload
     * @param array $statusUpload
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function doUpload($sipUpload, array $statusUpload) {

        // New upload
        $newArr = reset($_FILES);
        // Merge new upload date to existing status information
        $statusUpload = array_merge($statusUpload, $newArr);

        if ($statusUpload[FILES_ERROR] !== UPLOAD_ERR_OK) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'Upload: Error', ERROR_MESSAGE_TO_DEVELOPER => $this->uploadErrMsg[$newArr[FILES_ERROR]]]),
                ERROR_UPLOAD);
        }

        Logger::logMessageWithPrefix(UPLOAD_LOG_PREFIX . ': File under ' . $statusUpload['tmp_name'], $this->qfqLogFilename);

        $this->checkMaxFileSize($statusUpload['size']);

        $accept = $this->store->getVar(FE_FILE_MIME_TYPE_ACCEPT, STORE_SIP);
        if ($accept != '' && !HelperFile::checkFileType($statusUpload['tmp_name'], $statusUpload['name'], $accept)) {
            throw new \UserFormException('Filetype not allowed. Allowed: ' . $accept, ERROR_UPLOAD_FILE_TYPE);
        }

        // rename uploaded file: ?.cached
        $filenameCached = Support::extendFilename($statusUpload[FILES_TMP_NAME], UPLOAD_CACHED);

        error_clear_last();
        if (!move_uploaded_file($newArr[FILES_TMP_NAME], $filenameCached)) {
            $msg = error_get_last();
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'Upload: Error', ERROR_MESSAGE_TO_DEVELOPER => $msg]),
                ERROR_UPLOAD_FILE_TYPE);
        }

        $this->store->setVar($sipUpload, $statusUpload, STORE_EXTRA);
    }

    /**
     * @param $sipUpload
     * @param $statusUpload
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @internal param string $keyStoreExtra
     */
    private function doDelete($sipUpload, $statusUpload) {

        if (isset($statusUpload[FILES_TMP_NAME]) && $statusUpload[FILES_TMP_NAME] != '') {
            $file = Support::extendFilename($statusUpload[FILES_TMP_NAME], UPLOAD_CACHED);
            if (file_exists($file)) {
                HelperFile::unlink($file, $this->qfqLogFilename);
            }
            $statusUpload[FILES_TMP_NAME] = '';
        }

        $statusUpload[FILES_FLAG_DELETE] = '1';
        $this->store->setVar($sipUpload, $statusUpload, STORE_EXTRA);
    }
}