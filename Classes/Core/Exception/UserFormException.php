<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/1/16
 * Time: 12:43 PM
 */

use IMATHUZH\Qfq\Core\Exception\AbstractException;
use IMATHUZH\Qfq\Core\Store\Store;

/**
 * Class \UserFormException
 *
 * Thrown by Form or FormElement on User errors
 *
 * Throw with ONE message
 *
 *   throw new \UserFormException('Failed: chmod ....', ERROR_IO_CHMOD);
 *
 * Throw with message for User and message for Support.
 *
  * throw new \UserFormException(  json_encode(
                                   * [ERROR_MESSAGE_TO_USER => 'Failed: chmod',
                                    * ERROR_MESSAGE_SUPPORT => "Failed: chmod $mode '$pathFileName'",
                                    * ERROR_MESSAGE_HTTP_STATUS => 'HTTP/1.0 409 Bad Request' ]),
                                * ERROR_IO_CHMOD);
 *
 * Call
 *
 * @package Exception
 */
class UserFormException extends AbstractException {

    /**
     * $this->getMessage() might give a) a simple string or b) an JSON String.
     *
     * JSON String: There are 3+1 different messages:
     *   [ERROR_MESSAGE_TO_USER] 'toUser' - shown in the client to the user - no details here!!!
     *   [ERROR_MESSAGE_SUPPORT] 'support' - help for the developer
     *   [ERROR_MESSAGE_OS] 'os' - message from the OS, like 'file not found'
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function formatMessage() {

        $this->messageArrayDebug[EXCEPTION_TYPE] = 'User Form Exception';

        $this->messageArrayDebug[EXCEPTION_FORM] = Store::getVar(SYSTEM_FORM, STORE_SYSTEM);
        $this->messageArrayDebug[EXCEPTION_FORM_ELEMENT] = Store::getVar(SYSTEM_FORM_ELEMENT, STORE_SYSTEM);
        $this->messageArrayDebug[EXCEPTION_FORM_ELEMENT_COLUMN] = Store::getVar(SYSTEM_FORM_ELEMENT_COLUMN, STORE_SYSTEM);

        return parent::formatException();
    }
}
