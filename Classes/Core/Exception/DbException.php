<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/1/16
 * Time: 12:43 PM
 */

use IMATHUZH\Qfq\Core\Exception\AbstractException;
use IMATHUZH\Qfq\Core\Store\Store;

/**
 * Class UserException
 *
 * Thrown by Form or FormElement on User errors
 *
 * Throw with ONE message
 *
 *   throw new \UserFormException('Failed: chmod ....', ERROR_IO_CHMOD);
 *
 * Throw with message for User and message for Support.
 *
 * throw new \UserFormException(  json_encode(
 *                                  [ERROR_MESSAGE_TO_USER => 'Failed: chmod',
 *                                   ERROR_MESSAGE_SUPPORT => "Failed: chmod $mode '$pathFileName'"]),
 *                               ERROR_IO_CHMOD);
 *
 * @package Exception
 */
class DbException extends AbstractException {

    /**
     * $this->getMessage() might give a) a simple string or b) an JSON String.
     *
     * JSON String: There are 3+1 different messages:
     *   [ERROR_MESSAGE_TO_USER] 'toUser' - shown in the client to the user - no details here!!!
     *   [ERROR_MESSAGE_SUPPORT] 'support' - help for the developer
     *   [ERROR_MESSAGE_OS] 'os' - message from the OS, like 'file not found'
     *
      throw new \UserFormException(  json_encode(
                                       [ERROR_MESSAGE_TO_USER => 'Failed: chmod',
                                        ERROR_MESSAGE_SUPPORT => "Failed: chmod $mode '$pathFileName'",
                                        ERROR_MESSAGE_OS => 'os' - message from the OS, like 'file not found'
                                        ERROR_MESSAGE_HTTP_STATUS => 'HTTP/1.0 409 Bad Request' ]),
                                    ERROR_IO_CHMOD);
     *
     * @return string HTML formatted error string
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function formatMessage() {

        $this->messageArrayDebug[EXCEPTION_TYPE] = 'Db Exception';

        $this->messageArray[EXCEPTION_FORM] = Store::getVar(SYSTEM_FORM, STORE_SYSTEM);
        $this->messageArray[EXCEPTION_FORM_ELEMENT] = Store::getVar(SYSTEM_FORM_ELEMENT, STORE_SYSTEM);
        $this->messageArray[EXCEPTION_FORM_ELEMENT_COLUMN] = Store::getVar(SYSTEM_FORM_ELEMENT_COLUMN, STORE_SYSTEM);

        $this->messageArrayDebug[EXCEPTION_SQL_RAW] = Store::getVar(SYSTEM_SQL_RAW, STORE_SYSTEM);
        $this->messageArrayDebug[EXCEPTION_SQL_FINAL] = Store::getVar(SYSTEM_SQL_FINAL, STORE_SYSTEM);
        $this->messageArrayDebug[EXCEPTION_SQL_PARAM_ARRAY] = Store::getVar(SYSTEM_SQL_PARAM_ARRAY, STORE_SYSTEM);
        $this->messageArrayDebug[EXCEPTION_REPORT_FULL_LEVEL] = Store::getVar(SYSTEM_REPORT_FULL_LEVEL, STORE_SYSTEM);

        return parent::formatException();
    }
}
