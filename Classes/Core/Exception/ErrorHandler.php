<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 2/7/16
 * Time: 7:59 PM
 */

namespace IMATHUZH\Qfq\Core\Exception;

use IMATHUZH\Qfq\Core\Store\Store;

/**
 * Class ErrorHandler
 * @package qfq
 */
class ErrorHandler {


    /**
     * @param $severity
     * @param $message
     * @param $file
     * @param $line
     * @return bool|string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function exception_error_handler($severity, $message, $file, $line) {

        if (!(error_reporting() & $severity)) {
            // This error code is not included in error_reporting
            return false;
        }

        $store = Store::getInstance();
        if($store->getVar(SYSTEM_THROW_GENERAL_ERROR, STORE_SYSTEM) == 'yes'){

            // Do not show too much to the user. E.g. 'ldap_bind()' might have problems, but the user should not see the
            // file and line number. Often the filename is part of the message >> don't show the message to the user.
            throw new \CodeException(json_encode(
                [ERROR_MESSAGE_TO_USER => 'General error - please report.',
                    ERROR_MESSAGE_TO_DEVELOPER => "File: $file / Line: $line / $message"]), $severity, null);

        }

        return true;
    }

}