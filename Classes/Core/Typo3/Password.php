<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 2/1/19
 * Time: 10:31 PM
 */

namespace IMATHUZH\Qfq\Core\Typo3;

/**
 * Class FePassword
 * @package qfq
 */
class Password {

    /**
     * Based on https://docs.typo3.org/typo3cms/extensions/saltedpasswords/8.7/DevelopersGuide/Index.html
     * Convert a cleartext password to a hash. Respects if 'salted passwords' are enabled.
     *
     * @param string $newPassword
     * @return string
     */
    public static function getHash($newPassword) {

        $saltedPassword = md5($newPassword);  // Use md5 as fallback
        self::t3AutoloadIfNotRunning();
        if (\TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility::isUsageEnabled('FE')) {
            $objSalt = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance(NULL);
            if (is_object($objSalt)) {
                $saltedPassword = $objSalt->getHashedPassword($newPassword);
            }
        }
        return $saltedPassword;
    }

    /**
     * Based on https://docs.typo3.org/typo3cms/extensions/saltedpasswords/8.7/DevelopersGuide/Index.html
     * Check if the salted password corresponds to the password.
     *
     * @param string $saltedPassword
     * @param string $password
     * @return bool
     */
    public static function checkPassword($saltedPassword, $password) {

        self::t3AutoloadIfNotRunning();
        $success = FALSE;
        if (\TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility::isUsageEnabled('FE')) {
            $objSalt2 = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance($saltedPassword);
            if (is_object($objSalt2)) {
                $success = $objSalt2->checkPassword($password, $saltedPassword);
            }
        }
        return $success;
    }

    /**
     * Load Typo3 autoloader if Typo3 is not instantiated
     */
    public static function t3AutoloadIfNotRunning() {

        if (!class_exists('\TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility')) {
            //TODO: Get absolute 'path' via QFQ config
            require __DIR__ . '/../../../../../../typo3_src/vendor/autoload.php';

            // run typo3 bootstrap if not yet happened. Necessary if run in unittest.
            if (!defined('TYPO3_MODE')) {
                \TYPO3\CMS\Core\Core\Bootstrap::getInstance()
                    ->setRequestType(TYPO3_REQUESTTYPE_AJAX)
                    ->baseSetup(0);
//                Alternate error fix if you don't want to run Typo3 bootstrap:
//                error_reporting(E_ALL & ~(E_STRICT | E_NOTICE | E_DEPRECATED));
                define('TYPO3_MODE', 'FE');
            }
        }
    }
}