<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 16.02.19
 * Time: 18:44
 */

namespace IMATHUZH\Qfq\Core\Typo3;

class Misc
{
    public static function feLogOff()
    {
        $GLOBALS['TSFE']->fe_user->logoff();
    }
}