<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 7/9/17
 * Time: 3:14 PM
 */

namespace IMATHUZH\Qfq\Core\Store;

use IMATHUZH\Qfq\Core\Helper\Sanitize;
use IMATHUZH\Qfq\Core\Helper\OnString;

/**
 * Class Client
 * @package qfq
 */
class Client {

    /**
     * @return array|string
     * @throws \CodeException
     */
    public static function getParam() {

        // copy GET and POST and SERVER Parameter. Priority: SERVER, POST, GET
        $get = array();
        $post = array();
        $cookie = array();
        $server = array();

        // Dirty workaround to clean poisoned T3 cache
        Sanitize::digitCheckAndCleanGet(CLIENT_PAGE_TYPE);
        Sanitize::digitCheckAndCleanGet(CLIENT_PAGE_LANGUAGE);

        $header = self::getHeader();

        if (isset($_GET)) {
            $get = $_GET; // do not use urldecode() - http://php.net/manual/de/function.urldecode.php#refsect1-function.urldecode-notes
        }

        if (isset($_POST)) {
            $post = $_POST;
        }

        if (isset($_COOKIE[SESSION_NAME])) {
            $cookie[CLIENT_COOKIE_QFQ] = $_COOKIE[SESSION_NAME];
        }

        if (isset($_SERVER)) {
            $server = Sanitize::htmlentitiesArr($_SERVER); // $_SERVER values might be compromised.
        }

        // Necessary for phpUnit Tests
        if (!isset($server[CLIENT_REMOTE_ADDRESS])) {
            $server[CLIENT_REMOTE_ADDRESS] = '0.0.0.0';
        }

        // It's important to merge the SERVER array last: those entries shall overwrite client values.
        $arr = array_merge($header, $get, $post, $cookie, $server);

        return Sanitize::normalize($arr);
    }

    /**
     * @return array
     */
    private static function getHeader() {

        $arr = array();

        // getallheaders() does not exist for phpunit tests
        if (!function_exists('getallheaders')) {
            return array();
        }

        $headers = getallheaders();

        foreach ([HTTP_HEADER_AUTHORIZATION] as $key) {
            if (isset($headers[$key])) {
                $line = $headers[$key];

                $delimiter = (strpos($line, '=') === false) ? ':' : '=';

                // Header: 'Authorization: Token token=1234'
                $split = explode($delimiter, $line, 2);
                if (isset($split[1])) {
                    $arr[$key] = OnString::trimQuote($split[1]);
                }
            }
        }

        return $arr;
    }
}