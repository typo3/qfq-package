<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 4/8/17
 * Time: 6:53 PM
 */

namespace IMATHUZH\Qfq\Core\Store;


/**
 * Class T3Info
 * @package qfq
 */
class T3Info {

    /**
     * Collect some global T3 variables and return them as array.
     *
     * @return array
     */
    public static function getVars() {
        $t3vars = array();

        $t3vars[TYPO3_FE_USER] = isset($GLOBALS["TSFE"]->fe_user->user["username"]) ? $GLOBALS["TSFE"]->fe_user->user["username"] : '';

        $t3vars[TYPO3_FE_USER_UID] = isset($GLOBALS["TSFE"]->fe_user->user["uid"]) ? $GLOBALS["TSFE"]->fe_user->user["uid"] : '';

        $t3vars[TYPO3_FE_USER_GROUP] = isset($GLOBALS["TSFE"]->fe_user->user["usergroup"]) ? $GLOBALS["TSFE"]->fe_user->user["usergroup"] : '';

        // Will be set later
        $t3vars[TYPO3_TT_CONTENT_UID] = 'no set yet';

        $t3vars[TYPO3_PAGE_ID] = isset($GLOBALS["TSFE"]->id) ? $GLOBALS["TSFE"]->id : '';

        $t3vars[TYPO3_PAGE_ALIAS] = empty($GLOBALS["TSFE"]->page["alias"]) ? $t3vars[TYPO3_PAGE_ID] : $GLOBALS["TSFE"]->page["alias"];

        $t3vars[TYPO3_PAGE_TITLE] = isset($GLOBALS["TSFE"]->page["title"]) ? $GLOBALS["TSFE"]->page["title"] : '';

        $t3vars[TYPO3_PAGE_TYPE] = isset($GLOBALS["TSFE"]->type) ? $GLOBALS["TSFE"]->type : '';

        $t3vars[TYPO3_PAGE_LANGUAGE] = isset($GLOBALS["TSFE"]->sys_language_uid) ? $GLOBALS["TSFE"]->sys_language_uid : '';

        $t3vars[TYPO3_BE_USER_LOGGED_IN] = (isset($GLOBALS["TSFE"]->beUserLogin) && $GLOBALS["TSFE"]->beUserLogin === true) ? 'yes' : 'no';

        $t3vars[TYPO3_BE_USER] = isset($GLOBALS["BE_USER"]->user["username"]) ? $GLOBALS["BE_USER"]->user["username"] : '';

        return $t3vars;
    }
}