<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/1/16
 * Time: 6:51 PM
 */

namespace IMATHUZH\Qfq\Core\Store;

use IMATHUZH\Qfq\Core\Helper\KeyValueStringParser;
use IMATHUZH\Qfq\Core\Helper\OnArray;
use IMATHUZH\Qfq\Core\Helper\Sanitize;
use IMATHUZH\Qfq\Core\Helper\Logger;
use IMATHUZH\Qfq\Core\Database\Database;
 
use IMATHUZH\Qfq\Core\Helper\Support;

/*
 * Stores:
 * - SIP
 * - webVar
 * - record
 * - form
 * - formElement
 */

/**
 * Class Store
 * @package qfq
 */
class Store {

    /**
     * @var Store Instance of class Store. There should only be one class 'Store' at a time.
     */
    private static $instance = null;

    /**
     * @var Sip Instance of class SIP
     */
    private static $sip = null;

    /**
     * @var Session Instance of class Session
     */
//    private static $session = null;

    /**
     * @var array Stores all indiviudal stores with the variable raw values
     *
     * $raw['D']['id'] = 0  - Defaultvalues from Tabledefinition
     * ...
     * $raw['S']['r'] = 1234 - record ID from current SIP identifier
     * ...
     * $raw['C']['HTTP_SERVER'] = 'qfq' - Servername
     * $raw['C']['s'] = 'badcaffee1234' - recent SIP
     */
    private static $raw = array();

    /**
     * @var array Default sanitize classes.
     */
    private static $sanitizeClass = array();

    /**
     * $sanitizeClass['S'] = false
     * $sanitizeClass['C'] = true
     * ...
     *
     * @var array each entry with true/false - depending if store needs to be sanitized.
     */
    private static $sanitizeStore = array();

    private static $phpUnit = false;


    /**
     * @param string $bodytext
     * @param string $fileConfigPhp
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function __construct($bodytext = '', $fileConfigPhp = '') {

//        self::$session = Session::getInstance(self::$phpUnit);

        // This check is critical for some unwanted exception recursion during startup.
        if (!function_exists('normalizer_normalize')) {
            throw new \CodeException("Function normalizer_normalize() not found - Please install 'php-intl'", ERROR_MISSING_INTL);
        }

        self::$sanitizeClass = [
//            TYPO3_DEBUG_LOAD => SANITIZE_ALLOW_DIGIT,
//            TYPO3_DEBUG_SAVE => SANITIZE_ALLOW_DIGIT,
//            TYPO3_FORM => SANITIZE_ALLOW_ALNUMX,
//            TYPO3_FE_USER => SANITIZE_ALLOW_ALNUMX,
//            TYPO3_FE_USER_UID => SANITIZE_ALLOW_DIGIT,
//            TYPO3_FE_USER_GROUP => SANITIZE_ALLOW_ALNUMX,

            CLIENT_SIP => SANITIZE_ALLOW_ALNUMX,
            CLIENT_TYPO3VARS => SANITIZE_ALLOW_ALNUMX,
            CLIENT_RECORD_ID => SANITIZE_ALLOW_DIGIT,
            CLIENT_KEY_SEM_ID => SANITIZE_ALLOW_DIGIT,
            CLIENT_KEY_SEM_ID_USER => SANITIZE_ALLOW_DIGIT,
            CLIENT_PAGE_ID => SANITIZE_ALLOW_DIGIT,
            CLIENT_PAGE_TYPE => SANITIZE_ALLOW_DIGIT,
            CLIENT_PAGE_LANGUAGE => SANITIZE_ALLOW_DIGIT,
            CLIENT_FORM => SANITIZE_ALLOW_ALNUMX,

            // Part of $_SERVER. Missing vars must be requested individual with the needed sanitize class.
            CLIENT_SCRIPT_URL => SANITIZE_ALLOW_ALNUMX,
            CLIENT_SCRIPT_URI => SANITIZE_ALLOW_ALNUMX,
            CLIENT_HTTP_HOST => SANITIZE_ALLOW_ALNUMX,
            CLIENT_HTTP_USER_AGENT => SANITIZE_ALLOW_ALNUMX,
            CLIENT_SERVER_NAME => SANITIZE_ALLOW_ALNUMX,
            CLIENT_SERVER_ADDRESS => SANITIZE_ALLOW_ALNUMX,
            CLIENT_SERVER_PORT => SANITIZE_ALLOW_DIGIT,
            CLIENT_REMOTE_ADDRESS => SANITIZE_ALLOW_ALNUMX,
            CLIENT_REQUEST_SCHEME => SANITIZE_ALLOW_ALNUMX,
            CLIENT_REQUEST_METHOD => SANITIZE_ALLOW_ALNUMX,
            CLIENT_SCRIPT_FILENAME => SANITIZE_ALLOW_ALNUMX,
            CLIENT_QUERY_STRING => SANITIZE_ALLOW_ALL,
            CLIENT_REQUEST_URI => SANITIZE_ALLOW_ALL,
            CLIENT_SCRIPT_NAME => SANITIZE_ALLOW_ALNUMX,
            CLIENT_PHP_SELF => SANITIZE_ALLOW_ALNUMX,
//            CLIENT_UPLOAD_FILENAME => SANITIZE_ALLOW_ALLBUT,

//            SYSTEM_DBUSER => SANITIZE_ALLOW_ALNUMX,
//            SYSTEM_DBSERVER => SANITIZE_ALLOW_ALNUMX,
//            SYSTEM_DBPW => SANITIZE_ALLOW_ALL,
//            SYSTEM_DB => SANITIZE_ALLOW_ALNUMX,
//            SYSTEM_TESTDB => SANITIZE_ALLOW_ALNUMX,
//            SYSTEM_SESSIONNAME => SANITIZE_ALLOW_ALNUMX,
//            SYSTEM_DBH => SANITIZE_ALLOW_ALL,

//            SYSTEM_SQL_RAW => SANITIZE_ALLOW_ALL,
//            SYSTEM_SQL_FINAL => SANITIZE_ALLOW_ALL,
//            SYSTEM_SQL_COUNT => SANITIZE_ALLOW_DIGIT,
//            SYSTEM_SQL_PARAM_ARRAY => SANITIZE_ALLOW_ALL,

//            SIP_SIP => SANITIZE_ALLOW_ALNUMX,
//            SIP_RECORD_ID => SANITIZE_ALLOW_DIGIT,
//            SIP_FORM => SANITIZE_ALLOW_ALNUMX,
//            SIP_URLPARAM => SANITIZE_ALLOW_ALL

        ];

        self::$sanitizeStore = [
            STORE_FORM => true,
            STORE_SIP => false,
            STORE_RECORD => false,
            STORE_BEFORE => false,
            STORE_PARENT_RECORD => false,
            STORE_TABLE_DEFAULT => false,
            STORE_TABLE_COLUMN_TYPES => false,
            STORE_CLIENT => true,
            STORE_TYPO3 => false,
            STORE_VAR => false,
            STORE_ZERO => false,
            STORE_EMPTY => false,
            STORE_SYSTEM => false,
            STORE_EXTRA => false,
            STORE_USER => false,
            STORE_LDAP => false,
            STORE_ADDITIONAL_FORM_ELEMENTS => false,
        ];

        self::fillStoreTypo3($bodytext);  // should be filled before fillStoreSystem() to offer T3 variables, especially 'feUser'
        self::fillStoreClient();  // should be filled before fillStoreSystem() to offer Client variables
        self::fillStorePhpSession(STORE_EXTRA); // should be filled before fillStoreSystem() to restore variables like SYSTEM_SHOW_DEBUG_INFO
        self::fillStorePhpSession(STORE_USER); // should be filled before fillStoreSystem() to restore variables like SYSTEM_SHOW_DEBUG_INFO
        self::fillStoreSystem($fileConfigPhp);
        self::fillStoreSip();
    }

    /**
     * Returns a pointer to this Class.
     *
     * @param string $bodytext
     * @param bool|false $phpUnit
     * @param string $qfqConfigPhpUnit
     *
     * @return null|Store
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function getInstance($bodytext = '', $phpUnit = false, $qfqConfigPhpUnit = '') {

        if (defined('PHPUNIT_QFQ')) {
            self::$phpUnit = true;
        }

        if (self::$phpUnit) {

            // If $qfqConfigPhpUnit is given: clean STORE
//            if (self::$instance !== null && $qfqConfigPhpUnit != '') {
            if ($qfqConfigPhpUnit != '') {

                if ($qfqConfigPhpUnit == 'init') {
                    $qfqConfigPhpUnit = '';
                }
                // fake to have a clean environment for the next test.
                self::unsetStore(STORE_TYPO3);
                self::fillStoreTypo3($bodytext);

                self::unsetStore(STORE_CLIENT);
                self::fillStoreClient();
            }

            // Testing different config files means initialize completely
            if ($qfqConfigPhpUnit != '') {
                self::$instance = null;
            }
        }

        // Design Pattern: Singleton
        if (self::$instance === null) {
//            self::$phpUnit = $phpUnit;

            self::$instance = new self($bodytext, $qfqConfigPhpUnit);
        } else {
            // Class Store seems to be persistent over multiple QFQ instantiation. Set bodytext again, with every new request (if bodytext is given).
            if ($bodytext !== '') {
                self::fillStoreTypo3($bodytext);
                self::unsetStore(STORE_RECORD); // At least in Multi DB Setup: STORE_RECORD of former  tt-content QFQ Records interfere with loading a form.
            }
        }

        // Disable TYPO3_DEBUG_SHOW_BODY_TEXT=1 if SYSTEM_SHOW_DEBUG_INFO!='yes'
        if (self::getVar(TYPO3_DEBUG_SHOW_BODY_TEXT, STORE_TYPO3) === '1' &&
            !Support::findInSet(SYSTEM_SHOW_DEBUG_INFO_YES, self::getVar(SYSTEM_SHOW_DEBUG_INFO, STORE_SYSTEM))
        ) {
            self::setVar(TYPO3_DEBUG_SHOW_BODY_TEXT, '0', STORE_TYPO3);
        }

        return self::$instance;
    }

    /**
     * QFQ might be called via Typo3 (index.php) or directly via AJAX (directory: api). The
     *
     * @param array $config
     *
     * @return array
     */
    private static function doSystemPath(array $config) {

        // SYSTEM_PATH_EXT: compute only if not already defined.
        if (!isset($config[SYSTEM_EXT_PATH]) || $config[SYSTEM_EXT_PATH] === '' || $config[SYSTEM_EXT_PATH][0] !== '/') {
            $relExtDir = '/typo3conf/ext/' . EXT_KEY;

            if (defined('PHPUNIT_QFQ')) {
                $cwd = getcwd();
                $pos = strpos($cwd, '/typo3conf/');

                // this means phpUnit.
                $config[SYSTEM_SITE_PATH] = substr($cwd, 0, $pos);
                $config[SYSTEM_EXT_PATH] = $config[SYSTEM_SITE_PATH] . $relExtDir;

            } else {
                // If we are called through AJAX API (e.g. api/save.php), there is no TYPO3 environment.
                $pos = strpos($_SERVER['SCRIPT_FILENAME'] ?? '', $relExtDir);
                if ($pos === false && isset($GLOBALS['TYPO3_LOADED_EXT'][EXT_KEY]['ext_localconf.php'])) {

                    // Typo3 extension: probably index.php
                    $config[SYSTEM_EXT_PATH] = dirname($GLOBALS['TYPO3_LOADED_EXT'][EXT_KEY]['ext_localconf.php']);
                    $config[SYSTEM_SITE_PATH] = dirname($_SERVER['SCRIPT_FILENAME'] ?? '');
                } else {
                    // API
                    $config[SYSTEM_EXT_PATH] = substr($_SERVER['SCRIPT_FILENAME'] ?? '', 0, $pos + strlen($relExtDir));
                    $config[SYSTEM_SITE_PATH] = substr($_SERVER['SCRIPT_FILENAME'] ?? '', 0, $pos);
                }
            }
        }

        Logger::setSystemSitePath($config[SYSTEM_SITE_PATH]);

        return $config;
    }

    /**
     * Depending on some configuration value, update corresponding values.
     *
     * @param array $config
     *
     * @return array
     */
    private static function adjustConfig(array $config) {

        $config[SYSTEM_SHOW_DEBUG_INFO] = self::adjustConfigShowDebugInfo($config[SYSTEM_SHOW_DEBUG_INFO], self::beUserLoggdIn());

        $config[SYSTEM_SEND_E_MAIL] = $config[SYSTEM_EXT_PATH] . '/Classes/External/sendEmail';

        // Make path absolute
        foreach ([SYSTEM_MAIL_LOG, SYSTEM_QFQ_LOG, SYSTEM_SQL_LOG] AS $key) {
            if (!empty($config[$key]) && $config[$key][0] != '/') {
                $config[$key] = $config[SYSTEM_SITE_PATH] . '/' . $config[$key];
            }
        }

        // In case the database credentials are given in the old style: copy them to the new style
        if (!isset($config[SYSTEM_DB_1_USER]) && isset($config[SYSTEM_DB_USER])) {
            $config[SYSTEM_DB_1_USER] = $config[SYSTEM_DB_USER];
            $config[SYSTEM_DB_1_SERVER] = $config[SYSTEM_DB_SERVER];
            $config[SYSTEM_DB_1_PASSWORD] = $config[SYSTEM_DB_PASSWORD];
            $config[SYSTEM_DB_1_NAME] = $config[SYSTEM_DB_NAME];
        }

        if ($config[SYSTEM_THROW_GENERAL_ERROR] == 'auto') {
            $config[SYSTEM_THROW_GENERAL_ERROR] = $config[SYSTEM_FLAG_PRODUCTION] == 'yes' ? 'no' : 'yes';
        }

        return $config;
    }

    /**
     * @param string $value
     *
     * @param $flag
     * @return string
     */
    private static function adjustConfigShowDebugInfo($value, $flag) {

        // Check if SHOW_DEBUG_INFO contains 'auto'. Replace with appropriate.
        if (Support::findInSet(SYSTEM_SHOW_DEBUG_INFO_AUTO, $value) && $flag) {
            $value = str_replace(SYSTEM_SHOW_DEBUG_INFO_AUTO, SYSTEM_SHOW_DEBUG_INFO_YES, $value);
        }

        return $value;
    }

    /**
     * Check if there is a Typo3 beUser is logged in. This is only possible if QFQ is called via T3, not via API
     *
     * @return bool  true: current Browser session is a logged in BE User.
     */
    private static function beUserLoggdIn() {

        return (!empty($GLOBALS["TSFE"]->beUserLogin) && $GLOBALS["TSFE"]->beUserLogin === true);
    }

    /**
     * Iterate over all Parameter which have to exist in the config. Throw an array if any is missing.
     *
     * @param array $config
     *
     * @throws \UserFormException
     */
    private static function checkMandatoryParameter(array $config) {

        // Check mandatory config vars.
        $names = array_merge([SYSTEM_SQL_LOG, SYSTEM_SQL_LOG_MODE],
            self::dbCredentialName($config[SYSTEM_DB_INDEX_DATA]),
            self::dbCredentialName($config[SYSTEM_DB_INDEX_QFQ]));

        foreach ($names as $name) {
            if (!isset($config[$name])) {
                throw new \UserFormException ("Missing configuration in `" . CONFIG_QFQ_PHP . "`: $name", ERROR_MISSING_CONFIG_INI_VALUE);
            }
        }
    }

    /**
     * @param $index
     * @return array
     */
    private static function dbCredentialName($index) {
        $names = array();
        $names[] = 'DB_' . $index . '_USER';
        $names[] = 'DB_' . $index . '_SERVER';
        $names[] = 'DB_' . $index . '_PASSWORD';
        $names[] = 'DB_' . $index . '_NAME';

        return $names;
    }

    /**
     * Set or overwrite a complete store.
     *
     * @param array $dataArray
     * @param string $store
     * @param bool|false $flagOverwrite
     *
     * @throws \UserFormException
     * @throws \CodeException
     */
    public static function setStore(array $dataArray, $store, $flagOverwrite = false) {

        // Check valid store name
        if (!isset(self::$sanitizeStore)) {
            throw new \UserFormException("Unknown Store: $store", ERROR_UNNOWN_STORE);
        }

        if ($store === STORE_ZERO || $store === STORE_EMPTY) {
            throw new \CodeException("setVarArray() for STORE_ZERO/STORE_EMPTY is impossible - there are no values.", ERROR_SET_STORE_ZERO);
        }

        if (!$flagOverwrite && isset(self::$raw[$store]) && count(self::$raw[$store]) > 0) {
            throw new \CodeException("Raw values already been copied to store '$store'. Do this only one time.", ERROR_STORE_VALUE_ALREADY_CODPIED);
        }

        if ($store === STORE_EXTRA || $store === STORE_USER) {
            Session::set($store, $dataArray);
        }

        self::$raw[$store] = $dataArray;
    }

    /**
     * Deletes a store assigning a new empty array to it.
     *
     * @param $store
     *
     * @throws \UserFormException
     * @throws \CodeException
     */
    public static function unsetStore($store) {

        self::setStore(array(), $store, true);
    }

    /**
     * Returns a complete $store.
     *
     * @param string $store STORE_SYSTEM, ...
     *
     * @return array
     * @throws \UserFormException
     * @throws \CodeException
     */
    public static function getStore($store) {
        $vars = array();

        // Check valid store name
        if (!isset(self::$sanitizeStore[$store])) {
            throw new \UserFormException("Unknown Store: $store", ERROR_UNNOWN_STORE);
        }

        if ($store === STORE_ZERO || $store === STORE_EMPTY) {
            throw new \CodeException("getStore() for $store is impossible - there are no values saved.", ERROR_GET_INVALID_STORE);
        }

        if (isset(self::$raw[$store])) {
            $vars = self::$raw[$store];
            if ($store == STORE_SIP) {
                $vars = self::checkDecodeBase64Arr($vars);
            }
        }

        return $vars;
    }


    /**
     * Fill the system store by reading config.qfq.ini. Also setup config defaults.
     *
     * @param string $fileConfigPhp
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private static function fillStoreSystem($fileConfigPhp = '') {

        $config = Config::readConfig($fileConfigPhp);

        $config = self::doSystemPath($config);
        $config = self::adjustConfig($config);
        $config = self::setAutoConfigValue($config);

        self::checkMandatoryParameter($config);

        self::setStore($config, STORE_SYSTEM, true);
    }

    /**
     * Set automatic filled values
     *
     * @param array $config
     * @return array
     */
    public static function setAutoConfigValue(array $config) {

        $config[SYSTEM_DB_NAME_DATA] = $config['DB_' . $config[SYSTEM_DB_INDEX_DATA] . '_NAME'] ?? '';
        $config[SYSTEM_DB_NAME_QFQ] = $config['DB_' . $config[SYSTEM_DB_INDEX_QFQ] . '_NAME'] ?? '';

        return $config;
    }

    /**
     * Copy the BodyText as well as some T3 specific vars to STORE_TYPO3.
     * Attention: if called through API, there is no T3 environment. The only values which are available are fe_user
     * and fe_user_uid.
     *
     * @param $bodytext
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    private static function fillStoreTypo3($bodytext) {

        // form=, showDebugBodyText=, 10.20..
        $arr = KeyValueStringParser::parse($bodytext, "=", "\n");

        if (isset($GLOBALS["TSFE"])) {
            $arr = array_merge($arr, T3Info::getVars());
        } else {

            // No T3 environment (called by API): restore from SESSION
            foreach ([SESSION_FE_USER, SESSION_FE_USER_UID, SESSION_FE_USER_GROUP, SESSION_BE_USER] as $key) {
                if (isset($_SESSION[SESSION_NAME][$key])) {
                    $arr[$key] = $_SESSION[SESSION_NAME][$key];
                }
            }
        }

        self::setStore($arr, STORE_TYPO3, true);
    }

    /**
     * Fills the STORE_CLIENT
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    private static function fillStoreClient() {

        $arr = Client::getParam();

        self::setStore($arr, STORE_CLIENT, true);

    }

    /**
     * Fills the STORE_SIP. Reads therefore specified SIP, decode the values and stores them in STORE_SIP.
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private static function fillStoreSip() {

        self::$sip = new Sip(self::$phpUnit);

        $s = self::getVar(CLIENT_SIP, STORE_CLIENT);
        if ($s !== false) {
            // if session is given, copy values to store
            $param = self::$sip->getVarsFromSip($s);
            $param[SIP_SIP] = $s;
            $param[SIP_URLPARAM] = self::$sip->getQueryStringFromSip($s);

//            self::setVarArray(KeyValueStringParser::parse($param, "=", "&"), STORE_SIP);
            self::setStore($param, STORE_SIP, true);
        }
    }

    /**
     * Fills the STORE_EXTRA.
     *
     * @param string $storeName STORE_EXTRA, STORE_USER
     * @throws \CodeException
     * @throws \UserFormException
     */
    private static function fillStorePhpSession($storeName) {

        $value = Session::get($storeName);

        if (!isset($_SESSION[SESSION_NAME][$storeName]) || $_SESSION[SESSION_NAME][$storeName] === null) {
            $value = false;
        }

        if ($value === false) {
            self::setStore(array(), $storeName, true);
        } else {
            self::setStore($_SESSION[SESSION_NAME][$storeName], $storeName, true);
        }
    }

    /**
     * Fills STORE_TABLE_DEFAULT and STORE_TABLE_COLUMN_TYPES
     *
     * @param array $tableDefinition
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    public static function fillStoreTableDefaultColumnType(array $tableDefinition) {

        self::setStore(array_column($tableDefinition, 'Default', 'Field'), STORE_TABLE_DEFAULT, true);
        self::setStore(array_column($tableDefinition, 'Type', 'Field'), STORE_TABLE_COLUMN_TYPES, true);
    }


    /**
     * Set's a single $key/$value pair $store.
     *
     * @param string $key
     * @param string|array $value
     * @param string $storeName
     * @param bool|true $overWrite
     *
     * @throws \UserFormException
     * @throws \CodeException
     */
    public static function setVar($key, $value, $storeName, $overWrite = true) {
        // Check valid Storename
        if (!isset(self::$sanitizeStore)) {
            throw new \UserFormException("Unknown Store: $storeName", ERROR_UNNOWN_STORE);
        }

        if ($storeName === STORE_ZERO) {
            throw new \CodeException("setVar() for STORE_ZERO is impossible - there are no values.", ERROR_SET_STORE_ZERO);
        }

        // Complain if already set and different.
        if ($overWrite === false && isset(self::$raw[$storeName][$key]) && self::$raw[$storeName][$key] != $value) {
            throw new \UserFormException("Value of '$key' already set in store '$storeName'.", ERROR_STORE_KEY_EXIST);
        }

        self::$raw[$storeName][$key] = $value;

        // The STORE_EXTRA / STORE_USER saves arrays and is persistent
        if ($storeName === STORE_EXTRA || $storeName === STORE_USER) {

            $data = Session::get($storeName);

            if ($data === false) {
                $data = array();
            }

            // Switch FeUser: log this to qfq.log
            if ($storeName === STORE_USER && $key == TYPO3_FE_USER) {
                $qfqLog = self::getVar(SYSTEM_QFQ_LOG, STORE_SYSTEM);
                $feUserOld = isset($data[$key]) ? $data[$key] : self::getVar($key, STORE_TYPO3 . STORE_EMPTY);
                Logger::logMessage(date('Y.m.d H:i:s ') . ": Switch feUser '$feUserOld' to '$value'", $qfqLog);
            }

            $data[$key] = $value;
            Session::set($storeName, $data);
        }
    }

    /**
     * Unset a variable in store.
     *
     * @param $key
     * @param $store
     */
    public static function unsetVar($key, $store) {

        if (isset(self::$raw[$store][$key])) {
            unset(self::$raw[$store][$key]);
        }
    }

    /**
     * Cycles through all stores in $useStore.
     * First match will return the found value.
     * During cycling: fill cache with requested value and sanitize raw value.
     *
     * @param string $key
     * @param string $useStores f.e.: 'FSRVD'
     * @param string $sanitizeClass
     * @param string $foundInStore Returns the name of the store where $key has been found. If $key is not found,
     *                             return ''.
     *
     * @param string $typeMessageViolate
     * @return string|array a) if found: value, b) false. STORE_EXTRA returns an array for the given key.
     * @throws \CodeException
     * @throws \UserFormException    SANITIZE_TYPE_MESSAGE_VIOLATE_ZERO | SANITIZE_TYPE_MESSAGE_VIOLATE_EMPTY | SANITIZE_TYPE_MESSAGE_VIOLATE_CLASS
     */
    public static function getVar($key, $useStores = STORE_USE_DEFAULT, $sanitizeClass = '', &$foundInStore = '',
                                  $typeMessageViolate = SANITIZE_TYPE_MESSAGE_VIOLATE_CLASS) {

        // no store specified?
        if ($useStores === "" || $useStores === null) {
            $useStores = STORE_USE_DEFAULT;
        }

        // no sanitizeClass specified: take predefined (if exist) or default.
        if ($sanitizeClass === '' || $sanitizeClass === null) {
            $sanitizeClass = isset(self::$sanitizeClass[$key]) ? self::$sanitizeClass[$key] : SANITIZE_DEFAULT;
        }

        $len = strlen(SIP_PREFIX_BASE64);

        while ($useStores !== false && $useStores !== '') {
            $store = $useStores[0]; // current store

            $finalKey = $key;
            if ($store == STORE_LDAP) {
                $finalKey = strtolower($key); // in STORE_LDAP all keys are lowercase
            }

            $foundInStore = $store;
            $useStores = substr($useStores, 1); // shift left remaining stores

            if (!isset(self::$raw[$store][$finalKey])) {
                switch ($store) {
                    case STORE_ZERO:
                        return 0;
                    case STORE_EMPTY:
                        return '';
                    case STORE_VAR:
                        if ($finalKey === VAR_RANDOM) {
                            return Support::randomAlphaNum(RANDOM_LENGTH);
                        } else {
                            continue 2;  // no value provided, continue with while loop
                        }
                        break;
                    default:
                        continue 2; // no value provided, continue with while loop
                        break;
                }
            }

            $rawVal = isset(self::$raw[$store][$finalKey]) ? self::$raw[$store][$finalKey] : null;
            if (self::$sanitizeStore[$store] && $sanitizeClass != '') {
                if ($sanitizeClass == SANITIZE_ALLOW_PATTERN) {
                    // We do not have any pattern at this point. For those who be affected, they already checked earlier. So set 'no check'
                    $sanitizeClass = SANITIZE_ALLOW_ALL;
                }

                return Sanitize::sanitize($rawVal, $sanitizeClass, '', '', SANITIZE_EMPTY_STRING, '', $typeMessageViolate);
            } else {
                if ($store == STORE_SIP && (substr($key, 0, $len) == SIP_PREFIX_BASE64)) {
                    $rawVal = base64_decode($rawVal);
                }

                return $rawVal;
            }
        }
        $foundInStore = '';

        return false;
    }

    /**
     * Create a SIP after a form load. This is necessary on forms without a sip and on forms with r=0 (new record).
     *
     * @param $formName
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    public static function createSipAfterFormLoad($formName) {

        $recordId = self::getVar(CLIENT_RECORD_ID, STORE_TYPO3 . STORE_CLIENT);
        if ($recordId === false) {
            $recordId = 0;
        }

        // If there are existing SIP param, keep them by copying to the new SIP Param Array
        $tmpParam = self::getNonSystemSipParam();

        $tmpParam[SIP_RECORD_ID] = $recordId;
        $tmpParam[SIP_FORM] = $formName;

        if ($recordId == 0) {
            // SIPs for 'new records' needs to be uniq per TAB! Therefore add a uniq parameter
            $tmpParam[SIP_MAKE_URLPARAM_UNIQ] = uniqid();
        }

        // Construct fake urlparam
        $tmpUrlparam = OnArray::toString($tmpParam);

        // Create a fake SIP which has never been passed by URL - further processing might expect this to exist.
        $sip = self::getSipInstance()->queryStringToSip($tmpUrlparam, RETURN_SIP);
        self::setVar(CLIENT_SIP, $sip, STORE_CLIENT);

        // Store in SIP Store (cause it's empty until now).
        $tmpParam[SIP_SIP] = $sip;
        self::setStore($tmpParam, STORE_SIP, true);

    }

    /**
     * Return an array with non system SIP parameter. Take the whole STORE_SIP and search for non system parameter.
     *
     * @return array
     * @throws \UserFormException
     * @throws \CodeException
     */
    public static function getNonSystemSipParam() {
        $tmpParam = array();

        $sipArray = self::getStore(STORE_SIP);

        foreach ($sipArray as $key => $value) {
            if ($key[0] === '_') {
                continue;
            }
            switch ($key) {
                case SIP_SIP:
                case SIP_RECORD_ID:
                case SIP_FORM;
                case SIP_URLPARAM:
                    continue;
                default:
                    $tmpParam[$key] = $value;
            }
        }

        return $tmpParam;
    }

    /**
     * Iterate over an array and look for keynames starting with SIP_PREFIX_BASE64.
     * Found elements will be base64_decode().
     *
     * @param array $vars
     *
     * @return array - incl. decoded base64 vars.
     */
    private static function checkDecodeBase64Arr(array $vars) {

        $len = strlen(SIP_PREFIX_BASE64);

        foreach ($vars as $key => $value) {
            if (substr($key, 0, $len) == SIP_PREFIX_BASE64) {
                $vars[$key] = base64_decode($value);
            }
        }

        return $vars;
    }

    /**
     * Returns a pointer to this class.
     *
     * @return null|Sip
     */
    public static function getSipInstance() {
        return self::$sip;
    }

    /**
     * Saves a subset of STORE_TYPO3 vars as a SIP. The SIP will be transmitted as hidden form element.
     *
     * More docs: CODING.md > 'Faking the STORE_TYPO3 for API calls'
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    public static function copyT3VarsToSip() {

        $tempArray = self::getStore(STORE_TYPO3);

        foreach ([TYPO3_FE_USER, TYPO3_FE_USER_UID, TYPO3_FE_USER_GROUP, TYPO3_TT_CONTENT_UID, TYPO3_PAGE_ID, TYPO3_PAGE_ALIAS, TYPO3_PAGE_TYPE, TYPO3_PAGE_LANGUAGE, TYPO3_BE_USER_LOGGED_IN] as $key) {
            if (isset($tempArray[$key])) {
                $t3varsArray[$key] = $tempArray[$key];
            }
        }

        $t3varsArray[TYPO3_BE_USER_LOGGED_IN] = (isset($GLOBALS["TSFE"]->beUserLogin) && $GLOBALS["TSFE"]->beUserLogin === true) ? 'yes' : 'no';

        $t3varsString = KeyValueStringParser::unparse($t3varsArray, '=', '&');
        $t3sip = self::$sip->queryStringToSip($t3varsString, RETURN_SIP);

        return $t3sip;
    }

    /**
     * Get stored STORE_TYPO3 vars from SIP and restore the store.
     *
     * More docs: CODING.md > 'Faking the STORE_TYPO3 for API calls'
     *
     * @param string $sipTypo3Vars
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function fillTypo3StoreFromSip($sipTypo3Vars) {
        $t3vars = self::getStore(STORE_TYPO3);

        // Do nothing if STORE_TYPO3 is already filled
        if (isset($t3vars[TYPO3_TT_CONTENT_UID]) && $t3vars[TYPO3_TT_CONTENT_UID] != '0') {
            return;
        }

        $typo3VarsArray = self::$sip->getVarsFromSip($sipTypo3Vars);

        self::setStore($typo3VarsArray, STORE_TYPO3, true);

        $value = self::getVar(SYSTEM_SHOW_DEBUG_INFO, STORE_SYSTEM);
        $flag = isset($typo3VarsArray[TYPO3_BE_USER_LOGGED_IN]) && ($typo3VarsArray[TYPO3_BE_USER_LOGGED_IN] == 'yes');
        $value = self::adjustConfigShowDebugInfo($value, $flag);
        self::setVar(SYSTEM_SHOW_DEBUG_INFO, $value, STORE_SYSTEM);
    }

    /**
     * Load $recordId from $tableName using $db and saves it in $store.
     *
     * @param string $tableName
     * @param int $recordId
     * @param Database $db
     * @param string $primaryKey
     * @param string $store
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public static function fillStoreWithRecord($tableName, $recordId, Database $db,
                                               $primaryKey = F_PRIMARY_KEY_DEFAULT, $store = STORE_RECORD) {
        if ($recordId !== false && $recordId > 0 &&
            is_string($tableName) && $tableName !== '') {
            if (!$primaryKey) {
                $primaryKey = F_PRIMARY_KEY_DEFAULT;
            }

            $record = $db->sql("SELECT * FROM $tableName WHERE $primaryKey = ?", ROW_EXPECT_1, [$recordId]);
            self::setStore($record, $store, true);
        }
    }

    /**
     * Read SYSTEM_FILL_STORE_SYSTEM_BY_SQL_1|2|3 from SYSTEM_STORE and if set:
     * a) fire the SQL
     * b) merge all columns to STORE_SYSTEM
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function FillStoreSystemBySql() {

        $db = null;
        $flagDirty = false;
        $storeSystem = self::getStore(STORE_SYSTEM);

        for ($ii = 1; $ii <= 3; $ii++) {
            if (empty($storeSystem[SYSTEM_FILL_STORE_SYSTEM_BY_SQL . "$ii"])) {
                continue;
            }

            if ($db == null) {
                $db = new Database();
            }

            $errMsg = "More than 1 record found. " . CONFIG_QFQ_PHP . ": " . SYSTEM_FILL_STORE_SYSTEM_BY_SQL . "$ii";
            $mode = ROW_EXPECT_0_1;

            // If there is an error message defined, this means there should be exactly one record.
            if (!empty($storeSystem[SYSTEM_FILL_STORE_SYSTEM_ERROR_MSG . "$ii"])) {
                $mode = ROW_EXPECT_1;
                $errMsg = $storeSystem[SYSTEM_FILL_STORE_SYSTEM_ERROR_MSG . "$ii"];
            }

            $storeSystemAdd = $db->sql($storeSystem[SYSTEM_FILL_STORE_SYSTEM_BY_SQL . "$ii"], $mode, array(), $errMsg);
            $storeSystemAdd = OnArray::keyNameRemoveLeadingUnderscore($storeSystemAdd);
            $storeSystem = array_merge($storeSystem, $storeSystemAdd);
            $flagDirty = true;
        }

        if ($flagDirty) {
            self::setStore($storeSystem, STORE_SYSTEM, true);
        }
    }

    /**
     * Append an array (in case of 'array of array': the first row of array) to store $storeName.
     * Existing values will be overwritten.
     *
     * @param array $dataArray - in special cases it might be an empty string -therefore no type definition to 'array'.
     * @param $storeName
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function appendToStore($dataArray, $storeName) {

        if (empty($dataArray) || !is_array($dataArray)) {
            return;
        }

        if (isset($dataArray[0]) && is_array($dataArray[0])) {
            $append = $dataArray[0];
        } else {
            $append = $dataArray;
        }

        if (empty($append)) {
            return;
        }

        $store = Store::getInstance();
        $store->setStore(array_merge($store->getStore($storeName), $append), $storeName, true);
    }
}
