<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 2/10/18
 * Time: 3:14 PM
 */

namespace IMATHUZH\Qfq\Core\Helper;


 

/**
 * Class OnString
 * @package qfq
 */
class OnString {

    /**
     * Returns part of haystack string starting from and including the last occurrence of needle to the end of haystack.
     *
     * strrstr('hello/world/to/the/limit', '/') = 'limit'
     *
     * @param $haystack
     * @param $needle
     * @return string
     */
    public static function strrstr($haystack, $needle) {
        if (empty($needle) || empty($haystack)) {
            return '';
        }

        return substr($haystack, strrpos($haystack, $needle) + strlen($needle));
    }

    /**
     * Strips the first char $c from $data if the first char is equal to $c.
     * Example: with $c='_' the $data='_pId' becomes 'pId'
     *
     * @param $c
     * @param $data
     * @return string
     */
    public static function stripFirstCharIf($c, $data) {

        if (empty($data)) {
            return $data;
        }

        if ($data[0] == $c) {
            $data = substr($data, 1);
        }

        return $data;
    }

    /**
     * If the given $str is enclosed in SINGLE_TICK or DOUBLE_TICK, remove it. Only the outermost are removed.
     *
     * @param string $str
     * @return string  remove unquoted string
     */
    public static function trimQuote($str) {
        $len = strlen($str);

        if ($len < 2) {
            return $str;
        }

        switch ($str[0]) {
            case SINGLE_TICK:
                if ($str[$len - 1] == SINGLE_TICK) {
                    return substr($str, 1, $len - 2);
                }
                break;
            case DOUBLE_TICK:
                if ($str[$len - 1] == DOUBLE_TICK) {
                    return substr($str, 1, $len - 2);
                }
                break;
            default:
                break;
        }

        return $str;
    }

    /**
     * Split an Excel position string line to column and row.
     * E.g.: $pos = 'A1' to [ 'A', '1'], resp.  'ACD4567' to [ 'ACD', '4567' ]
     *
     * @param $pos
     * @param &$column - return the alpha part of $pos
     * @param &$row - return the digit part of $pos
     * @return bool - true if a alpha string and a numeric string is found, else false.
     */
    public static function splitExcelPos($pos, &$column, &$row) {

        preg_match_all('/[A-Z]+|\d+/', $pos, $matches);

        if (count($matches[0]) != 2) {
            return false;
        }

        $column = $matches[0][0];
        $row = $matches[0][1];

        return true;
    }

    /**
     * Split the wkthtml Parameter between:
     * - general 'urlParam' for the specific website (page which will be converted to PDF).
     * - `wkthml` Parameter. Those will always start with '-' and control wkhtml how to render the PDF.
     * - '_sip' - to activate that `urlParam` parameters will be SIP encoded.
     *
     * @param string $urlParamString
     * @param array $rcArgs
     * @param bool $rcSipEncode
     *
     * @return array The remaining 'real' URL parameter to call the T3 page.
     * @throws \UserFormException
     */
    public static function splitParam($urlParamString, array &$rcArgs, &$rcSipEncode) {
        $urlParamNew = array();
        $rcArgs[WKHTML_OPTION_VIEWPORT] = WKHTML_OPTION_VIEWPORT_VALUE; // Forces wkhtml to render Bootstrap page in 'md', not 'xs'

        $urlParam = KeyValueStringParser::parse($urlParamString, '=', '&', KVP_IF_VALUE_EMPTY_COPY_KEY);
        foreach ($urlParam as $key => $value) {
            switch (substr($key, 0, 1)) {
                case '-':
                    $rcArgs[$key] = $value;
                    break;
                case '_':
                    if ($key == DOWNLOAD_SIP_ENCODE_PARAMETER) {
                        $rcSipEncode = true;
                    }
                    break;
                default:
                    $urlParamNew[$key] = $value;
                    break;
            }
        }

        return $urlParamNew;
    }

    /**
     * Removes new lines (\n) from expressions like {{var:SC0:alnumx}} and replaces them with spaces.
     * Handles nested expressions like {{SELECT {{var::alnumx}}}}
     * This function is useful to allow multiline expressions in form definitions such as:
     *   sqlInsert = {{INSERT INTO test (grId)
     *                 VALUES(123) }}
     *
     * @param $str - the string to be parsed
     * @return string - the resulting string with new lines in expressions replaced
     * @throws \UserFormException - alerts the user if the delimiters are not balanced
     */
    public static function removeNewlinesInNestedExpression($str) {
        $delimStart = '{{';
        $delimEnd = '}}';
        $lastDelimPos = -strlen($delimStart);
        $exprDepth = 0; // keeps count of the level of expression depth
        $nestingStart = 0;

        // Process the string one start/end delimiter at a time
        while (true) {
            // find the next start/end delimiter
            $nextDelimStartPos = strpos($str, $delimStart, $lastDelimPos + strlen($delimStart));
            $nextDelimEndPos = strpos($str, $delimEnd, $lastDelimPos + strlen($delimEnd));
            if ($nextDelimStartPos === false && $nextDelimEndPos === false) break;
            $nextDelimPos = min($nextDelimStartPos, $nextDelimEndPos);
            if ($nextDelimStartPos === false) $nextDelimPos = $nextDelimEndPos;
            if ($nextDelimEndPos === false) $nextDelimPos = $nextDelimStartPos;

            if ($nextDelimPos == $nextDelimStartPos) { // opening delimiter
                if ($exprDepth == 0) $nestingStart = $nextDelimPos;
                $exprDepth++;
            } else { // closing delimiter
                $exprDepth--;
                if ($exprDepth < 0) {
                    throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => "Too many closing delimiters '$delimEnd'",
                        ERROR_MESSAGE_TO_DEVELOPER => "in '$str'"]), ERROR_MISSING_OPEN_DELIMITER);


                    break;
                } elseif ($exprDepth == 0) {
                    // end of nesting -> replace \n inside nested expression with space
                    $pre = substr($str, 0, $nestingStart);
                    $nest = substr($str, $nestingStart, $nextDelimPos - $nestingStart);
                    $nestNew = str_replace('\n', ' ', $nest);
                    $post = substr($str, $nextDelimPos);
                    $str = substr($str, 0, $nestingStart) .
                        str_replace("\n", " ", substr($str, $nestingStart, $nextDelimPos - $nestingStart)) .
                        substr($str, $nextDelimPos);
                }
            }

            $lastDelimPos = $nextDelimPos;
        }
        if ($exprDepth > 0) {
            throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => "Missing close delimiter '$delimEnd'",
                ERROR_MESSAGE_TO_DEVELOPER => "in '$str'"]), ERROR_MISSING_CLOSE_DELIMITER);
        }

        return $str;
    }

    /**
     * Split a $_SERVER['PATH_INFO'] of the form '/form1/id1/form2/id2/form3/id3/.../formN[/idN])' to
     *   $rcArrrIds=[ id1, id2, ..., idN]
     *   return: 'formN'
     *
     * @param $pathInfo
     * @param array $rcArrId
     * @param array $rcArrForm
     * @return string
     * @throws \UserFormException
     */
    public static function splitPathInfoToIdForm($pathInfo, array &$rcArrId, array &$rcArrForm) {

        // Empty: do nothing
        if ($pathInfo == '') {
            return '';
        }

        // Remove optional leading '/'
        if ($pathInfo[0] == '/') {
            $pathInfo = substr($pathInfo, 1);
        }

        // Remove optional trailing '/'
        $len = strlen($pathInfo);
        if ($len > 0 && $pathInfo[$len - 1] == '/') {
            $pathInfo = substr($pathInfo, 0, $len - 1);
        }

        // Empty? do nothing
       if ($pathInfo == '') {
            return '';
        }

        $param = explode('/', $pathInfo);
        $cnt = count($param);

        // No 'id'. Append '0'
        if ($cnt % 2 == 1) {
            array_push($param, 0);
        }

        $rcArrId = array();
        $rcArrForm = array();

        while (count($param)>0) {

            $form= array_shift($param);
            if (!ctype_alnum($form)) {
                throw new \UserFormException('Expect alphanumeric string', ERROR_BROKEN_PARAMETER);
            }
            $rcArrForm[]=$form;

            $id = array_shift($param);
            if (!ctype_digit((string) $id)) {
                throw new \UserFormException('Expect numerical id', ERROR_BROKEN_PARAMETER);
            }
            $rcArrId[] = $id;
        }

        return $form;
    }
}
