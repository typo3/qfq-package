<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 4/30/17
 * Time: 1:45 PM
 */

namespace IMATHUZH\Qfq\Core\Helper;


 

/**
 * Class SessionCookie
 * @package qfq
 */
class SessionCookie {

    /**
     * @var string
     */
    private $pathFileNameCookie = '';

    /**
     * @var bool
     */
    private $cleanTempFiles = true;

    /**
     * Copy all current cookies to a temporary file.
     *
     * @param array $config
     *
     * @throws \CodeException
     */
    public function __construct(array $config) {
        $lines = '';

        $this->cleanTempFiles = !Support::findInSet(SYSTEM_SHOW_DEBUG_INFO_DOWNLOAD, $config[SYSTEM_SHOW_DEBUG_INFO]);

        $urlParts = parse_url($config[SYSTEM_BASE_URL]);
        $domain = $urlParts['host'];
        $path = $urlParts['path'];

        // $_COOKIES[]
        if (false === ($this->pathFileNameCookie = tempnam(sys_get_temp_dir(), SESSION_COOKIE_PREFIX))) {
            throw new \CodeException('Error creating output file.', ERROR_IO_CREATE_FILE);
        }

        foreach ($_COOKIE as $name => $value) {
            // e.g.: SANDBOXSESSION=a83f1o69jbv12932q54hmgphk6; domain=qfq.math.uzh.ch; path=/;
            $lines .= $name . "=" . $value . "; domain=$domain; path=$path;\n";
        }

        file_put_contents($this->pathFileNameCookie, $lines, FILE_APPEND);
    }

    /**
     * Unlink current cookie file
     * @throws \UserFormException
     * @throws \CodeException
     */
    public function __destruct() {

        if ($this->cleanTempFiles) {
            if (file_exists($this->pathFileNameCookie)) {
                HelperFile::unlink($this->pathFileNameCookie);
                $this->pathFileNameCookie = '';
            }
        }
    }

    /**
     * @return string PathFilename of cookie file
     */
    public function getFile() {

        return $this->pathFileNameCookie;
    }

}