<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/28/16
 * Time: 8:05 AM
 */

namespace IMATHUZH\Qfq\Core\Helper;

 
use IMATHUZH\Qfq\Core\Store\Store;

const LONG_CURLY_OPEN = '#/+open+/#';
const LONG_CURLY_CLOSE = '#/+close+/#';

/**
 * Class Support
 * @package qfq
 */
class Support {

    /**
     * @var Store
     */
    private static $store = null;

    /**
     * @param array $queryArray Empty or prefilled assoc array with url parameter
     * @param string $mode PARAM_T3_NO_ID, PARAM_T3_ALL
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function appendTypo3ParameterToArray(array &$queryArray, $mode = PARAM_T3_ALL) {

        if (!isset(self::$store)) {
            self::$store = Store::getInstance();
        }

        if ($mode === PARAM_T3_ALL) {
            $queryArray['id'] = self::$store->getVar(TYPO3_PAGE_ID, STORE_TYPO3);
        }

        // TYPE
        $tmp = self::$store->getVar(TYPO3_PAGE_TYPE, STORE_TYPO3);
        if ($tmp !== false && $tmp != 0) {
            $queryArray['type'] = $tmp;
        }

        // Language
        $tmp = self::$store->getVar(TYPO3_PAGE_LANGUAGE, STORE_TYPO3);
        if ($tmp !== false && $tmp != 0) {
            $queryArray['L'] = $tmp;
        }
    }

    /**
     * Build the form log filename. Depending on $formLog=FORM_LOG_ALL one file for all BE_USER, or $formLog=FORM_LOG_SESSION one file per BE_USER.
     *
     * @param $formName
     * @param $formLogMode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function getFormLogFileName($formName, $formLogMode) {

        if (!isset(self::$store)) {
            self::$store = Store::getInstance();
        }

        switch ($formLogMode) {
            case FORM_LOG_ALL:
                $perBeSession = '';
                break;
            case FORM_LOG_SESSION:
                $perBeSession = self::$store->getVar(TYPO3_BE_USER, STORE_TYPO3) . '.';
                if (empty($perBeSession)) {
                    throw new \UserFormException('formLog: no BE User logged in', ERROR_NO_BE_USER_LOGGED);
                }
                break;
            default:
                throw new \CodeException('Unknown mode: ' . $formLogMode, ERROR_UNKNOWN_TOKEN);
        }

        $filename = SYSTEM_FILEADMIN_PROTECTED_LOG . '/' . $formName . "." . $perBeSession . "log";

        return sanitize::safeFilename($filename, false, true);
    }

    /**
     * Builds a urlencoded query string of an assoc array.
     *
     * @param array $queryArray
     *
     * @return string Querystring (e.g.: id=23&type=99
     */
    public static function arrayToQueryString(array $queryArray) {
        $items = array();

        foreach ($queryArray as $key => $value) {
            if (is_int($key)) {
                $items[] = urlencode($value);
            } else {
                $items[] = $key . '=' . urlencode($value);
            }
        }

        return implode('&', $items);
    }

    /**
     * Extract Tag(s) from $tag (eg: <div><input class="form-control">, might contain further attributes) and wrap it
     * around
     * $value. If $flagOmitEmpty==true && $value=='': return ''.
     *
     * @param string $tag
     * @param string $value
     * @param bool|false $omitIfValueEmpty
     *
     * @return string
     */
    public static function wrapTag($tag, $value, $omitIfValueEmpty = false) {

        $tag = trim($tag);

        if ($tag == '' || ($omitIfValueEmpty && $value == "")) {
            return $value;
        }

        $tagArray = explode('>', $tag, 2);
        if (count($tagArray) > 1 && $tagArray[1] != '') {
            $value = self::wrapTag($tagArray[1], $value, $omitIfValueEmpty);
            $tag = $tagArray[0] . '>';
        }

        // a) <div class="container-fluid">, b) <label>
        $arr = explode(' ', $tag);

        $tagPlain = (count($arr) === 1) ? substr($arr[0], 1, strlen($arr[0]) - 2) : substr($arr[0], 1);
        $closing = '</' . $tagPlain . '>';

        return $tag . $value . $closing;
    }

    /**
     * @param $glyphIcon
     * @param string $value
     * @return string
     */
    public static function renderGlyphIcon($glyphIcon, $value = '') {
        return Support::wrapTag("<span class='" . GLYPH_ICON . " $glyphIcon'>", $value);
    }

    /**
     * Removes '$tag' and closing $tag from $value, if they are the outermost.
     * E.g.  unWrapTag('<p>', '<p>Hello World</p>')    returns  'Hello World'
     *
     * @param string $tag
     * @param string $value
     *
     * @return string
     */
    public static function unWrapTag($tag, $value) {

        if ($tag == '' || $value == '') {
            return $value;
        }

        $lenTag = strlen($tag);
        $lenValue = strlen($value);

        if ($lenValue < $lenTag + $lenTag + 1) {
            return $value;
        }

        $closeTag = $tag[0] . '/' . substr($tag, 1);

        if (substr($value, 0, $lenTag) == $tag && substr($value, $lenValue - $lenTag - 1) == $closeTag) {
            $value = substr($value, $lenTag, $lenValue - $lenTag - $lenTag - 1);
        }

        return $value;
    }

    /**
     * Wraps some $inner fragment with a CSS styled $tooltipText . CSS is configured in 'Resources/Public/qfq-jqw.css'.
     *
     * Based on: http://www.w3schools.com/howto/howto_css_tooltip.asp
     *
     * @param string $htmlId
     * @param string $tooltipText
     *
     * @return string
     * @throws \CodeException
     */
    public static function doTooltip($htmlId, $tooltipText) {

        return "<img " . self::doAttribute('id', $htmlId) . " src='" . GFX_INFO . "' title=\"" . htmlentities($tooltipText) . "\">";
    }

    /**
     * Format's an attribute: $type=$value. If $flagOmitEmpty==true && $value=='': return ''.
     * Escape double tick - assumes that attributes will always be enclosed by double ticks.
     * Add's a space at the end.
     *
     * @param string $type
     * @param string|array $value
     * @param bool $flagOmitEmpty true|false
     * @param string $modeEscape ESCAPE_WITH_BACKSLASH | ESCAPE_WITH_HTML_QUOTE
     *
     * @return string correctly formatted attribute. Space at the end.
     * @throws \CodeException
     */
    public static function doAttribute($type, $value, $flagOmitEmpty = true, $modeEscape = ESCAPE_WITH_HTML_QUOTE) {

        // several attributes might be given as an array - concat to a string
        if (is_array($value)) {
            $value = implode(' ', $value);
        }

        if ($flagOmitEmpty && trim($value) === "") {
            return '';
        }

        switch (strtolower($type)) {
            case 'size':
            case 'maxlength':
                // empty or '0' for attributes of type 'size' or 'maxlength' result in unsuable input elements: skip this.
                if ($value === '' || $value == 0) {
                    return '';
                }
                break;
            // Bad idea to do urlencode on this place: it will convert ?, &, ... which are necessary for a proper URL.
            // Instead the value of a parameter needs to encode. Unfortunately, it's too late on this place.
//            case 'href':
//                $value = urlencode($value);
//                break;
            default:
                break;
        }

        $value = self::escapeDoubleTick(trim($value), $modeEscape);

        return $type . '="' . $value . '" ';
    }

    /**
     * Escapes Double Ticks ("), which are not already escaped.
     * modeEscape: ESCAPE_WITH_BACKSLASH | ESCAPE_WITH_HTML_QUOTE
     *
     * TinyMCE: Encoding JS Attributes (keys & values) for TinyMCE needs to be encapsulated in '&quot;' instead of '\"'.
     *
     * @param        $str
     * @param string $modeEscape
     *
     * @return string
     * @throws \CodeException
     */
    public static function escapeDoubleTick($str, $modeEscape = ESCAPE_WITH_BACKSLASH) {
        $newStr = '';

        for ($ii = 0; $ii < strlen($str); $ii++) {
            if ($str[$ii] === '"') {
                if ($ii === 0 || $str[$ii - 1] != '\\') {
                    switch ($modeEscape) {
                        case ESCAPE_WITH_BACKSLASH:
                            $newStr .= '\\';
                            break;
                        case ESCAPE_WITH_HTML_QUOTE:
                            $newStr .= '&quot;';
                            continue 2;
                        default:
                            throw new \CodeException('Unknown modeEscape=' . $modeEscape, ERROR_UNKNOWN_ESCAPE_MODE);
                    }
                }
            }
            $newStr .= $str[$ii];
        }

        return $newStr;
    }

    /**
     * Format's an attribute and inserts them at the beginning of the $htmlTag:
     * If $flagOmitEmpty==true && $value=='': insert nothing
     *
     * @param string $htmlTag with open and closing angle.
     * @param string $type
     * @param string|array $value
     * @param bool $flagOmitEmpty
     * @param string $modeEscape
     *
     * @return string correctly fomratted attribute. Space at the end.
     * @throws \CodeException
     */
    public static function insertAttribute($htmlTag, $type, $value, $flagOmitEmpty = true, $modeEscape = ESCAPE_WITH_BACKSLASH) {
        $htmlTag = trim($htmlTag);

        // e.g. '<div class=...' will be exploded to '<div' and 'class=...'
        $parts = explode(' ', $htmlTag, 2);
        if (count($parts) < 2) {
            if (strlen($htmlTag) < 3) {
                throw new \CodeException('HTML Token too short (<3 chars):' . $htmlTag, ERROR_HTML_TOKEN_TOO_SHORT);
            } else {
                $parts[0] = substr($htmlTag, 0, -1);
                $parts[1] = '>';
            }
        }

        $attr = self::doAttribute($type, $value, $flagOmitEmpty, $modeEscape);

        return $parts[0] . ' ' . $attr . $parts[1];
    }

    /**
     * Search for the parameter $needle in $haystack. The arguments has to be separated by ','.
     *
     * Returns false if not found, or index (starting with 0) of found place. Be careful: use unary operator to compare for 'false'
     *
     * @param string $needle
     * @param string $haystack
     *
     * @return boolean     true if found, else false
     */
    public static function findInSet($needle, $haystack) {
        $arr = explode(',', $haystack);

        return array_search($needle, $arr) !== false;
    }

    /**
     * Converts a dateTime String to the international format:
     * 1.2.79 > 1979-02-01 00:00:00
     * 01.02.13 3:24 >  1979-02-01 03:24:00
     * 1.2.1979 14:21:5 > 1979-02-01 14:21:05
     *
     * @param string $dateTimeString
     *
     * @return string
     * @throws \UserFormException
     */
    public static function dateTimeGermanToInternational($dateTimeString) {
        $dateRaw = '';
        $timeRaw = '';


//        const REGEXP_DATE_INT_ = '^\d{2,4}-\d{2}-\d{2}$';
//        const REGEXP_DATE_GER = '^\d{1,2}\.\d{1,2}\.\d{2}(\d{2})?$';
//        const REGEXP_TIME = '^\d{1,2}:\d{1,2}(:\d{1,2})?$';

        $tmpArr = explode(' ', $dateTimeString);
        switch (count($tmpArr)) {
            case 0:
                return '';

            case 1:
                if (strpos($tmpArr[0], ':') === false) {
                    $dateRaw = $tmpArr[0];
                } else {
                    $timeRaw = $tmpArr[0];
                }
                break;

            case 2:
                $dateRaw = $tmpArr[0];
                $timeRaw = $tmpArr[1];
                break;

            default:
                throw new \UserFormException('Date/time format not recognised.', ERROR_DATE_TIME_FORMAT_NOT_RECOGNISED);
                break;
        }

        if ($dateRaw === '' || $dateRaw === '0000-00-00' || $dateRaw === '00.00.0000') {
            $date = '0000-00-00';
        } else {
            // International format: YYYY-MM-DD

            if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $dateRaw) === 1) {
                $date = $dateRaw;

                // German format: 1.1.01 - 11.12.1234
            } elseif (preg_match('/^\d{1,2}\.\d{1,2}\.\d{2}(\d{2})?$/', $dateRaw) === 1) {
                $tmpArr = explode('.', $dateRaw);

                if ($tmpArr[2] < 70) {
                    $tmpArr[2] = 2000 + $tmpArr[2];
                } elseif ($tmpArr[2] < 100) {
                    $tmpArr[2] = 1900 + $tmpArr[2];
                }
                $date = sprintf("%04d-%02d-%02d", $tmpArr[2], $tmpArr[1], $tmpArr[0]);

            } else {
                throw new \UserFormException('Date/time format not recognised.', ERROR_DATE_TIME_FORMAT_NOT_RECOGNISED);
            }
        }

        if ($timeRaw === '' || $timeRaw === '00:00:00') {
            $time = '00:00:00';
        } else {
            if (preg_match('/^\d{1,2}:\d{1,2}(:\d{1,2})?$/', $timeRaw) !== 1) {
                throw new \UserFormException('Date/time format not recognised.', ERROR_DATE_TIME_FORMAT_NOT_RECOGNISED);
            }

            $tmpArr = explode(':', $timeRaw);
            switch (count($tmpArr)) {
                case 2:
                    $time = sprintf("%02d:%02d:00", $tmpArr[0], $tmpArr[1]);
                    break;
                case 3:
                    $time = sprintf("%02d:%02d:%02d", $tmpArr[0], $tmpArr[1], $tmpArr[2]);
                    break;
                default:
                    throw new \UserFormException('Date/time format not recognised.', ERROR_DATE_TIME_FORMAT_NOT_RECOGNISED);
                    break;
            }
        }

        return $date . ' ' . $time;
    }

    /**
     * @param string $type date | datetime | time
     * @param string $format FORMAT_DATE_INTERNATIONAL | FORMAT_DATE_GERMAN
     * @param string $timeIsOptional
     *
     * @return string
     * @throws \UserFormException
     */
    public static function dateTimeRegexp($type, $format, $timeIsOptional = '0') {

        if ($format === FORMAT_DATE_GERMAN) {
            // yyyy-mm-dd | 0000-00-00
            $date = '(([1-9]|0[1-9]|1[0-9]|2[0-9]|3[01])\.([1-9]|0[1-9]|1[012])\.([0-9]{4}|[0-9]{2})|00\.00\.(00){1,2})';
        } else {
            // FORMAT_DATE_INTERNATIONAL: [d]d.[m]m.[yy]yy | 00.00.0000
            $date = '([0-9]{4}-([1-9]|0[1-9]|1[012])-([1-9]|0[1-9]|1[0-9]|2[0-9]|3[01])|0000-00-00)';
        }

        // hh:mm[:ss]
        $time = '(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]){1,2}';

        switch ($type) {
            case 'date':
                $pattern = $date;
                break;
            case 'datetime':
                if ($timeIsOptional == '1')
                    $pattern = $date . '( ' . $time . ')?';
                else
                    $pattern = $date . ' ' . $time;
                break;
            case 'time':
                $pattern = $time;
                break;
            default:
                throw new \UserFormException("Unknown mode: '$type'", ERROR_UNKNOWN_MODE);
        }

        return '^' . $pattern . '$';
    }

    /**
     * Parses $dateTimeString. If a date is given, detect international or german format automatically.
     * Convert to $dateFormat. 'yy' and 'd' will be converted to 'yyyy' and 'dd'
     * Returned value will be 'date only', 'datetime' oder 'time only', depending on the input value.
     *
     * @param string $dateTimeString
     * @param string $dateFormat FORMAT_DATE_INTERNATIONAL | FORMAT_DATE_GERMAN
     * @param string $showZero
     * @param string $showTime
     * @param string $showSeconds
     *
     * @return string
     * @throws \UserFormException
     */
    public static function convertDateTime($dateTimeString, $dateFormat, $showZero, $showTime, $showSeconds) {
        $givenDate = '';
        $givenTime = '';
        $newDate = '';
        $newTime = '';
        $delim = '';
        $flagDateAndTime = false;

        $dateTimeString = trim($dateTimeString);


        switch ($dateTimeString) {
            case '':
            case '0':
            case '0000-00-00 00:00:00':
            case '0000-00-00':
            case '00.00.0000 00:00:00':
            case '00.00.0000':
            case '00:00:00':
                return (self::dateTimeZero($dateFormat, $showZero, $showTime, $showSeconds));
                break;

            default:
                break;
        }

        // Main convert
        $arr = explode(' ', $dateTimeString);
        switch (count($arr)) {
            case 1:
                if (strpos($dateTimeString, ':') === false) {
                    $givenDate = $dateTimeString;
                } else {
                    $givenTime = $dateTimeString;
                }
                break;

            case 2:
                $givenDate = $arr[0];
                $givenTime = $arr[1];
                $flagDateAndTime = true;
            default:
        }

        // Date
        if ($givenDate != '') {
            if ($givenDate == '0') {
                $givenDate = '0000-00-00';
            }

            $arr = self::splitDateToArray($givenDate);

            switch ($dateFormat) {
                case FORMAT_DATE_INTERNATIONAL:
                    $newDate = sprintf("%04d-%02d-%02d", $arr[0], $arr[1], $arr[2]);
                    break;
                case FORMAT_DATE_GERMAN:
                    $newDate = sprintf("%02d.%02d.%04d", $arr[2], $arr[1], $arr[0]);
                default:
            }
        }

        // Time
        if ($givenTime != '') {
            if ($givenTime == '0') {
                $givenTime = '0:0:0';
            }

            $arr = explode(':', $givenTime);
            if (count($arr) < 3) {
                $arr[2] = 0;
            }

            if ($showSeconds == 1) {
                $newTime = sprintf("%02d:%02d:%02d", $arr[0], $arr[1], $arr[2]);
            } else {
                $newTime = sprintf("%02d:%02d", $arr[0], $arr[1]);
            }
        }

        if ($flagDateAndTime) {
            $delim = ' ';
        }

        return $newDate . $delim . $newTime;
    }

    /**
     * Returns a representation of 0 in a choosen variant.
     *
     * @param string $dateFormat FORMAT_DATE_INTERNATIONAL | FORMAT_DATE_GERMAN
     * @param string $showZero
     * @param string $showTime '0' | '1'
     * @param string $showSeconds '0' | '1'
     *
     * @return string
     */
    private static function dateTimeZero($dateFormat, $showZero, $showTime, $showSeconds) {

        if ($showZero != '1') {
            return '';
        }

        // $dateFormat (INT/GER),  $showTime, $showSeconds
        $arr[0][0][0] = '0000-00-00';
        $arr[0][0][1] = '0000-00-00';
        $arr[0][1][0] = '0000-00-00 00:00';
        $arr[0][1][1] = '0000-00-00 00:00:00';
        $arr[1][0][0] = '00.00.0000';
        $arr[1][0][1] = '00.00.0000';
        $arr[1][1][0] = '00.00.0000 00:00';
        $arr[1][1][1] = '00.00.0000 00:00:00';

        $showFormat = ($dateFormat === FORMAT_DATE_INTERNATIONAL) ? 0 : 1;

        return $arr[$showFormat][$showTime][$showSeconds];
    }

    /**
     * Split date FORMAT_DATE_GERMAN | FORMAT_DATE_INTERNATIONAL to array with arr[0]=yyyy, arr[1]=mm, arr[2]=dd.
     *
     * @param string $dateString
     *
     * @return array
     * @throws \UserFormException
     */
    private static function splitDateToArray($dateString) {

        if (strpos($dateString, '-') === false) {
            // FORMAT_DATE_GERMAN
            $arr = explode('.', $dateString);
            if (count($arr) != 3) {
                throw new \UserFormException("Unexpected format for date: $dateString", ERROR_DATE_UNEXPECTED_FORMAT);
            }
            $tmp = $arr[0];
            $arr[0] = $arr[2];
            $arr[2] = $tmp;
        } else {
            // FORMAT_DATE_INTERNATIONAL
            $arr = explode('-', $dateString);
            if (count($arr) != 3) {
                throw new \UserFormException("Unexpected format for date: $dateString", ERROR_DATE_UNEXPECTED_FORMAT);
            }
        }

        // Adjust yy to yyyy. See https://dev.mysql.com/doc/refman/5.7/en/datetime.html > Dates containing two-digit year values ...
        if ($arr[0] < 100) {
            $add = ($arr[0] < 70) ? 2000 : 1900;
            $arr[0] = $add + $arr[0];
        }

        return $arr;
    }

    /**
     * Calculates the placeholder for date/dateTime/local input fields.
     *
     * @param array $formElement
     *
     * @return string
     * @throws \UserFormException
     */
    public static function getDateTimePlaceholder(array $formElement) {

        $timePattern = ($formElement[FE_SHOW_SECONDS] == 1) ? 'hh:mm:ss' : 'hh:mm';
        switch ($formElement[FE_TYPE]) {
            case 'date':
                $placeholder = $formElement[FE_DATE_FORMAT];
                break;
            case 'datetime':
                $placeholder = $formElement[FE_DATE_FORMAT] . ' ' . $timePattern;
                break;
            case 'time':
                $placeholder = $timePattern;
                break;
            default:
                throw new \UserFormException("Unexpected Formelement type: '" . $formElement[FE_TYPE] . "'", ERROR_FORMELEMENT_TYPE);
        }

        return $placeholder;
    }


    /**
     * Encrypt curly braces by an uncommon string. Helps preventing unwished action on curly braces.
     *
     * @param string $text
     *
     * @return mixed
     */
    public static function encryptDoubleCurlyBraces($text) {
        $text = str_replace('{{', LONG_CURLY_OPEN, $text);
        $text = str_replace('}}', LONG_CURLY_CLOSE, $text);

        return $text;
    }

    /**
     * Decrypt curly braces by an uncommon string. Helps preventing unwished action on curly braces
     *
     * @param string $text
     *
     * @return mixed
     */
    public static function decryptDoubleCurlyBraces($text) {

        if (!is_string($text)) {
            return $text;
        }

        $text = str_replace(LONG_CURLY_OPEN, '{{', $text);
        $text = str_replace(LONG_CURLY_CLOSE, '}}', $text);

        return $text;
    }

    /**
     * @param int $length Length of the required hash string
     *
     * @return string       A random alphanumeric hash
     */
    public static function randomAlphaNum($length) {
        $possible_characters = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $string = "";
        while (strlen($string) < $length) {
            $string .= substr($possible_characters, rand() % (strlen($possible_characters)), 1);
        }

        return ($string);
    }

    /**
     * Concatenate URL and Parameter. Depending of if there is a '?' in URL or not,  append the param with '?' or '&'..
     *
     * @param string $url
     * @param string|array $param
     *
     * @return string
     */
    public static function concatUrlParam($url, $param) {

        if (is_array($param)) {
            $param = implode('&', $param);
        }

        if ($param == '') {
            return $url;
        }

        $token = ((strpos($url, '?') === false) && (strpos($url, '&')) === false) ? '?' : '&';

        return $url . $token . $param;
    }

    /**
     * Concatenate $host, $path and $query. A given $host will always append a '/' if none is given.
     * End of  $path, there is no assumption to append '/' - the user needs full control.
     * $query: trailing 'index.php' will be skipped.
     *
     * If HOST and PATH is combined in a var, pass the value as $hostOrPath, leave $host empty.
     *
     * @param string $host
     * @param string $hostOrPath
     * @param string $query
     *
     * @return string
     * @throws \CodeException
     */
    public static function mergeUrlComponents($host, $hostOrPath, $query) {
        $url = '';


        if ($host != '' && substr($host, -1, 1) != '/') {
            $host .= '/';
        }

        if ($host != '' && substr($hostOrPath, 0, 1) == '/') {
            $hostOrPath = substr($hostOrPath, 1);
        }

        if ($host != '' || $hostOrPath != '') {
            $url = $host . $hostOrPath;
        }

        if (substr($query, 0, 9) == 'index.php') {
            $query = substr($query, 9);
        }

        if (false !== strpos(substr($query, 1), '?')) {
            throw new \CodeException('Found a "?" after the beginning of the query - this is forbidden', ERROR_BROKEN_PARAMETER);
        }

        if ($query != '') {
            $url = $url . '?' . $query;
            $url = str_replace('??', '?', $url);
        }

        return $url;
    }

    /**
     * Set Defaults for the current formElement.
     *
     * @param array $formElement
     * @param array $formSpec
     * @return array
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function setFeDefaults(array $formElement, array $formSpec = array()) {

        $store = Store::getInstance();

        // Some Defaults
        self::setIfNotSet($formElement, FE_SHOW_SECONDS, '0');
        self::setIfNotSet($formElement, FE_TIME_IS_OPTIONAL, '0');
        self::setIfNotSet($formElement, FE_SHOW_ZERO, '0');
        self::setIfNotSet($formElement, FE_HIDE_ZERO, '0');
        self::setIfNotSet($formElement, FE_DATE_FORMAT, $store->getVar(SYSTEM_DATE_FORMAT, STORE_SYSTEM));

        self::setIfNotSet($formElement, FE_HTML_BEFORE);
        self::setIfNotSet($formElement, FE_HTML_AFTER);

        self::setIfNotSet($formElement, FE_DATA_REFERENCE, ($formElement[FE_NAME] == '' ? $formElement[FE_ID] : $formElement[FE_NAME]));

        self::setIfNotSet($formElement, FE_SUBRECORD_TABLE_CLASS, SUBRECORD_TABLE_CLASS_DEFAULT);

        if (isset($formSpec[F_BS_LABEL_COLUMNS])) {
            self::setIfNotSet($formElement, F_BS_LABEL_COLUMNS, $formSpec[F_BS_LABEL_COLUMNS], '');
            self::setIfNotSet($formElement, F_BS_INPUT_COLUMNS, $formSpec[F_BS_INPUT_COLUMNS], '');
            self::setIfNotSet($formElement, F_BS_NOTE_COLUMNS, $formSpec[F_BS_NOTE_COLUMNS], '');
        }

        if ($formElement[FE_MODE_SQL] != '') {
            $formElement[FE_MODE] = $formElement[FE_MODE_SQL];
        }

        if (isset($formSpec[F_MODE])) {
            $formElement[FE_MODE] = self::applyFormModeToFormElement($formElement[FE_MODE], $formSpec[F_MODE]);
        }

        // set typeAheadPedantic
        if (isset($formElement[FE_TYPEAHEAD_PEDANTIC]) && $formElement[FE_TYPEAHEAD_PEDANTIC] === '') {
            $formElement[FE_TYPEAHEAD_PEDANTIC] = '1'; // support legacy option of 'typeAheadPedantic' without a value
        }
        if (isset($formElement[FE_TYPEAHEAD_LDAP]) || isset($formElement[FE_TYPEAHEAD_SQL])) {
            self::setIfNotSet($formElement, FE_TYPEAHEAD_PEDANTIC, '1');
        }

        // Will be used to change dynamicUpdate behaviour
        if (isset($formElement[FE_WRAP_ROW_LABEL_INPUT_NOTE])) {
            $formElement[FE_FLAG_ROW_OPEN_TAG] = self::findInSet('row', $formElement[FE_WRAP_ROW_LABEL_INPUT_NOTE]);
            $formElement[FE_FLAG_ROW_CLOSE_TAG] = self::findInSet('/row', $formElement[FE_WRAP_ROW_LABEL_INPUT_NOTE]);
        } else {
            $formElement[FE_FLAG_ROW_OPEN_TAG] = true;
            $formElement[FE_FLAG_ROW_CLOSE_TAG] = false;
        }

        self::setIfNotSet($formElement, FE_INPUT_EXTRA_BUTTON_INFO_CLASS, $store->getVar(FE_INPUT_EXTRA_BUTTON_INFO_CLASS, STORE_SYSTEM));

        // For specific FE hard coded 'checkType'
        switch ($formElement[FE_TYPE]) {
            case FE_TYPE_IMAGE_CUT:
                $formElement[FE_CHECK_TYPE] = SANITIZE_ALLOW_ALLBUT;
                break;
            case FE_TYPE_ANNOTATE:
                $formElement[FE_CHECK_TYPE] = SANITIZE_ALLOW_ALL;
                break;
            default:
                break;
        }

        self::setIfNotSet($formElement, FE_MIN);
        self::setIfNotSet($formElement, FE_MAX);
        self::setIfNotSet($formElement, FE_DECIMAL_FORMAT);

        self::setIfNotSet($formElement, F_FE_DATA_PATTERN_ERROR);

        $typeSpec = $store->getVar($formElement[FE_NAME], STORE_TABLE_COLUMN_TYPES);
        self::adjustFeToColumnDefinition($formElement, $typeSpec);

        return $formElement;
    }

    /**
     * Adjusts several FE parameters using smart guesses based on the table column definition and other parameters.
     *
     * @param array $formElement
     * @param $typeSpec
     * @throws \UserFormException
     */
    public static function adjustFeToColumnDefinition(array &$formElement, $typeSpec) {

        self::adjustMaxLength($formElement, $typeSpec);
        self::adjustDecimalFormat($formElement, $typeSpec);

        // Make educated guesses about the desired $min, $max, $checkType, and $inputType

        // $typeSpec = 'tinyint(3) UNSIGNED NOT NULL' | 'int(11) NOT NULL'
        $arr = explode(' ', $typeSpec, 2);
        if (empty($arr[1])) {
            $sign = 'signed';
        } else {
            $arr = explode(' ', $arr[1], 2);
            $sign = $arr[0] == 'unsigned' ? 'unsigned' : 'signed';
        }

        $arr = explode('(', $typeSpec, 2);
        $token = $arr[0];

        # s: signed, u: unsigned.
        # s-min, s-max, s-checktype, u-min, u-max, u-checktype
        $control = [
            'tinyint' => [-128, 127, SANITIZE_ALLOW_NUMERICAL, 0, 255, SANITIZE_ALLOW_DIGIT],
            'smallint' => [-32768, 32767, SANITIZE_ALLOW_NUMERICAL, 0, 65535, SANITIZE_ALLOW_DIGIT],
            'mediumint' => [-8388608, 8388607, SANITIZE_ALLOW_NUMERICAL, 0, 16777215, SANITIZE_ALLOW_DIGIT],
            'int' => [-2147483648, 2147483647, SANITIZE_ALLOW_NUMERICAL, 0, 4294967295, SANITIZE_ALLOW_DIGIT],
            'bigint' => [-9223372036854775808, 9223372036854775807, SANITIZE_ALLOW_NUMERICAL, 0, 18446744073709551615, SANITIZE_ALLOW_DIGIT],
        ];

        $min = '';
        $max = '';
        $checkType = SANITIZE_ALLOW_ALNUMX;
        $inputType = '';
        $isANumber = true;

        switch ($formElement[FE_TYPE]) {
            case FE_TYPE_PASSWORD:
            case FE_TYPE_NOTE:
                $checkType = SANITIZE_ALLOW_ALL;
                break;

            case FE_TYPE_EDITOR:
            case FE_TYPE_TEXT:
                if ($formElement[FE_ENCODE] === FE_ENCODE_SPECIALCHAR)
                    $checkType = SANITIZE_ALLOW_ALL;
                break;
        }

        switch ($token) {
            case 'tinyint':
            case 'smallint':
            case 'mediumint':
            case 'int':
            case 'bigint':
                $inputType = HTML_INPUT_TYPE_NUMBER;
                $arr = $control[$token];
                if ($sign == 'signed') {
                    $min = $arr[0];
                    $max = $arr[1];
                    $checkType = $arr[2];
                } else {
                    $min = $arr[3];
                    $max = $arr[4];
                    $checkType = $arr[5];
                }
                break;

            case 'decimal':
            case 'float':
            case 'double':
                $checkType = SANITIZE_ALLOW_NUMERICAL;
                break;

            case 'bit':
                $inputType = HTML_INPUT_TYPE_NUMBER;
                $checkType = SANITIZE_ALLOW_DIGIT;
                break;

            default:
                $isANumber = false;
                break;
        }

        // Numbers don't need a maxLength because they are being handled by min/max and/or decimalFormat
        if ($isANumber) {
            $formElement[FE_MAX_LENGTH] = '';
        }

        if (!empty($formElement[FE_TYPEAHEAD_SQL]) || !empty($formElement[FE_TYPEAHEAD_LDAP])) {
            $inputType = '';
            $checkType = SANITIZE_ALLOW_ALNUMX;
        }

        // Set parameters if not set by user
        if ($formElement[FE_CHECK_TYPE] === SANITIZE_ALLOW_AUTO) {
            $formElement[FE_CHECK_TYPE] = $checkType;
        }

        // If nothing given, set default
        if ($formElement[FE_MIN] == '') {
            $formElement[FE_MIN] = $min;
        }

        // If nothing given, set default
        if ($formElement[FE_MAX] == '') {
            $formElement[FE_MAX] = $max;
        }

        if (empty($formElement[FE_INPUT_TYPE])) {
            $formElement[FE_INPUT_TYPE] = $inputType;
        }

        // If a  $formElement[FE_STEP] is given, the optional boundaries (FE_MIN / FE_MAX) have to be aligned to a multiple of $formElement[FE_STEP].
        if (!empty($formElement[FE_STEP])) {
            if (!empty($formElement[FE_MIN])) {
                $formElement[FE_MIN] = ceil($formElement[FE_MIN] / $formElement[FE_STEP]) * $formElement[FE_STEP];
            }
            if (!empty($formElement[FE_MAX])) {
                $formElement[FE_MAX] = floor($formElement[FE_MAX] / $formElement[FE_STEP]) * $formElement[FE_STEP];
            }
        }

        // If min or max is set and if there is the standard error text given, define a more detailed error text.
        if (($formElement[FE_MIN] != '' || $formElement[FE_MAX] != '') && ($formElement[F_FE_DATA_ERROR] ?? '') == F_FE_DATA_ERROR_DEFAULT) {
            $formElement[F_FE_DATA_ERROR] = F_FE_DATA_ERROR_DEFAULT . ' - allowed values: ' . $formElement[FE_MIN] . '...' . $formElement[FE_MAX];
        }
    }

    /**
     * Sets the decimalFormat of a FormElement based on the parameter definition and table.field definition
     * Affected FormElement fields: FE_DECIMAL_FORMAT
     *
     * @param array $formElement
     * @param $typeSpec
     * @throws \UserFormException
     */
    private static function adjustDecimalFormat(array &$formElement, $typeSpec) {

        if (isset($formElement[FE_DECIMAL_FORMAT])) {
            if ($formElement[FE_DECIMAL_FORMAT] === '') {
                // Get decimal format from column definition
                if ($typeSpec !== false) {
                    $fieldTypeInfoArray = preg_split("/[()]/", $typeSpec);
                    if ($fieldTypeInfoArray[0] === 'decimal')
                        $formElement[FE_DECIMAL_FORMAT] = $fieldTypeInfoArray[1];
                }
            } else {
                // Decimal format is defined in parameter field
                $isValidDecimalFormat = preg_match("/^[0-9]+,[0-9]+$/", $formElement[FE_DECIMAL_FORMAT]);
                if ($isValidDecimalFormat) {
                    $decimalFormatArray = explode(',', $formElement[FE_DECIMAL_FORMAT]);
                    $isValidDecimalFormat = $decimalFormatArray[0] >= $decimalFormatArray[1];
                }
                if (!$isValidDecimalFormat) {
                    throw new \UserFormException("Invalid decimalFormat: '" . $formElement[FE_DECIMAL_FORMAT] . "'", ERROR_INVALID_DECIMAL_FORMAT);
                }
            }
        }
    }

    /**
     * Calculates the maxLength of an input field, based on formElement type, formElement user definition and
     * table.field definition.
     * Affected formElement fields: FE_MAX_LENGTH
     *
     * @param array $formElement
     * @param $typeSpec
     */
    private static function adjustMaxLength(array &$formElement, $typeSpec) {

        // MIN( $formElement['maxLength'], table definition)
        $maxLength = self::getColumnSize($typeSpec);

        $feMaxLength = false;
        switch ($formElement[FE_TYPE]) {
            case 'date':
                $feMaxLength = 10;
                break;
            case 'datetime':
                $feMaxLength = 19;
                break;
            case 'time':
                $feMaxLength = 8;
                break;
        }

        // In case there is no limit of the underlying table column, or a non primary table column, and the FE_TYPE is date/time.
        if ($maxLength === false && $feMaxLength !== false) {
            $maxLength = $feMaxLength;
        }

        // In case the underlying table column is not of type date/time, the $maxLength might be too high: correct
        if ($feMaxLength !== false && $maxLength !== false && $feMaxLength < $maxLength) {
            $maxLength = $feMaxLength;
        }

        // date/datetime
        if ($maxLength !== false) {
            if (isset($formElement[FE_MAX_LENGTH]) && is_numeric($formElement[FE_MAX_LENGTH]) && $formElement[FE_MAX_LENGTH] != 0) {
                if ($formElement[FE_MAX_LENGTH] > $maxLength) {
                    $formElement[FE_MAX_LENGTH] = $maxLength;
                }
            } else {
                $formElement[FE_MAX_LENGTH] = $maxLength;
            }
        }
    }

    /**
     * Get column spec from table definition and parse size of it. If nothing defined, return false.
     *
     * @param $typeSpec
     * @return bool|int a) 'false' if there is no length definition, b) length definition, c)
     *                   date|time|datetime|timestamp use hardcoded length
     */
    private static function getColumnSize($typeSpec) {

        $matches = array();

        switch ($typeSpec) {
            case 'date': // yyyy-mm-dd
                return 10;
            case 'datetime': // yyyy-mm-dd hh:mm:ss
            case 'timestamp': // yyyy-mm-dd hh:mm:ss
                return 19;
            case 'time': // hh:mm:ss
                return 8;
            default:
                if (substr($typeSpec, 0, 4) === 'set(' || substr($typeSpec, 0, 5) === 'enum(') {
                    return self::maxLengthSetEnum($typeSpec);
                }
                break;
        }

        // e.g.: string(64) >> 64, enum('yes','no') >> false
        if (1 === preg_match('/\((.+)\)/', $typeSpec, $matches)) {
            if (is_numeric($matches[1]))
                return $matches[1];
        }

        return false;
    }

    /**
     * Get the strlen of the longest element in enum('val1','val2',...,'valn')
     * or the maximum total length of a set('val1','val2',...,'valn')
     *
     * @param string $typeSpec
     *
     * @return int
     */
    private static function maxLengthSetEnum($typeSpec) {
        $isSet = substr($typeSpec, 0, 4) === 'set(';
        $startPos = $isSet ? 4 : 5;
        $max = 0;

        $valueList = substr($typeSpec, $startPos, strlen($typeSpec) - $startPos - 1);
        $valueArr = explode(',', $valueList);

        if ($isSet) { // set
            return strlen(implode(', ', $valueArr));
        } else { // enum
            foreach ($valueArr as $value) {
                $value = trim($value, "'");
                $len = strlen($value);
                if ($len > $max) {
                    $max = $len;
                }
            }

            return $max;
        }
    }

    /**
     * Depending on $formMode and $feMode, calculate a new $feMode.
     * - If $formMode='' : no change.
     * - If $formMode=F_MODE_READ_ONLY: set feMode=FE_MODE_READ_ONLY, for all visible elements.
     * - If $formMode=F_MODE_REQUIRED_OFF: set feMode=FE_MODE_SHOW, who have have FE_MODE_REQUIRE before.
     *
     * @param string $feMode FE_MODE_SHOW | FE_MODE_REQUIRED | FE_MODE_READONLY | FE_MODE_HIDDEN |
     * @param string $formMode '' | F_MODE_READONLY | F_MODE_REQUIRED_OFF
     *
     * @return array|string
     * @throws \CodeException
     */
    private static function applyFormModeToFormElement($feMode, $formMode) {

        if ($formMode == '') {
            return $feMode; //no change
        }

        switch ($feMode) {
            case FE_MODE_HIDDEN:
            case FE_MODE_READONLY:
                break;

            case FE_MODE_SHOW:
            case FE_MODE_REQUIRED:
                if ($formMode == F_MODE_READONLY) {
                    $feMode = FE_MODE_READONLY;
                } elseif ($formMode == F_MODE_REQUIRED_OFF && $feMode == FE_MODE_REQUIRED) {
                    $feMode = FE_MODE_SHOW_REQUIRED;
                }
                break;

            default:
                throw new \CodeException('Unknown mode: ' . $feMode, ERROR_UNKNOWN_MODE);
        }

        return $feMode;
    }

    /**
     * Check $arr, if there is an element $index. If not, set it to $value.
     * If  $overwriteThis!=false, replace the the original value with $value, if $arr[$index]==$overwriteThis.
     *
     * @param array $arr
     * @param string $index
     * @param string $value
     * @param string|bool $overwriteThis If there is already something which is equal to $overwrite: take new default.
     *
     * @return mixed
     */
    public static function setIfNotSet(array &$arr, $index, $value = '', $overwriteThis = false) {

        if (!isset($arr[$index])) {
            $arr[$index] = $value;
        }

        if ($overwriteThis !== false && $arr[$index] === $overwriteThis) {
            $arr[$index] = $value;
        }

        return $arr[$index];
    }

    /**
     * Append $extend to $filename
     *
     * @param string $filename
     * @param string $extend
     *
     * @return string
     */
    public static function extendFilename($filename, $extend) {
        return $filename . $extend;
    }

    /**
     * Join $path and $file.
     * If $mode=FILE_PRIORITY and $file starts with '/' (=absolute), the $path is skipped.
     *
     *
     * @param string $path
     * @param string $file
     * @param string $mode FILE_PRIORITY | PATH_FILE_CONCAT
     * @return string
     */
    public static function joinPath($path, $file, $mode = FILE_PRIORITY) {

        if ($file == '') {
            return $path;
        }

        if ($path == '') {
            return $file;
        }

        if ($file[0] == DIRECTORY_SEPARATOR) {

            if ($mode == FILE_PRIORITY) {
                return $file; // absolute
            } else {
                if ($path != '') {
                    $file = substr($file, 1);
                }
            }
        }

        if (substr($path, -1) == DIRECTORY_SEPARATOR) {
            return $path . $file; // SEPARATOR already inside
        }

        return $path . DIRECTORY_SEPARATOR . $file;
    }

    /**
     * @param $srcFile
     * @param $pathFileName
     * @param bool $overwrite
     * @param bool|int $chmodDir , 'false' if not change
     * @throws \CodeException
     * @throws \UserFormException
     */
    public static function moveFile($srcFile, $pathFileName, $overwrite, $chmodDir = false) {

        if (file_exists($pathFileName)) {
            if ($overwrite) {
                HelperFile::unlink($pathFileName);
            } else {
                throw new \UserFormException(json_encode(
                    [ERROR_MESSAGE_TO_USER => 'Copy upload failed - file already exist',
                        ERROR_MESSAGE_TO_DEVELOPER => 'File: ' . $pathFileName]), ERROR_IO_FILE_EXIST);
            }
        }

        HelperFile::mkDirParent($pathFileName, $chmodDir);

        // Do not use 'rename' - might cause trouble if src and dest are on different filesystems.
        HelperFile::copy($srcFile, $pathFileName);

        HelperFile::unlink($srcFile);
    }

    /**
     * Convert 'false' and '<empty string>' to '0'.
     *
     * @param $val
     *
     * @return string
     */
    public static function falseEmptyToZero($val) {
        return ($val == '' || $val == false) ? '0' : $val;
    }

    /**
     * If there is an element $key in array $arr and if that is empty (acts as a switch) or not equal '0': return true, else false
     *
     * @param array $arr
     * @param string $key
     * @return bool  true: if $key exists and a) is empty or b) =='1'
     */
    public static function isEnabled(array $arr, $key) {
        if (!array_key_exists($key, $arr)) {
            return false;
        }

        if ($arr[$key] === '' || $arr[$key] !== '0') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if the string starts with the comment sign - return an empty string.
     * Check if the string starts with an escaped comment sign - strip the escape character.
     * Check if the string starts or ends with an 'escape space' - strip the escape character.
     *
     * @param string $str
     *
     * @return string
     */
    public static function handleEscapeSpaceComment($str) {

        $str = trim($str);
        if ($str == '') {
            return '';
        }
        // Skip comments.
        if ($str[0] == '#') {
            $str = ''; // It's necessary to create an empty entry - E.g. Form.title will not exist if is a comment, but later processing expects that there is an string.
        } else {
            switch (substr($str, 0, 2)) {
                case '\#':
                case '\ ':
                    $str = substr($str, 1);
                    break;
                default:
                    break;
            }

            if (substr($str, -1) == '\\') {
                $str = substr($str, 0, strlen($str) - 1);
            }
        }

        return $str;
    }

    /**
     * TODO: as soon as we don't support PHP 5.6.0 anymore, this local implemention can be removed.
     * Workaround for PHP < 5.6.0: there is no ldap_escape() - use this code instead.
     *
     * @param string $subject The subject string
     * @param string $ignore Set of characters to leave untouched
     * @param int $flags Any combination of LDAP_ESCAPE_* flags to indicate the
     *                        set(s) of characters to escape.
     *
     * @return string
     **/
    public static function ldap_escape($subject, $ignore = '', $flags = 0) {

        if (function_exists('ldap_escape')) {

            return ldap_escape($subject, $ignore, $flags);

        } else {

//            define('LDAP_ESCAPE_FILTER', 0x01);
//            define('LDAP_ESCAPE_DN',     0x02);

            static $charMaps = array(
                LDAP_ESCAPE_FILTER => array('\\', '*', '(', ')', "\x00"),
                LDAP_ESCAPE_DN => array('\\', ',', '=', '+', '<', '>', ';', '"', '#'),
            );
            // Pre-process the char maps on first call
            if (!isset($charMaps[0])) {
                $charMaps[0] = array();
                for ($i = 0; $i < 256; $i++) {
                    $charMaps[0][chr($i)] = sprintf('\\%02x', $i);;
                }
                for ($i = 0, $l = count($charMaps[LDAP_ESCAPE_FILTER]); $i < $l; $i++) {
                    $chr = $charMaps[LDAP_ESCAPE_FILTER][$i];
                    unset($charMaps[LDAP_ESCAPE_FILTER][$i]);
                    $charMaps[LDAP_ESCAPE_FILTER][$chr] = $charMaps[0][$chr];
                }
                for ($i = 0, $l = count($charMaps[LDAP_ESCAPE_DN]); $i < $l; $i++) {
                    $chr = $charMaps[LDAP_ESCAPE_DN][$i];
                    unset($charMaps[LDAP_ESCAPE_DN][$i]);
                    $charMaps[LDAP_ESCAPE_DN][$chr] = $charMaps[0][$chr];
                }
            }
            // Create the base char map to escape
            $flags = (int)$flags;
            $charMap = array();
            if ($flags & LDAP_ESCAPE_FILTER) {
                $charMap += $charMaps[LDAP_ESCAPE_FILTER];
            }
            if ($flags & LDAP_ESCAPE_DN) {
                $charMap += $charMaps[LDAP_ESCAPE_DN];
            }
            if (!$charMap) {
                $charMap = $charMaps[0];
            }
            // Remove any chars to ignore from the list
            $ignore = (string)$ignore;
            for ($i = 0, $l = strlen($ignore); $i < $l; $i++) {
                unset($charMap[$ignore[$i]]);
            }
            // Do the main replacement
            $result = strtr($subject, $charMap);
            // Encode leading/trailing spaces if LDAP_ESCAPE_DN is passed
            if ($flags & LDAP_ESCAPE_DN) {
                if ($result[0] === ' ') {
                    $result = '\\20' . substr($result, 1);
                }
                if ($result[strlen($result) - 1] === ' ') {
                    $result = substr($result, 0, -1) . '\\20';
                }
            }

            return $result;
        }
    }


    /**
     * @param $mode
     * @param $data
     * @return string
     * @throws \UserFormException
     */
    public static function htmlEntityEncodeDecode($mode, $data) {

        switch ($mode) {
            case MODE_ENCODE:
                $data = htmlspecialchars($data, ENT_QUOTES);
                break;
            case MODE_DECODE:
                $data = htmlspecialchars_decode($data, ENT_QUOTES);
                break;
            case MODE_NONE:
                break;
            default:
                throw new \UserFormException('Unknown mode=' . $mode, ERROR_UNKNOWN_MODE);
        }

        return $data;
    }

    /**
     * Calculates a value with 'm', 'k', 'g' in Bytes.
     * @param $size_str
     * @return float|int|string
     */
    public static function returnBytes($size_str) {

        $size_str = trim($size_str);
        switch (substr($size_str, -1)) {
            case 'M':
            case 'm':
                return (int)$size_str * 1048576;
            case 'K':
            case 'k':
                return (int)$size_str * 1024;
            case 'G':
            case 'g':
                return (int)$size_str * 1073741824;
            default:
                return $size_str;
        }
    }

    /**
     * Executes the Command in $cmd
     * RC: if RC==0 Returns Output, else 'RC - Output'
     *
     * @param    string $cmd : command to start
     *
     * @param int $rc
     * @return string The content that is displayed on the website
     */
    public static function qfqExec($cmd, &$rc = 0) {

        exec($cmd, $arr, $rc);

        $output = implode('<br>', $arr);
        if ($rc != 0) {
            $output = "[rc=$rc] $output";
        }

        return $output;
    }

    /**
     * @param string $prefix
     * @return string
     */
    public static function uniqIdQfq($prefix) {

        if (defined('PHPUNIT_QFQ')) {
            return 'badcaffee1234';
        }

        return uniqid();
    }
}