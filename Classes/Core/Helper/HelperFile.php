<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/28/16
 * Time: 8:05 AM
 */

namespace IMATHUZH\Qfq\Core\Helper;


 

/**
 * Class HelperFile
 * @package qfq
 */
class HelperFile {

    /**
     * Iterate over array $files. Delete only named files which are stored in '/tmp/' . DOWNLOAD_FILE_PREFIX.
     *
     * @param array $files
     * @throws \CodeException
     * @throws \UserFormException
     */
    public static function cleanTempFiles(array $files) {

        foreach ($files as $file) {
            if (self::isQfqTemp($file)) {
                self::unlink($file);
            }

            $dir = dirname($file);
            if (self::isQfqTemp($dir)) {
                self::rmdir($dir);
            }
        }
    }

    /**
     * Returns a uniqe (use for temporary) filename, prefixed with QFQ TMP_FILE_PREFIX
     *
     * @return bool|string
     */
    public static function tempnam() {
        return tempnam(sys_get_temp_dir(), TMP_FILE_PREFIX);
    }

    /**
     * Check against standard QFQ Temp location. If it is a Qfq Temp location, return true, else false.
     *
     * @param string $name Absolute filename.
     *
     * @return bool
     */
    public static function isQfqTemp($name) {
        $prefix = sys_get_temp_dir() . '/' . TMP_FILE_PREFIX;
        $len = strlen($prefix);

        return (substr($name, 0, $len) == $prefix);
    }

    /**
     * Creates a temporary directory.
     * Be aware: '/tmp' is under systemd/apache2 (Ubuntu 18...) remapped to something like: '/tmp/systemd-private-...-apache2.service-.../tmp'
     *
     * @return bool|string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public static function mktempdir() {
        $name = tempnam(sys_get_temp_dir(), TMP_FILE_PREFIX);
        self::unlink($name);
        mkdir($name);

        return $name;
    }

    /**
     * Return the mimetype of $pathFilename
     *
     * @param string $pathFilename
     * @param bool $flagIgnoreError
     * @return string
     * @throws \UserFormException
     */
    public static function getMimeType($pathFilename, $flagIgnoreError = false) {

        // E.g.: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=binary'
        $fileMimeType = exec('file --brief --mime ' . $pathFilename, $output, $return_var);
        if ($return_var != 0) {

            if ($flagIgnoreError) {
                return '';
            }

            throw new \UserFormException('Error get mime type of upload.', ERROR_UPLOAD_GET_MIME_TYPE);
        }

        return $fileMimeType;
    }

    /**
     * Returns an array with filestat information to $pathFileName
     *
     * @param $pathFileName
     * @return array
     * @throws \UserFormException
     */
    public static function getFileStat($pathFileName) {
        $vars = [VAR_FILE_MIME_TYPE => '-', VAR_FILE_SIZE => '-'];

        if (empty($pathFileName)) {
            return $vars;
        }

        $pathFileName = self::correctRelativePathFileName($pathFileName);

        if (!file_exists($pathFileName)) {
            return $vars;
        }

        $vars[VAR_FILE_MIME_TYPE] = self::getMimeType($pathFileName);
        $vars[VAR_FILE_SIZE] = filesize($pathFileName);

        if ($vars[VAR_FILE_SIZE] === false) {
            $vars[VAR_FILE_SIZE] = '-';
        }

        return $vars;
    }

    /**
     * Correct $pathFilename, if the cwd is .../qfq/Api'
     *
     * @param $pathFileName
     * @return string
     */
    public static function correctRelativePathFileName($pathFileName) {

        if (empty($pathFileName)) {
            return '';
        }

        if ($pathFileName[0] == '/') {
            return $pathFileName;
        }

        $length = strlen('/' . API_DIR_EXT);
        if (substr(getcwd(), -$length) == '/' . API_DIR_EXT) {
            return '../../../../../' . $pathFileName;
        }

        return $pathFileName;
    }

    /**
     * Split $pathFileName into it's components and fill an array, with array keys like used in STORE_VAR.
     *
     * @param string $pathFileName
     * @return array
     */
    public static function pathInfo($pathFileName) {
        $vars = array();

        $pathParts = pathinfo($pathFileName);
        $vars[VAR_FILENAME] = $pathFileName;

        if (isset($pathParts['basename'])) {
            $vars[VAR_FILENAME_ONLY] = $pathParts['basename'];
        }

        if (isset($pathParts['filename'])) {
            $vars[VAR_FILENAME_BASE] = $pathParts['filename'];
        }

        if (isset($pathParts['extension'])) {
            $vars[VAR_FILENAME_EXT] = $pathParts['extension'];
        }

        return $vars;
    }

    /**
     * Checks the file filetype against the allowed mimeType definition. Return true as soon as one match is found.
     * Types recognized:
     *   * 'mime type' as delivered by `file` which matches a definition on
     *   http://www.iana.org/assignments/media-types/media-types.xhtml
     *   * Joker based: audio/*, video/*, image/*
     *   * Filename extension based: .pdf,.doc,..
     *
     * @param string $pathFileName
     * @param string $origFileName
     * @param string $mimeTypeListAccept
     *
     * @return bool
     * @throws \UserFormException
     */
    public static function checkFileType($pathFileName, $origFileName, $mimeTypeListAccept) {

        // E.g.: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=binary'
        $fileMimeType = self::getMimeType($pathFileName);

        // Strip optional '; charset=binary'
        $arr = explode(';', $fileMimeType, 2);
        $fileMimeType = $arr[0];

        // Split between 'Media Type' and 'Media Subtype'
        $fileMimeTypeSplitted = explode('/', $arr[0], 2);

        $path_parts = pathinfo($origFileName); // to extract the filename extension of the uploaded file.

        // Process all listed mimeTypes (incl. filename extension and joker)
        // $accept e.g.: 'image/*,application/pdf,.pdf'
        $arr = explode(',', $mimeTypeListAccept); // Split multiple defined mimetypes/extensions in single chunks.
        foreach ($arr as $listElementMimeType) {
            $listElementMimeType = trim(strtolower($listElementMimeType));
            if ($listElementMimeType == '') {
                continue; // will be skipped
            } elseif ($listElementMimeType[0] == '.') { // Check for definition 'filename extension'
                if ('.' . strtolower($path_parts['extension']) == $listElementMimeType) {
                    return true;
                }
            } else {
                // Check for Joker, e.g.: 'image/*'
                $splitted = explode('/', $listElementMimeType, 2);

                if ($splitted[1] == '*') {
                    if ($splitted[0] == $fileMimeTypeSplitted[0]) {
                        return true;
                    }
                } elseif ($fileMimeType == $listElementMimeType) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $pathFileName
     * @param int|bool $mode
     * @throws \UserFormException
     */
    public static function chmod($pathFileName, $mode = false) {

        if ($mode !== false) {
            if (false === chmod($pathFileName, $mode)) {
                throw new \UserFormException(
                    json_encode([ERROR_MESSAGE_TO_USER => 'Failed: chmod', ERROR_MESSAGE_TO_DEVELOPER => "Failed: chmod $mode '$pathFileName'"]),
                    ERROR_IO_CHMOD);
            }
        }
    }

    /**
     * @return string
     */
    public static function errorGetLastAsString() {

        if (NULL === ($errors = error_get_last())) {
            return 'error_get_last(): no error';
        }

        return $errors['type'] . ' - ' . $errors['message'];

    }

    /**
     * PHP System function: chdir() with QFQ exception
     *
     * @param $cwd
     * @return string
     * @throws \UserFormException
     */
    public static function chdir($cwd) {

        if (false === chdir($cwd)) {
            $msg = self::errorGetLastAsString() . " - chdir($cwd)";
            throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => 'chdir failed', ERROR_MESSAGE_TO_DEVELOPER => $msg]), ERROR_IO_CHDIR);
        }

        return true;
    }

    /**
     * PHP System function: unlink() with QFQ exception
     *
     * @param $filename
     * @param string $logFilename
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public static function unlink($filename, $logFilename = '') {

        if ($logFilename != '') {
            Logger::logMessageWithPrefix("Unlink: $filename", $logFilename);
        }

        if (false === unlink($filename)) {
            $msg = self::errorGetLastAsString() . " - unlink($filename)";
            throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => 'unlink failed', ERROR_MESSAGE_TO_DEVELOPER => $msg]), ERROR_IO_UNLINK);
        }

        return true;
    }

    /**
     * PHP System function: rmdir() with QFQ exception
     *
     * @param $tempDir
     * @return string
     * @throws \UserFormException
     */
    public static function rmdir($tempDir) {

        if (false === rmdir($tempDir)) {
            $msg = self::errorGetLastAsString() . " - rmdir($tempDir)";
            throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => 'rmdir failed', ERROR_MESSAGE_TO_DEVELOPER => $msg]), ERROR_IO_RMDIR);
        }

        return true;
    }

    /**
     * PHP System function: rename() with QFQ exception
     *
     * @param $oldname
     * @param $newname
     * @return string
     * @throws \UserFormException
     */
    public static function rename($oldname, $newname) {

        if (false === rename($oldname, $newname)) {
            $msg = self::errorGetLastAsString() . " - rename($oldname ,$newname)";
            throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => 'unlink failed', ERROR_MESSAGE_TO_DEVELOPER => $msg]), ERROR_IO_RENAME);
        }

        return true;
    }

    /**
     * PHP System function: copy() with QFQ exception
     *
     * @param $source
     * @param $dest
     * @return string
     * @throws \UserFormException
     */
    public static function copy($source, $dest) {

        self::mkDirParent($dest);
        if (!is_file($dest)) {
            touch($dest);
        }

        if (false === copy($source, $dest)) {

            if (!is_readable($source)) {
                throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => 'copy failed', ERROR_MESSAGE_TO_DEVELOPER => "Can't read file '$source'"]), ERROR_IO_READ_FILE);
            }

            if (!is_writeable($dest)) {
                throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => 'copy failed', ERROR_MESSAGE_TO_DEVELOPER => "Can't write to file '$dest'"]), ERROR_IO_WRITE_FILE);
            }

            $msg = self::errorGetLastAsString(); // Often, there is no specific error string.
            $msg .= " - copy($source, $dest)";
            throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => 'copy failed', ERROR_MESSAGE_TO_DEVELOPER => $msg]), ERROR_IO_COPY);
        }

        return true;
    }

    /**
     * Creates all necessary directories in $pathFileName, but not the last part, the filename. A filename has to be
     * specified.
     *
     * @param string $pathFileName Path with Filename
     * @param bool|int $chmodDir false if not explicit set
     * @throws \UserFormException
     */
    public static function mkDirParent($pathFileName, $chmodDir = false) {
        $path = "";
        $cwd = '';

        // Leading '/' will be removed - chdir to / to still use correct path
        if ($pathFileName[0] == '/') {
            $cwd = getcwd();
            self::chdir('/');
        }

        // Teile "Directory/File.Extension" auf
        $pathParts = pathinfo($pathFileName);


        // Zerlege Pfad in einzelne Directories
        $arr = explode("/", $pathParts["dirname"]);

        // Durchlaufe die einzelnen Dirs und überprüfe ob sie angelegt sind.
        // Wenn nicht, lege sie an.
        foreach ($arr as $part) {

            if ($part == '') {// Happens with '/...'
                continue;
            }

            $path .= $part;

            // Check ob der Pfad ein Link ist
            if ("link" == @filetype($path)) {
                if ("0" == ($path1 = readlink($path))) {
                    throw new \UserFormException("Can't create '$pathFileName': '$path' contains an invalid link.", ERROR_IO_INVALID_LINK);
                }
            } else {
                if (file_exists($path)) {
                    if ("dir" != filetype($path)) {
                        throw new \UserFormException("Can't create '$pathFileName': There is already a file with the same name as '$path'", ERROR_IO_DIR_EXIST_AS_FILE);
                    }
                } else {
                    mkdir($path);
                    HelperFile::chmod($path, $chmodDir);
                }
            }
            $path .= "/";
        }

        if ($cwd != '') {
            self::chdir($cwd);
        }
    }

    /**
     * Determine highlight name, based on the given $highlight or provided filename  (the extension will be used)
     *
     * @param $highlight
     * @param $fileName
     * @return string
     * @throws \UserFormException
     */
    public static function getFileTypeHighlight($highlight, $fileName) {

        $extToFileType = [
            'js' => 'javascript.json'
            , 'javascript' => 'javascript.json'
            , 'qfq' => 'highlight.qfq.json'
            , 'php' => 'highlight.php.json'
            , 'py' => 'highlight.py.json'
            , 'python' => 'highlight.py.json'
            , 'matlab' => 'highlight.m.json'
            , 'm' => 'highlight.m.json'
        ];

        switch ($highlight) {
            case FE_HIGHLIGHT_AUTO:
                $arr = explode('.', $fileName);
                $ext = strtolower(end($arr));
                $highlight = (isset($extToFileType[$ext])) ? $extToFileType[$ext] : '';
                break;
            case FE_HIGHLIGHT_JAVASCRIPT:
            case FE_HIGHLIGHT_QFQ:
            case FE_HIGHLIGHT_PYTHON:
            case FE_HIGHLIGHT_MATLAB:
                $ext = $highlight;
                break;
            case FE_HIGHLIGHT_OFF:
            case '':
                $ext = '';
                break;
            default:
                throw new \UserFormException("Unknown highlight type: " . $highlight, ERROR_UNKNOWN_MODE);
                break;
        }

        if (isset($extToFileType[$ext])) {
            return DIR_HIGHLIGHT_JSON . '/' . $extToFileType[$ext];
        }

        return '';
    }

    /**
     * Get array of split file names. Remove '.', '..', QFQ_TEMP_SOURCE.
     * @param $dir
     * @return array
     * @throws \UserFormException
     */
    public static function getSplitFileNames($dir) {

        // Array of created file names.
        if (false === ($files = scandir($dir))) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'Splitted files not found', ERROR_MESSAGE_TO_DEVELOPER => "[cwd=$dir] scandir(.)" . HelperFile::errorGetLastAsString()]),
                ERROR_PDF2JPEG);
        }

        $new = Array();
        foreach ($files as $key => $value) {

            if ($value == '.' || $value == '..' || $value == QFQ_TEMP_SOURCE) {
                continue;
            }

            // IM 'convert' use a strange auto numbering: if there is only one page: split.jpg, if there are multiple pages, first: split-0.jpg, ....
            if ($value == 'split.jpg') {
                $value = 'split-0.jpg';
                self::rename($dir . '/' . $files[$key], $dir . '/' . $value);
            }

            $new[] = $value;
        }

        return $new;
    }

}

