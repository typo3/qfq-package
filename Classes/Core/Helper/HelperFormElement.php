<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/26/16
 * Time: 11:25 AM
 */

namespace IMATHUZH\Qfq\Core\Helper;

use IMATHUZH\Qfq\Core\Store\Store;


/**
 * Class HelperFormElement
 * @package qfq
 */
class HelperFormElement {

    /**
     * Expand column $keyName to row array as virtual columns.
     * E.g.: [ 'id' => '1', 'name' => 'John', 'parameter' => 'detail=xId:grId\nShowEmptyAtStart=1' ] becomes:
     *  [ 'id' => '1', 'name' => 'John', 'parameter' => 'detail=xId:grId\nShowEmptyAtStart=1', 'detail' => 'xId:grId',
     *  'showEmptyAtStart'='1']
     *
     * @param array $elements
     * @param string $keyName Typically F_PARAMETER or FE_PARAMETER (both: 'parameter')
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function explodeParameterInArrayElements(array &$elements, $keyName) {

        foreach ($elements AS $key => $element) {
            self::explodeParameter($element, $keyName);
            $elements[$key] = $element;
        }
    }


    /**
     * Set default values for given FormElement.
     * Hint: to copy values from Form, copyAttributesToFormElements() is more appropriate.
     *
     * @param array $elements
     *
     * @param array $formSpec
     * @return array
     */
    public static function formElementSetDefault(array $elements, array $formSpec) {

        foreach ($elements AS $key => $element) {
            $elements[$key][FE_TG_INDEX] = 0;
            unset($elements[$key][FE_ADMIN_NOTE]);
//            $elements[$key][FE_DATA_REFERENCE] = '';
        }

        return $elements;
    }

    /**
     * Take all rows from field $element[$keyName] and merge them with $element itself. '$element' grows in size.
     *
     * @param array $element
     * @param string $keyName Typically F_PARAMETER or FE_PARAMETER (both: 'parameter')
     *
     * @param bool $flagAllowOverwrite
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function explodeParameter(array &$element, $keyName, $flagAllowOverwrite = false) {

        // Something to explode?
        if (isset($element[$keyName]) && $element[$keyName] !== '') {
            // Explode
            $arr = KeyValueStringParser::parse($element[$keyName], "=", "\n");

            if (!$flagAllowOverwrite) {
                // Check if some of the exploded keys conflict with existing keys
                $checkKeys = array_keys($arr);
                foreach ($checkKeys AS $checkKey) {
                    if (isset($element[$checkKey])) {
                        $store = Store::getInstance();
                        $store->setVar(SYSTEM_FORM_ELEMENT, Logger::formatFormElementName($element), STORE_SYSTEM);
                        $store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, $keyName, STORE_SYSTEM);
                        throw new \UserFormException("Found reserved keyname '$checkKey'", ERROR_RESERVED_KEY_NAME);
                    }
                }
            }
            $element = array_merge($element, $arr);

            // For retype elements: copy the language specific value.
            if (isset($element[FE_RETYPE_SOURCE_NAME])) {

                if (!empty($element[FE_RETYPE_LABEL])) {
                    $element[FE_LABEL] = $element[FE_RETYPE_LABEL];
                }

                if (!empty($element[FE_RETYPE_NOTE])) {
                    $element[FE_NOTE] = $element[FE_RETYPE_NOTE];
                }
            }

            $element[$keyName] = ''; // to not expand it a second time
        }
    }

    /**
     * Take language specific definitions and overwrite the default values.
     *
     * @param array $formSpecFeSpecNative
     *
     * @param $parameterLanguageFieldName
     * @return array
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function setLanguage(array $formSpecFeSpecNative, $parameterLanguageFieldName) {

        if (is_string($parameterLanguageFieldName) && isset($formSpecFeSpecNative[$parameterLanguageFieldName])) {
            self::explodeParameter($formSpecFeSpecNative, $parameterLanguageFieldName, true);
        }

        return $formSpecFeSpecNative;
    }

    /**
     * Build the FE name: <field>-<record index)
     *
     * @param array $formElement
     * @param string $id
     *
     * @return string
     */
    public static function buildFormElementName(array $formElement, $id) {
        $field = ($formElement[FE_NAME] == '') ? $formElement[FE_ID] : $formElement[FE_NAME];

        return "$field" . HTML_DELIMITER_NAME . "$id";
    }

    /**
     * Build the internal FE name for an imageCut element (used only in SIP): <field>-imageCut
     *
     * @param array $formElement
     * @return string
     */
    public static function AppendFormElementNameImageCut(array $formElement) {
        $field = ($formElement[FE_NAME] == '') ? $formElement[FE_ID] : $formElement[FE_NAME];

        return "$field" . HTML_DELIMITER_NAME . FE_TYPE_IMAGE_CUT;
    }

    /**
     * Build the FE id: <$formId>-<$formElementId>-<$formElementCopy>
     * Attention: Radio's get's an additional index count as fourth parameter (not here).
     *
     * @param $formId
     * @param $formElementId
     * @param $recordId
     * @param $formElementCopy
     *
     * @return string
     */
    public static function buildFormElementId($formId, $formElementId, $recordId, $formElementCopy) {
        return "$formId" . HTML_DELIMITER_ID . "$formElementId" . HTML_DELIMITER_ID . "$recordId" . HTML_DELIMITER_ID . "$formElementCopy";
    }


    /**
     * In an array for $feSpecNative, set FE_HTML_ID for all fe.class=FE_CONTAINER Elements.
     *
     * @param array $feSpecNative
     * @param $formId
     * @param $recordId
     * @param int $formElementCopy
     * @return array
     */
    public static function setFeContainerFormElementId(array $feSpecNative, $formId, $recordId, $formElementCopy = 0) {

        foreach ($feSpecNative as $key => $fe) {

            switch ($fe[FE_CLASS]) {
                case FE_CLASS_CONTAINER:
                    $feSpecNative[$key][FE_HTML_ID] = self::buildFormElementId($formId, $fe[FE_ID], $recordId, $formElementCopy);
                    break;
                default:
                    break;
            }

        }

        return $feSpecNative;
    }

    /**
     * Checkboxen, belonging to one element, grouped together by name: <fe>_<field>_<index>
     *
     * @param string $field
     * @param string $index
     *
     * @return string
     */
    public static function prependFormElementNameCheckBoxMulti($field, $index) {
        return '_' . $index . '_' . $field;
    }

    /**
     * Check all FormElements if there are some with the attribute FE_RETYPE.
     * If yes: duplicate such elements and update FE_NAME, FE_LABEL, FE_NOTE.
     *
     * @param array $elements
     *
     * @return array
     */
    public static function duplicateRetypeElements(array $elements) {
        $arr = array();

        foreach ($elements as $fe) {

            // adjust FE_RETYPE=1
            if (isset($fe[FE_RETYPE]) && ($fe[FE_RETYPE] == '' || $fe[FE_RETYPE] == '1')) {
                $fe[FE_RETYPE] = '1';
            }

            $arr[] = $fe;

            if (isset($fe[FE_RETYPE]) && $fe[FE_RETYPE] == '1') {

                // Reference to Source FormElement
                $fe[FE_RETYPE_SOURCE_NAME] = $fe[FE_NAME];

                // Create copy of FE, adjust name, label, note
                $fe[FE_NAME] .= RETYPE_FE_NAME_EXTENSION;

                if (isset($fe[FE_RETYPE_LABEL])) {
                    $fe[FE_LABEL] = $fe[FE_RETYPE_LABEL];
                    unset($fe[FE_RETYPE_LABEL]);
                }

                if (isset($fe[FE_RETYPE_NOTE])) {
                    $fe[FE_NOTE] = $fe[FE_RETYPE_NOTE];
                    unset($fe[FE_RETYPE_NOTE]);
                }

                $fe[FE_TG_INDEX] = 1; // Not sure if this is helpfull in case of dynamic update - but it will make the element uniqe.

                unset($fe[FE_RETYPE]);
                $arr[] = $fe;
            }
        }

        return $arr;
    }

    /**
     * Copy specific attributes defined on the form to all FormElements.
     *
     * @param array $formSpec
     * @param array $feSpecNative
     *
     * @return mixed
     */
    public static function copyAttributesToFormElements(array $formSpec, array $feSpecNative) {

        // Iterate over all FormElement
        foreach ($feSpecNative as $key => $element) {

            $feSpecNative[$key][F_FE_DATA_PATTERN_ERROR_SYSTEM] = $formSpec[F_FE_DATA_PATTERN_ERROR_SYSTEM];

            Support::setIfNotSet($feSpecNative[$key], F_FE_DATA_PATTERN_ERROR, $formSpec[F_FE_DATA_PATTERN_ERROR]);
            Support::setIfNotSet($feSpecNative[$key], F_FE_DATA_REQUIRED_ERROR, $formSpec[F_FE_DATA_REQUIRED_ERROR]);
            Support::setIfNotSet($feSpecNative[$key], F_FE_DATA_MATCH_ERROR, $formSpec[F_FE_DATA_MATCH_ERROR]);
            Support::setIfNotSet($feSpecNative[$key], F_FE_DATA_ERROR, $formSpec[F_FE_DATA_ERROR]);
            if ($feSpecNative[$key][F_FE_LABEL_ALIGN] == F_FE_LABEL_ALIGN_DEFAULT) {
                $feSpecNative[$key][F_FE_LABEL_ALIGN] = $formSpec[F_FE_LABEL_ALIGN];
            }
            Support::setIfNotSet($feSpecNative[$key], F_FE_REQUIRED_POSITION, $formSpec[F_FE_REQUIRED_POSITION]);
        }

        return $feSpecNative;
    }


    /**
     * Set all necessary keys - subsequent 'isset()' are not necessary anymore.
     *
     * @param array $fe
     *
     * @return array
     */
    public static function initActionFormElement(array $fe) {

        $list = [FE_TYPE, FE_SLAVE_ID, FE_SQL_VALIDATE, FE_SQL_BEFORE, FE_SQL_INSERT, FE_SQL_UPDATE, FE_SQL_DELETE,
            FE_SQL_AFTER, FE_EXPECT_RECORDS, FE_REQUIRED_LIST, FE_MESSAGE_FAIL, FE_SENDMAIL_TO, FE_SENDMAIL_CC,
            FE_SENDMAIL_BCC, FE_SENDMAIL_FROM, FE_SENDMAIL_SUBJECT, FE_SENDMAIL_REPLY_TO, FE_SENDMAIL_FLAG_AUTO_SUBMIT,
            FE_SENDMAIL_GR_ID, FE_SENDMAIL_X_ID, FE_SENDMAIL_X_ID2, FE_SENDMAIL_X_ID3, FE_SENDMAIL_BODY_MODE,
            FE_SENDMAIL_BODY_HTML_ENTITY, FE_SENDMAIL_SUBJECT_HTML_ENTITY];

        foreach ($list as $key) {
            Support::setIfNotSet($fe, $key);
        }

        return $fe;
    }

    /**
     * Set all necessary keys - subsequent 'isset()' are not necessary anymore.
     *
     * @param array $fe
     *
     * @return array
     */
    public static function initUploadFormElement(array $fe) {

        $list = [FE_SQL_BEFORE, FE_SQL_INSERT, FE_SQL_UPDATE, FE_SQL_DELETE, FE_SQL_AFTER];

        foreach ($list as $key) {
            Support::setIfNotSet($fe, $key);
        }

        return $fe;
    }

    /**
     * Prepare code of 'lock', 'password', 'info' to extend a FormElement.
     * The 'info' will always be added, 'lock' and 'password' only on FE with mode=show|required
     * Depending on $showInside:
     *    * true: a button is shown inside the 'input' or 'select' element.
     *    * false: an icon is shown below the FormElement.
     *
     * 'Add' means:
     *    * $rcButton contains all HTML button code. The calling function is responsible to build the correct code.
     *    * $formElement[FE_INPUT_EXTRA_BUTTON_INFO] has been wrapped with HTML-tag and HTML-id.
     *    * $formElement[FE_MODE] - for 'lock' it will be faked to 'readonly'
     *    * $formElement[FE_TYPE] - for 'password' it will be faked to 'password'
     *
     * @param array $formElement
     * @param bool $showInline
     *
     * @return array
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function prepareExtraButton(array $formElement, $showInline) {

        $store = Store::getInstance();

        $infoSymbolInside = $store->getVar(SYSTEM_EXTRA_BUTTON_INFO_INLINE, STORE_SYSTEM);
        $infoSymbolOutside = $store->getVar(SYSTEM_EXTRA_BUTTON_INFO_BELOW, STORE_SYSTEM);

        if (SYSTEM_EXTRA_BUTTON_INFO_POSITION_BELOW == $store->getVar(SYSTEM_EXTRA_BUTTON_INFO_POSITION, STORE_SYSTEM)) {
            $showInline = false;
        }

        $extraButton = '';
        $id = $formElement[FE_HTML_ID];

        if (false !== strpos($formElement[FE_NAME], FE_TEMPLATE_GROUP_NAME_PATTERN)) {
            $id .= '-' . FE_TEMPLATE_GROUP_NAME_PATTERN;
        }

        $formElement[FE_TMP_EXTRA_BUTTON_HTML] = '';

        // INFO: $showinline =- TRUE ('input' elemente)
        if (isset($formElement[FE_INPUT_EXTRA_BUTTON_INFO]) && $showInline) {
            $extraButton .= <<<EOF
            <button class="btn btn-info" onclick="$('#$id-extra-info').slideToggle('swing')">
                $infoSymbolInside
            </button>
EOF;

            $value = $formElement[FE_INPUT_EXTRA_BUTTON_INFO];
            $formElement[FE_INPUT_EXTRA_BUTTON_INFO] = <<<EOF
            <div class="alert alert-info" id="$id-extra-info" style="display: none;">
                <p>$value</p>
            </div>
EOF;
        }

//        <span class="glyphicon glyphicon-info-sign text-info" aria-hidden="true" onclick="$('#$id-extra-info').slideToggle('swing')"></span>

        $js = " onclick=\"$('#$id-extra-info').slideToggle('swing')\" ";
        $arr = explode(' ', $infoSymbolOutside, 2);
        $infoSymbolOutside = $arr[0] . $js . $arr[1];

        // INFO: $showinline == FALSE (e.g. 'textarea' elemente)
        if (isset($formElement[FE_INPUT_EXTRA_BUTTON_INFO]) && !$showInline) {
            $class = $formElement[FE_INPUT_EXTRA_BUTTON_INFO_CLASS];
            $extraButton .= <<<EOF
            <span class="$class">$infoSymbolOutside</span>
EOF;

            $value = $formElement[FE_INPUT_EXTRA_BUTTON_INFO];
            $formElement[FE_INPUT_EXTRA_BUTTON_INFO] = <<<EOF
            <div class="alert alert-info" id="$id-extra-info" style="display: none;">
                <p>$value</p>
            </div>
EOF;
        }

        $skip = (!($formElement[FE_MODE] == FE_MODE_SHOW || $formElement[FE_MODE] == FE_MODE_REQUIRED || $formElement[FE_MODE] == FE_MODE_SHOW_REQUIRED));

        // LOCK
        if (!$skip && HelperFormElement::booleParameter($formElement[FE_INPUT_EXTRA_BUTTON_LOCK] ?? '-')) {
            $formElement[FE_MODE] = FE_MODE_READONLY;

            $extraButton .= <<<EOF
            <button class="btn btn-info"
                    onclick="$('#$id').prop('readonly',!$('#$id').prop('readonly'))">
                <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
            </button>
EOF;
        }

        // PASSWORD
//        if (!$skip && isset($formElement[FE_INPUT_EXTRA_BUTTON_PASSWORD])) {
        if (!$skip && HelperFormElement::booleParameter($formElement[FE_INPUT_EXTRA_BUTTON_PASSWORD] ?? '-')) {

            $formElement[FE_TYPE] = 'password';

            $extraButton .= <<<EOF
            <button class="btn btn-info"
                    onclick="$('#$id').attr('type',$('#$id').attr('type')==='password' ? 'text': 'password')">
                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
            </button>
EOF;
        }

        $formElement[FE_TMP_EXTRA_BUTTON_HTML] = Support::wrapTag('<div class="input-group-btn" style="font-size: 1em;">', $extraButton, true);
        Support::setIfNotSet($formElement, FE_INPUT_EXTRA_BUTTON_INFO);

        return $formElement;
    }

    /**
     * Returns $maxLength if greater than 0, else FE_TEMPLATE_GROUP_DEFAULT_MAX_LENGTH
     *
     * @param $maxLength
     *
     * @return int
     */
    public static function tgGetMaxLength($maxLength) {
        return (empty($maxLength)) ? FE_TEMPLATE_GROUP_DEFAULT_MAX_LENGTH : $maxLength;
    }

    /**
     * Converts a string of '00ff00' and returns a string of '{"red": 0, "green": 255, "blue": 0}'.
     *
     * @param array $formElement
     * @return string
     * @throws \UserFormException
     */
    public static function penColorToHex(array $formElement) {

        if (empty($formElement[FE_DEFAULT_PEN_COLOR])) {
            return '';
        }

        if (strlen($formElement[FE_DEFAULT_PEN_COLOR]) != LENGTH_HEX_COLOR) {
            throw new \UserFormException("Invalid Format for " . FE_DEFAULT_PEN_COLOR .
                ". Expect like '#ffdd00', got: '" . $formElement[FE_DEFAULT_PEN_COLOR] . "'", ERROR_INVALID_OR_MISSING_PARAMETER);
        }

        $rgb['red'] = hexdec($formElement[FE_DEFAULT_PEN_COLOR][0] . $formElement[FE_DEFAULT_PEN_COLOR][1]);
        $rgb['green'] = hexdec($formElement[FE_DEFAULT_PEN_COLOR][2] . $formElement[FE_DEFAULT_PEN_COLOR][3]);
        $rgb['blue'] = hexdec($formElement[FE_DEFAULT_PEN_COLOR][4] . $formElement[FE_DEFAULT_PEN_COLOR][5]);

        return json_encode($rgb);
    }

    /**
     * Depending on value in $requiredPosition the array $classArr will contain the CSS class to align the required mark.
     * @param $requiredPosition
     * @return array
     * @throws \UserFormException
     */
    public static function getRequiredPositionClass($requiredPosition) {
        $classArr[FE_LABEL] = '';
        $classArr[FE_TYPE] = '';
        $classArr[FE_NOTE] = '';

        switch ($requiredPosition) {
            case F_FE_REQUIRED_POSITION_LABEL_LEFT:
                $classArr[FE_LABEL] = CSS_REQUIRED_LEFT;
                break;
            case F_FE_REQUIRED_POSITION_LABEL_RIGHT:
                $classArr[FE_LABEL] = CSS_REQUIRED_RIGHT;
                break;
            case F_FE_REQUIRED_POSITION_INPUT_LEFT:
                $classArr[FE_INPUT] = CSS_REQUIRED_LEFT;
                break;
            case F_FE_REQUIRED_POSITION_INPUT_RIGHT:
                $classArr[FE_INPUT] = CSS_REQUIRED_RIGHT;
                break;
            case F_FE_REQUIRED_POSITION_NOTE_LEFT:
                $classArr[FE_NOTE] = CSS_REQUIRED_LEFT;
                break;
            case F_FE_REQUIRED_POSITION_NOTE_RIGHT:
                $classArr[FE_NOTE] = CSS_REQUIRED_RIGHT;
                break;
            default:
                throw new \UserFormException('Unkown value for ' . F_FE_REQUIRED_POSITION . ': ' . $requiredPosition, ERROR_INVALID_VALUE);
        }

        return $classArr;
    }

    /**
     * Function to check if a parameter is set (no value) or 0 or 1
     * Example:
     *   a) extraButtonLock
     *   b) extraButtonLock=0
     *   c) extraButtonLock=1
     *
     * a) and c) means 'on' and b) means off
     *
     * IMPORTANT: Call this function :   HelperFormElement::booleParameter( $fe[FE_INPUT_EXTRA_BUTTON_LOCK] ?? '-' )
     *
     * @param $data
     * @return bool
     */
    public static function booleParameter($data) {
        return $data == '' || $data == '1';
    }

    /**
     * Calculates (estimate) number of rows needed for a pre filled textarea element.
     * Explode $value into lines. Check each line for number of textarea width rows. Count all and return this number.
     * Apply min & max settings.
     * If no <height> is given, return always 1 - easy way to detect single or multile line input/textarea.
     *
     * @param array $colsRows
     * @param string $value
     * @return int
     */
    public static function textareaAutoHeight(array $colsRows, $value) {
        $cnt = 0;

        // Check
        if (count($colsRows) < 3) {
            return (empty($colsRows[1]) ? 1 : $colsRows[1]);
        }

        if (empty($colsRows[0])) {
            $colsRows[0] = 80; // just a default if no number is given.
        }

        // Explode into lines
        $lines = explode(PHP_EOL, $value);

        foreach ($lines as $line) {
            $cnt += floor(strlen($line) / $colsRows[0]) + 1;
        }

        // Respect min/max.
        if ($cnt < $colsRows[1]) {
            $cnt = $colsRows[1];
        }

        if ($colsRows[2] < $cnt) {
            $cnt = $colsRows[2];
        }

        return $cnt;
    }

}