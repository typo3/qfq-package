<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/10/16
 * Time: 11:47 AM
 */

namespace IMATHUZH\Qfq\Core\Helper;


const SUBSTITUTE = '#%SUB%#';

/**
 * Class OnArray
 * @package qfq
 */
class OnArray {

    /**
     * Builds a string from an assoc array.
     *
     * key/value are combined with $keyValueGlue.
     * values are enclosed by $encloseValue.
     * rows combined with $rowGlue
     *
     * @param array $dataArray
     * @param string $keyValueGlue
     * @param string $rowGlue
     * @param string $encloseValue - char (or string) to enclose the value with.
     *
     * @return string
     */
    public static function toString(array $dataArray, $keyValueGlue = '=', $rowGlue = '&', $encloseValue = '') {

        if (count($dataArray) === 0) {
            return '';
        }

        $dataString = '';

        foreach ($dataArray as $key => $value) {
            if (is_array($value)) {
                $value = self::toString($value);
            }

            $dataString .= $key . $keyValueGlue . $encloseValue . $value . $encloseValue . $rowGlue;
        }

        $glueLength = strlen($rowGlue);

        return substr($dataString, 0, strlen($dataString) - $glueLength);
    }

    /**
     * Sort array by array keys
     *
     * @param array $a
     */
    public static function sortKey(array &$a) {

        $result = array();

        $keys = array_keys($a);
        sort($keys);
        foreach ($keys as $key) {
            $result[$key] = $a[$key];
        }
        $a = $result;

        return;
    }

    /**
     * Trim all elements in an array.
     * The array has to be a 1-dimensional array.
     *
     * @param array $arr
     * @param string $character_mask
     *
     * @return array
     */
    public static function trimArray(array $arr, $character_mask = " \t\n\r\0\x0B") {
        foreach ($arr as $key => $item) {
            $arr[$key] = trim($item, $character_mask);
        }

        return $arr;
    }

    /**
     * Iterates over all records and return those with $row[$column]==$value
     *
     * @param array $dataArray
     * @param       $column
     * @param       $value
     *
     * @return array
     */
    public static function filter(array $dataArray, $column, $value) {
        $result = array();

        foreach ($dataArray as $row) {
            if (isset($row[$column]) && $row[$column] == $value) {
                $result[] = $row;
            }
        }

        return $result;
    }

    /**
     * Iterates over all elements and return those with $needle in $row
     *
     * @param array $dataArray
     * @param $needle
     * @return array
     */
    public static function filterValueSubstring(array $dataArray, $needle) {
        $result = array();

        foreach ($dataArray as $row) {
            if (strpos($row, $needle) !== false) {
                $result[] = $row;
            }
        }

        return $result;
    }


    /**
     * Converts a onedimensional array by using htmlentities on all elements
     *
     * @param array $arr
     *
     * @return array
     */
    public static function htmlentitiesOnArray(array $arr) {
        foreach ($arr as $key => $value) {
            $arr[$key] = htmlentities($arr[$key], ENT_QUOTES);
        }

        return $arr;
    }

    /**
     * Iterates over an key/value array. Each value will be 'var_export'.
     *
     * @param array $arr
     * @return array
     */
    public static function varExportArray(array $arr) {
        foreach ($arr as $key => $value) {
            if (is_array($value)) {
                $arr[$key] = empty($value) ? 'array ()' : var_export($value, true);
            }
        }

        return $arr;
    }

    /**
     * @param array $arr
     * @param string $title
     * @param string $class
     *
     * @return string
     */
    public static function arrayToHtmlTable(array $arr, $title, $class) {

        $html = '';

        foreach ($arr as $key => $value) {
            $html .= '<tr><td>' . $key . '</td><td>' . $value . '</td></tr>';
        }

        return "<table class='$class'><thead><tr><th colspan='2'>$title</th></tr></thead>$html</table>";
    }

    /**
     * @param array $arr
     * @return string
     */
    public static function arrayToLog(array $arr) {

        $output = EXCEPTION_UNIQID . ':: ';
        $output .= (empty($arr[EXCEPTION_UNIQID])) ? ' - ' : $arr[EXCEPTION_UNIQID];
        $output .= PHP_EOL . '------------------------------------------------' . PHP_EOL;

        foreach ($arr as $key => $value) {
            if (!empty($value) && $key != EXCEPTION_UNIQID) {
                $output .= $key . ':: ' . $value . PHP_EOL;
            }
        }

        $output .= '==================================================' . PHP_EOL;

        return $output;
    }

    /**
     * Split Array around $str to $arr around $delimiter. Escaped $delimiter will be preserved.
     *
     * @param string $delimiter
     * @param string $str
     *
     * @return array
     * @throws \UserReportException
     */
    public static function explodeWithoutEscaped($delimiter, $str) {

        if (strpos($str, SUBSTITUTE) !== false) {
            throw new \UserReportException ("Can't replace token by SUBSTITUTE, cause SUBSTITUTE already exist", ERROR_SUBSTITUTE_FOUND);
        }

        $encodedStr = str_replace('\\' . $delimiter, SUBSTITUTE, $str);

        $arr = explode($delimiter, $encodedStr);

        for ($ii = 0; $ii < count($arr); $ii++) {
//            $arr[$ii] = str_replace(SUBSTITUTE, '\\' . $delimiter, $arr[$ii]);
            $arr[$ii] = str_replace(SUBSTITUTE, $delimiter, $arr[$ii]);
        }

        return $arr;
    }

    /**
     * Iterates over $arr and removes all empty (='') elements. Preserves keys
     *
     * @param array $arr
     *
     * @return array
     */
    public static function removeEmptyElementsFromArray(array $arr) {

        $new = array();

//        for($ii=0; $ii<count($arr); $ii++) {
//            if($arr[$ii]!='') {
//                $new[]=$arr[$ii];
//            }
//        }

        $ii = 0;
        foreach ($arr as $key => $value) {
            if ($value != '') {
                if (is_int($key)) {
                    $key = $ii;
                    $ii++;
                }
                $new[$key] = $value;
            }
        }

        return $new;
    }

    /**
     * Convert all values of an array to lowercase.
     *
     * @param array $arr
     *
     * @return array
     */
    public static function arrayValueToLower(array $arr) {
        $new = array();

        foreach ($arr as $key => $value) {
            $new[$key] = strtolower($value);
        }

        return $new;
    }

    /**
     * Remove from all keynames an optional '_'.
     *
     * @param array $arr
     *
     * @return array
     */
    public static function keyNameRemoveLeadingUnderscore(array $arr) {

        foreach ($arr as $key => $value) {
            if ($key[0] == TOKEN_COLUMN_CTRL) {
                $newKey = substr($key, 1);
                if (!empty($newKey)) {
                    $arr[$newKey] = $value;
                    unset($arr[$key]);
                }
            }
        }

        return $arr;
    }

    /**
     * Search in array $dest for all $keyNames if they exist. If not, check if they exist in $src. If yes, copy.
     *
     * @param array $src
     * @param array $dest
     * @param array $keyNames
     *
     * @return array $dest filled with new values
     */
    public static function copyArrayItemsIfNotAlreadyExist(array $src, array $dest, array $keyNames) {

        foreach ($keyNames as $key) {

            if (!isset($dest[$key])) {
                if (isset($src[$key])) {
                    $dest[$key] = $src[$key];
                }
            }
        }

        return $dest;
    }

    /**
     * Copies all items whose $keyNames are listed to $new and return $new
     *
     * @param array $src
     * @param array $keyNames
     * @param bool $createMissing
     *
     * @param bool $skipZero
     * @param bool $skipEmpty
     * @return array
     */
    public static function getArrayItems(array $src, array $keyNames, $createMissing = false, $skipZero = false, $skipEmpty = false) {
        $new = array();

        // Extract necessary elements
        foreach ($keyNames as $key) {
            if (isset($src[$key])) {

                if (($src[$key] === '' && $skipEmpty) || ($src[$key] == '0' && $skipZero)) {
                    $copy = false;
                } else {
                    $new[$key] = $src[$key];
                }

            } elseif ($createMissing == true) {
                $new[$key] = '';
            }
        }

        return $new;
    }

    /**
     * Copies all items whose keyNames starts with $keyName.
     * Remove Prefix '$keyName' from all keys.
     * Return $new
     *
     * E.g. [ 'mode' => 'pdf', '1_pageId' => 123, '2_file' => 'example.pdf' ], with $keyName='1_' >> [ 'pageId' => 123 ]
     *
     * @param array $src
     * @param string $keyName
     *
     * @return array
     */
    public static function getArrayItemKeyNameStartWith(array $src, $keyName) {
        $new = array();
        $length = strlen($keyName);

        // Extract necessary elements
        foreach ($src as $key => $value) {
            $keyTmp = substr($key, 0, $length);
            if ($keyTmp == $keyName) {
                $newKey = substr($key, $length);
                $new[$newKey] = $value;
            }
        }

        return $new;
    }

    /**
     * Iterates over an array and replaces $search with $replace in all elements. Returns the new array.
     *
     * @param array $src
     * @param       $search
     * @param       $replace
     *
     * @return array
     */
    public static function arrayValueReplace(array $src, $search, $replace) {
        $new = array();

        foreach ($src AS $key => $element) {
            $new[$key] = str_replace($search, $replace, $element);
        }

        return $new;
    }

    /**
     * Performs escapeshellarg() on all elements of an array.
     *
     * @param array $src
     *
     * @return array
     */
    public static function arrayEscapeshellarg(array $src) {
        $new = array();

        foreach ($src as $key => $value) {
            if (is_array($value)) {
                $new[$key] = self::arrayEscapeshellarg($value);
            } else {
                $new[$key] = escapeshellarg($value);
            }
        }

        return $new;
    }

    /**
     * @param array $data
     *
     * @return string md5 of implode $data
     */
    public static function getMd5(array $data) {
        return md5(implode($data));
    }
}