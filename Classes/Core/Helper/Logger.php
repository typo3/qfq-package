<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 2/18/16
 * Time: 5:30 PM
 */

namespace IMATHUZH\Qfq\Core\Helper;

 

/**
 * Class Logger
 * @package qfq
 */
class Logger {

    /**
     * @var String
     */
    private static $systemSitePath = '';

    /**
     * Copy the SystemSitePath to a local variable.
     *
     * @param $path
     */
    public static function setSystemSitePath($path) {
        self::$systemSitePath = $path;
    }

    /**
     * Append $msg to $filename. Create the file it it not exist.
     *
     * @param $msg
     * @param $filename
     *
     * @param string $mode
     * @param bool $recursion
     * @throws \CodeException
     * @throws \UserFormException
     */
    public static function logMessage($msg, $filename, $mode = FILE_MODE_APPEND, $recursion = false) {

        $handle = false;

        if ($filename == '') {
            return;
        }

        $filename = self::makePathAbsolute($filename);

        try {
            $handle = fopen($filename, $mode);
        } catch (\Exception $e) {
            $dummy = 1;
        }

        $cwd1 = getcwd();

        if ($handle === false) {

            if ($recursion) {
                throw new \UserFormException(
                    json_encode([ERROR_MESSAGE_TO_USER => 'Error: cannot open file',
                        ERROR_MESSAGE_TO_DEVELOPER => "Error - cannot open. File: " . $filename .
                            " ( CWD: " . getcwd() . ") - " . HelperFile::errorGetLastAsString()]),
                    ERROR_IO_OPEN);
            }

            // If open fails, maybe the directory does not exist. Create it:
            HelperFile::mkDirParent($filename);
            self::logMessage($msg, $filename, $mode, true);
            return;
        }

        if (fwrite($handle, $msg . PHP_EOL) === false) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'Error: cannot write file',
                    ERROR_MESSAGE_TO_DEVELOPER => "Error - cannot open. File: " . $filename .
                        " ( CWD: " . getcwd() . ") - " . HelperFile::errorGetLastAsString()]),
                ERROR_IO_WRITE);
        }

        fclose($handle);
    }

    /**
     * Prefix every message with linePre().
     *
     * @param $msg
     * @param $filename
     * @param string $mode
     * @throws \CodeException
     * @throws \UserFormException
     */
    public static function logMessageWithPrefix($msg, $filename, $mode = FILE_MODE_APPEND) {
        self::logMessage(self::linePre() . $msg, $filename, $mode);
    }

    /**
     * In case $filename is not absolute and if we're in the API directory: Check if we're in api - update relative filename
     *
     * @param $filename
     * @return string
     */
    private static function makePathAbsolute($filename) {

        if (isset($filename[0]) && $filename[0] != '/') {

            if (self::$systemSitePath == '') {

                if (defined('PHPUNIT_QFQ')) {
                    if (strpos(getcwd(), 'qfq/' . API_DIR_EXT) !== false) {
                        return ('../../../../../' . $filename);
                    }
                    return $filename;
                }

                // In case of attack detection, the config is not available - extract the installation directory from the server vars.
                return dirname($_SERVER['SCRIPT_FILENAME']) . DIRECTORY_SEPARATOR . $filename;
//                throw new \CodeException('SystemSitePath is not set and the given logfile should be made absolute.', ERROR_MISSING_VALUE);
            }

            return self::$systemSitePath . DIRECTORY_SEPARATOR . $filename;
        }

        return $filename;
    }

    /**
     * Returns a timestamp, IP, cookie.
     *
     * @return string
     */
    public static function linePre() {

        $cookie = isset($_COOKIE[SESSION_NAME]) ? $_COOKIE[SESSION_NAME] : '<no session cookie>';

        $str = '[' . date('Y-m-d H:i:s');
        $str .= ' / ' . htmlentities(empty($_SERVER['REMOTE_ADDR']) ? '<no ip>' : $_SERVER['REMOTE_ADDR']);
//        $str .= ' / ' . htmlentities(empty($_SERVER['HTTP_USER_AGENT']) ? '<no user agent>' : $_SERVER['HTTP_USER_AGENT']);
        $str .= ' / ' . htmlentities($cookie);


        $str .= '] ';

        return $str;
    }

    /**
     * Format details of a FormElement.
     *
     * @param array $fe
     * @return string
     */
    public static function formatFormElementName(array $fe) {

        return ($fe['id']??'') . ' / ' . ($fe[FE_NAME]??'') . ' / ' . ($fe[FE_LABEL]??'');
    }

    /**
     * Logs a line to all of the given logfiles.
     * If $data is an array, it will be json encoded.
     *
     * @param array $form
     * @param $pre
     * @param $data
     * @param bool $flagNewLineFirst
     * @throws \CodeException
     * @throws \UserFormException
     */
    public static function logFormLine(array $form, $pre, $data, $flagNewLineFirst = false) {

        $line = (is_array($data)) ? json_encode($data) : $data;
        $line = '[' . $pre . '] ' . $line;

        if ($flagNewLineFirst) {
            $line = PHP_EOL . $line;
        }

        foreach ([FORM_LOG_FILE_ALL, FORM_LOG_FILE_SESSION] as $filename) {
            if (!empty($form[$filename])) {
                Logger::logMessage($line, $form[$filename]);
            }
        }
    }
}