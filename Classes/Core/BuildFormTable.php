<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/25/16
 * Time: 9:59 PM
 */

namespace IMATHUZH\Qfq\Core;

 
use IMATHUZH\Qfq\Core\Helper\OnArray;
use IMATHUZH\Qfq\Core\Helper\Support;


/**
 * Class BuildFormTable
 * @package qfq
 */
class BuildFormTable extends AbstractBuildForm {

    /**
     *
     */
    public function fillWrap() {
        $this->wrap[WRAP_SETUP_TITLE][WRAP_SETUP_START] = '<h3>';
        $this->wrap[WRAP_SETUP_TITLE][WRAP_SETUP_END] = '</h3>';

        $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_CLASS] = '';
        $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_START] = '<tr>';
        $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_END] = '</tr>';

        $this->wrap[WRAP_SETUP_LABEL][WRAP_SETUP_START] = '<td align="right">';
        $this->wrap[WRAP_SETUP_LABEL][WRAP_SETUP_END] = '</td>';
        $this->wrap[WRAP_SETUP_INPUT][WRAP_SETUP_START] = '<td>';
        $this->wrap[WRAP_SETUP_INPUT][WRAP_SETUP_END] = '</td>';
        $this->wrap[WRAP_SETUP_NOTE][WRAP_SETUP_START] = '<td>';
        $this->wrap[WRAP_SETUP_NOTE][WRAP_SETUP_END] = '</td>';

        $this->wrap[WRAP_SETUP_SUBRECORD][WRAP_SETUP_START] = '<p>';
        $this->wrap[WRAP_SETUP_SUBRECORD][WRAP_SETUP_END] = '</p>';

        $this->wrap[WRAP_SETUP_IN_FIELDSET][WRAP_SETUP_START] = '<p>';
        $this->wrap[WRAP_SETUP_IN_FIELDSET][WRAP_SETUP_END] = '</p>';

        $this->wrap[WRAP_SETUP_IN_TEMPLATE_GROUP][WRAP_SETUP_START] = "";
        $this->wrap[WRAP_SETUP_IN_TEMPLATE_GROUP][WRAP_SETUP_END] = "";

    }

    /**
     * @param $label
     * @param $input
     * @param $note
     */
    public function fillWrapLabelInputNote($label, $input, $note) {

    }

    /**
     * @return string
     */
    public function getProcessFilter() {
        return FORM_ELEMENTS_NATIVE;
    }

    /**
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function doSubrecords() {
        //TODO: $json is not returned - which is wrong. In this case, dynamic update won't work for subrecords
        $json = array();

        return $this->elements($this->store->getVar(SIP_RECORD_ID, STORE_SIP), FORM_ELEMENTS_SUBRECORD, $json);
    }

    /**
     * @param string $mode
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function head($mode = FORM_LOAD) {
        $html = '';

        $html .= '<div ' . Support::doAttribute('class', $this->formSpec[F_CLASS], true) . '>'; // main <div class=...> around everything

        // Logged in BE User will see a FormEdit Link
        $sipParamString = OnArray::toString($this->store->getStore(STORE_SIP), ':', ', ', "'");
        $formEditUrl = $this->createFormEditorUrl(FORM_NAME_FORM, $this->formSpec[F_ID]);
        $html .= "<p><a " . Support::doAttribute('href', $formEditUrl) . ">Edit</a><small>[$sipParamString]</small></p>";

        $deleteUrl = $this->createDeleteUrl($this->formSpec[F_FINAL_DELETE_FORM], $this->store->getVar(SIP_RECORD_ID, STORE_SIP));
        $html .= "<p><a " . Support::doAttribute('href', $deleteUrl) . ">Delete</a>";

        $html .= $this->wrapItem(WRAP_SETUP_TITLE, $this->formSpec[F_TITLE], true);
        $html .= $this->getFormTag();
        $html .= '<table>';

        return $html;
    }

    /**
     * @param array $formElement
     *
     * @param $htmlElement
     * @param $htmlFormElementName
     * @return string
     */
    public function buildRowNative(array $formElement, $htmlElement, $htmlFormElementName) {
        $html = '';

        // Construct Marshaller Name
        $buildElementFunctionName = 'build' . $this->buildElementFunctionName[$formElement[FE_TYPE]];

        if ($formElement[FE_TYPE] === 'subrecord') {
            // subrecord in render='table' are outside the table
            $html .= $this->wrapItem(WRAP_SETUP_SUBRECORD, $formElement[FE_LABEL]);
            $html .= $this->wrapItem(WRAP_SETUP_SUBRECORD, $htmlElement);
            $html .= $this->wrapItem(WRAP_SETUP_SUBRECORD, $formElement[FE_NOTE]);
        } else {
            if ($formElement['nestedInFieldSet'] === 'no') {
                $html .= $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_START];
                $html .= $this->wrapItem(WRAP_SETUP_LABEL, $formElement[FE_LABEL]);
                $html .= $this->wrapItem(WRAP_SETUP_INPUT, $htmlElement);
                $html .= $this->wrapItem(WRAP_SETUP_NOTE, $formElement[FE_NOTE]);
                $html .= $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_END];
            } else {
                $html .= $this->wrapItem(WRAP_SETUP_IN_FIELDSET, $formElement[FE_LABEL]);
                $html .= $this->wrapItem(WRAP_SETUP_IN_FIELDSET, $htmlElement);
                if ($formElement[FE_NOTE] !== '')
                    $html .= $this->wrapItem(WRAP_SETUP_IN_FIELDSET, $formElement[FE_NOTE]);
            }
        }

        return $html;
    }

    /**
     * @return string
     */
    public function tail() {
        $html = '';


        $html .= $this->wrapItem(WRAP_SETUP_LABEL, '', false);
        $html .= $this->wrapItem(WRAP_SETUP_INPUT, '<input type="submit" value="Submit">');
        $html = $this->wrapItem(WRAP_SETUP_ELEMENT, $html);
        $html .= '</table>';
//        $html .= $this->buildNewSip();
        $html .= '</form>';
        $html .= '</div>'; // main <div class=...> around everything


        return $html;
    }

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed|void
     */
    public function buildRowPill(array $formElement, $elementHtml) {
        // TODO: Implement buildRowPill() method.
    }

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed|void
     */
    public function buildRowFieldset(array $formElement, $elementHtml) {
        // TODO: Implement buildRowFieldset() method.
    }

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed|void
     */
    public function buildRowTemplateGroup(array $formElement, $elementHtml) {
        // TODO: Implement buildRowTemplate() method.
    }

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed|void
     */
    public function buildRowSubrecord(array $formElement, $elementHtml) {
        // TODO: Implement buildRowSubrecord() method.
    }
}