<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/25/16
 * Time: 9:46 PM
 */

namespace IMATHUZH\Qfq\Core;


 

/**
 * Class BuildFormPlain
 * @package qfq
 */
class BuildFormPlain extends AbstractBuildForm {

    /**
     *
     */
    public function fillWrap() {
        $this->wrap[WRAP_SETUP_TITLE][WRAP_SETUP_START] = '<h3>';
        $this->wrap[WRAP_SETUP_TITLE][WRAP_SETUP_END] = '</h3>';

        $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_CLASS] = '';
        $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_START] = '<p>';
        $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_END] = '</p>';

        $this->wrap[WRAP_SETUP_LABEL][WRAP_SETUP_START] = '';
        $this->wrap[WRAP_SETUP_LABEL][WRAP_SETUP_END] = '';
        $this->wrap[WRAP_SETUP_INPUT][WRAP_SETUP_START] = '';
        $this->wrap[WRAP_SETUP_INPUT][WRAP_SETUP_END] = '';
        $this->wrap[WRAP_SETUP_NOTE][WRAP_SETUP_START] = '';
        $this->wrap[WRAP_SETUP_NOTE][WRAP_SETUP_END] = '';

        $this->wrap[WRAP_SETUP_IN_FIELDSET][WRAP_SETUP_START] = '<p>';
        $this->wrap[WRAP_SETUP_IN_FIELDSET][WRAP_SETUP_END] = '</p>';

        $this->wrap[WRAP_SETUP_IN_TEMPLATE_GROUP][WRAP_SETUP_START] = "";
        $this->wrap[WRAP_SETUP_IN_TEMPLATE_GROUP][WRAP_SETUP_END] = "";

    }

    /**
     * @param $label
     * @param $input
     * @param $note
     */
    public function fillWrapLabelInputNote($label, $input, $note) {

    }

    /**
     * @return string
     */
    public function getProcessFilter() {
        return FORM_ELEMENTS_NATIVE;
    }

    /**
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function doSubrecords() {
        $json = array();

        //TODO: $json is not returned - which is wrong. In this case, dynamic update won't work for subrecords
        return $this->elements($this->store->getVar(SIP_RECORD_ID, STORE_SIP), FORM_ELEMENTS_SUBRECORD, $json);
    }

    /**
     * @param array $formElement
     * @param $htmlElement
     * @param $htmlFormElementName
     * @return string
     */
    public function buildRowNative(array $formElement, $htmlElement, $htmlFormElementName) {
        $html = '';

        // Construct Marshaller Name
//        $buildElementFunctionName = 'build' . $this->buildElementFunctionName[$formElement[FE_TYPE]];


        if ($formElement['nestedInFieldSet'] === 'no') {
            $html .= $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_START];
        }
        $html .= $this->wrapItem(WRAP_SETUP_LABEL, $formElement[FE_LABEL]);
        $html .= $this->wrapItem(WRAP_SETUP_INPUT, $htmlElement);
        $html .= $this->wrapItem(WRAP_SETUP_NOTE, $formElement[FE_NOTE]);
        if ($formElement['nestedInFieldSet'] === 'no') {
            $html .= $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_END];
        }

        return $html;
    }

    /**
     * @return string
     */
    public function tail() {
        $html = '';

//        $html .= $this->buildNewSip();

        $html .= $this->wrapItem(WRAP_SETUP_INPUT, '<input type="submit" value="Submit">');
        $html = $this->wrapItem(WRAP_SETUP_ELEMENT, $html);
        $html .= '</form>';
        $html .= '</div>'; // main <div class=...> around everything

        return $html;
    }


    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed|void
     */
    public function buildRowPill(array $formElement, $elementHtml) {
        // TODO: Implement buildRowPill() method.
    }

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed|void
     */
    public function buildRowFieldset(array $formElement, $elementHtml) {
        // TODO: Implement buildRowFieldset() method.
    }

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed|void
     */
    public function buildRowTemplateGroup(array $formElement, $elementHtml) {
        // TODO: Implement buildRowTemplate() method.
    }

    /**
     * @param array $formElement
     * @param $elementHtml
     * @return mixed|void
     */
    public function buildRowSubrecord(array $formElement, $elementHtml) {
        // TODO: Implement buildRowSubrecord() method.
    }
}