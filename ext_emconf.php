<?php
/**
 * @author Carsten Rose <carsten.rose@math.uzh.ch>
 */

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Quick Form Query',
    'description' => 'Framework to build web applications: Form generator (bootstrap based), dynamic update, typeahead, multi language, image annotation (via fabric), reports (defined via SQL), SIP protected links, PDF rendering, send mail (dynamic attachments, PDFs), multiple databases, record locking, secure up/download.',
    'category' => 'fe',
    'author' => 'Carsten Rose, Benjamin Baer',
    'author_email' => 'carsten.rose@math.uzh.ch',
    'dependencies' => 'fluid,extbase',
    'clearcacheonload' => true,
    'state' => 'stable',
    'version' => '19.9.1',
    'constraints' => [
        'depends' => [
            'typo3' => '7.0.0-9.2.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],

);

