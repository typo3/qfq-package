<?php
/**
 * @author Carsten Rose <carsten.rose@math.uzh.ch>
 */

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'IMATHUZH.' . $_EXTKEY,
    'Qfq',
    'QFQ Element',
    'EXT:qfq/ext_icon.png'
);

