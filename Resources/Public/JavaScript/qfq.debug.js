/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global console */
/* global CodeMirror */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initializes codemirror. Only `<textarea/>` elements having the class `qfq-codemirror` are initialized.
     *
     * The codemirror configuration has to be provided in the `data-config` attribute as JSON. E.g.
     *
     *      <textarea class="qfq-codemirror" data-config='{ "mode": "text/x-sql", "lineNumbers": true }'></textarea>
     *
     * @function
     */
    var codemirror = function () {
        if (typeof CodeMirror === 'undefined') {
            //QfqNS.log.error("CodeMirror not loaded, cannot initialize Qfq CodeMirror.");
            return;
        }

        $("textarea.qfq-codemirror").each(
            function () {
                var config = {};
                var $this = $(this);
                var configData = $this.data('config');
                if (configData) {
                    if (configData instanceof Object) {
                        // jQuery takes care of decoding data-config to JavaScript object.
                        config = configData;
                    } else {
                        QfqNS.Log.warning("'data-config' is invalid: " + configData);
                    }
                }
                var cm = CodeMirror.fromTextArea(this, configData);
                cm.on('change', (function ($form, $textArea) {
                    return function (instance, changeObj) {
                        $textArea.val(instance.getValue());
                        $form.change();
                    };
                })($(this).closest('form'), $this));
            }
        );
    };

    n.codemirror = codemirror;


})(QfqNS.Helper);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */


/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initialize a jqxComboBox.
     *
     * Only `<div>` elements having a `jqw-combobox` class are taken into account during initialization.
     *
     * Data for the jqxComboBox has to be provided in a `<script>` element. In order for the widget to find the script
     * tag, the `<div>` element requries a `data-control-name` attribute. The id of the corresponding `<script>` tag is
     * derived by appending `_source` to the value of `data-control-name`.
     *
     * The content of the data `<script>` element has to be a valid JSON array of objects, e.g.
     *
     *     [
     *       {"text": "A", "value": "1"},
     *       {"text": "B", "value": "2"},
     *       ...
     *     ]
     *
     * The `text` and `value` attributes are required. `text` will be displayed to the enduser, and `value` is
     * submitted to the backend.
     *
     * @function
     * @name QfqNS.Helper.jqxComboBox
     */
    var jqxComboBox = function () {
        var index;
        var $containers = $("div.jqw-combobox");

        $containers.each(function (index, object) {
            (function ($container) {
                var controlName = $container.data('control-name');
                if (!controlName) {
                    QfqNS.Log.error("jqwComboBox container does not have a 'data-control-name' attribute.");
                    return;
                }

                var sourceId = controlName + "_source";
                var $sourceScript = $('#' + sourceId);
                if ($sourceScript.length !== 1) {
                    QfqNS.Log.error("Unable to find data for jqwComboBox using id '" + sourceId + "'");
                    return;
                }

                var source = JSON.parse($sourceScript.text());


                $container.jqxComboBox({
                    source: source,
                    displayMember: "text",
                    valueMember: "value"
                });

                // Our code creates a hidden input element for each jqxwidget as sibling of the widget. We do this,
                // because jqxwidget don't create named input elements, and thus the value would not be sent to the
                // server using a Plain Old Form submission (even if performed by an ajax request).
                var $hiddenInput = $("<input>")
                    .attr('type', 'hidden')
                    .attr('name', controlName);

                $container.after($hiddenInput);

                $hiddenInput.val($container.jqxComboBox('val'));

                $container.on('change', function (event) {
                    $hiddenInput.val(event.args.item.value);
                });
            })($(object));
        });
    };

    n.jqxComboBox = jqxComboBox;

})(QfqNS.Helper);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */


/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initializes jqxDateTimeInput widgets.
     *
     * It configures all `<div>`s having a class `jqw-datetimepicker` as jqxDateTimeInput.
     *
     * Given the HTML snippet
     *
     *     ...
     *     <div class="jqw-datetimepicker" data-control-name="datetimepicker" ></div>
     *     ...
     *
     * it will create a hidden input sibling element of the `<div>` where the selected date is stored for plain old
     * form submission, thus rendering the above snippet effectively to
     *
     *     ...
     *     <div class="jqw-datetimepicker" data-control-name="datetimepicker" ></div>
     *     <input type="hidden" name="datetimepicker">
     *     ...
     *
     * The jqxDateTimeInput can be configured using following `data` attributes
     *
     *  * `data-control-name`: Mandatory attribute. Hold the name of the input element.
     *  * `data-format-string': Optional Format string as required by jqxDateTimeInput. See also
     *    {@link http://www.jqwidgets.com/jquery-widgets-documentation/documentation/jqxdatetimeinput/jquery-datetimeinput-api.htm}.
     *    Default: "F".
     *  * `data-show-time-button`: Boolean value `true` or `false`, indicating whether or not a time picker will be
     *    displayed.
     *
     * @function
     * @name QfqNS.Helper.jqxDateTimeInput
     */
    var jqxDateTimeInput = function () {
        var index;
        var $containers = $("div.jqw-datetimepicker");

        $containers.each(function (index, object) {
            (function ($container) {
                var controlName = $container.data('control-name');
                if (!controlName) {
                    QfqNS.Log.error("jqwDateTimePicker does not have a 'data-control-name' attribute.");
                    return;
                }

                var formatString = $container.data('format-string');
                if (!formatString) {
                    formatString = "F";
                }

                var showTimeButton = $container.data('show-time-button');
                if (showTimeButton === undefined) {
                    showTimeButton = false;
                }

                var jqxDateTimeInputConfig = {
                    formatString: formatString,
                    showTimeButton: showTimeButton,
                    theme: "bootstrap"
                };

                $container.jqxDateTimeInput(jqxDateTimeInputConfig);

                // Our code creates a hidden input element for each jqxwidget as sibling of the widget. We do this,
                // because jqxwidget don't create named input elements, and thus the value would not be sent to the
                // server using a Plain Old Form submission (even if performed by an ajax request).
                var $hiddenInput = $("<input>")
                    .attr('type', 'hidden')
                    .attr('name', controlName);

                $container.after($hiddenInput);

                $hiddenInput.val($container.jqxDateTimeInput('value').toISOString());

                $container.on('valueChanged', function (event) {
                    $hiddenInput.val(event.args.date.toISOString());
                });
            })($(object));
        });

    };

    n.jqxDateTimeInput = jqxDateTimeInput;


})(QfqNS.Helper);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initialize a jqxEditor widget on all `<textarea>` elements having the class `jqw-editor`.
     * @function
     * @name QfqNS.Helper.jqxEditor
     */
    var jqxEditor = function () {
        var index;
        var $containers = $("textarea.jqw-editor");

        $containers.each(function (index, object) {
            (function ($container) {
                $container.jqxEditor();
            })($(object));
        });

    };

    n.jqxEditor = jqxEditor;


})(QfqNS.Helper);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     *
     * @param jqHXR
     * @param textStatus
     * @param errorThrown
     *
     * @function QfqNS.Helper.showAjaxError
     */
    n.showAjaxError = function (jqHXR, textStatus, errorThrown) {
        var alert = new QfqNS.Alert("Error:<br> " +
            errorThrown, "error");
        alert.show();
    };

    /**
     *
     * @param string
     * @returns {*}
     *
     * @function QfqNS.Helper.stringBool
     */
    n.stringToBool = function (string) {
        if (typeof string !== "string") {
            return string;
        }
        var lowerCase = string.toLowerCase().trim();

        switch (lowerCase) {
            case "1":
            case "yes":
            case "y":
            case "t":
            case "true":
            case "enabled":
            case "enable":
                return true;
            case "0":
            case "no":
            case "n":
            case "f":
            case "false":
            case "disabled":
            case "disable":
                return false;
            default:
                return false;
        }
    };
})(QfqNS.Helper);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global console */
/* global tinymce */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initialize tinyMce. All HTML elements having the `qfq-tinymce` class will be initialized. TinyMce configuration
     * is provided in the `data-config` attribute. Each element further requires the `id` attribute to be present.
     *
     * @function
     */
    var tinyMce = function () {
        if (typeof tinymce === 'undefined') {
            //QfqNS.log.error("tinymce not loaded, cannot initialize Qfq tinymce.");
            return;
        }

        $(".qfq-tinymce").each(
            function () {
                var config = {};
                var $this = $(this);
                var tinyMCEId = $this.attr('id');
                if (!tinyMCEId) {
                    QfqNS.Log.warning("TinyMCE container does not have an id attribute. Ignoring.");
                    return;
                }

                var configData = $this.data('config');
                if (configData) {
                    if (configData instanceof Object) {
                        // jQuery takes care of decoding data-config to JavaScript object.
                        config = configData;
                    } else {
                        QfqNS.Log.warning("'data-config' is invalid: " + configData);
                    }
                }

                config.selector = "#" + QfqNS.escapeJqueryIdSelector(tinyMCEId);
                config.setup = function (editor) {
                    editor.on('Change', function (e) {
                        // Ensure the associated form is notified of changes in editor.
                        QfqNS.Log.debug('Editor was changed');
                        var eventTarget = e.target;
                        var $parentForm = $(eventTarget.formElement);
                        $parentForm.trigger("change");

                    });
                };

                tinymce.init(config);
            }
        );
    };

    /**
     * Force update of the shadowed `<textarea>`. Usually called before a form submit.
     */
    tinyMce.prepareSave = function () {
        if (typeof tinymce === 'undefined') {
            return;
        }

        tinymce.triggerSave();
    };

    n.tinyMce = tinyMce;


})(QfqNS.Helper);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';

    /**
     * Known values of `type` attribute of `<input>` elements
     *
     * @type {string[]}
     */
    n.knownElementTypes = [
        'text',
        'password',
        'checkbox',
        'radio',
        'button',
        'submit',
        'reset',
        'file',
        'hidden',
        'image',
        'datetime',
        'datetime-local',
        'date',
        'month',
        'time',
        'week',
        'number',
        'range',
        'email',
        'url',
        'search',
        'tel',
        'color'
    ];

    /*
     * See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input for input types ignoring the readonly
     * attribute.
     */
    n.readOnlyIgnored = [
        'hidden',
        'range',
        'checkbox',
        'radio',
        'file',
        'select'
    ];

})(QfqNS.Element);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';

    /**
     * Form Group represents a `<input>/<select>` element including the label and help block.
     *
     * It is not meant to be used directly. Use the specialized objects instead.
     *
     * The parameter `elementSelector` seems to be redundant, but `$enclosedElement` can be any element enclosed in
     * the form group, thus we need a way to identify the actual form element(s).
     *
     * @param $enclosedElement {jQuery} a jQuery object contained in the Form Group. It used to find the enclosing
     * HTML element having the `.form-group` class assigned.
     *
     * @param elementSelector {string} jQuery selector selecting the form element. It is used to find all elements
     * contained in the form group identified by $enclosedElement.
     *
     * @constructor
     * @name QfqNS.Element.FormGroup
     */
    n.FormGroup = function ($enclosedElement, elementSelector) {
        elementSelector = elementSelector || 'input:not([type="hidden"])';
        if (!$enclosedElement || $enclosedElement.length === 0) {
            throw new Error("No enclosed element");
        }

        this.$formGroup = this.$findFormGroup($enclosedElement);
        this.$element = this.$formGroup.find(elementSelector);
        this.$label = this.$formGroup.find('.control-label');
        this.$helpBlock = this.$formGroup.find(".help-block");
    };

    /**
     * Test if the Form Group is of the given type
     *
     * @param {string} type type name
     * @returns {boolean} true if the Form Group is of the given type. False otherwise
     * @protected
     */
    n.FormGroup.prototype.isType = function (type) {
        var lowerCaseType = type.toLowerCase();
        var isOfType = false;
        this.$element.each(function () {
            if (this.hasAttribute('type')) {
                if (this.getAttribute('type') === lowerCaseType) {
                    isOfType = true;
                    return true;
                } else {
                    isOfType = false;
                    return false;
                }
            } else {
                // <select> is not an attribute value, obviously, so check for nodename
                if (this.nodeName.toLowerCase() === lowerCaseType) {
                    isOfType = true;
                    return true;
                } else if (lowerCaseType === 'text') {
                    isOfType = true;
                    return true;
                } else {
                    isOfType = false;
                    return false;
                }
            }
        });

        return isOfType;
    };

    /**
     *
     * @param $enclosedElement
     * @returns {*}
     *
     * @private
     */
    n.FormGroup.prototype.$findFormGroup = function ($enclosedElement) {
        var $formGroup = $enclosedElement.closest("div.form-group");

        if (!$formGroup || $formGroup.length === 0) {
            throw new Error("Unable to find Form Group");
        }

        if ($formGroup.length > 1) {
            $formGroup = $('#' + $enclosedElement.attr('id') + '-r');
            console.log("Enclosed Element Id: " + $enclosedElement.attr('id'));
            if ($formGroup.length !== 1) {
                throw new Error("enclosed element yields ambiguous form group");
            }
        }

        return $formGroup;
    };

    /**
     * @public
     * @returns {boolean}
     */
    n.FormGroup.prototype.hasLabel = function () {
        return this.$label.length > 0;
    };

    /**
     * @public
     * @returns {boolean}
     */
    n.FormGroup.prototype.hasHelpBlock = function () {
        return this.$helpBlock.length > 0;
    };

    /**
     * @deprecated
     *
     * Read-only is mapped onto setEnabled(). We do not distinguish between those two.
     *
     * @param readonly
     * @public
     */
    n.FormGroup.prototype.setReadOnly = function (readonly) {
        this.setEnabled(!readonly);
    };

    /**
     * @public
     * @param enabled
     */
    n.FormGroup.prototype.setEnabled = function (enabled) {
        this.$element.prop('disabled', !enabled);

        if (enabled) {
            //this.$formGroup.removeClass("text-muted");
            //this.$label.removeClass("disabled");
            this.$element.parents("div.radio").removeClass("disabled");
        } else {
            //this.$formGroup.addClass("text-muted");
            //this.$label.addClass("disabled");
            this.$element.parents("div.radio").addClass("disabled");
        }
    };

    /**
     * @public
     * @param hidden
     */
    n.FormGroup.prototype.setHidden = function (hidden) {
        if (hidden) {
            this.$formGroup.addClass("hidden");
        } else {
            this.$formGroup.removeClass("hidden");
        }
    };

    /**
     * @public
     * @param required
     */
    n.FormGroup.prototype.setRequired = function (required) {
        this.$element.prop('required', required);
    };

    /**
     * @public
     * @param isError
     */
    n.FormGroup.prototype.setError = function (isError) {
        if (isError) {
            this.$formGroup.addClass("has-error has-danger");
        } else {
            this.$formGroup.removeClass("has-error has-danger");
        }
    };

    n.FormGroup.prototype.setHelp = function (help) {
        if (!this.hasHelpBlock()) {
            return;
        }

        this.$helpBlock.text(help);
    };

    n.FormGroup.prototype.clearHelp = function () {
        if (!this.hasHelpBlock()) {
            return;
        }

        this.$helpBlock.empty();
    };

})(QfqNS.Element);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';

    /**
     * Factory for FormGroupS.
     *
     * @param name
     * @returns {*}
     * @function QfqNS.Element.getElement
     */
    n.getElement = function (name) {
        var elementName;
        var $element = $('[name="' + QfqNS.escapeJqueryIdSelector(name) + '"]:not([type="hidden"])');
        if ($element.length === 0) {
            throw Error('No element with name "' + name + '" found.');
        }

        // Handle <select> and <textarea>
        elementName = $element[0].nodeName.toLowerCase();
        if (elementName === "select") {
            return new n.Select($element);
        }

        if (elementName === "textarea") {
            return new n.TextArea($element);
        }

        // Since it is neither a <select> nor a <textarea>, we assume it is an <input> element. Thus we analyze the
        // type attribute
        if (!$element[0].hasAttribute('type')) {
            return new n.Textual($element);
        }

        var type = $element[0].getAttribute('type').toLowerCase();
        switch (type) {
            case 'checkbox':
                return new n.Checkbox($element);
            case 'radio':
                return new n.Radio($element);
            case 'text':
            case 'number':
            case "email":
            case "url":
            case "password":
            case "datetime":
            case "datetime-local":
            case "date":
            case "month":
            case "time":
            case "week":
                return new n.Textual($element);
            default:
                throw new Error("Don't know how to handle <input> of type '" + type + "'");
        }

    };
})(QfqNS.Element);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';


    /**
     * Radio (`<radio>`) Form Group.
     *
     * @param $element
     * @constructor
     * @name QfqNS.Element.Radio
     */
    function Radio($element) {
        n.FormGroup.call(this, $element);

        if (!this.isType("radio")) {
            throw new Error("$element is not of type 'radio'");
        }
    }

    Radio.prototype = Object.create(n.FormGroup.prototype);
    Radio.prototype.constructor = Radio;

    Radio.prototype.setValue = function (val) {
        this.$element.prop('checked', false);
        this.$element.filter('[value="' + val.replace(/"/g, "\\\"") + '"]').prop('checked', true);
    };

    Radio.prototype.getValue = function () {
        return this.$element.filter(':checked').val();
    };

    n.Radio = Radio;

})(QfqNS.Element);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';


    /**
     * Select (`<select>`) Form Group.
     *
     * @param $element
     * @constructor
     * @name QfqNS.Element.Select
     */
    function Select($element) {
        n.FormGroup.call(this, $element, 'select');

        if (!this.isType("select")) {
            throw new Error("$element is not of type 'select'");
        }
    }

    Select.prototype = Object.create(n.FormGroup.prototype);
    Select.prototype.constructor = Select;

    /**
     * Set the value or selection of a `<select>` tag
     *
     * @param {string|array} val when passing a string, the corresponding <option> tag will get selected. If passed
     * array of objects, `<select>` will have its `<option>` tags set correspondingly.
     */
    Select.prototype.setValue = function (val) {
        if (['string', 'number'].indexOf(typeof(val)) !== -1) {
            this.setSelection(val);
        } else if (Array.isArray(val)) {
            this.$element.empty();

            // Fill array with new <select> elements first and add it to the dom in one step, instead of appending
            // each '<select>' separately.
            var selectArray = [];
            val.forEach(function (selectObj) {
                var $option = $('<option>')
                    .attr('value', selectObj.value ? selectObj.value : selectObj.text)
                    .prop('selected', selectObj.selected ? selectObj.selected : false)
                    .append(selectObj.text);
                selectArray.push($option);
            });
            this.$element.append(selectArray);
        } else {
            throw Error('Unsupported type of argument in Select.setValue: "' + typeof(val) + '". Expected either' +
                ' "string" or "array"');
        }
    };

    /**
     *
     * @param val
     *
     * @private
     */
    Select.prototype.setSelection = function (val) {
        this.clearSelection();

        // First, see if we find an <option> tag having an attribute 'value' matching val. If that doesn't work,
        // fall back to comparing text content of <option> tags.
        var $selectionByValue = this.$element.find('option[value="' + val.replace(/"/g, "\\\"") + '"]');
        if ($selectionByValue.length > 0) {
            $selectionByValue.prop('selected', true);
        } else {
            this.$element.find('option').each(function () {
                var $element = $(this);
                if ($element.text() === val) {
                    $element.prop('selected', true);
                }

                return true;
            });
        }
    };

    /**
     * @private
     */
    Select.prototype.clearSelection = function () {
        this.$element.find(':selected').each(function () {
            $(this).prop('selected', false);
        });
    };

    Select.prototype.getValue = function () {
        var returnValue = [];
        this.$element.find(':selected').each(
            function () {
                if (this.hasAttribute('value')) {
                    returnValue.push(this.getAttribute('value'));
                } else {
                    returnValue.push($(this).text());
                }
            }
        );

        return returnValue;
    };

    n.Select = Select;

})(QfqNS.Element);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';


    /**
     * Textarea (`<textarea>`) Form Group.
     *
     * @param $element
     * @constructor
     * @name QfqNS.Element.TextArea
     */
    function TextArea($element) {
        n.FormGroup.call(this, $element, 'textarea');

        if (!this.isType("textarea")) {
            throw new Error("$element is not of type 'textarea'");
        }
    }

    TextArea.prototype = Object.create(n.FormGroup.prototype);
    TextArea.prototype.constructor = TextArea;

    /**
     * Set the value or selection of a `<textarea>` tag
     *
     * @param {string} val content of the textarea
     */
    TextArea.prototype.setValue = function (val) {
        this.$element.val(val);
    };

    TextArea.prototype.getValue = function () {
        return this.$element.val();
    };

    n.TextArea = TextArea;

})(QfqNS.Element);

/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';


    /**
     * Textual `<input>` form groups.
     *
     * @param $element
     * @constructor
     * @name QfqNS.Element.Textual
     */
    function Textual($element) {
        n.FormGroup.call(this, $element);

        var textualTypes = [
            'text',
            'datetime',
            'datetime-local',
            'date',
            'month',
            'time',
            'week',
            'number',
            'range',
            'email',
            'url',
            'search',
            'tel',
            'password',
            'hidden'
        ];
        var textualTypesLength = textualTypes.length;
        var isTextual = false;

        for (var i = 0; i < textualTypesLength; i++) {
            if (this.isType(textualTypes[i])) {
                isTextual = true;
                break;
            }
        }

        if (!isTextual) {
            throw new Error("$element is not of type 'text'");
        }
    }

    Textual.prototype = Object.create(n.FormGroup.prototype);
    Textual.prototype.constructor = Textual;

    Textual.prototype.setValue = function (val) {
        this.$element.val(val);
    };

    Textual.prototype.getValue = function () {
        return this.$element.val();
    };

    n.Textual = Textual;

})(QfqNS.Element);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */


/* global $ */

var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    n.CharacterCount = {};

    /**
     * Initialize character count.
     *
     * Character count keeps track of characters in `<input>` and `<textarea>` elements (tracked elements). By default,
     * all elements having the `qfq-character-count` class are initialized. When non-`<input>/<textarea>` elements are
     * encountered during initialization, the behavior is undefined.
     *
     * Each element eligible for character count must provide a `data-character-count-id` attribute holding the element
     * id of the element receiving the character count. The receiving element's text is replaced by the current number
     * of characters of the tracked element. The number of characters in a tracked element is updated in the receiving
     * element upon a `change` or `keyup` event.
     *
     * If the `maxlength` attribute is present on the tracked element, the receiving element will display
     *
     *     N/<maxlength>
     *
     * where `N` is the current number of characters of the tracked element. If `maxlength` is not present, the
     * receiving element will display
     *
     *     N/∞
     *
     * where `N` is the current number of characters of the tracked element.
     *
     * @param selector {string} optional selector. Defaults to `.qfq-character-count`.
     */
    n.CharacterCount.initialize = function (selector) {
        selector = selector || ".qfq-character-count";
        $(selector).each(function () {
            var characterCountTarget, $targetElement;

            var $element = $(this);

            characterCountTarget = "#" + n.CharacterCount.getCharacterCountTargetId($element);

            $element.data('character-count-display', $(characterCountTarget));

            n.CharacterCount.updateCountForElement($element);

            $element.on('change keyup', function (evt) {
                n.CharacterCount.updateCountForElement($(evt.delegateTarget));
            });

        });
    };

    n.CharacterCount.updateCountForElement = function ($targetElement) {
        var maxLength = $targetElement.attr('maxlength') || "∞";
        var currentLength = $targetElement.val().length;
        $targetElement.data('character-count-display').text(currentLength + "/" + maxLength);
    };

    n.CharacterCount.getCharacterCountTargetId = function ($element) {
        return $element.data('character-count-id');
    };

})(QfqNS);


/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

/**
 * A custom history to use for undo and redo functionality.
 **/

    n.History = function() {
        this.history = [];
        this.pointer = 0;
    };

    n.History.prototype.put = function(object) {
        if (this.history.length > 1) {
            if (this.canGoForward()) {
                console.log("trying to remove history");
                this._removeForwardHistory();
            }
        }
        if (JSON.stringify(this.history[this.pointer]) !== JSON.stringify(object)) {
            this.history.push(object);
            this.pointer = this.history.length - 1;
        }
        console.log(this);
    };

    n.History.prototype.back = function() {
        if (this.canGoBack()) {
            this.pointer = this.pointer - 1;
            console.log(this.pointer + "/" + this.history.length);
            console.log(this.history);
            return this.history[this.pointer];
        } else {
            console.log("At the beginning of history");
            return false;
        }
    };

    n.History.prototype.forward = function() {
        console.log(this.pointer);
        if (this.canGoForward()) {
            this.pointer = this.pointer + 1;
            return this.history[this.pointer];
        } else {
            console.log("At the end of history");
            return false;
        }
    };

    n.History.prototype.canGoBack = function() {
        return this.pointer > 0;
    };

    n.History.prototype.canGoForward = function() {
        return this.pointer < this.history.length - 1;
    };

    n.History.prototype._removeForwardHistory = function() {
        this.history.splice(this.pointer + 1, this.history.length - this.pointer);
    };



})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global console */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     *
     * @type {{level: number, message: Function, debug: Function, warning: Function, error: Function}}
     *
     * @name QfqNS.Log
     */
    n.Log = {
        level: 3,
        message: function (msg) {
            if (this.level <= 0) {
                console.log('[message] ' + msg);
            }
        },
        debug: function (msg) {
            if (this.level <= 1) {
                console.log('[debug] ' + msg);
            }
        },
        warning: function (msg) {
            if (this.level <= 2) {
                console.log('[warning] ' + msg);
            }
        },
        error: function (msg) {
            if (this.level <= 3) {
                console.log('[error] ' + msg);
            }
        }

    };
})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     *
     * @type {{set: Function, get: Function, setSubTitle: Function}}
     *
     * @name QfqNS.PageTitle
     */
    n.PageTitle = {
        set: function (title) {
            document.title = title;
        },
        get: function () {
            return document.title;
        },
        setSubTitle: function (subTitle) {
            var currentTitle = this.get();
            var subtitleStrippedOff = currentTitle.replace(/ - (.*)$/, '');
            document.title = subtitleStrippedOff + " - (" + subTitle + ")";
        }
    };
})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global EventEmitter */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     *
     * @type {{makePayload: Function, onMixin: Function}}
     *
     */
    n.EventEmitter = {
        makePayload: function (target, data, additionalArgs) {
            return [$.extend({},
                typeof additionalArgs === "object" ? additionalArgs : null,
                {
                    target: target,
                    data: data
                }
            )];
        },
        onMixin: function (event, func) {
            this.eventEmitter.addListener(event, func);
        }
    };

})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global console */
/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     *
     * @param settings
     * @constructor
     *
     * @name QfqNS.QfqPage
     */
    n.QfqPage = function (settings) {
        this.qfqForm = {};
        this.settings = $.extend(
            {
                tabsId: "qfqTabs",
                formId: "qfqForm",
                submitTo: "typo3conf/ext/qfq/Classes/Api/save.php",
                deleteUrl: "typo3conf/ext/qfq/Classes/Api/delete.php",
                refreshUrl: "typo3conf/ext/qfq/Classes/Api/load.php",
                fileUploadTo: "typo3conf/ext/qfq/Classes/Api/upload.php",
                fileDeleteUrl: "typo3conf/ext/qfq/Classes/Api/filedelete.php",
                typeAheadUrl: "typo3conf/ext/qfq/Classes/Api/typeahead.php",
                dirtyUrl: "typo3conf/ext/qfq/Classes/Api/dirty.php",
                pageState: new n.PageState()
            }, settings
        );

        this.intentionalClose = false;

        try {
            this.bsTabs = new n.BSTabs(this.settings.tabsId);

            var currentState = this.settings.pageState.getPageState();
            if (currentState !== "") {
                this.bsTabs.activateTab(currentState);
                n.PageTitle.setSubTitle(this.bsTabs.getTabName(currentState));
            } else {
                this.settings.pageState.setPageState(this.bsTabs.getCurrentTab(), n.PageTitle.get());
            }

            this.bsTabs.on('bootstrap.tab.shown', this.tabShowHandler.bind(this));
            this.settings.pageState.on('pagestate.state.popped', this.popStateHandler.bind(this));
        } catch (e) {
            n.Log.message(e.message);
            this.bsTabs = null;
        }

        try {
            this.qfqForm = new n.QfqForm(
                this.settings.formId,
                this.settings.submitTo,
                this.settings.deleteUrl,
                this.settings.refreshUrl,
                this.settings.fileUploadTo,
                this.settings.fileDeleteUrl,
                this.settings.dirtyUrl);
            this.qfqForm.setBsTabs(this.bsTabs);
            this.qfqForm.on('qfqform.destroyed', this.destroyFormHandler.bind(this));

            var that = this;
            this.qfqForm.on('qfqform.close-intentional', function () {
                that.intentionalClose = true;
            });

            window.addEventListener("beforeunload", this.beforeUnloadHandler.bind(this));
            // We have to use 'pagehide'. 'unload' is too late and the ajax request is lost.
            window.addEventListener("pagehide", (function (that) {
                return function () {
                    that.qfqForm.releaseLock(false);
                };
            })(this));
        } catch (e) {
            n.Log.error(e.message);
            this.qfqForm = null;
        }

        var page = this;
        // Initialize Fabric to access form events
        try {
            $(".annotate-graphic").each(function() {
                var qfqFabric = new QfqNS.Fabric();
                qfqFabric.initialize($(this), page);
            });
        } catch (e) {
            n.Log.error(e.message);
        }

        try {
            $(".annotate-text").each(function() {
                var codeCorrection = new QfqNS.CodeCorrection();
                codeCorrection.initialize($(this), page);
            });
        } catch (e) {
            n.Log.error(e.message);
        }

        QfqNS.TypeAhead.install(this.settings.typeAheadUrl);
        QfqNS.CharacterCount.initialize();
    };

    /**
     * @private
     */
    n.QfqPage.prototype.beforeUnloadHandler = function (event) {
        var message = "\0/";
        n.Log.debug("beforeUnloadHandler()");

        if (this.qfqForm.isFormChanged() && !this.intentionalClose) {

            event.returnValue = message;
            return message;
        }

        this.qfqForm.releaseLock();
    };

    /**
     * @private
     */
    n.QfqPage.prototype.destroyFormHandler = function (obj) {
        this.settings.qfqForm = null;
        $('#' + this.settings.tabsId).remove();
    };

    n.QfqPage.prototype.tabShowHandler = function (obj) {
        // tabShowHandler will be called every time the tab will be shown, regardless of whether or not this happens
        // because of BSTabs.activateTab() or user interaction.
        //
        // Therefore, we have to make sure, that tabShowHandler() does not save the page state while we're restoring
        // a previous state, i.e. we're called because of the popStateHandler() below.
        if (this.settings.pageState.inPoppingHandler) {
            n.Log.debug("Prematurely terminating QfqPage.tabShowHandler(): called due to page state" +
                " restoration.");
            return;
        }
        var currentTabId = obj.target.getCurrentTab();
        n.Log.debug('Saving state: ' + currentTabId);
        n.PageTitle.setSubTitle(obj.target.getTabName(currentTabId));
        this.settings.pageState.setPageState(currentTabId, n.PageTitle.get());
    };

    n.QfqPage.prototype.popStateHandler = function (obj) {
        this.bsTabs.activateTab(obj.target.getPageState());
        n.PageTitle.set(obj.target.getPageData());
    };

})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global $ */
/* global console */

var QfqNS = QfqNS || {};

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
(function (n) {
    'use strict';

    /**
     *
     * @param deleteUrl
     * @constructor
     *
     * @name QfqNS.QfqRecordList
     */
    n.QfqRecordList = function (deleteUrl) {
        this.deleteUrl = deleteUrl;
        this.deleteButtonClass = 'record-delete';
        this.recordClass = 'record';
        this.sipDataAttribute = 'sip';

        this.connectClickHandler();
    };

    /**
     * @private
     */
    n.QfqRecordList.prototype.connectClickHandler = function () {
        $("." + this.deleteButtonClass).click(this.handleDeleteButtonClick.bind(this));
    };

    n.QfqRecordList.prototype.handleDeleteButtonClick = function (event) {
        var $eventTarget = $(event.delegateTarget);
        var $recordElement = this.getRecordElement(event.target);

        if ($recordElement.length !== 1) {
            throw new Error($recordElement.length + ' match(es) found for record class');
        }

        var sip = $eventTarget.data(this.sipDataAttribute);

        if (!sip) {
            throw new Error('No `sip` on delete button');
        }


        var alert = new n.Alert({
            message: "Do you really want to delete the record?",
            type: "warning",
            modal: true,
            buttons: [
                {label: "Yes", eventName: "ok"},
                {label: "No", eventName: "cancel", focus: true}
            ]
        });
        var that = this;
        alert.on('alert.ok', function () {
            $.post(that.deleteUrl + "?s=" + sip)
                .done(that.ajaxDeleteSuccessDispatcher.bind(that, $recordElement))
                .fail(n.Helper.showAjaxError);
        });
        alert.show();
    };

    /**
     *
     * @param $recordElement
     * @param data
     * @param textStatus
     * @param jqXHR
     *
     * @private
     */
    n.QfqRecordList.prototype.ajaxDeleteSuccessDispatcher = function ($recordElement, data, textStatus, jqXHR) {
        if (!data.status) {
            throw new Error("No 'status' property 'data'");
        }

        switch (data.status) {
            case "error":
                this.handleLogicDeleteError(data);
                break;
            case "success":
                this.handleDeleteSuccess($recordElement, data);
                break;
            default:
                throw new Error("Status '" + data.status + "' unknown.");
        }
    };

    n.QfqRecordList.prototype.handleDeleteSuccess = function ($recordElement, data) {
        if (data.redirect && data.redirect === "url" && data['redirect-url']) {
            window.location = data['redirect-url'];
            return;
        }
        if (data.redirect && data.redirect === "no") {
            var alert = new n.Alert("redirect=='no' not allowed", "error");
            alert.show();
        }

        var info = new n.Alert("Record successfully deleted", "info");
        info.timeout = 1500;
        info.show();
        $recordElement.fadeOut(function () {
            $recordElement.remove();
        });
    };

    n.QfqRecordList.prototype.getRecordElement = function (element) {
        return $(element).closest('.' + this.recordClass);
    };

    /**
     *
     * @param data
     *
     * @private
     */
    n.QfqRecordList.prototype.handleLogicDeleteError = function (data) {
        if (!data.message) {
            throw Error("Status is 'error' but required 'message' attribute is missing.");
        }
        var alert = new n.Alert(data.message, "error");
        alert.show();
    };


})(QfqNS);
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Highlights Syntax with provided JSON
     *
     */
    n.SyntaxHighlighter = function () {
        this.highlightInstructions = {};
        this.uri = "";
        this.importDone = false;
        this.waitingForEnd = false;
        this.multiLineClass = "";
        this.line = '';
    };

    n.SyntaxHighlighter.prototype.importInstructions = function(json, callbackFn) {
        var that = this;
        console.log("Import instructions: " + json);
        $.getJSON(json, function(data) {
            that.highlightInstructions = data;
            that.importDone = true;
            if (callbackFn && typeof(callbackFn) === "function") {
                console.log("callback found");
                callbackFn();
            }
        });
    };

    n.SyntaxHighlighter.prototype.setLanguageUri = function(uri) {
        this.uri = uri;
    };


    n.SyntaxHighlighter.prototype.highlightLine = function(line) {
        this.line = line;
        if(!this.waitingForEnd) {
            if (this._multiLineHighlight()) {
                return this.line;
            }
            this._wordHighlight();
        } else {
            if (this._multiLineHighlight()) {
                return this.line;
            } else {
                this.line = this.wrapLine(this.multiLineClass, this.line);
                return this.line;
            }
        }
        return this.line;
    };

    n.SyntaxHighlighter.prototype._wordHighlight = function() {
        for (var i = 0; i < this.highlightInstructions.singleWord.length; i++) {
            var word = this.highlightInstructions.singleWord[i];
            var regex = new RegExp(word.regex, "g");
            var wrapClass = this.highlightInstructions.classes[word.styleId].name;
            this.line = this.wrapMatch(wrapClass, regex, this.line);
        }
    };

    n.SyntaxHighlighter.prototype._multiLineHighlight = function() {
        for (var i = 0; i < this.highlightInstructions.multiLine.length; i++) {
            var multiLine = this.highlightInstructions.multiLine[i];
            var regex = {};
            if (this.waitingForEnd) {
                regex = new RegExp(multiLine.end,"g");
            } else {
                regex = new RegExp(multiLine.start,"g");
            }

            if (regex.test(this.line)) {
                if(this.waitingForEnd) {
                    this.line = this.endWrap(this.multiLineClass, regex, this.line);
                    this.waitingForEnd = false;
                    this.multiLineClass = "";
                } else {
                    this.multiLineClass = this.highlightInstructions.classes[multiLine.styleId].name;
                    this.line = this.startWrap(this.multiLineClass, regex, this.line);
                    this.waitingForEnd = true;
                }
                return true;
            }
        }
        return false;
    };

    n.SyntaxHighlighter.prototype.wrapLine = function(spanClass, text) {
        var line = "<span class=\"" + spanClass + "\">" + text + "</span>";
        return line;
    };

    n.SyntaxHighlighter.prototype.startWrap = function(spanClass, regex, line) {
        var newLine = line.replace(regex, "<span class=\"" + spanClass + "\">$1$2</span>");
        return newLine;
    };

    n.SyntaxHighlighter.prototype.endWrap = function(spanClass, regex, line) {
        var newLine = line.replace(regex, "<span class=\"" + spanClass + "\">$1</span>$2");
        return newLine;
    };

    n.SyntaxHighlighter.prototype.wrapMatch = function (spanClass, regex, line) {
        var newLine = line.replace(regex, "$1<span class=\"" + spanClass + "\">$2</span>$3");
        return newLine;
    };

})(QfqNS);
/**
 * @author Elias Villiger <elias.villiger@uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */

var QfqNS = QfqNS || {};
(function (n) {

    n.TablesorterController = function () {
        if (window.hasOwnProperty('tablesorterMockApi')) {
            this.tablesorterApiUrl = window.tablesorterMockApi;
        } else {
            this.tablesorterApiUrl = 'typo3conf/ext/qfq/Classes/Api/setting.php';
        }
        $.tablesorter.themes.bootstrap.table = "";
    };

    // table: jquery selector of table!
    n.TablesorterController.prototype.setup = function (table, uniqueIdentifier) {
        var hasFilter = $(table).hasClass('tablesorter-filter');
        var hasPager = $(table).hasClass('tablesorter-pager');
        var hasColumnSelector = $(table).hasClass('tablesorter-column-selector');
        var hasViewSaver = $(table)[0].hasAttribute("data-tablesorter-view");

        var tablesorterConfig = $(table).data("tablesorterConfig");
        if (!tablesorterConfig) { // revert to default
            tablesorterConfig = {
                theme: "bootstrap",
                widthFixed: true,
                headerTemplate: "{content} {icon}",
                dateFormat: "ddmmyyyy",
                widgets: ["uitheme", "filter", "saveSort", "columnSelector"],
                widgetOptions: {
                    filter_columnFilters: hasFilter, // turn filters on/off with true/false
                    filter_reset: ".reset",
                    filter_cssFilter: "form-control",
                    columnSelector_mediaquery: false
                } };
        }

        $(table).tablesorter(tablesorterConfig);

        var tablesorterMenuWrapper;
        if (hasColumnSelector || hasViewSaver) { tablesorterMenuWrapper = this._doTablesorterMenuWrapper(table); }
        if (hasViewSaver) { this._doViewSaver(table, tablesorterMenuWrapper); }
        if (hasColumnSelector) { this._doColumnSelector(table, tablesorterMenuWrapper, uniqueIdentifier); }
        if (hasPager) { this._doPager(table, uniqueIdentifier); }
    };

    n.TablesorterController.prototype.setTableView = function (table, newView) {
        if(newView.hasOwnProperty('columnSelection') && $.tablesorter.hasWidget( table, 'columnSelector')) {
            table.trigger('refreshColumnSelector', [ newView.columnSelection ]);
        }
        if(newView.hasOwnProperty('filters') && $.tablesorter.hasWidget( table, 'filter')) {
            // correct filter array length if shorter than no. of columns
            var columns = $.tablesorter.getFilters(table).length;
            var len = newView.filters.length;
            var arrayAppend = Array.apply(null, Array(columns - len)).map(function(){return "";});
            var filtersPadded = newView.filters.concat(arrayAppend);
            table.trigger( 'search', [ filtersPadded ]);
        }
        if(newView.hasOwnProperty('sortList')) {
            table.trigger( 'sorton', [ newView.sortList ] );
        }
    };

    n.TablesorterController.prototype.getTableView = function (table) {
        var view = {};
        var config = table[0].config;
        if($.tablesorter.hasWidget( table, 'columnSelector')) {
            view.columnSelection = config.selector.states.map(function (e,i) { return e ? i : false; }).filter(function (e) { return e !== false; });
        }
        if($.tablesorter.hasWidget( table, 'filter')) {
            view.filters = $.tablesorter.getFilters(table);
        }
        view.sortList = config.sortList;
        return view;
    };

    n.TablesorterController.prototype._doTablesorterMenuWrapper = function (table) {
        var tablesorterMenuWrapperHtml = '<div class="qfq-tablesorter-menu-wrapper"></div>';
        return $(tablesorterMenuWrapperHtml).insertBefore($(table));

    };

    n.TablesorterController.prototype._doViewSaver = function (table, tablesorterMenuWrapper) {
        var tableViews = JSON.parse(table.attr("data-tablesorter-view"));

        // decode views from base64 (sql injection prevention)
        tableViews.forEach(function (view) {
            view.view = JSON.parse(atob(view.view));
        });

        // add 'Clear' public view if not exists
        if(!tableViews.some(function (v) {return v.name === 'Clear' && v.public;})) {
            var allColumns = Array($.tablesorter.getFilters(table).length).fill(0).map(function (e,i) {return i;});
            var view = {
                name: "Clear",
                public: true,
                view: {columnSelection: allColumns, filters: [], sortList: []}
            };
            tableViews.push(view);
        }

        // create view select dropdown
        var options = '';
        tableViews.forEach(function (view) {
            options += '<option class="qfq-fontawesome" value="' + (view.public ? 'public:' : 'private:') + view.name + '">' +
                (view.public ? '&#xf0c0; ' : '&nbsp;&#xf007;&nbsp;&nbsp;') + view.name + '</option>';
        });
        var viewSelectorHtml = '<select class="form-control qfq-view-editor qfq-fontawesome qfq-tablesorter-menu-item" style="right: 92px;">' +
            '<option disabled selected value>Table view</option>' + options + '</select>';
        var select = $(viewSelectorHtml).appendTo(tablesorterMenuWrapper);
        var that = this;
        var tableId = table.attr("data-tablesorter-id");
        select.change(function () {
            var viewFromSelect = that._parseViewSelectValue($(this).val());
            var view = tableViews.find(function (v) {
                return v.name === viewFromSelect.name && v.public === viewFromSelect.public;
            });
            that.setTableView(table, view.view);
            that._updateTablesorterUrlHash(table, $(this).val());

            // save view choice in local storage
            localStorage.setItem('tablesorterView_' + tableId, $(this).val());
        });

        // select view on page load: first priority from url hash, second priority localstorage, third public view named 'Default', fourth is 'Clear'

        // Default (third priority) + Clear (fourth priority)
        var publicDefaultExists = tableViews.some(function (v) {return v.name === 'Default' && v.public;});
        var setValue = publicDefaultExists ? "public:Default" : "public:Clear";

        // local storage (second priority)
        var localStorageView = localStorage.getItem('tablesorterView_' + tableId);
        if (localStorageView !== 'undefined' && select.children('option[value="' + localStorageView + '"]').length > 0) {
            setValue = localStorageView;
        }

        // url hash (first priority)
        var hashParameters = this._getTablesorterUrlHash();
        var value = hashParameters[tableId];
        if (typeof value !== 'undefined' && select.children('option[value="' + value + '"]').length > 0) {
            setValue = value;
        }

        // apply view change
        select.val(setValue).change();

        // create edit view dropdown
        var viewDropdownHtml = '<div class="btn-group qfq-tablesorter-menu-item" style="right: 53px;">' +
            '<button type="button" class="btn btn-default form-control qfq-view-editor dropdown-toggle" data-toggle="dropdown">' +
            '<i class="fa fa-pencil"></i>' +
            '</button>' +
            '<ul class="dropdown-menu pull-right" role="menu">' +
            '<li><a href="#" data-save-private-view>Save Private View</a></li>' +
            '<li><a href="#" data-save-public-view>Save Public View</a></li>' +
            '<li><a href="#" data-delete-view>Delete View</a></li>' +
            '</ul>' +
            '</div>';
        var viewDropdown = $(viewDropdownHtml).appendTo(tablesorterMenuWrapper);

        var SavePrivateViewButton = viewDropdown.find('[data-save-private-view]');
        SavePrivateViewButton.click(function () {
            var viewFromSelect = that._parseViewSelectValue(select.val());
            that._saveTableViewPrompt(table, viewFromSelect.name, false);
        });

        var SavePublicViewButton = viewDropdown.find('[data-save-public-view]');
        SavePublicViewButton.click(function () {
            var viewFromSelect = that._parseViewSelectValue(select.val());
            that._saveTableViewPrompt(table, viewFromSelect.name, true);
        });

        var DeleteViewButton = viewDropdown.find('[data-delete-view]');
        DeleteViewButton.click(function () {
            var viewFromSelect = that._parseViewSelectValue(select.val());
            viewFromSelect.mode = 'delete';
            var sip = table.attr("data-tablesorter-sip");
            $.post(that.tablesorterApiUrl  + "?s=" + sip, viewFromSelect, function (response) {
                location.reload(true);
            }, 'json').fail(function (xhr, status, error) {
                that._alert('Error while trying to save view:<br>' + JSON.parse(xhr.responseText).message);
            });
        });
    };

    n.TablesorterController.prototype._doColumnSelector = function (table, tablesorterMenuWrapper, uniqueIdentifier) {
        var columnSelectorId = "qfq-column-selector-" + uniqueIdentifier;
        var columnSelectorTargetId = "qfq-column-selector-target-" + uniqueIdentifier;
        var columnSelectorHtml = '<button id="' + columnSelectorId + '" class="btn btn-default qfq-tablesorter-menu-item qfq-column-selector" ' +
            'type="button">' +
            '<span class="dropdown-text"><i class="fa fa-columns"></i></span>' +
            '<span class="caret"></span></button>' +
            '<div class="hidden"><div id="' + columnSelectorTargetId + '" class="qfq-column-selector-target"> </div></div>';
        $(columnSelectorHtml).appendTo(tablesorterMenuWrapper);
        $.tablesorter.columnSelector.attachTo($(table), '#' + columnSelectorTargetId);
        $('#' + columnSelectorId).popover({
            placement: 'left',
            html: true, // required if content has HTML
            content: $('#' + columnSelectorTargetId)
        });
    };

    n.TablesorterController.prototype._doPager = function (table, uniqueIdentifier) {
        var pagerId = "qfq-pager-" + uniqueIdentifier;
        var pagerHtml = '<div id="' + pagerId + '" class="form-inline" style="margin-top:-20px; margin-bottom:20px;">' +
            '<div class="btn-group btn-group-sm" role="group">' +
            '<button type="button" class="btn btn-default first"><span class="glyphicon glyphicon-step-backward"></span></button>' +
            '<button type="button" class="btn btn-default prev"><span class="glyphicon glyphicon-backward"></span></button>' +
            '</div><span class="pagedisplay"></span>' +
            '<div class="btn-group btn-group-sm" role="group">' +
            '<button type="button" class="btn btn-default next"><span class="glyphicon glyphicon-forward"></span></button>' +
            '<button type="button" class="btn btn-default last"><span class="glyphicon glyphicon-step-forward"></span></button>' +
            '</div>' +
            '<select class="form-control input-sm pagesize" title="Select page size">' +
            '<option selected="selected" value="10">10</option>' +
            '<option value="25">25</option>' +
            '<option value="50">50</option>' +
            '<option value="100">100</option>' +
            '<option value="all">All Rows</option>' +
            '</select>' +
            '<select class="form-control input-sm pagenum" title="Select page number"></select>' +
            '</div>';
        $(pagerHtml).insertAfter($(table));
        $(table).tablesorterPager({
            container: $("#" + pagerId),
            cssGoto: ".pagenum",
            removeRows: false,
            output: '{startRow} - {endRow} / {filteredRows}'
        });
    };

    n.TablesorterController.prototype._setTablesorterUrlHash = function (parameters) {
        var hash = '';
        for (var key in parameters) {
            hash += ',' + key + '=' + parameters[key];
        }
        window.location.hash = hash.substr(1);
    };

    n.TablesorterController.prototype._getTablesorterUrlHash = function () {
        var parameterList = window.location.hash.substr(1).split(',');
        var parameters = {};
        parameterList.forEach(function (par) {
            var keyValue = par.split(/=(.+)/);
            parameters[keyValue[0]] = keyValue[1];
        });
        delete parameters[""];
        return parameters;
    };

    n.TablesorterController.prototype._updateTablesorterUrlHash = function (table, value) {
        var tableId = table.attr("data-tablesorter-id");
        var hashParameters = this._getTablesorterUrlHash();
        hashParameters[tableId] = value;
        this._setTablesorterUrlHash(hashParameters);
    };

    n.TablesorterController.prototype._saveTableViewPrompt = function (table, viewNamePreset, isPublicView) {
        var tableId = table.attr("data-tablesorter-id");
        var viewName = prompt("Please enter a name for the view. If it already exists it will be overwritten.", viewNamePreset !== null ? viewNamePreset : "");
        // check if given view name is valid
        if (viewName === "") {
            this._alert("View not saved. Name is empty.");
            return;
        }
        if (viewName === null) {
            return;
        }
        var view = {name: viewName, public: isPublicView, tableId: tableId, view: this.getTableView(table)};
        // check if there are filters set on hidden columns.
        if (view.view.filters.some(function (f, i) { return f !== '' && !view.view.columnSelection.includes(i); })) {
            if (!confirm('There are filters set on hidden columns. Would you like to save anyway?')) {
                return;
            }
        }
        var that = this;
        var sip = table.attr("data-tablesorter-sip");
        view.view = btoa(JSON.stringify(view.view)); // encode view to base64 to prevent sql injections
        $.post(this.tablesorterApiUrl  + "?s=" + sip, view, function (response) {
            that._updateTablesorterUrlHash(table, (view.public ? 'public:' : 'private:') + view.name);
            location.reload(true);
        }, 'json').fail(function (xhr, status, error) {
            that._alert('Error while trying to save view:<br>' + JSON.parse(xhr.responseText).message);
        });
    };

    n.TablesorterController.prototype._parseViewSelectValue = function (value) {
        var splitValue = value.split(/:(.+)/);
        return {name: splitValue[1], public: splitValue[0] === 'public'};
    };

    n.TablesorterController.prototype._alert = function (alertMessage) {
        var messageButtons = [{
            label: "Ok",
            eventName: 'close'
        }];
        var alert = new n.Alert({ "message": alertMessage, "type": "error", modal: true, buttons: messageButtons});
        alert.show();
    };

})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    n.escapeJqueryIdSelector = function (idSelector) {
        return idSelector.replace(/(:|\.)/, "\\$1");
    };

})(QfqNS);


// Hide long text and append "more.." button
$(document).ready(function () {
    var more = 'more..';
    var less = 'less..';
    var moreButtonHtml = '<button class="btn btn-link" style="outline-width: 0px;">' + more + '</button>';
    var moreButton = $(moreButtonHtml).insertAfter('span.qfq-more-text');
    moreButton.click(function () {
        var moreText = $(this).siblings('span.qfq-more-text');
        if ($(this).text() === more) {
            $(this).text(less);
            moreText.show();
        } else {
            $(this).text(more);
            moreText.hide();
        }
    });
});
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* @depend FormGroup.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';

    /**
     * Checkbox (`<checkbox>`) Form Group.
     *
     * @param $element
     * @constructor
     * @name QfqNS.Element.Checkbox
     */
    function Checkbox($element) {
        n.FormGroup.call(this, $element);

        var type = "checkbox";

        if (!this.isType(type)) {
            throw new Error("$element is not of type 'checkbox'");
        }

        // We allow one Form Group to have several checkboxes. Therefore, we have to remember which checkbox was
        // selected if possible.
        if ($element.length === 1 && $element.attr('type') === type) {
            this.$singleElement = $element;
        } else {
            this.$singleElement = null;
        }
    }

    Checkbox.prototype = Object.create(n.FormGroup.prototype);
    Checkbox.prototype.constructor = Checkbox;

    Checkbox.prototype.setValue = function (val) {
        if (this.$singleElement) {
            this.$singleElement.prop('checked', val);
        } else {
            this.$element.prop('checked', val);
        }
    };

    Checkbox.prototype.getValue = function () {
        if (this.$singleElement) {
            return this.$singleElement.prop('checked');
        } else {
            return this.$element.prop('checked');
        }
    };

    n.Checkbox = Checkbox;

})(QfqNS.Element);

/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend ../QfqEvents.js */


/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    n.ElementBuilder = function(obj, parent) {
        this.type  = obj.type;
        this.class = obj.class || false;
        this.text  = obj.text || false;
        this.tooltip = obj.tooltip || false;
        this.label = obj.label || false;
        this.value = obj.value || false;
        this.width = obj.width || false;
        this.name = obj.name || false;
        this.onClick = obj.onClick || false;
        this.parent = parent || false;
        this.submitTo = obj.submitTo || false;
        this.checked = obj.checked || false;
        this.$element = {};
        this.children = [];
        this.eventEmitter = new EventEmitter();


        if (obj.children) {
            for (var i = 0; i < obj.children.length; i++) {
                var iparent = this;
                if (this.type !== "form") {
                    iparent = this.parent;
                }
                var element = new n.ElementBuilder(obj.children[i], iparent);

                this.children.push(element);
            }
        }

    };

    n.ElementBuilder.prototype.on = n.EventEmitter.onMixin;

    n.ElementBuilder.prototype.display = function() {
        var $element = {};

        if (this.type === "form") {
            $element = this._buildForm();
            var that = this;
            $element.submit(function(event) {
                event.preventDefault();
                that.handleSubmit();
            });
            this.$form = $element;
        }

        if (this.type === "row") $element = this._buildRow();
        if (this.type === "checkbox" ||
            this.type === "radio" ||
            this.type === "hidden") $element = this._buildInput();
        if (this.type === "label") $element = this._buildLabel();

        if (this.children) {
            for (var i = 0; i < this.children.length; i++) {
                var $child = this.children[i].display();
                $element.append($child);
            }
        }

        this.$element = $element;
        return $element;
    };

    n.ElementBuilder.prototype._buildRow = function() {
        var options = {
            class: "row" + this._getOption(this.class),
            text: this._getOption(this.text)
        };
        return $("<div />", options);
    };

    n.ElementBuilder.prototype._buildInput = function() {
        var $block = {};

        var options = {
            class: this._getOption(this.class),
            type: this._getOption(this.type),
            name: this._getOption(this.name),
            value: this._getOption(this.value)
        };

        if (this.type === "checkbox" || this.type === "radio") {
            options.checked = this.checked;
        }

        if (this.type !== "hidden") {
            $block = this._buildBlock(this.width);
        } else {
            options.required = false;
        }

        var $input = $("<input />", options);

        if (this.onClick === "submit") {
            var that = this;
            $input.on("click", function() {
                that.submit();
            });
        }

        if (this.type !== "hidden") {
            $block.append($input);
            return $block;
        } else {
            $input.removeAttr("pattern");
            return $input;
        }
    };

    n.ElementBuilder.prototype._buildLabel = function() {
        var $block = this._buildBlock(this.width, "qfq-label");
        var options = {
            class: "control-label" + this._prepareClass(this.class, true),
            text: this._getOption(this.text)
        };
        var $label = $("<span />", options);
        $block.append($label);
        return $block;
    };

    n.ElementBuilder.prototype._buildBlock = function(size, cssClass) {
        var options = {
            class: "col-md-" + size + this._prepareClass(cssClass, true)
        };
        return $("<div />", options);
    };

    n.ElementBuilder.prototype._prepareClass = function(value, isAddition) {
        if (isAddition) return " " + value;
        return "" + value;
    };

    n.ElementBuilder.prototype._buildForm = function() {
        return $("<form />");
    };

    n.ElementBuilder.prototype._getOption = function(o) {
        if (o !== undefined && o) {
            return o;
        } else {
            return "";
        }
    };

    n.ElementBuilder.prototype.submit = function() {
          if (this.type !== "form") {
              this.parent.$element.submit();
          } else {
              this.$element.submit();
          }
    };

    n.ElementBuilder.prototype.handleSubmit = function() {
        $.post(this.submitTo, this.$element.serialize())
            .done(this.submitSuccessHandler.bind(this))
            .fail(this.submitFailureHandler.bind(this));
    };

    n.ElementBuilder.prototype.submitSuccessHandler = function(data, textStatus, jqXHR) {
        var configuration = data['element-update'];
        n.ElementUpdate.updateAll(configuration);
        this.eventEmitter.emitEvent('form.submit.success',
            n.EventEmitter.makePayload(this, "submit"));
    };

    n.ElementBuilder.prototype.submitFailureHandler = function (data, textStatus, jqXHR) {
        console.error("Submit failed");
    };

})(QfqNS);

/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Manages multiple Alerts and provides an Alert Container.
     *
     * Is usually initialized with the first Alert that is created. (see n.Alert)
     *
     * @param {object} options For later
     */

    n.AlertManager = function(options) {
        this.screenBlocked = false;
        this.alerts = [];
        this.$container = $("<div>");
        this.blockingAlert = {};
        this.eventEmitter = new EventEmitter();
        this.regularCheck = {};

        $("body").append(this.$container);
        console.log("Created Alert Container");
    };

    n.AlertManager.prototype.on = n.EventEmitter.onMixin;

    /**
     * Add an Alert to the Alert Manager
     * @param alert
     */
    n.AlertManager.prototype.addAlert = function(alert) {
        this.alerts.push(alert);
        alert.setIdentifier(this.alerts.length);
        console.log(this.alerts);
    };

    n.AlertManager.prototype.removeAlert = function(identifier) {
        for(var i=0; this.alerts.length > i; i++) {
            if (this.alerts[i].identifier === identifier) {
                this.alerts.splice(i, 1);
            }
        }
    };

    /**
     * Removes the last Alert in the Array. Can be used to safely delete all alerts in a loop.
     * Returns false when the AlertManager has no more Alerts.
     * @returns Boolean
     */
    n.AlertManager.prototype.removeLastAlert = function() {
        if (this.alert.length > 0) {
            var alert = this.alerts.pop();
            alert.removeAlert();
            return true;
        } else {
            return false;
        }
    };

    /**
     * Savely removes outdated Alerts with isShown = false
     */
    n.AlertManager.prototype.removeOutdatedAlerts = function() {
        for(var i = 0; this.alerts.length > i; i++) {
            if(!this.alerts[i].isShown) {
                this.alerts[i].removeAlert();
                this.alerts.splice(i, 1);
            }
        }
    };

    /**
     * Returns the number of Alerts currently active
     * @returns {number}
     */
    n.AlertManager.prototype.count = function() {
        return this.alerts.length;
    };

    /**
     * Creates a semi-transparent black screen behind the alert.
     * Used to block other user input by modal alerts
     * @param alert
     */
    n.AlertManager.prototype.createBlockScreen = function(alert) {
        if (!this.screenBlocked) {
            var $blockScreen = $("<div>")
                .addClass("blockscreenQfq")
                .appendTo(this.$container);
            $blockScreen.css({
                'position': 'fixed',
                'left': 0,
                'right': 0,
                'top': 0,
                'z-index': 998,
                'background': 'rgba(0,0,0,0.5)',
                'width': '100%',
                'height': Math.max($(document).height(), $(window).height()) + "px"
            });

            var that = this;
            this.screenBlocked = true;
            this.blockingAlert = alert;
            this.regularCheck = setInterval(function () {
                that.checkAlert();
            }, 500);
        }
    };

    /**
     * Is used by the interval this.regularcheck to guarantee
     * that the screen block is removed.
     */
    n.AlertManager.prototype.checkAlert = function() {
        if (!this.blockingAlert.isShown) {
            this.removeModalAlert();
        }
    };

    /**
     * Remove modal alerts
     */
    n.AlertManager.prototype.removeModalAlert = function() {
        if (this.screenBlocked) {
            $(".blockscreenQfq").remove();
            clearInterval(this.regularCheck);
            this.screenBlocked = false;
        }
        this.removeOutdatedAlerts();
    };

})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global console */
/* global EventEmitter */

/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};


(function (n) {
    'use strict';
    /**
     * Tab Constructor.
     *
     * Programmatically access Bootstrap nav-tabs.
     *
     * @param {string} tabId HTML id of the element having `nav` and `nav-tabs` classes
     * @constructor
     *
     * @name QfqNS.BSTabs
     */
    n.BSTabs = function (tabId) {
        this.tabId = tabId;
        this._tabContainerLinkSelector = '#' + this.tabId + ' a[data-toggle="tab"]';
        this._tabActiveSelector = '#' + this.tabId + ' .active a[data-toggle="tab"]';
        this.tabs = {};
        this.currentTab = this.getActiveTabFromDOM();
        this.eventEmitter = new EventEmitter();

        // Fill this.tabs
        this.fillTabInformation();

        // Enable update of current tab field
        this.installTabHandlers();
    };

    n.BSTabs.prototype.on = n.EventEmitter.onMixin;

    /**
     * Get active tab from DOM.
     *
     * Used upon object creation to fill the currentTab. It gets the ID of the currently shown tab. It does it, by
     * targeting the element in the navigator having the `active` class set.
     *
     * @private
     */
    n.BSTabs.prototype.getActiveTabFromDOM = function () {
        var activeTabAnchors = $(this._tabActiveSelector);
        if (activeTabAnchors.length < 1) {
            // This could be due to the DOM not fully loaded. If that's really the case, then the active tab
            // attribute should be set by the show.bs.tab handler
            return null;
        }

        return activeTabAnchors[0].hash.slice(1);
    };

    /**
     * Fill tabs object.
     *
     * Fill the tabs object using the tab HTML id as attribute name
     *
     * @private
     */
    n.BSTabs.prototype.fillTabInformation = function () {
        var tabLinks = $(this._tabContainerLinkSelector);
        if ($(tabLinks).length === 0) {
            throw new Error("Unable to find a BootStrap container matching: " + this._tabContainerLinkSelector);
        }

        var that = this;
        tabLinks.each(function (index, element) {
                if (element.hash !== "") {
                    var tabId = element.hash.slice(1);

                    that.tabs[tabId] = {
                        index: index,
                        element: element
                    };
                }
            }
        );
    };

    /**
     * @private
     */
    n.BSTabs.prototype.installTabHandlers = function () {
        $(this._tabContainerLinkSelector)
            .on('show.bs.tab', this.tabShowHandler.bind(this));

    };

    /**
     * Tab Show handler.
     *
     * Sets this.currentTab to the clicked tab and calls all registered tab click handlers.
     *
     * @private
     * @param event
     */
    n.BSTabs.prototype.tabShowHandler = function (event) {
        n.Log.debug('Enter: BSTabs.tabShowHandler()');
        this.currentTab = event.target.hash.slice(1);

        n.Log.debug("BSTabs.tabShowHandler(): invoke user handler(s)");
        this.eventEmitter.emitEvent('bootstrap.tab.shown', n.EventEmitter.makePayload(this, null));
        n.Log.debug('Exit: BSTabs.tabShowHandler()');
    };

    /**
     * Get all tab IDs.
     *
     * @returns {Array}
     *
     * @public
     */
    n.BSTabs.prototype.getTabIds = function () {
        var tabIds = [];
        for (var tabId in this.tabs) {
            if (this.tabs.hasOwnProperty(tabId)) {
                tabIds.push(tabId);
            }
        }
        return tabIds;
    };

    /**
     *
     * @returns {Array}
     *
     * @public
     */
    n.BSTabs.prototype.getTabAnchors = function () {
        var tabLinks = [];
        for (var tabId in this.tabs) {
            if (this.tabs.hasOwnProperty(tabId)) {
                tabLinks.push(this.tabs[tabId].element);
            }
        }

        return tabLinks;
    };

    /**
     * Activate a given tab.
     *
     * @param {string} tabId Id of the tab to activate
     *
     */
    n.BSTabs.prototype.activateTab = function (tabId) {
        if (!this.tabs[tabId]) {
            console.error("Unable to find tab with id: " + tabId);
            return false;
        }

        $(this.tabs[tabId].element).tab('show');
        return true;
    };

    n.BSTabs.prototype.getCurrentTab = function () {
        return this.currentTab;
    };

    n.BSTabs.prototype.getTabName = function (tabId) {
        if (!this.tabs[tabId]) {
            console.error("Unable to find tab with id: " + tabId);
            return null;
        }

        return $(this.tabs[tabId].element).text().trim();
    };

    n.BSTabs.prototype.getActiveTab = function () {
        return this.currentTab;
    };

    n.BSTabs.prototype.getContainingTabIdForFormControl = function (formControlName) {
        var $formControl = $("[name='" + formControlName + "']");
        if ($formControl.length === 0) {
            n.Log.debug("BSTabs.getContainingTabForFormControl(): unable to find form control with name '" + formControlName + "'");
            return null;
        }

        var iterator = $formControl[0];
        while (iterator !== null) {
            if (iterator.hasAttribute('role') &&
                iterator.getAttribute('role') === 'tabpanel') {
                return iterator.id || null;
            }

            iterator = iterator.parentElement;
        }

        return null;
    };

})(QfqNS);
/**
 * Created by raos on 6/28/17.
 */

/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    n.Dirty = function (dirtyUrl) {
        this.dirtyUrl = dirtyUrl;

        this.eventEmitter = new EventEmitter();
        this.successTimerId = null;
        this.deniedTimerId = null;
        this.lockTimeoutInMilliseconds = n.Dirty.NO_LOCK_TIMEOUT;
    };

    n.Dirty.NO_LOCK_TIMEOUT = -1;

    n.Dirty.EVENTS = {
        STARTED: 'dirty.notify.started',
        SUCCESS: 'dirty.notify.success',
        ENDED: 'dirty.notify.ended',
        FAILED: 'dirty.notify.failed',
        DENIED: 'dirty.notify.denied',
        DENIED_TIMEOUT: 'dirty.timeout.denied',
        SUCCESS_TIMEOUT: 'dirty.timeout.success',

        RELEASE_STARTED: 'dirty.release.started',
        RELEASE_ENDED: 'dirty.release.ended',
        RELEASE_SUCCESS: 'dirty.release.success',
        RELEASE_FAILED: 'dirty.release.failed',

        RENEWAL_STARTED: 'dirty.renewal.started',
        RENEWAL_ENDED: 'dirty.renewal.ended',
        RENEWAL_SUCCESS: 'dirty.renewal.success',
        RENEWAL_DENIED: 'dirty.renewal.denied',
        RENEWAL_FAILED: 'dirty.renewal.failed'
    };

    n.Dirty.ENDPOINT_OPERATIONS = {
        /** Aquire Lock */
        LOCK: "lock",
        /** Release Lock */
        RELEASE: "release",
        /** Renew Lock */
        RENEW: "extend"
    };

    n.Dirty.MINIMUM_TIMER_DELAY_IN_SECONDS = 5;
    n.Dirty.MILLISECONDS_PER_SECOND = 1000;

    n.Dirty.prototype.on = n.EventEmitter.onMixin;

    /**
     * Notify the server that SIP is becoming dirty.
     *
     * @param sip {string} sip.
     * @public
     */
    n.Dirty.prototype.notify = function (sip, optionalQueryParameters) {
        var eventData;

        if (!this.dirtyUrl) {
            n.Log.debug("notify: cannot contact server, no dirtyUrl.");
            return;
        }
        eventData = n.EventEmitter.makePayload(this, null);
        this.eventEmitter.emitEvent(n.Dirty.EVENTS.STARTED, eventData);
        $.ajax({
            url: this.makeUrl(sip, n.Dirty.ENDPOINT_OPERATIONS.LOCK, optionalQueryParameters),
            type: 'GET',
            cache: false
        })
            .done(this.ajaxNotifySuccessHandler.bind(this))
            .fail(this.ajaxNotifyErrorHandler.bind(this));
    };

    /**
     * @private
     * @param data
     * @param textStatus
     * @param jqXHR
     */
    n.Dirty.prototype.ajaxNotifySuccessHandler = function (data, textStatus, jqXHR) {
        var eventData = n.EventEmitter.makePayload(this, data);
        if (data.status && data.status === "success") {
            this.eventEmitter.emitEvent(n.Dirty.EVENTS.SUCCESS, eventData);
        } else {
            this.eventEmitter.emitEvent(n.Dirty.EVENTS.DENIED, eventData);
        }

        this.setTimeoutIfRequired(data, eventData);
        this.eventEmitter.emitEvent(n.Dirty.EVENTS.ENDED, eventData);
    };

    /**
     * Set timers based on server response. Timers are only set when `ajaxData` contains a `lock_timeout` attribute.
     * `lock_timeout` is expected to hold the timeout in seconds.
     *
     * @param ajaxData data received from server
     * @param eventData data passed when event is emitted.
     * @private
     */
    n.Dirty.prototype.setTimeoutIfRequired = function (ajaxData, eventData) {
        var timeoutInMilliseconds;

        if (!ajaxData.lock_timeout || ajaxData.lock_timeout < n.Dirty.MINIMUM_TIMER_DELAY_IN_SECONDS) {
            this.lockTimeoutInMilliseconds = n.Dirty.NO_LOCK_TIMEOUT;
            return;
        }

        this.lockTimeoutInMilliseconds = ajaxData.lock_timeout * n.Dirty.MILLISECONDS_PER_SECOND;

        if (ajaxData.status &&
            ajaxData.status === "success") {
            this.clearSuccessTimeoutTimerIfSet();

            this.successTimerId = setTimeout((function (that, data) {
                return function () {
                    that.eventEmitter.emitEvent(n.Dirty.EVENTS.SUCCESS_TIMEOUT, data);
                };
            })(this, eventData), this.lockTimeoutInMilliseconds);
        } else {
            this.clearDeniedTimeoutTimerIfSet();

            this.deniedTimerId = setTimeout((function (that, data) {
                return function () {
                    that.eventEmitter.emitEvent(n.Dirty.EVENTS.DENIED_TIMEOUT, data);
                };
            })(this, eventData), this.lockTimeoutInMilliseconds);
        }
    };

    /**
     * @public
     */
    n.Dirty.prototype.clearSuccessTimeoutTimerIfSet = function () {
        if (this.successTimerId) {
            clearTimeout(this.successTimerId);
            this.successTimerId = null;
        }
    };

    /**
     * @private
     */
    n.Dirty.prototype.clearDeniedTimeoutTimerIfSet = function () {
        if (this.deniedTimerId) {
            clearTimeout(this.deniedTimerId);
            this.deniedTimerId = null;
        }
    };


    /**
     * @private
     * @param jqXHR
     * @param textStatus
     * @param errorThrown
     */
    n.Dirty.prototype.ajaxNotifyErrorHandler = function (jqXHR, textStatus, errorThrown) {
        var eventData = n.EventEmitter.makePayload(this, null, {
            textStatus: textStatus,
            errorThrown: errorThrown,
            jqXHR: jqXHR
        });

        this.lockTimeoutInMilliseconds = n.Dirty.NO_LOCK_TIMEOUT;

        this.eventEmitter.emitEvent(n.Dirty.EVENTS.FAILED, eventData);
        this.eventEmitter.emitEvent(n.Dirty.EVENTS.ENDED, eventData);
    };

    /**
     * Release lock.
     *
     * @param sip {string} sip
     * @public
     */
    n.Dirty.prototype.release = function (sip, optionalQueryParameters, async) {
        var eventData;
        if (async === undefined) {
            async = true;
        }

        if (!this.dirtyUrl) {
            n.Log.debug("release: cannot contact server, no dirtyUrl.");
            return;
        }
        eventData = n.EventEmitter.makePayload(this, null);
        this.eventEmitter.emitEvent(n.Dirty.EVENTS.RELEASE_STARTED, eventData);
        $.ajax({
            url: this.makeUrl(sip, n.Dirty.ENDPOINT_OPERATIONS.RELEASE, optionalQueryParameters),
            type: 'GET',
            cache: false,
            async: async
        })
            .done(this.ajaxReleaseSuccessHandler.bind(this))
            .fail(this.ajaxReleaseErrorHandler.bind(this));
    };

    /**
     * @private
     * @param data
     * @param textStatus
     * @param jqXHR
     */
    n.Dirty.prototype.ajaxReleaseSuccessHandler = function (data, textStatus, jqXHR) {
        n.Log.debug("Dirty Release: Response received.");
        var eventData = n.EventEmitter.makePayload(this, data);
        if (data.status && data.status === "success") {
            this.eventEmitter.emitEvent(n.Dirty.EVENTS.RELEASE_SUCCESS, eventData);
        } else {
            this.eventEmitter.emitEvent(n.Dirty.EVENTS.RELEASE_FAILED, eventData);
        }

        this.lockTimeoutInMilliseconds = n.Dirty.NO_LOCK_TIMEOUT;

        this.eventEmitter.emitEvent(n.Dirty.EVENTS.RELEASE_ENDED, eventData);
    };

    /**
     * @private
     * @param jqXHR
     * @param textStatus
     * @param errorThrown
     */
    n.Dirty.prototype.ajaxReleaseErrorHandler = function (jqXHR, textStatus, errorThrown) {
        var eventData = n.EventEmitter.makePayload(this, null, {
            textStatus: textStatus,
            errorThrown: errorThrown,
            jqXHR: jqXHR
        });

        this.lockTimeoutInMilliseconds = n.Dirty.NO_LOCK_TIMEOUT;

        this.eventEmitter.emitEvent(n.Dirty.EVENTS.RELEASE_FAILED, eventData);
        this.eventEmitter.emitEvent(n.Dirty.EVENTS.RELEASE_ENDED, eventData);
    };

    /**
     * Request renewal of lock.
     *
     * @param sip {string} sip.
     * @public
     */
    n.Dirty.prototype.renew = function (sip, optionalQueryParameters) {
        var eventData;

        if (!this.dirtyUrl) {
            n.Log.debug("renew: cannot contact server, no dirtyUrl.");
            return;
        }
        eventData = n.EventEmitter.makePayload(this, null);
        this.eventEmitter.emitEvent(n.Dirty.EVENTS.RENEWAL_STARTED, eventData);
        $.ajax({
            url: this.makeUrl(sip, n.Dirty.ENDPOINT_OPERATIONS.RENEW, optionalQueryParameters),
            type: 'GET',
            cache: false
        })
            .done(this.ajaxRenewalSuccessHandler.bind(this))
            .fail(this.ajaxRenewalErrorHandler.bind(this));
    };

    /**
     * @private
     * @param data
     * @param textStatus
     * @param jqXHR
     */
    n.Dirty.prototype.ajaxRenewalSuccessHandler = function (data, textStatus, jqXHR) {
        var eventData = n.EventEmitter.makePayload(this, data);
        if (data.status && data.status === "success") {
            this.eventEmitter.emitEvent(n.Dirty.EVENTS.RENEWAL_SUCCESS, eventData);
        } else {
            this.eventEmitter.emitEvent(n.Dirty.EVENTS.RENEWAL_DENIED, eventData);
        }
        this.eventEmitter.emitEvent(n.Dirty.EVENTS.RENEWAL_ENDED, eventData);

        this.setTimeoutIfRequired(data, eventData);
    };

    /**
     * @private
     * @param jqXHR
     * @param textStatus
     * @param errorThrown
     */
    n.Dirty.prototype.ajaxRenewalErrorHandler = function (jqXHR, textStatus, errorThrown) {
        var eventData = n.EventEmitter.makePayload(this, null, {
            textStatus: textStatus,
            errorThrown: errorThrown,
            jqXHR: jqXHR
        });

        this.lockTimeoutInMilliseconds = n.Dirty.NO_LOCK_TIMEOUT;

        this.eventEmitter.emitEvent(n.Dirty.EVENTS.RENEWAL_FAILED, eventData);
        this.eventEmitter.emitEvent(n.Dirty.EVENTS.RENEWAL_ENDED, eventData);
    };

    /**
     * Create URL to endpoint.
     *
     * @param sip {string} sip of the request
     * @param operation {string} operation
     * @returns {string} complete url
     * @private
     */
    n.Dirty.prototype.makeUrl = function (sip, operation, optionalQueryParameters) {
        var queryString, mergedQueryParameterObject;

        mergedQueryParameterObject = $.extend(
            optionalQueryParameters,
            {
                s: sip,
                action: operation
            }
        );

        queryString = $.param(mergedQueryParameterObject);
        return this.dirtyUrl + "?" + queryString;
    };
})(QfqNS);

/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global console */

/* @depend Utils.js */

var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Update HTML elements by a given id. Supports adding, setting, and removing attributes as well as setting the
     * text enclosed by the element.
     *
     * @type {{}}
     */
    n.ElementUpdate = {};


    /**
     * Update all elements according to configuration.
     *
     * @param config JSON configuration
     * @public
     */
    n.ElementUpdate.updateAll = function (config) {
        for (var idName in config) {
            if (!config.hasOwnProperty(idName)) {
                continue;
            }

            n.ElementUpdate.update(idName, config[idName]);
        }
    };

    /**
     *
     * @param elementId id of the element to update
     * @param config configuration
     */
    n.ElementUpdate.update = function (elementId, config) {
        var $element = n.ElementUpdate.$getElementById(elementId);

        if (config.attr) {
            n.ElementUpdate.handleAttributeUpdate($element, config.attr);
        }

        if (config.content) {
            n.ElementUpdate.setElementText($element, config.content);
        }
    };

    n.ElementUpdate.$getElementById = function (id) {
        return $("#" + n.escapeJqueryIdSelector(id));
    };

    n.ElementUpdate.handleAttributeUpdate = function ($element, attributes) {
        var attributeValue;
        for (var attributeName in attributes) {
            if (!attributes.hasOwnProperty(attributeName)) {
                continue;
            }

            attributeValue = attributes[attributeName];

            if (attributeValue === null || attributeValue === false || attributeValue === "false") {
                n.ElementUpdate.deleteAttribute($element, attributeName);
            } else {
                n.ElementUpdate.setAttribute($element, attributeName, attributeValue);
            }
        }
    };

    n.ElementUpdate.setAttribute = function ($element, attributeName, attributeValue) {
        $element.attr(attributeName, attributeValue);
        if (attributeName.toLowerCase() === "value") {
            $element.val(attributeValue);
        }
    };

    n.ElementUpdate.deleteAttribute = function ($element, attributeName) {
        $element.removeAttr(attributeName);
    };

    n.ElementUpdate.setElementText = function ($element, text) {
        $element.empty().append($.parseHTML(text));
    };
})(QfqNS);

/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */


/* global $ */
/* @depend Utils.js */

var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * @name addFields
     * @function
     * @global
     * @deprecated use appendTemplate().
     *
     */
    n.addFields = function (templateSelector, targetSelector, maxLines) {
        n.appendTemplate(templateSelector, targetSelector, maxLines);
    };

    /**
     * Append a child created from a template to a target. All occurrences of `%d` in attribute values will be replaced
     * by the number of children of target.
     *
     * @name appendTemplate
     * @public
     * @global
     * @function
     *
     * @param {string} templateSelector jQuery selector uniquely identifying the template.
     * @param {string} targetSelector jQuery selector uniquely identifying the target container.
     * @param {number} maxChildren do not allow more than `maxChildren` of children in target.
     *
     */
    n.appendTemplate = function (templateSelector, targetSelector, maxChildren) {
        var responsibleButton;
        var escapedTemplateSelector = n.escapeJqueryIdSelector(templateSelector);
        var escapedTargetSelector = n.escapeJqueryIdSelector(targetSelector);

        var $template = $(escapedTemplateSelector);

        var $target = $(escapedTargetSelector);

        var lines = n.countLines(escapedTargetSelector) + 1;

        if (lines >= maxChildren + 1) {
            return;
        }

        var deserializedTemplate = n.deserializeTemplateAndRetainPlaceholders($template.text());
        n.expandRetainedPlaceholders(deserializedTemplate, lines);

        // Store the button object, so we can easily access it when this `line` is removed by the user
        responsibleButton = n.getResponsibleButtonFromTarget($target);
        if (responsibleButton) {
            deserializedTemplate.data('field.template.addButton', responsibleButton);
        }

        $target.append(deserializedTemplate);
        n.informFormOfChange($target);

        lines = n.countLines(escapedTargetSelector);
        if (lines >= maxChildren && responsibleButton) {
            n.disableButton(responsibleButton);
        }
    };

    n.getResponsibleButtonFromTarget = function ($target) {
        var $button;
        var buttonSelector = $target.data('qfq-line-add-button');
        if (!buttonSelector) {
            return null;
        }

        $button = $(buttonSelector);
        if ($button.length === 0) {
            return null;
        }

        return $button[0];
    };

    n.disableButton = function (button) {
        $(button).attr('disabled', 'disabled');
    };

    n.enableButton = function (button) {
        $(button).removeAttr('disabled');
    };

    n.informFormOfChange = function ($sourceOfChange) {
        var $enclosingForm = $sourceOfChange.closest("form");
        $enclosingForm.change();
    };

    /**
     * @name initializeFields
     * @global
     * @function
     * @deprecated use initializeTemplateTarget()
     */
    n.initializeFields = function (element, templateSelectorData) {
        n.initializeTemplateTarget(element, templateSelectorData);
    };

    /**
     * When the template target contains no children, it initializes the target element by appending the first child
     * created from the template.
     *
     * @name initializeTemplateTarget
     * @global
     * @function
     *
     * @param {HTMLElement} element the target HTMLElement to be initialized
     * @param {string} [templateSelectorData=qfq-line-template] name of the `data-` attribute containing the jQuery
     * selector
     * selecting the template
     */
    n.initializeTemplateTarget = function (element, templateSelectorData) {
        var responsibleButton;
        var $element = $(element);
        var templateSelector, escapedTemplateSelector, $template, deserializedTemplate;

        templateSelector = $element.data(templateSelectorData || 'qfq-line-template');
        escapedTemplateSelector = n.escapeJqueryIdSelector(templateSelector);

        $template = $(escapedTemplateSelector);

        if ($element.children().length > 0) {
            n.setPlaceholderRetainers($template.text(), element);
            return;
        }

        deserializedTemplate = n.deserializeTemplateAndRetainPlaceholders($template.text());
        n.expandRetainedPlaceholders(deserializedTemplate, 1);

        deserializedTemplate.find('.qfq-delete-button').remove();
        $element.append(deserializedTemplate);
    };

    /**
     * @name removeFields
     * @global
     * @function
     * @deprecated use removeThisChild()
     */
    n.removeFields = function (target) {
        n.removeThisChild(target);
    };

    /**
     * Remove the element having a class `qfq-line`. Uses `eventTarget` as start point for determining the closest
     * element.
     *
     * @name removeFields
     * @global
     * @function
     * @param {HTMLElement} eventTarget start point for determining the closest `.qfq-line`.
     */
    n.removeThisChild = function (eventTarget) {
        var $line = $(eventTarget).closest('.qfq-line');
        var $container = $line.parent();
        var buttonToEnable = $line.data('field.template.addButton');

        $line.remove();

        n.reExpandLineByLine($container);
        n.informFormOfChange($container);
        if (buttonToEnable) {
            n.enableButton(buttonToEnable);
        }
    };

    /**
     * Takes a template as string and deserializes it into DOM. Any attributes having a value containing `%d` will be
     *
     * @private
     * @param template
     *
     */
    n.deserializeTemplateAndRetainPlaceholders = function (template) {
        var $deserializedTemplate = $(template);

        $deserializedTemplate.find("*").each(function () {
            var $element = $(this);
            $.each(this.attributes, function () {
                if (this.value.indexOf('%d') !== -1) {
                    $element.data(this.name, this.value);
                }
            });

            if (n.isTextRetainable($element)) {
                $element.data('element-text', $element.text());
            }
        });

        return $deserializedTemplate;
    };


    /**
     * Set placeholder retainer on existing lines
     * @private
     * @param template
     * @param element container element
     */
    n.setPlaceholderRetainers = function (template, element) {
        var responsibleButton;
        var $deserializedTemplate = $(template);

        var $flatTemplate = $deserializedTemplate.find('*');

        var $element = $(element);

        var $childrenOfElement = $element.children();

        responsibleButton = n.getResponsibleButtonFromTarget($element);

        $childrenOfElement.each(function () {
            var $this = $(this);

            // Add button reference
            $this.data('field.template.addButton', responsibleButton);

            // We use .find() to increase chances of $flatTemplate and $childrenOfChild having the same ordering.
            var $childrenOfChild = $this.find('*');
            $childrenOfChild.each(function (childIndex) {
                var correspondingTemplateElement, $correspondingTemplateElement, $child;

                if (childIndex >= ($flatTemplate.length - 1)) {
                    // the current child element has no corresponding element in the template, so no use of trying to
                    // copy over retaining information.
                    return;
                }

                $child = $(this);

                correspondingTemplateElement = $flatTemplate[childIndex];
                $correspondingTemplateElement = $(correspondingTemplateElement);

                // Create the retainers on the child for each child
                $.each(correspondingTemplateElement.attributes, function () {
                    if (this.value.indexOf('%d') !== -1) {
                        $child.data(this.name, this.value);
                    }
                });

                if (n.isTextRetainable($correspondingTemplateElement)) {
                    $child.data('element-text', $correspondingTemplateElement.text());
                }
            });
        });
    };

    /**
     * @private
     */
    n.expandRetainedPlaceholders = function ($elements, value) {
        $elements.find('*').each(function () {
            var $element = $(this);
            $.each(this.attributes, function () {
                var retainedPlaceholder = $element.data(this.name);
                if (retainedPlaceholder) {
                    this.value = n.replacePlaceholder(retainedPlaceholder, value);
                }
            });

            if (n.hasRetainedText($element)) {
                $element.text(n.replacePlaceholder($element.data('element-text'), value));
            }

        });
    };

    /**
     * @private
     * @param $element
     * @returns {*}
     */
    n.isTextRetainable = function ($element) {
        return $element.is("label") && $element.text().indexOf('%d') !== -1 && $element.children().length === 0;
    };

    /**
     * @private
     * @param $element
     * @returns {boolean}
     */
    n.hasRetainedText = function ($element) {
        return $element.data('element-text') !== undefined;
    };

    /**
     * @private
     * @param targetSelector
     * @returns {jQuery}
     */
    n.countLines = function (targetSelector) {
        var escapedTargetSelector = n.escapeJqueryIdSelector(targetSelector);
        return $(targetSelector).children().length;
    };

    /**
     * @private
     */
    n.replacePlaceholder = function (haystack, by) {
        return haystack.replace(/%d/g, by);
    };

    /**
     * @private
     * @param $container
     */
    n.reExpandLineByLine = function ($container) {
        $container.children().each(function (index) {
            var $element = $(this);
            n.expandRetainedPlaceholders($element, index + 1);
        });
    };
})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global $ */
/* global EventEmitter */

/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Handle file deletion.
     *
     * Upon instantiation, it attaches to all `<button>` elements having the class `delete-file`, within the elements
     * selected by the `formSelector`.
     *
     * @param formSelector jQuery selector of the `<form>` this instance is responsible for.
     * @param targetUrl endpoint URL to send delete requests to.
     * @constructor
     *
     * @name QfqNS.FileDelete
     */
    n.FileDelete = function (formSelector, targetUrl) {
        this.formSelector = formSelector;
        this.targetUrl = targetUrl;
        this.eventEmitter = new EventEmitter();

        this.setupOnClickHandler();
    };

    n.FileDelete.prototype.on = n.EventEmitter.onMixin;

    n.FileDelete.prototype.setupOnClickHandler = function () {
        $(this.formSelector + " button.delete-file").click(this.buttonClicked.bind(this));
    };

    n.FileDelete.prototype.buttonClicked = function (event) {
        event.preventDefault();
        var alert = new n.Alert({
            message: "Do you want to delete the file?",
            type: "warning",
            modal: true,
            buttons: [
                {label: "OK", eventName: "ok"},
                {label: "Cancel", eventName: "cancel", focus: true}
            ]
        });
        alert.on('alert.ok', function () {
            this.performFileDelete(event);
        }.bind(this));
        alert.show();
    };

    n.FileDelete.prototype.performFileDelete = function (event) {
        this.eventEmitter.emitEvent('filedelete.started', n.EventEmitter.makePayload(event.delegateTarget, null));

        var data = this.prepareData(event.delegateTarget);

        $.ajax({
            url: this.targetUrl,
            type: 'POST',
            data: data,
            cache: false
        })
            .done(this.ajaxSuccessHandler.bind(this, event.delegateTarget))
            .fail(this.ajaxErrorHandler.bind(this, event.delegateTarget));
    };

    n.FileDelete.prototype.prepareData = function (htmlButton) {
        if (!htmlButton.hasAttribute("name")) {
            throw new Error("File delete button element requires 'name' attribute");
        }

        var fileDeleteName = htmlButton.getAttribute('name');

        var data = {
            s: $(htmlButton).data('sip'),
            name: fileDeleteName
        };

        return data;
    };

    n.FileDelete.prototype.ajaxSuccessHandler = function (uploadTriggeredBy, data, textStatus, jqXHR) {
        var eventData = n.EventEmitter.makePayload(uploadTriggeredBy, data, {
            textStatus: textStatus,
            jqXHR: jqXHR
        });
        this.eventEmitter.emitEvent('filedelete.delete.successful', eventData);
        this.eventEmitter.emitEvent('filedelete.ended', eventData);
    };

    n.FileDelete.prototype.ajaxErrorHandler = function (uploadTriggeredBy, jqXHR, textStatus, errorThrown) {
        var eventData = n.EventEmitter.makePayload(uploadTriggeredBy, null, {
            textStatus: textStatus,
            errorThrown: errorThrown,
            jqXHR: jqXHR
        });
        this.eventEmitter.emitEvent('filedelete.delete.failed', eventData);
        this.eventEmitter.emitEvent('filedelete.ended', eventData);
    };


})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */

/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     *
     * @param formSelector
     * @param targetUrl
     * @constructor
     * @name QfqNS.FileUpload
     */
    n.FileUpload = function (formSelector, targetUrl) {
        this.formSelector = formSelector;
        this.targetUrl = targetUrl;
        this.eventEmitter = new EventEmitter();

        this.setupOnChangeHandler();
    };

    n.FileUpload.prototype.on = n.EventEmitter.onMixin;

    /**
     *
     * @private
     */
    n.FileUpload.prototype.setupOnChangeHandler = function () {
        $(this.formSelector + " :file").on('change', this.performFileUpload.bind(this));
    };

    /**
     * @private
     * @param event
     */
    n.FileUpload.prototype.performFileUpload = function (event) {
        this.eventEmitter.emitEvent('fileupload.started', n.EventEmitter.makePayload(event.target, null));

        var data = this.prepareData(event.target);

        $.ajax({
            url: this.targetUrl,
            type: 'POST',
            data: data,
            cache: false,
            processData: false,
            contentType: false
        })
            .done(this.ajaxSuccessHandler.bind(this, event.target))
            .fail(this.ajaxErrorHandler.bind(this, event.target));
    };

    n.FileUpload.prototype.prepareData = function (htmlFileInput) {
        if (!htmlFileInput.hasAttribute("name")) {
            throw new Error("File input element requires 'name' attribute");
        }

        var fileInputName = htmlFileInput.getAttribute('name');

        var data = new FormData();
        $.each(htmlFileInput.files, function (key, value) {
            data.append(fileInputName + '_' + key, value);
        });

        data.append('s', $(htmlFileInput).data('sip'));
        data.append('name', fileInputName);

        return data;
    };

    /**
     * @private
     * @param data
     * @param textStatus
     * @param jqXHR
     */

    n.FileUpload.prototype.ajaxSuccessHandler = function (uploadTriggeredBy, data, textStatus, jqXHR) {
        var eventData = n.EventEmitter.makePayload(uploadTriggeredBy, data, {
            textStatus: textStatus,
            jqXHR: jqXHR
        });
        this.eventEmitter.emitEvent('fileupload.upload.successful', eventData);
        this.eventEmitter.emitEvent('fileupload.ended', eventData);
    };

    /**
     * @private
     * @param jqXHR
     * @param textStatus
     * @param errorThrown
     */
    n.FileUpload.prototype.ajaxErrorHandler = function (uploadTriggeredBy, jqXHR, textStatus, errorThrown) {
        var eventData = n.EventEmitter.makePayload(uploadTriggeredBy, null, {
            textStatus: textStatus,
            errorThrown: errorThrown,
            jqXHR: jqXHR
        });
        this.eventEmitter.emitEvent('fileupload.upload.failed', eventData);
        this.eventEmitter.emitEvent('fileupload.ended', eventData);
    };


})(QfqNS);

/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Abstraction of `<form>`.
     *
     * @param formId
     * @param formChanged
     * @constructor
     * @name QfqNS.Form
     */
    n.Form = function (formId, formChanged) {
        this.formId = formId;
        this.eventEmitter = new EventEmitter();
        // In order to immediately emit events, we bind event handlers on `<form>` `change` and `input` and `paste` on
        // `<input> and `<textarea>`s. Without precaution, this will lead to emitting `form.changed` twice upon
        // `input` and `paste` events, since they eventually will raise a form `change` event.  We perform bookkeeping
        // using this flag, to avoid emitting `form.changed` twice when bubbling.
        //
        // Since we cannot predict the effect on disable bubbling of the `input` and `paste` events, we resort to this
        // home-brew solution.
        this.inputAndPasteHandlerCalled = false;
        this.saveInProgress = false;

        if (!document.forms[this.formId]) {
            throw new Error("Form '" + formId + "' does not exist.");
        }

        this.formChanged = !!formChanged;
        this.formChangedTimestampInMilliseconds = n.Form.NO_CHANGE_TIMESTAMP;
        this.$form = $(document.forms[this.formId]);
        this.$form.on("change", this.changeHandler.bind(this));
        this.$form.on("invalid.bs.validator", this.validationError.bind(this));
        this.$form.on("valid.bs.validator", this.validationSuccess.bind(this));

        // On <input> elements, we specifically bind this events, in order to update the formChanged property
        // immediately, not only after loosing focus. Same goes for <textarea>
        this.$form.find("input, textarea").on("input paste", this.inputAndPasteHandler.bind(this));

        this.$form.on('submit', function (event) {
            event.preventDefault();
        });
    };

    n.Form.NO_CHANGE_TIMESTAMP = -1;

    n.Form.prototype.on = n.EventEmitter.onMixin;

    /**
     *
     * @param event
     *
     * @private
     */
    n.Form.prototype.changeHandler = function (event) {
        if (this.inputAndPasteHandlerCalled) {
            // reset the flag
            this.inputAndPasteHandlerCalled = false;
            // and return. The `form.changed` event has already been emitted by `Form#inputAndPasteHandler()`.
            return;
        }
        this.markChanged();
    };

    n.Form.prototype.inputAndPasteHandler = function (event) {
        this.inputAndPasteHandlerCalled = true;
        this.markChanged();
    };

    n.Form.prototype.getFormChanged = function () {
        return this.formChanged;
    };

    n.Form.prototype.markChanged = function () {
        this.setFormChangedState();
        this.eventEmitter.emitEvent('form.changed', n.EventEmitter.makePayload(this, null));
    };

    /**
     * @private
     */
    n.Form.prototype.setFormChangedState = function () {
        this.formChanged = true;
        this.formChangedTimestampInMilliseconds = Date.now();
    };

    n.Form.prototype.resetFormChanged = function () {
        this.resetFormChangedState();
        this.eventEmitter.emitEvent('form.reset', n.EventEmitter.makePayload(this, null));

    };

    /**
     * @private
     */
    n.Form.prototype.resetFormChangedState = function () {
        this.formChanged = false;
        this.formChangedTimestampInMilliseconds = n.Form.NO_CHANGE_TIMESTAMP;
    };

    n.Form.prototype.submitTo = function (to, queryParameters) {
        var submitUrl;

        this.eventEmitter.emitEvent('form.submit.before', n.EventEmitter.makePayload(this, null));
        submitUrl = this.makeUrl(to, queryParameters);
        $.post(submitUrl, this.$form.serialize())
            .done(this.ajaxSuccessHandler.bind(this))
            .fail(this.submitFailureHandler.bind(this));
        console.log(this.$form.serialize());
    };

    n.Form.prototype.serialize = function () {
        return this.$form.serialize();
    };


    /**
     * @private
     * @param url base url
     * @param queryParameters additional query parameters
     * @returns {*}
     */
    n.Form.prototype.makeUrl = function (url, queryParameters) {
        var notFound = -1;
        var querySeparator = '?';
        var parameterSeparator = '&';
        var queryString;

        if (!queryParameters) {
            return url;
        }

        queryString = $.param(queryParameters);
        if (url.indexOf(querySeparator) === notFound) {
            return url + querySeparator + queryString;
        } else {
            return url + parameterSeparator + queryString;
        }
    };

    /**
     *
     * @param data
     * @param textStatus
     * @param jqXHR
     *
     * @private
     */
    n.Form.prototype.ajaxSuccessHandler = function (data, textStatus, jqXHR) {
        this.resetFormChangedState();
        this.eventEmitter.emitEvent('form.submit.successful',
            n.EventEmitter.makePayload(this, data, {
                textStatus: textStatus,
                jqXHR: jqXHR
            }));
        $("#save-button").removeClass('btn-warning active disabled');
        $("#save-button").addClass('btn-default');
        this.saveInProgress = false;
    };

    /**
     *
     *
     * @private
     */
    n.Form.prototype.submitFailureHandler = function (jqXHR, textStatus, errorThrown) {
        this.eventEmitter.emitEvent('form.submit.failed', n.EventEmitter.makePayload(this, null, {
            textStatus: textStatus,
            errorThrown: errorThrown,
            jqXHR: jqXHR
        }));
    };

    /**
     * @public
     * @returns {*}
     */
    n.Form.prototype.validate = function () {
        this.eventEmitter.emitEvent('form.validation.before', n.EventEmitter.makePayload(this, null));
        var isValid;
        var form = document.forms[this.formId];
        var $form = $(form);

        if ($form.data('bs.validator')) {
            $form.validator('validate');
            isValid = !$form.data('bs.validator').hasErrors();
        } else {
            isValid = form.checkValidity();
        }

        this.eventEmitter.emitEvent('form.validation.after', n.EventEmitter.makePayload(this, {validationResult: isValid}));

        return isValid;
    };

    /**
     * @private
     */
    n.Form.prototype.validationError = function (data) {
        this.eventEmitter.emitEvent('form.validation.failed', n.EventEmitter.makePayload(this, {element: data.relatedTarget}));
    };

    /**
     * @private
     */
    n.Form.prototype.validationSuccess = function (data) {
        this.eventEmitter.emitEvent('form.validation.success', n.EventEmitter.makePayload(this, {element: data.relatedTarget}));
    };

    /**
     * Uses standard HTML Validation in native javascript for input elements
     *
     * @public
     */
    n.Form.prototype.getFirstNonValidElement = function () {
        var index;
        var form = document.getElementById(this.formId);
        var inputs = form.getElementsByTagName("input");
        var elementNumber = inputs.length;

        for (index = 0; index < elementNumber; index++) {
            var element = inputs[index];
            if (!element.willValidate) {
                continue;
            }

            if (!element.checkValidity()) {
                return element;
            }
        }

        return null;
    };

})(QfqNS);

/* global $ */
/* @depend TablesorterController.js */

var QfqNS = QfqNS || {};

$(document).ready( function () {
    (function (n) {

        var tablesorterController = new n.TablesorterController();
        $('.tablesorter').each(function(i) {
            tablesorterController.setup($(this), i);
        }); // end .each()

    })(QfqNS);
});
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* @depend QfqEvents.js */
/* global EventEmitter */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';


    /**
     *
     * @constructor
     * @name QfqNS.PageState
     */
    n.PageState = function () {
        this.pageState = location.hash.slice(1);
        this.data = null;
        this.inPoppingHandler = false;
        this.eventEmitter = new EventEmitter();

        window.addEventListener("popstate", this.popStateHandler.bind(this));
    };

    n.PageState.prototype.on = n.EventEmitter.onMixin;
    /**
     *
     * @param event
     *
     * @private
     */
    n.PageState.prototype.popStateHandler = function (event) {

        n.Log.debug("Enter: PageState.popStateHandler()");

        this.inPoppingHandler = true;
        this.pageState = location.hash.slice(1);
        this.data = window.history.state;

        n.Log.debug("PageState.popStateHandler(): invoke user pop state handler(s)");

        this.eventEmitter.emitEvent('pagestate.state.popped', n.EventEmitter.makePayload(this, null));

        this.inPoppingHandler = false;
        n.Log.debug("Exit: PageState.popStateHandler()");

    };

    n.PageState.prototype.getPageState = function () {
        return this.pageState;
    };

    n.PageState.prototype.getPageData = function () {
        return this.data;
    };

    n.PageState.prototype.setPageState = function (state, data) {
        if (state.startsWith('#')) {
            this.pageState = state.slice(1);
            window.history.replaceState(data, null, state);
        } else {
            this.pageState = state;
            window.history.replaceState(data, null, '#' + state);
        }
        this.data = data;
    };

    n.PageState.prototype.newPageState = function (state, data) {
        if (state.startsWith('#')) {
            this.pageState = state.slice(1);
            window.history.pushState(data, null, state);
        } else {
            this.pageState = state;
            window.history.pushState(data, null, '#' + state);
        }

        this.data = data;
    };

})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend ElementUpdate.js */
/* @depend Dirty.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    // TODO: This object is getting its own gravitational field. Start refactoring.
    /**
     * Represents a QFQ Form.
     *
     * QfqForm will autonomously fire an lock `extend` request when the lock expired, but the last change `t_c` has
     * been made during the lock period `t_l`. I.e. let `t_{current}` be the current time, an `extend` request is made
     * when
     *
     *    t_c + t_l > t_{current}
     *
     * holds.
     *
     * @param formId {string} value of the form's id attribute
     * @param submitTo {string} url where data will be submitted to
     * @param deleteUrl {string} url to call upon record deletion
     * @param dataRefreshUrl {string} url where to fetch new element values from
     * @param fileUploadTo {string} url used for file uploads
     * @param fileDeleteUrl {string} url used to delete files
     * @param dirtyUrl {string} url used to query
     * @constructor
     *
     * @name QfqNS.QfqForm
     */
    n.QfqForm = function (formId, submitTo, deleteUrl, dataRefreshUrl, fileUploadTo, fileDeleteUrl, dirtyUrl) {
        this.formId = formId;
        this.submitTo = submitTo;
        this.deleteUrl = deleteUrl;
        this.dataRefreshUrl = dataRefreshUrl;
        this.fileUploadTo = fileUploadTo;
        this.fileDeleteUrl = fileDeleteUrl;
        this.dirtyUrl = dirtyUrl;
        this.dirtyFired = false;
        this.lockAcquired = false;
        this.formImmutableDueToConcurrentAccess = false;
        this.lockRenewalPhase = false;
        this.goToAfterSave = false;

        this.additionalQueryParameters = {
            'recordHashMd5': this.getRecordHashMd5()
        };

        if (!!$('#' + QfqNS.escapeJqueryIdSelector(this.formId)).data('enable-save-button')) {
            this.form = new n.Form(this.formId, true);
            this.getSaveButton().removeClass("disabled").removeAttr("disabled");
        } else {
            this.getSaveButton().addClass("disabled").attr("disabled", "disabled");
            this.form = new n.Form(this.formId, false);
        }

        this.infoLockedButton = this.infoLockedButton.bind(this);

        // This is required when displaying validation messages, in order to activate the tab, which has validation
        // issues
        this.bsTabs = null;
        this.lastButtonPress = null;

        this.eventEmitter = new EventEmitter();

        this.dirty = new n.Dirty(this.dirtyUrl);
        this.dirty.on(n.Dirty.EVENTS.SUCCESS, this.dirtyNotifySuccess.bind(this));
        this.dirty.on(n.Dirty.EVENTS.DENIED, this.dirtyNotifyDenied.bind(this));
        this.dirty.on(n.Dirty.EVENTS.FAILED, this.dirtyNotifyFailed.bind(this));
        this.dirty.on(n.Dirty.EVENTS.SUCCESS_TIMEOUT, this.dirtyTimeout.bind(this));
        this.dirty.on(n.Dirty.EVENTS.RENEWAL_DENIED, this.dirtyRenewalDenied.bind(this));
        this.dirty.on(n.Dirty.EVENTS.RENEWAL_SUCCESS, this.dirtyRenewalSuccess.bind(this));


        this.form.on('form.changed', this.changeHandler.bind(this));
        this.form.on('form.reset', this.resetHandler.bind(this));
        this.form.on('form.submit.successful', this.submitSuccessDispatcher.bind(this));
        this.form.on('form.submit.failed', function (obj) {
            n.Helper.showAjaxError(null, obj.textStatus, obj.errorThrown);
        });

        this.getSaveButton().click(this.handleSaveClick.bind(this));
        this.getCloseButton().click(this.handleCloseClick.bind(this));
        this.getNewButton().click(this.handleNewClick.bind(this));
        this.getDeleteButton().click(this.handleDeleteClick.bind(this));

        var that = this;
        $('.external-save').click(function(e) {
            var uri = $(this).data('target');
            that.callSave(uri);
        });

        this.setupFormUpdateHandler();
        if (!!$('#' + QfqNS.escapeJqueryIdSelector(this.formId)).data('disable-return-key-submit')) {
            // Nothing to do
        } else {
            this.setupEnterKeyHandler();
        }

        this.fileUploader = new n.FileUpload('#' + this.formId, this.fileUploadTo);
        this.startUploadHandler = this.startUploadHandler.bind(this);
        this.fileUploader.on('fileupload.started', this.startUploadHandler);
        this.fileUploader.on('fileupload.upload.successful', that.fileUploadSuccessHandler);

        this.fileUploader.on('fileupload.upload.failed',
            function (obj) {
                n.Helper.showAjaxError(null, obj.textStatus, obj.errorThrown);
            });
        this.endUploadHandler = this.endUploadHandler.bind(this);
        this.fileUploader.on('fileupload.ended', this.endUploadHandler);

        this.fileDeleter = new n.FileDelete("#" + this.formId, this.fileDeleteUrl);
        this.fileDeleter.on('filedelete.delete.successful', this.fileDeleteSuccessHandler.bind(this));

        this.fileDeleter.on('filedelete.delete.failed',
            function (obj) {
                n.Helper.showAjaxError(null, obj.textStatus, obj.errorThrown);
            });

        var configurationData = this.readFormConfigurationData();
        this.applyFormConfiguration(configurationData);

        // Initialize jqxDateTimeInput elements.
        n.Helper.jqxDateTimeInput();
        // Initialize jqxComboBox elements.
        n.Helper.jqxComboBox();
        // Deprecated
        //n.Helper.jqxEditor();
        n.Helper.tinyMce();
        n.Helper.codemirror();
        this.form.on('form.submit.before', n.Helper.tinyMce.prepareSave);
        this.form.on('form.validation.before', n.Helper.tinyMce.prepareSave);
        this.form.on('form.validation.failed', this.validationError);
        this.form.on('form.validation.success', this.validationSuccess);

    };

    n.QfqForm.prototype.on = n.EventEmitter.onMixin;

    n.QfqForm.prototype.dirtyNotifySuccess = function (obj) {
        this.lockAcquired = true;
        // Intentionally empty. May be used later on.
    };

    n.QfqForm.prototype.dirtyRenewalSuccess = function (obj) {
        this.lockAcquired = true;
    };

    /**
     * @public
     */
    n.QfqForm.prototype.releaseLock = function (async) {
        if (!this.lockAcquired) {
            n.Log.debug("releaseLock(): no lock acquired or already released.");
            return;
        }
        n.Log.debug("releaseLock(): releasing lock.");
        this.dirty.release(this.getSip(), this.getRecordHashMd5AsQueryParameter(), async);
        this.resetLockState();
    };

    n.QfqForm.prototype.resetLockState = function () {
        this.dirty.clearSuccessTimeoutTimerIfSet();
        this.dirtyFired = false;
        this.formImmutableDueToConcurrentAccess = false;
        this.lockRenewalPhase = false;
        this.lockAcquired = false;
    };

    n.QfqForm.prototype.dirtyRenewalDenied = function (obj) {
        var that = this;
        var messageButtons = [{
            label: "Reload",
            eventName: 'reload'
        }];
        if (obj.data.status == "conflict_allow_force") {
            messageButtons.push({
                label: "Ignore",
                eventName: 'ignore'
            });
        }
        var alert = new n.Alert(
            {
                type: "error",
                message: obj.data.message,
                modal: true,
                buttons: messageButtons
            }
        );
        alert.on('alert.reload', function () {
            that.eventEmitter.emitEvent('qfqform.close-intentional', n.EventEmitter.makePayload(that, null));
            window.location.reload(true);
        });
        alert.on('alert.ignore', function () {
            console.log("Ignored Recordlock");
        });
        alert.show();
    };

    n.QfqForm.prototype.dirtyTimeout = function (obj) {
        this.dirtyFired = false;
        this.lockAcquired = false;
        this.lockRenewalPhase = true;

        // Figure out whether the user made changes in the lock timeout period
        if (this.form.formChangedTimestampInMilliseconds + this.dirty.lockTimeoutInMilliseconds >
            Date.now()) {
            // Renew without user intervention.
            this.fireDirtyRequestIfRequired();
            // and bail out
            return;
        }
        var alert = new n.Alert(
            {
                message: "Exclusive access to document timed out.",
                type: "warning"
            }
        );
        alert.show();
    };

    n.QfqForm.prototype.dirtyNotifyDenied = function (obj) {
        var messageType;
        var isModal = true;
        var messageButtons = [{
            label: "Reload",
            eventName: 'reload'
        }];
        var message;
        var that = this;

        switch (obj.data.status) {
            case "conflict":
                messageType = "error";
                this.setButtonEnabled(this.getSaveButton(), false);
                this.getSaveButton().removeClass(this.getSaveButtonAttentionClass());
                this.setButtonEnabled(this.getDeleteButton(), false);
                this.formImmutableDueToConcurrentAccess = true;
                this.lockAcquired = false;
                break;
            case "conflict_allow_force":
                messageType = "warning";
                messageButtons.push({
                    label: "Ignore",
                    eventName: 'ignore'
                });
                break;
            case "error":
                messageType = "error";
                this.setButtonEnabled(this.getSaveButton(), false);
                this.getSaveButton().removeClass(this.getSaveButtonAttentionClass());
                this.setButtonEnabled(this.getDeleteButton(), false);
                // Do not make the form ask for saving changes.
                this.form.formChanged = false;
                this.formImmutableDueToConcurrentAccess = true;
                this.lockAcquired = false;
                break;
            default:
                n.Log.error('Invalid dirty status: \'' + obj.data.status + '\'. Assume messageType \'error\'');
                messageType = "error";
                break;
        }

        message = new n.Alert({
            message: obj.data.message,
            type: messageType,
            timeout: n.Alert.constants.NO_TIMEOUT,
            modal: isModal,
            buttons: messageButtons
        });
        message.on('alert.reload', function () {
            that.eventEmitter.emitEvent('qfqform.close-intentional', n.EventEmitter.makePayload(that, null));
            window.location.reload(true);
        });
        message.show();
    };

    n.QfqForm.prototype.dirtyNotifyFailed = function () {
        this.dirtyFired = false;
        this.lockAcquired = false;
    };

    n.QfqForm.prototype.validationError = function (info) {
        var $formControl = $(info.data.element);
        var $messageContainer = $formControl.siblings('.hidden.with-errors');

        if ($messageContainer.length === 0) {
            if ($formControl.parent().hasClass('input-group')) {
                $messageContainer = $formControl.parent().siblings('.hidden.with-errors');
            }
        }

        $messageContainer.data('qfq.hidden.message', true);
        $messageContainer.removeClass('hidden');
    };

    n.QfqForm.prototype.validationSuccess = function (info) {
        var $formControl = $(info.data.element);
        var $messageContainer = $formControl.siblings('.with-errors');

        if ($messageContainer.length === 0) {
            if ($formControl.parent().hasClass('input-group')) {
                $messageContainer = $formControl.parent().siblings('.hidden.with-errors');
            }
        }

        if ($messageContainer.data('qfq.hidden.message') === true) {
            $messageContainer.addClass('hidden');
        }
    };

    /**
     * @private
     */
    n.QfqForm.prototype.setupEnterKeyHandler = function () {
        $("input").keyup(function (event) {
            if (this.formImmutableDueToConcurrentAccess) {
                return;
            }
            if (event.which === 13 && this.submitOnEnter()) {
                if (this.isFormChanged()) {
                    this.lastButtonPress = "save&close";
                    n.Log.debug("save&close click");
                    this.submit();
                }
                event.preventDefault();
            }
        }.bind(this));
    };


    /**
     *
     * @private
     */
    n.QfqForm.prototype.readFormConfigurationData = function () {
        var $configuredElements = $("#" + this.formId + " [data-hidden],#" + this.formId + " [data-disabled],#" + this.formId + " [data-required]");

        var configurationArray = [];
        $configuredElements.each(function (index, element) {
            try {
                var $element = $(element);
                if (!element.hasAttribute("name")) {
                    n.Log.warning("Element has configuration data, but no name. Skipping");
                    return;
                }

                var configuration = {};
                configuration['form-element'] = $element.attr('name');

                var hiddenVal = $element.data('hidden');
                if (hiddenVal !== undefined) {
                    configuration.hidden = n.Helper.stringToBool(hiddenVal);
                }

                var disabledVal = $element.data('disabled');
                if (disabledVal !== undefined) {
                    configuration.disabled = n.Helper.stringToBool(disabledVal);
                }

                var requiredVal = $element.data("required");
                if (requiredVal !== undefined) {
                    configuration.required = n.Helper.stringToBool(requiredVal);
                }

                configurationArray.push(configuration);
            } catch (e) {
                n.Log.error(e.message);
            }
        });

        return configurationArray;

    };

    /**
     * @public
     * @param bsTabs
     */
    n.QfqForm.prototype.setBsTabs = function (bsTabs) {
        this.bsTabs = bsTabs;
    };

    n.QfqForm.prototype._createError = function (message) {
        var messageButtons = [{
            label: "Ok",
            eventName: 'close'
        }];
        var alert = new n.Alert({ "message": message, "type": "error", modal: true, buttons: messageButtons});
        alert.show();
    };

    /**
     * @private
     */
    n.QfqForm.prototype.fileDeleteSuccessHandler = function (obj) {
        if (!obj.data.status) {
            throw Error("Response on file upload missing status");
        }

        if (obj.data.status === "error") {
            this._createError(obj.data.message);
            return;
        }

        var $button = $(obj.target);
        $button.prop("disabled", true);

        var $buttonParent = $button.parent();
        $buttonParent.addClass('hidden');

        var $inputFile = $buttonParent.siblings('label');
        $inputFile.children(':file').prop("disabled", false);
        $inputFile.removeClass('hidden');
        $inputFile.children(':file').removeClass('hidden');

        $inputFile.children(':file').val("");
        if ($inputFile.children(':file').data('required') == 'required') {
            $inputFile.children(':file').prop("required", true);
        }

        this.form.markChanged();
    };

    /**
     * @private
     */
    n.QfqForm.prototype.fileUploadSuccessHandler = function (obj) {
        if (!obj.data.status) {
            throw Error("Response on file upload missing status");
        }

        if (obj.data.status === "error") {
            //this._createError(obj.data.message);
            var messageButtons = [{
                label: "Ok",
                eventName: 'close'
            }];
            var alert = new n.Alert({ "message": obj.data.message, "type": obj.data.status, modal: true, buttons: messageButtons});
            alert.show();
            return false;
        }

        var $fileInput = $(obj.target);
        $fileInput.prop("disabled", true);
        $fileInput.addClass("hidden");
        $fileInput.parent().addClass("hidden");

        var $deleteContainer = $fileInput.parent().siblings('div.uploaded-file');


        var fileNamesString = obj.target.files[0].name;
        var $fileNameSpan = $deleteContainer.find("span.uploaded-file-name");
        $fileNameSpan.empty().append(fileNamesString);

        var $deleteButton = $deleteContainer.find("button");
        $deleteButton.prop("disabled", false);

        $deleteContainer.removeClass("hidden");
    };

    /**
     *
     * @param $button
     * @param enabled {boolean}
     *
     * @private
     */
    n.QfqForm.prototype.setButtonEnabled = function ($button, enabled) {
        if (!$button) {
            n.Log.error("QfqForm#setButtonEnabled(): no button provided.");
            return;
        }
        if (!enabled) {
            $button.addClass("disabled");
            $button.prop("disabled", true);
        } else {
            $button.removeClass("disabled");
            $button.prop("disabled", false);
        }
    };

    n.QfqForm.prototype.setupFormUpdateHandler = function () {
        $('input[data-load],select[data-load]').on('change', this.formUpdateHandler.bind(this));
    };

    n.QfqForm.prototype.formUpdateHandler = function () {
        var that = this;
        if (this.formImmutableDueToConcurrentAccess) {
            return;
        }
        $.post(this.dataRefreshUrl, this.form.serialize(), "json")
            .fail(n.Helper.showAjaxError)
            .done(function (data) {
                this.handleFormUpdate(data);
            }.bind(that));

    };

    n.QfqForm.prototype.handleFormUpdate = function (data) {
        if (!data.status) {
            throw new Error("Expected 'status' attribute to be present.");
        }

        if (data.status === "error") {
            this._createError("Error while updating form:<br>" +
                             (data.message ? data.message : "No reason given"));
            return;
        }

        if (data.status === "success") {
            if (!data['form-update']) {
                throw new Error("'form-update' attribute missing in form update data");
            }


            this.applyFormConfiguration(data['form-update']);
            this.applyElementConfiguration(data['element-update']);
            return;
        }

        throw new Error("Unexpected status: '" + data.status + "'");
    };

    /**
     * @private
     */
    n.QfqForm.prototype.destroyFormAndSetText = function (text) {
        this.form = null;
        $('#' + this.formId).replaceWith($("<p>").append(text));
        this.eventEmitter.emitEvent('qfqform.destroyed', n.EventEmitter.makePayload(this, null));
    };

    /**
     * @private
     */
    n.QfqForm.prototype.handleSaveClick = function () {
        this.lastButtonPress = "save";
        n.Log.debug("save click");
        this.getSaveButton().removeClass('btn-info');
        this.getSaveButton().addClass('btn-warning active disabled');
        if (!this.form.saveInProgress) {
            this.submit();
        }
    };

    n.QfqForm.prototype.callSave = function(uri) {
        console.log("target: " + uri);
        if(this.isFormChanged()) {
            this.handleSaveClick();
            this.goToAfterSave = uri;
        } else {
            window.location = uri;
            return;
        }
    };

    /**
     * @private
     */
    n.QfqForm.prototype.handleCloseClick = function () {
        this.lastButtonPress = "close";
        if (this.form.getFormChanged()) {
            var alert = new n.Alert({
                message: "You have unsaved changes. Do you want to save first?",
                type: "warning",
                modal: true,
                buttons: [
                    {label: "Yes", eventName: "yes"},
                    {label: "No", eventName: "no", focus: true},
                    {label: "Cancel", eventName: "cancel"}
                ]
            });
            var that = this;
            alert.on('alert.yes', function () {
                that.submit();
            });
            alert.on('alert.no', function () {
                that.releaseLock();
                that.eventEmitter.emitEvent('qfqform.close-intentional', n.EventEmitter.makePayload(that, null));

                that.goBack();
            });
            alert.show();
        } else {
            this.goBack();
        }
    };

    n.QfqForm.prototype.submit = function (queryParameters) {
        var submitQueryParameters;
        var alert;
        var submitReason;

        if (this.form.validate() !== true) {

            var element = this.form.getFirstNonValidElement();
            if (element.hasAttribute('name') && this.bsTabs) {
                var tabId = this.bsTabs.getContainingTabIdForFormControl(element.getAttribute('name'));
                if (tabId) {
                    this.bsTabs.activateTab(tabId);
                }
            }

            // Since we might have switched the tab, re-validate to highlight errors
            this.form.$form.validator('update');
            this.form.$form.validator('validate');

            alert = new n.Alert("Form is incomplete.", "warning");
            alert.timeout = 3000;
            alert.show();
            return;
        }

        // First, remove all validation states, in case a previous submit has set a validation state, thus we're not
        // stockpiling them.
        this.clearAllValidationStates();


        submitReason = {
            "submit_reason": this.lastButtonPress === "close" ? "save,close" : this.lastButtonPress
        };

        submitQueryParameters = $.extend({}, queryParameters, submitReason);
        this.form.submitTo(this.submitTo, submitQueryParameters);
        this.form.saveInProgress = true;
    };

    /**
     * @private
     */
    n.QfqForm.prototype.handleNewClick = function (event) {
        event.preventDefault();

        this.lastButtonPress = "new";
        if (this.form.getFormChanged()) {
            var alert = new n.Alert({
                message: "You have unsaved changes. Do you want to save first?",
                type: "warning",
                modal: true,
                buttons: [
                    {label: "Yes", eventName: "yes", focus: true},
                    {label: "No", eventName: "no"},
                    {label: "Cancel", eventName: "cancel"}
                ]
            });
            var that = this;
            alert.on('alert.no', function () {
                that.eventEmitter.emitEvent('qfqform.close-intentional', n.EventEmitter.makePayload(that, null));

                var anchorTarget = that.getNewButtonTarget();
                window.location = anchorTarget;
            });
            alert.on('alert.yes', function () {
                that.submit();
            });
            alert.show();
        } else {
            var anchorTarget = this.getNewButtonTarget();
            window.location = anchorTarget;
        }
        n.Log.debug("new click");
    };

    /**
     * @private
     */
    n.QfqForm.prototype.handleDeleteClick = function () {
        this.lastButtonPress = "delete";
        n.Log.debug("delete click");
        var alert = new n.Alert({
            message: "Do you really want to delete the record?",
            type: "warning",
            modal: true,
            buttons: [
                {label: "Yes", eventName: "ok"},
                {label: "No", eventName: "cancel", focus: true}
            ]
        });

        var that = this;
        alert.on('alert.ok', function () {
            $.post(that.appendQueryParametersToUrl(that.deleteUrl, that.getRecordHashMd5AsQueryParameter()))
                .done(that.ajaxDeleteSuccessDispatcher.bind(that))
                .fail(n.Helper.showAjaxError);
        });
        alert.show();
    };

    n.QfqForm.prototype.getRecordHashMd5AsQueryParameter = function () {
        return {
            'recordHashMd5': this.getRecordHashMd5()
        };
    };

    /**
     *
     * @param data
     * @param textStatus
     * @param jqXHR
     *
     * @private
     */
    n.QfqForm.prototype.ajaxDeleteSuccessDispatcher = function (data, textStatus, jqXHR) {
        if (!data.status) {
            throw new Error("No 'status' property 'data'");
        }

        switch (data.status) {
            case "error":
                this.handleLogicDeleteError(data);
                break;
            case "success":
                this.handleDeleteSuccess(data);
                break;
            default:
                throw new Error("Status '" + data.status + "' unknown.");
        }
    };

    /**
     *
     * @param data
     *
     * @private
     */
    n.QfqForm.prototype.handleDeleteSuccess = function (data) {
        this.setButtonEnabled(this.getCloseButton(), false);
        this.setButtonEnabled(this.getDeleteButton(), false);
        this.setButtonEnabled(this.getSaveButton(), false);
        this.setButtonEnabled(this.getNewButton(), false);
        this.destroyFormAndSetText("Record has been deleted!");

        if (!data.redirect || data.redirect === "auto") {
            this.goBack();
            return;
        }

        if (data.redirect === "no") {
            this._createError("redirect=='no' not allowed");
            return;
        }

        if (data.redirect === "url" && data['redirect-url']) {
            window.location = data['redirect-url'];
        }

        if (data.redirect === "url-skip-history" && data['redirect-url']) {
            window.location.replace(data['redirect-url']);
        }
    };

    /**
     *
     * @param data
     *
     * @private
     */
    n.QfqForm.prototype.handleLogicDeleteError = function (data) {
        if (!data.message) {
            throw Error("Status is 'error' but required 'message' attribute is missing.");
        }
        this._createError(data.message);

        this.setButtonEnabled(this.getDeleteButton(), false);
    };

    /**
     * Called when form is changed.
     *
     * @param obj {n.QfqForm}
     *
     * @private
     */
    n.QfqForm.prototype.changeHandler = function (obj) {
        if (this.formImmutableDueToConcurrentAccess) {
            return;
        }
        this.getSaveButton().removeClass("disabled");
        this.getSaveButton().addClass(this.getSaveButtonAttentionClass());
        this.getSaveButton().removeAttr("disabled");
        this.fireDirtyRequestIfRequired();
    };

    n.QfqForm.prototype.fireDirtyRequestIfRequired = function () {
        if (this.dirtyFired) {
            return;
        }

        if (this.lockRenewalPhase) {
            this.dirty.renew(this.getSip(), this.getRecordHashMd5AsQueryParameter());
        } else {
            this.dirty.notify(this.getSip(), this.getRecordHashMd5AsQueryParameter());
        }
        this.dirtyFired = true;
    };

    /**
     *
     * @param obj {n.QfqForm}
     *
     * @private
     */
    n.QfqForm.prototype.resetHandler = function (obj) {
        this.getSaveButton().removeClass(this.getSaveButtonAttentionClass());
        this.getSaveButton().addClass("disabled");
        this.getSaveButton().attr("disabled", "disabled");
        this.resetLockState();
    };

    n.QfqForm.prototype.deactivateSaveButton = function () {
        this.getSaveButton().addClass("disabled");
        //this.getSaveButton().attr("disabled", "disabled");
        this.getSaveButton().off('click');
        this.getSaveButton().on('click', this.infoLockedButton);
        this.getSaveButton().css("color", "#fff");
    };

    n.QfqForm.prototype.infoLockedButton = function(e) {
        var alert = new n.Alert({
            message: "Please wait until the upload finishes to save this form",
            buttons: [{ label: "Ok", eventName: "ok"}],
            modal: true
        });
        alert.show();
        e.preventDefault();
        return false;
    };

    n.QfqForm.prototype.activateSaveButton = function () {
        this.getSaveButton().off('click');
        this.getSaveButton().removeClass("disabled");
        //this.getSaveButton().removeAttr("disabled");
        this.getSaveButton().css("color", "");
        this.getSaveButton().click(this.handleSaveClick.bind(this));
    };

    n.QfqForm.prototype.getSaveButtonAttentionClass = function () {
        var $saveButton = this.getSaveButton();

        return $saveButton.data('class-on-change') || 'alert-warning';
    };

    /**
     *
     * @returns {jQuery|HTMLElement}
     *
     * @private
     */
    n.QfqForm.prototype.getSaveButton = function () {
        return $("#save-button");
    };

    /**
     *
     * @returns {jQuery|HTMLElement}
     *
     * @private
     */
    n.QfqForm.prototype.getCloseButton = function () {
        return $("#close-button");
    };

    /**
     *
     * @returns {jQuery|HTMLElement}
     *
     * @private
     */
    n.QfqForm.prototype.getDeleteButton = function () {
        return $("#delete-button");
    };

    /**
     *
     * @returns {jQuery|HTMLElement}
     *
     * @private
     */
    n.QfqForm.prototype.getNewButton = function () {
        return $("#form-new-button");
    };



    /**
     * @private
     */
    n.QfqForm.prototype.submitSuccessDispatcher = function (obj) {
        if (!obj.data.status) {
            throw new Error("No 'status' property in 'data'");
        }

        switch (obj.data.status) {
            case "error":
                this.handleLogicSubmitError(obj.target, obj.data);
                break;
            case "success":
                this.handleSubmitSuccess(obj.target, obj.data);
                break;
            case "conflict":
                this.handleConflict(obj.target, obj.data);
                break;
            case "conflict_allow_force":
                this.handleOverrideableConflict(obj.target, obj.data);
                break;
            default:
                throw new Error("Status '" + obj.data.status + "' unknown.");
        }

    };

    /**
     *
     * @param form
     * @param data
     *
     * @private
     */
    n.QfqForm.prototype.handleLogicSubmitError = function (form, data) {
        if (!data.message) {
            throw Error("Status is 'error' but required 'message' attribute is missing.");
        }
        this._createError(data.message);

        if (data["field-name"] && this.bsTabs) {
            var tabId = this.bsTabs.getContainingTabIdForFormControl(data["field-name"]);
            if (tabId) {
                this.bsTabs.activateTab(tabId);
            }

            this.setValidationState(data["field-name"], "error");
            this.setHelpBlockValidationMessage(data["field-name"], data["field-message"]);
        }
    };

    /**
     *
     */
    n.QfqForm.prototype.handleConflict = function (form, data) {
        this.setButtonEnabled(this.getSaveButton(), false);
        this.getSaveButton().removeClass(this.getSaveButtonAttentionClass());
        this.setButtonEnabled(this.getDeleteButton(), false);
        this.formImmutableDueToConcurrentAccess = true;
        this.lockAcquired = false;
        this._createError(data.message);
    };

    n.QfqForm.prototype.handleOverrideableConflict = function (form, data) {
        var that = this;
        var alert = new n.Alert({
            message: data.message + 'Save anyway?',
            type: "warning",
            modal: true,
            buttons: [
                {label: "Yes", eventName: "yes"},
                {label: "No", eventName: "no", focus: true}
            ]
        });
        alert.on('alert.yes', function () {
            if (data.tokenForce) {
                that.submit({
                    tokenForce: data.tokenForce
                });
            } else {
                that.submit();
            }

        });
        alert.show();
    };

    /**
     *
     * @param form
     * @param data
     *
     * @private
     */
    n.QfqForm.prototype.handleSubmitSuccess = function (form, data) {
        n.Log.debug('Reset form state');
        form.resetFormChanged();
        this.resetLockState();

        switch (this.lastButtonPress) {
            case 'save&close':
                this.goBack();
                break;
            case 'save':
                if (data.message) {
                    var alert = new n.Alert(data.message);
                    alert.timeout = 3000;
                    alert.show();
                }

                // Skip other checks if external Save is called
                if (this.goToAfterSave) {
                    console.log("Called goToAfterSave = " + this.goToAfterSave);
                    window.location = this.goToAfterSave;
                    return;
                }

                // do we have to update the HTML Form?
                if (data['form-update']) {
                    this.applyFormConfiguration(data['form-update']);
                }

                if (data['element-update']) {this.applyElementConfiguration(data['element-update']);
                }

                if (data.redirect === "url" && data['redirect-url']) {
                    window.location = data['redirect-url'];
                    return;
                }

                if (data.redirect === "url-skip-history" && data['redirect-url']) {
                    window.location.replace(data['redirect-url']);
                    return;
                }

                if (data.redirect === "close") {
                    this.goBack();
                    return;
                }

                break;
            case 'close':
                if (!data.redirect || data.redirect === "no") {
                    return;
                }

                if (data.redirect === "auto" || data.redirect === "close") {
                    this.goBack();
                    return;
                }

                if (data.redirect === "url" && data['redirect-url']) {
                    window.location = data['redirect-url'];
                    return;
                }

                if (data.redirect === "url-skip-history" && data['redirect-url']) {
                    window.location.replace(data['redirect-url']);
                    return;
                }

                break;
            case 'new':
                var target = this.getNewButtonTarget();

                window.location.replace(target);
                return;

            default:
                if (data.redirect === "auto") {
                    this.goBack();
                    return;
                }

                if (data.redirect === "url" && data['redirect-url']) {
                    window.location = data['redirect-url'];
                    return;
                }

                if (data.redirect === "url-skip-history" && data['redirect-url']) {
                    window.location.replace(data['redirect-url']);
                    return;
                }

                break;
        }
    };

    n.QfqForm.prototype.getNewButtonTarget = function () {
        return $('#form-new-button').attr('href');
    };

    n.QfqForm.prototype.getFormGroupByControlName = function (formControlName) {
        var $formControl = $("[name='" + formControlName + "']");
        if ($formControl.length === 0) {
            n.Log.debug("QfqForm.setValidationState(): unable to find form control with name '" + formControlName + "'");
            return null;
        }

        var iterator = $formControl[0];
        while (iterator !== null) {
            var $iterator = $(iterator);
            if ($iterator.hasClass('form-group')) {
                return $iterator;
            }

            iterator = iterator.parentElement;
        }

        return null;
    };

    n.QfqForm.prototype.setValidationState = function (formControlName, state) {
        var $formGroup = this.getFormGroupByControlName(formControlName);
        if ($formGroup) {
            $formGroup.addClass("has-" + state);
        }
    };

    n.QfqForm.prototype.resetValidationState = function (formControlName) {
        var $formGroup = this.getFormGroupByControlName(formControlName);
        $formGroup.removeClass("has-warning");
        $formGroup.removeClass("has-error");
        $formGroup.removeClass("has-success");
        $formGroup.removeClass("has-danger");
    };


    n.QfqForm.prototype.clearAllValidationStates = function () {
        // Reset any messages/states added by bootstrap-validator.
        this.form.$form.validator('reset');

        // Reset any states added by a call to QfqForm#setValidationState()
        $('.has-warning,.has-error,.has-success,.has-danger').removeClass("has-warning has-error has-success" +
            " has-danger");

        // Remove all messages received from server upon form submit.
        $('[data-qfq=validation-message]').remove();
    };

    /**
     *
     * @param formControlName
     * @param text
     */
    n.QfqForm.prototype.setHelpBlockValidationMessage = function (formControlName, text) {
        /*
         * Why is this method here and not in FormGroup? Adding this particular method to FormGroup is easy, however
         * QfqForm.clearAllValidationStates() would not find its proper place in FormGroup, since FormGroup operates
         * on one element. We would end up having the responsibilities spread across several classes, which would be
         * confusing.
         */
        var $formGroup = this.getFormGroupByControlName(formControlName);
        if (!$formGroup) {
            return;
        }
        var $helpBlockColumn;
        var $formGroupSubDivs = $formGroup.find("div");
        if ($formGroupSubDivs.length < 3) {
            $helpBlockColumn = $("<div>").addClass("col-md-4");
            $formGroup.append($helpBlockColumn);
        } else {
            $helpBlockColumn = $($formGroupSubDivs[2]);
        }

        $helpBlockColumn.append(
            $("<p>")
                .addClass("help-block")
                .attr("data-qfq", "validation-message")
                .append(text)
        );
    };

    /**
     *
     * @param configuration {array} array of objects.
     */
    n.QfqForm.prototype.applyFormConfiguration = function (configuration) {
        var arrayLength = configuration.length;
        for (var i = 0; i < arrayLength; i++) {
            var configurationItem = configuration[i];
            var formElementName = configurationItem["form-element"];
            if (formElementName === undefined) {
                n.Log.error("configuration lacks 'form-element' attribute. Skipping.");
                continue;
            }
            try {
                var element = n.Element.getElement(formElementName);

                if (configurationItem.value !== undefined) {
                    element.setValue(configurationItem.value);
                }

                if (configurationItem.readonly !== undefined) {
                    // Readonly and disabled is the same in our domain
                    element.setEnabled(!configurationItem.readonly);
                }

                if (configurationItem.disabled !== undefined) {
                    // Readonly and disabled is the same in our domain
                    element.setEnabled(!configurationItem.disabled);
                }

                if (configurationItem.hidden !== undefined) {
                    element.setHidden(configurationItem.hidden);
                }

                if (configurationItem.required !== undefined) {
                    element.setRequired(configuration.required);
                }
            } catch (e) {
                n.Log.error(e.message);
            }
        }
    };

    n.QfqForm.prototype.applyElementConfiguration = function (configuration) {
        if (!configuration) {
            console.error("No configuration for Element Update found");
            return;
        }

        n.ElementUpdate.updateAll(configuration);
    };

    /**
     * @private
     * @param obj
     */
    n.QfqForm.prototype.startUploadHandler = function (obj) {
        $(obj.target).after(
            $('<i>').addClass('spinner')
        );
        this.deactivateSaveButton();
    };

    /**
     * @private
     * @param obj
     */
    n.QfqForm.prototype.endUploadHandler = function (obj) {
        var $siblings = $(obj.target).siblings();
        $siblings.filter("i").remove();
        this.activateSaveButton();
    };

    /**
     * Retrieve SIP as stored in hidden input field.
     *
     * @returns {string} sip
     */
    n.QfqForm.prototype.getSip = function () {
        return this.getValueOfHiddenInputField('s');
    };

    /**
     * Retrieve recordHashMd5 as stored in hidden input field.
     *
     * @returns {string} sip
     */
    n.QfqForm.prototype.getRecordHashMd5 = function () {
        return this.getValueOfHiddenInputField('recordHashMd5');
    };

    n.QfqForm.prototype.getValueOfHiddenInputField = function (fieldName) {
        return $('#' + this.formId + ' input[name=' + fieldName + ']').val();
    };

    /**
     * @public
     */
    n.QfqForm.prototype.isFormChanged = function () {
        return this.form.formChanged;
    };

    /**
     * @private
     */
    n.QfqForm.prototype.submitOnEnter = function () {
        return !(!!this.form.$form.data('disable-return-key-submit'));
    };

    n.QfqForm.prototype.appendQueryParametersToUrl = function (url, queryParameterObject) {
        var queryParameterString = $.param(queryParameterObject);
        if (url.indexOf('?') !== -1) {
            return url + "&" + queryParameterString;
        }

        return url + "?" + queryParameterString;
    };

    /**
     * @private
     *
     * Go back in the history, or pop an alert when no history.
     */
    n.QfqForm.prototype.goBack = function () {
        var alert;

        if (window.history.length < 2) {
            alert = new n.Alert(
                {
                    type: "info",
                    message: "Please close the tab/window."
                }
            );

            alert.show();
            return;
        }

        window.history.back();
    };

})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global console */
/* global Bloodhound */
/* global Math */

/* @depend Utils.js */

var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    n.TypeAhead = {};

    /**
     * Coerce corejs-typeahead into our use-case.
     *
     * @param typeahead_endpoint the endpoint to be called
     * @constructor
     */
    n.TypeAhead.install = function (typeahead_endpoint) {

        $('.qfq-typeahead').each(function () {
            var $shadowElement, bloodhoundConfiguration;

            var $element = $(this);
            bloodhoundConfiguration = {
                // We need to be notified on success, so we need a promise
                initialize: false,
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('key', 'value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                identify: function (obj) {
                    return obj.key;
                },
                remote: {
                    url: n.TypeAhead.makeUrl(typeahead_endpoint, $element),
                    wildcard: '%QUERY'
                }
            };
            if ($element.val() !== '') {
                // We prefetch the value provided
                bloodhoundConfiguration.prefetch = {};
                bloodhoundConfiguration.prefetch.url = n.TypeAhead.makePrefetchUrl(typeahead_endpoint, $element.val(), $element);
                // Disable cache, we expect only a few entries. Caching gives sometimes strange behavior.
                bloodhoundConfiguration.prefetch.cache = false;
            }

            $shadowElement = n.TypeAhead.makeShadowElement($element);

            var suggestions = new Bloodhound(bloodhoundConfiguration);
            var promise = suggestions.initialize();
            promise.done((function ($element, suggestions) {
                return function () {
                    n.TypeAhead.fillTypeAheadFromShadowElement($element, suggestions);
                };
            })($element, suggestions));


            $element.typeahead({
                    hint: n.TypeAhead.getHint($element),
                    highlight: n.TypeAhead.getHighlight($element),
                    minLength: n.TypeAhead.getMinLength($element)
                },
                {
                    display: 'value',
                    source: suggestions,
                    limit: n.TypeAhead.getLimit($element),
                    templates: {
                        suggestion: function (obj) {
                            return "<div>" + n.TypeAhead.htmlEncode(obj.value) + "</div>";
                        },
                        // No message if field is not set to pedantic.
                        notFound: (function ($_) {
                            return function (obj) {
                                if (!!$_.data('typeahead-pedantic'))
                                    return "<div>'" + n.TypeAhead.htmlEncode(obj.query) + "' not found";
                            };
                        })($element)
                    }
                });


            $element.bind('typeahead:select typeahead:autocomplete', function (event, suggestion) {
                var $shadowElement = n.TypeAhead.getShadowElement($(event.delegateTarget));
                $shadowElement.val(suggestion.key);
            });

            if (!!$element.data('typeahead-pedantic')) {
                $element.bind('typeahead:change', n.TypeAhead.makePedanticHandler(suggestions));
                $element.on('keydown', (function (suggestions) {
                    return function (event) {
                        if (event.which === 13) {
                            n.TypeAhead.makePedanticHandler(suggestions)(event);
                        }
                    };
                })(suggestions));
                // The pedantic handler will test if the shadow element has a value set (the KEY). If not, the
                // typeahead element is cleared. Thus we have to guarantee that no value exists in the shadow
                // element the instant the user starts typing since we don't know the outcome of the search.
                //
                // If we don't clear the shadow element the instant the user starts typing, and simply let the
                // `typeahead:select` or `typeahead:autocomplete` handler set the selected value, the
                // user might do following steps and end up in an inconsistent state:
                //
                //  1. Use typeahead to select/autocomplete a suggestion
                //  2. delete the suggestion
                //  3. enter a random string
                //  4. submit form
                //
                // This would leave a stale value in the shadow element (from step 1.), and the pedantic handler
                // would not clear the typeahead element, giving the impression the value in the typeahead element will be submitted.
                $element.on('input', (function ($shadowElement) {
                    return function () {
                        $shadowElement.val('');
                    };
                })($shadowElement));
            } else {
                $element.bind('typeahead:change', function (event, suggestion) {
                    var $shadowElement = n.TypeAhead.getShadowElement($(event.delegateTarget));
                    // If none pendatic, suggestion key might not exist, so use suggestion instead.
                    $shadowElement.val(suggestion.key || suggestion);
                });
            }
        });

    };

    n.TypeAhead.makePedanticHandler = function (bloodhound) {
        return function (event) {
            var $typeAhead = $(event.delegateTarget);
            var $shadowElement = n.TypeAhead.getShadowElement($typeAhead);
            if ($shadowElement.val() === '') {
                $typeAhead.typeahead('val', '');
                $typeAhead.closest('form').change();
            }
        };
    };

    n.TypeAhead.makeUrl = function (endpoint, element) {
        return endpoint + "?query=%QUERY" + "&sip=" + n.TypeAhead.getSip(element);
    };
    n.TypeAhead.makePrefetchUrl = function (endpoint, prefetchKey, element) {
        return endpoint + "?prefetch=" + encodeURIComponent(prefetchKey) + "&sip=" + n.TypeAhead.getSip(element);
    };

    n.TypeAhead.getLimit = function ($element) {
        return $element.data('typeahead-limit');
    };

    n.TypeAhead.getSip = function ($element) {
        return $element.data('typeahead-sip');
    };

    n.TypeAhead.getName = function ($element) {
        return $element.attr('name');
    };

    n.TypeAhead.getValue = function ($element) {
        return $element.val();
    };

    n.TypeAhead.getMinLength = function ($element) {
        return $element.data('typeahead-minlength') || 2;
    };

    n.TypeAhead.getHighlight = function ($element) {
        return $element.data('typeahead-highlight') || true;
    };

    n.TypeAhead.getHint = function ($element) {
        return $element.data('typeahead-hint') || true;
    };

    n.TypeAhead.htmlEncode = function (value) {
        return $('<div/>').text(value).html();
    };

    n.TypeAhead.makeShadowElement = function ($element) {
        var $parent, inputName, inputValue, uniqueId, $shadowElement;

        $parent = $element.parent();
        inputName = $element.attr('name');
        $element.removeAttr('name');

        inputValue = $element.val();

        $shadowElement = $('<input>')
            .attr('type', 'hidden')
            .attr('name', inputName)
            .val(inputValue);

        $element.data('shadow-element', $shadowElement);

        $parent.append($shadowElement);

        return $shadowElement;
    };

    n.TypeAhead.getShadowElement = function ($element) {
        return $element.data('shadow-element');
    };

    n.TypeAhead.fillTypeAheadFromShadowElement = function ($element, bloodhound) {
        var results;
        var $shadowElement = n.TypeAhead.getShadowElement($element);
        var key = $shadowElement.val();
        if (key === '') {
            return;
        }

        results = bloodhound.get(key);
        if (results.length === 0) {
            return;
        }
        $element.typeahead('val', results[0].value);
    };
})(QfqNS);
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend AlertManager.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Display a message.
     *
     * A typical call sequence might look like:
     *
     *     var alert = new QfqNS.Alert({
     *       message: "Text being displayed",
     *       type: "info"
     *     });
     *     alert.show();
     *
     * Messages may have different background colors (severity levels), controlled by the `type` property. Possible
     * values are
     *
     *  * `"success"`
     *  * `"info"`
     *  * `"warning"`
     *  * `"error"`, `"danger"`
     *
     * The values are translated into Bootstrap `alert-*` classes internally.
     *
     * If no buttons are configured, a click anywhere on the alert will close it.
     *
     * Buttons are configured by passing an array of objects in the `buttons` property. The properties of the object
     * are as follows
     *
     *     {
     *       label: <button label>,
     *       focus: true | false,
     *       eventName: <eventname>
     *     }
     *
     * You can connect to the button events by using
     *
     *     var alert = new QfqNS.Alert({
     *        message: "Text being displayed",
     *        type: "info",
     *        buttons: [
     *             { label: "OK", eventName: "ok" }
     *        ]
     *     });
     *     alert.on('alert.ok', function(...) { ... });
     *
     * Events are named according to `alert.<eventname>`.
     *
     * If the property `modal` is set to `true`, a kind-of modal alert will be displayed, preventing clicks
     * anywhere but the alert.
     *
     * For compatibility reasons, the old constructor signature is still supported but deprecated
     *
     *      var alert = new QfqNS.Alert(message, type, buttons)
     *
     * @param {object} options option object has following properties
     * @param {string} options.message message to be displayed
     * @param {string} [options.type] type of message, can be `"info"`, `"warning"`, or `"error"`. Default is `"info"`.
     * @param {number} [options.timeout] timeout in milliseconds. If timeout is less than or equal to 0, the alert
     * won't timeout and stay open until dismissed by the user. Default `n.Alert.constants.NO_TIMEOUT`.
     * @param {boolean} [options.modal] whether or not alert is modal, i.e. prevent clicks anywhere but the dialog.
     * Default is `false`.
     * @param {object[]} options.buttons what buttons to display on alert. If empty array is provided, no buttons are
     * displayed and a click anywhere in the alert will dismiss it.
     * @param {string} options.buttons.label label of the button
     * @param {string} options.buttons.eventName name of the event when button is clicked.
     * @param {boolean} [options.buttons.focus] whether or not button has focus by default. Default is `false`.
     *
     * @constructor
     */
    n.Alert = function (options) {
        // Emulate old behavior of method signature
        //  function(message, messageType, buttons)
        if (typeof options === "string") {
            this.message = arguments[0];
            this.messageType = arguments[1] || "info";
            this.buttons = arguments[2] || [];
            this.modal = false;
            // this.timeout < 1 means forever
            this.timeout = n.Alert.constants.NO_TIMEOUT;
        } else {
            // new style
            this.message = options.message || "MESSAGE";
            this.messageType = options.type || "info";
            this.messageTitle = options.title || false;
            this.errorCode = options.code || false;
            this.buttons = options.buttons || [];
            this.modal = options.modal || false;
            this.timeout = options.timeout || n.Alert.constants.NO_TIMEOUT;
        }

        this.$alertDiv = null;
        this.$modalDiv = null;
        this.shown = false;

        this.fadeInDuration = 400;
        this.fadeOutDuration = 400;
        this.timerId = null;
        this.parent = {};
        this.identifier = false;

        this.eventEmitter = new EventEmitter();
    };

    n.Alert.prototype.on = n.EventEmitter.onMixin;
    n.Alert.constants = {
        alertContainerId: "alert-interactive",
        alertContainerSelector: "#qfqAlertContainer",
        jQueryAlertRemoveEventName: "qfqalert.remove:",
        NO_TIMEOUT: 0
    };

    /**
     *
     * @private
     */
    n.Alert.prototype.makeAlertContainerSingleton = function () {
        if (!n.QfqPage.alertManager) {
            n.QfqPage.alertManager = new n.AlertManager({});
        }

        return n.QfqPage.alertManager;
    };

    n.Alert.prototype.setIdentifier = function (i) {
        this.identifier = i;
    };

    /**
     *
     * @returns {number|jQuery}
     * @private
     */
    n.Alert.prototype.countAlertsInAlertContainer = function () {
        return $(n.Alert.constants.alertContainerSelector + " > div").length;
    };

    /**
     * @private
     */
    n.Alert.prototype.removeAlertContainer = function () {
        if (this.modal) {
            this.shown = false;
            this.parent.removeModalAlert();
        }
    };

    /**
     * @private
     */
    n.Alert.prototype.getAlertClassBasedOnMessageType = function () {
        switch (this.messageType) {
            case "warning":
                return "border-warning";
            case "danger":
            case "error":
                return "border-error";
            case "info":
                return "border-info";
            case "success":
                return "border-success";
            /* jshint -W086 */
            default:
                n.Log.warning("Message type '" + this.messageType + "' unknown. Use default type.");
            /* jshint +W086 */
        }
    };

    /**
     * @private
     */
    n.Alert.prototype.getButtons = function () {
        var $buttons = null;
        var $container = $("<p>").addClass("buttons");
        var numberOfButtons = this.buttons.length;
        var index;
        var buttonConfiguration;

        for (index = 0; index < numberOfButtons; index++) {
            buttonConfiguration = this.buttons[index];

            if (!$buttons) {
                if (numberOfButtons > 1) {
                    $buttons = $("<div>").addClass("btn-group");
                } else {
                    $buttons = $container;
                }
            }

            var focus = buttonConfiguration.focus ? buttonConfiguration.focus : false;

            $buttons.append($("<button>").append(buttonConfiguration.label)
                .attr('type', 'button')
                .addClass("btn btn-default" + (focus ? " wants-focus" : ""))
                .click(buttonConfiguration, this.buttonHandler.bind(this)));
        }

        if (numberOfButtons > 1) {
            $container.append($buttons);
            $buttons = $container;
        }

        return $buttons;
    };

    /**
     * @public
     */
    n.Alert.prototype.show = function () {
        $(".removeMe").remove();
        var alertContainer;
        if (this.shown) {
            // We only allow showing once
            return;
        }

        this.parent = this.makeAlertContainerSingleton();
        this.parent.addAlert(this);

        if (this.modal) {
            this.$modalDiv = $("<div>", {
                class: "removeMe"
            });
            this.parent.createBlockScreen(this);
        }

        if (this.messageTitle) {
            this.$titleWrap = $("<p>")
                .addClass("title")
                .append(this.messageTitle);
        }

        this.$messageWrap = $("<p>")
            .addClass("body")
            .append(this.message);

        this.$alertDiv = $("<div>")
            .hide()
            .addClass(this.getAlertClassBasedOnMessageType())
            .attr("role", "alert")
            .append(this.$messageWrap);

        if (this.$titleWrap) {
            this.$alertDiv.prepend(this.$titleWrap);
        }

        if (this.modal) {
            this.$alertDiv.addClass("alert-interactive");
            this.$alertDiv.css('z-index', 1000);
        } else {
            this.$alertDiv.addClass("alert-side");
        }
        this.$alertDiv.addClass("removeMe");

        var buttons = this.getButtons();
        if (buttons) {
            // Buttons will take care of removing the message
            this.$alertDiv.append(buttons);
        } else {
            // Click on the message anywhere will remove the message
            this.$alertDiv.click(this.removeAlert.bind(this));
            // Allows to remove all alerts that do not require user interaction, programmatically. Yes, we could send
            // the "click" event, but we want to communicate our intention clearly.
            this.$alertDiv.on(n.Alert.constants.jQueryAlertRemoveEventName, this.removeAlert.bind(this));
        }

        this.parent.$container.append(this.$alertDiv);


        //this.$alertDiv.slideDown(this.fadeInDuration, this.afterFadeIn.bind(this));
        if (!this.modal) {
            this.$alertDiv.animate({width:'show'}, this.fadeInDuration, this.afterFadeIn.bind(this));
        } else {
            this.$alertDiv.fadeIn(this.fadeInDuration);
        }

        this.$alertDiv.find(".wants-focus").focus();
        this.shown = true;
    };

    /**
     * @private
     */
    n.Alert.prototype.afterFadeIn = function () {
        if (this.timeout > 0) {
            this.timerId = window.setTimeout(this.removeAlert.bind(this), this.timeout);
        }
    };

    /**
     *
     *
     * @private
     */
    n.Alert.prototype.removeAlert = function () {

        // In case we have an armed timer (or expired timer, for that matter), disarm it.
        if (this.timerId) {
            window.clearTimeout(this.timerId);
            this.timerId = null;
        }

        var that = this;
        if (!this.modal) {
            this.$alertDiv.animate({width:'hide'}, this.fadeOutDuration, function () {
                that.$alertDiv.remove();
                that.$alertDiv = null;
                that.shown = false;
                that.parent.removeOutdatedAlerts();
            });
        } else {
            this.$alertDiv.fadeOut(this.fadeOutDuration, function(){
                that.$alertDiv.remove();
                that.$alertDiv = null;
                that.shown = false;
                that.removeAlertContainer();
            });
        }
        this.parent.removeAlert(this.identifier);
    };

    /**
     *
     * @param handler
     *
     * @private
     */
    n.Alert.prototype.buttonHandler = function (event) {
        this.removeAlert();
        this.eventEmitter.emitEvent('alert.' + event.data.eventName, n.EventEmitter.makePayload(this, null));
    };

    n.Alert.prototype.isShown = function () {
        return this.shown;
    };

})(QfqNS);
/* @author Benjamin Baer <benjamin.baer@math.uzh.ch> */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */


var QfqNS = QfqNS || {};

(function (n) {

    n.Clipboard = function (data) {
        this.text = '';
        this.events = [];
        if(data.text) {
            this.copyTextToClipboardAsync(data.text);
            return;
        }
        if(data.uri) {
            this.getDataFromUri(data.uri);
            return;
        }
        this.buildError("Called Clipboard without any Data to copy");
        console.error("Clipboard has to be called with an url or text to copy");
    };

    /**
     * @private
     * Has to be a bit hacky, since copy to clipboard only works with user confirmation from an API.
     * We fake this user confirmation by listening for the mouseup event after the button press.
     * As a fallback we also listen to a click event if the API took longer than the original click.
     * @param uri
     */
    n.Clipboard.prototype.getDataFromUri = function(uri) {
        var that = this;
        $.getJSON(uri, function(data) {
            if (data.text) {
                that.text = data.text;
                $(document).click(function() {that.copyTextToClipboardAsync(that.text); $(this).off();});
                $(document).mouseup(function() {that.copyTextToClipboardAsync(that.text); $(this).off();});
            } else {
                console.error("JSON Response didn't include a variable called 'text'");
                that.buildError("Didn't receive any Data to copy");
            }
        });
    };

    n.Clipboard.prototype.copyTextToClipboard = function(text) {
        var textArea = document.createElement("textarea");
        var focusedElement = $(":focus");
        textArea.value = text;
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Fallback: Copying text command was ' + msg);
        } catch (err) {
            this.buildError("Couldn't copy text to clipboard: " + err);
        }

        document.body.removeChild(textArea);
        focusedElement.focus();
    };

    /**
     * Tries to copy text to the clipboard using asynchronous browser API.
     * If it doesn't exist, calls copyTextToClipboard (synchronous) instead.
     * @private String text
     */
    n.Clipboard.prototype.copyTextToClipboardAsync = function(text) {
        if (!navigator.clipboard) {
            this.copyTextToClipboard(text);
            return;
        }

        var that = this;
        navigator.clipboard.writeText(text).then(function() {
            console.log('Async: Copying to clipboard was successful!');
        }, function(err) {
            that.buildError("Could not copy text: " + err);
        });
    };

    n.Clipboard.prototype.buildError = function(message) {
        var alert = new n.Alert(
            {
                type: "error",
                message: message,
                modal: true,
                buttons: [{label: "Ok", eventName: 'close'}]
            }
        );
        alert.show();
    };

})(QfqNS);

/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Displays Comment or an Editor to create a comment
     *
     * https://www.quora.com/What-is-the-best-way-to-check-if-a-property-or-variable-is-undefined
     *
     */
    n.Comment = function (comment, user, $container, options) {
        this.comment = comment;
        this.user = user;
        this.$parent = $container;
        this.$comment = {};
        this.$text = {};
        if (arguments.length === 3) {
            this.options = { readOnly: false };
        } else {
            this.options = options;
        }
        this.childrenController = {};
        this.eventEmitter = new EventEmitter();
        this.deleted = false;
    };

    n.Comment.prototype.on = n.EventEmitter.onMixin;

    n.Comment.prototype.display = function() {
        var displayElement;
        displayElement = this._buildComment();
        displayElement.appendTo(this.$parent);
        this.$comment = displayElement;
    };

    n.Comment.prototype.getParent = function() {
        return this.$parent;
    };

    n.Comment.prototype.height = function() {
        return this.$comment.height();
    };

    n.Comment.prototype._buildComment = function(allowEdit) {
        var $commentWrap = $('<div/>', {
            class: "qfqComment"
        });
        var $avatar = $('<img>', {
            src: this.user.avatar,
            class: "qfqCommentAvatar"
        });
        $avatar.appendTo($commentWrap);
        var $topLine = $('<div />', {
            class: "qfqCommentTopLine"
        });
        $('<span />', {
            class: "qfqCommentAuthor",
            text: this.user.name + ":"
        }).appendTo($topLine);
        $('<span />', {
            class: "qfqCommentDateTime",
            text: this.comment.dateTime
        }).appendTo($topLine);
        $topLine.appendTo($commentWrap);
        var $comment = $('<div />', {
            class: "qfqCommentText"
        });
        $comment.html(this.comment.comment);
        if (!this.options.readOnly) {
            $comment.append(this._getCommands());
        }
        this.$text= $comment;
        $comment.appendTo($commentWrap);
        return $commentWrap;
    };

    n.Comment.prototype._updateText = function(text) {
          this.$text.html(text);
          this.$text.append(this._getCommands());
    };

    n.Comment.prototype._getCommands = function () {
        var $commentCommands = $("<div />", {
            class: "qfqCommentCommands"
        });
        var that = this;
        $commentCommands.append(this._getCommand("Edit", "pencil", function(e) {
            that._editMe(e);
        }));
        $commentCommands.append(this._getCommand("Delete", "trash", function(e) {
            that._deleteMe(e);
        }));
        $commentCommands.append(this._getCommand("Reply", "comment", function(e) {
            that._replyToMe(e);
        }));
        return $commentCommands;
    };

    n.Comment.prototype._getCommand = function(description, icon, onClick) {
        var $command = $('<span />', {
            class: "glyphicon glyphicon-" + icon + " qfqCommentCommand",
            title: description
        });
        $command.bind("click", this, onClick);
        return $command;
    };

    n.Comment.prototype._deleteMe = function(e) {
        console.log("Delete pressed for:");
        console.log(this.comment);
        this.deleted = true;
        this.$comment.remove();
        this.eventEmitter.emitEvent('comment.deleted',
            n.EventEmitter.makePayload(this, this.comment));
    };

    n.Comment.prototype._editMe = function(e) {
        console.log("Edit pressed for:");
        console.log(this.comment);
        this.$comment.hide();
        var that = this;
        var editor = new QfqNS.Editor();
        var $editor = editor.buildEditor(this.comment.comment);
        editor.on("editor.submit", function(e) {
             that._updateComment(e);
        });
        this.$comment.after($editor);

    };

    n.Comment.prototype._replyToMe = function(e) {
        console.log("Reply to me:");
        console.log(this.comment);
        console.log(e);
        this.eventEmitter.emitEvent('comment.reply',
            n.EventEmitter.makePayload(this, this.comment));
    };

    n.Comment.prototype._updateComment = function(e) {
        console.log(e);
        this.comment.comment = e.data.text;
        this._updateText(e.data.text);
        this.$comment.show();
        e.data.$container.remove();
        this.eventEmitter.emitEvent('comment.edited',
            n.EventEmitter.makePayload(this, this.comment));
    };

})(QfqNS);
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */
/* @depend Comment.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Manages a group of comments
     *
     */
    n.CommentController = function () {
        this.comments = [];
        this.currentUser = {};
        this.$container = {};
        this.$parent = {};
        this.height = "auto";
        this.options = {};
        // Event Emitter is a Library qfq uses to emit custom Events.
        this.eventEmitter = new EventEmitter();
    };

    n.CommentController.prototype.on = n.EventEmitter.onMixin;

    n.CommentController.prototype.setCurrentUser = function(user) {
        this.currentUser = user;
    };

    /**
     * changeHandler emits custom events for actions.
     * Additionally writes log entries to console for easier
     * testing.
     * @private
     * @param event String containing possible change states
     * @return {boolean} true on success
     */
    n.CommentController.prototype._changeHandler = function(event, comment) {
        if (event === "edit") {
            this.eventEmitter.emitEvent('comment.edited',
                n.EventEmitter.makePayload(this, "edit"));
            console.log("[CommentController] Event comment.edit emitted");
            return true;
        } else if (event === "new") {
            this.eventEmitter.emitEvent('comment.added',
                n.EventEmitter.makePayload(this, comment));
            console.log("[CommentController] Event comment.add emitted");
            return true;
        } else if (event === "remove") {
            this.eventEmitter.emitEvent('comment.removed',
                n.EventEmitter.makePayload(this, comment));
        }
        console.error("[CommentController] Changehandler called without valid event");
        return false;
    };

    n.CommentController.prototype.emitEvent = function(event) {
        this._changeHandler(event);
    };

    n.CommentController.prototype.buildContainer = function($hook, options) {
        var $container = $("<div />", {
            class: "qfqCommentContainer"
        });
        this.options = options;
        $hook.after($container);
        this.$container = $container;
    };

    n.CommentController.prototype.hasComments = function() {
        if (this.comments.length > 0) {
            return true;
        } else {
            return false;
        }
    };

    n.CommentController.prototype.toggle = function() {
        this.$container.slideToggle("swing");
    };

    n.CommentController.prototype.getComment = function(reference) {
        if (reference < this.comments.length && reference >= 0) {
            return this.comments[reference];
        } else {
            console.error("[CommentController] Requested Comment doesn't exist");
            return false;
        }
    };

    n.CommentController.prototype.addComment = function(comment, user) {
        var commentObject = new n.Comment(comment, user, this.$container, this.options);
        commentObject.display();
        this.comments.push(commentObject);
        this._changeHandler("new", commentObject);
        this._setListeners(commentObject);
        this.updateHeight();
        return this.comments.length - 1;
    };

    n.CommentController.prototype._setListeners = function(commentObject) {
        var that = this;
        commentObject.on('comment.edited', function(e) {
            that.updateComment(e.data);
        });
        commentObject.on('comment.reply', function(e) {
            that.requestReply(e.data);
        });
        commentObject.on('comment.deleted', function(e) {
            that.removeComment(e);
        });
    };

    n.CommentController.prototype.displayComments = function() {
        for (var i = 0; this.comments; i++) {
            this.comments[i].display();
        }
        this.updateHeight();
    };

    n.CommentController.prototype.displayEditor = function() {
        if (!this.options.readOnly) {
            var editor = new n.Editor();
            var that = this;
            var $editor = editor.buildEditor();
            editor.on("editor.submit", function (editor) {
                that._handleEditorSubmit(editor);
            });
            $editor.appendTo(this.$container);
            editor.$textArea.focus();
        }
    };

    n.CommentController.prototype._handleEditorSubmit = function(editor) {
        var comment = this.buildCommentObject(editor.data.text);
        this.addComment(comment, this.currentUser);
        editor.data.destroy();
    };

    n.CommentController.prototype.buildCommentObject = function(text) {
        var comment = {};
        comment.comment = text;
        comment.dateTime = new Date().toLocaleString('de-CH');
        comment.uid = this.currentUser.uid;
        return comment;
    };

    n.CommentController.prototype.getContainer = function() {
        return this.$container;
    };

    n.CommentController.prototype.removeComment = function(reference) {
        this._changeHandler("remove", reference);
    };

    n.CommentController.prototype.updateComment = function(data) {
        console.log("[Comment Changed] User: " + data.uid +
            " Text:" + data.comment.substring(0, 20) + "...");
        this.emitEvent("edit");
    };

    n.CommentController.prototype.requestReply = function(data) {
        this.displayEditor();
    };

    n.CommentController.prototype.updateHeight = function() {
        //this.height = this.$container.height();
        //this.$container.css("max-height", this.height);
    };

    n.CommentController.prototype.importComments = function(comments, users) {
        for (var i=0; i < comments.length; i++) {
            var user = this._searchUsersByUid(users, comments[i].uid);
            this.addComment(comments[i], user);
        }
        if (comments.length === 0) {
            this.displayEditor();
        }
    };

    n.CommentController.prototype.exportComments = function() {
        var comments = [];
        for(var i=0; i < this.comments.length; i++) {
            if (!this.comments[i].deleted) {
                comments.push(this.comments[i].comment);
            }
        }
        return comments;
    };

    n.CommentController.prototype._searchUsersByUid = function (users, uid) {
        for (var i=0; i < users.length; i++) {
            if (users[i].uid === uid) {
                return users[i];
            }
        }
    };

})(QfqNS);
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Display content of a file
     *
     *
     * @param {object} options option object has following properties
     * @param {string} options.webworker Path to webworker JS file
     * @param {string} options.filePath file that has to be displayed
     * @param {number} [options.interval] time interval in milliseconds.
     * @param {string} [options.highlight] Path to highlight instructions, optional
     * @param {string} [options.targetId] HTML id of the target where it should be displayed
     * @param {boolean} [options.isContinuous] Appends the text instead of replacing it
     *
     * @constructor
     */
    n.DisplayFile = function (options) {
        this.options = options;
        this.options.webworker = options.webworker ||
            "typo3conf/ext/qfq/Resources/Public/Javascript/Worker/GetFileContent.js";
    };

    n.DisplayFile.prototype.show = function() {
        var that = this;
        if (this.options.filePath) {
            if (this.options.interval) {
                var webWorker = new Worker(this.options.webworker);
                webWorker.postMessage([this.options.filePath, this.options.interval]);

                webWorker.onmessage = function(e) {
                      that._handleResponse(e);
                };
            }
        } else {
            console.alert("No filePath supplied");
          }
    };

    n.DisplayFile.prototype._handleResponse = function(e) {
        var result = e.data;
        if (this.options.targetId) {
            var $target = $("#" + this.options.targetId);
            if (this.options.isContinuous) {
                $target.append(result);
            } else {
                $target.text(result);
            }
        }
    };

})(QfqNS);
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */
/* @depend ElementUpdate.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     *
     */
    n.DragAndDrop = function ($hook) {
        this.$container = $hook;
        this.eventEmitter = new EventEmitter();
        this.dropZones = [];
        this.elements = [];
        this.active = false;
        this.api = $hook.data("dnd-api");
        this.draggedId = "";
        this.lastChild = "";
        this.$tempObject = {};
    };

    n.DragAndDrop.prototype.on = n.EventEmitter.onMixin;

    n.DragAndDrop.prototype.buildDropArea = function(position, relatedId, otherPos) {
        var that = this;
        var $dropArea = {};

        if (this.$container.data("columns")) {
            $dropArea = $("<tr />", {
                class: "qfqDropTarget"
            });
            var $fluff = $("<td />", {
                class: "qfqDropTarget",
                colspan: this.$container.data("columns")
            });
            $fluff.appendTo($dropArea);
        } else {
            $dropArea = $("<div />", {
                class: "qfqDropTarget"
            });
        }

        $dropArea.data("position", position);
        $dropArea.data("related", relatedId);
        $dropArea.data("other-pos", otherPos);
        $dropArea.on("dragenter", function(e) {
            e.preventDefault();
            $dropArea.addClass("qfqTargetDisplay");
        });
        $dropArea.on("dragover", function(e) {
            e.preventDefault();
            e.originalEvent.dataTransfer.dropEffect = "move";
        });
        $dropArea.on("drop", function(e) {
            e.originalEvent.preventDefault();
            that._moveObjectBefore(e, $dropArea);
        });

        return $dropArea;
    };

    n.DragAndDrop.prototype.setDropZones = function($objects) {
        var that = this;

        $objects.each(function() {
            var $dropZone = $(this);
            $dropZone.on("dragenter", function(e) {
                e.preventDefault();
                $dropZone.addClass("qfqTargetDisplay");
                e.Effect = "all";
                that._handleDragEnter(e);
            });
            $dropZone.on("dragleave", function(e) {
                e.preventDefault();
                $dropZone.removeClass("qfqTargetDisplay");
                that._handleDragLeave(e);
            });
            $dropZone.on("dragover", function(e) {
                e.preventDefault();
                e.stopPropagation();
            });
            $dropZone.on("drop", function(e) {
                e.preventDefault();
                e.stopPropagation();
                that._dropHandler(e);
            });
            $dropZone.css("z-index", 5);
            that.dropZones.push($dropZone);
        });
    };

    n.DragAndDrop.prototype.setElements = function($objects) {
        var that = this;

        $objects.each(function() {
            var $element = $(this);
            $element.prop("draggable", true);
            $element.on("dragstart", function(e) {
                that.draggedId = $element[0].id;
            });
            that.elements.push($element);
        });
    };

    n.DragAndDrop.prototype._handleDragEnter = function(event) {
        var $tempObject = $("#" + this.draggedId).clone();
        $tempObject.css("opacity", 0.5);
        $tempObject.css("z-index", 0);
        $tempObject.off();
        if (this.$tempObject[0]) {
            if ($tempObject[0].id !== this.$tempObject[0].id) {
                this.$tempObject = $tempObject;
                this.$tempObject.appendTo($("#" + event.currentTarget.id));
            }
        } else {
            this.$tempObject = $tempObject;
            this.$tempObject.appendTo($("#" + event.currentTarget.id));
        }
    };

    n.DragAndDrop.prototype._handleDragLeave = function(event) {
        if(this.$tempObject[0]) {
            this.$tempObject.remove();
            this.$tempObject = {};
        }
    };

    n.DragAndDrop.prototype.makeBasketCase = function() {
        var dzSelector = this.$container.data("dnd-dropzone") || false;
        var elSelector = this.$container.data("dnd-element") || false;

        if (elSelector) {
            this.setElements($("." + elSelector));
        }
        if (dzSelector) {
            this.setDropZones($("." + dzSelector));
        }
    };

    n.DragAndDrop.prototype._dropHandler = function(event) {
        if(this.$tempObject[0]) {
            this.$tempObject.remove();
            this.$tempObject = {};
        }
        $("#" + this.draggedId).appendTo($("#" + event.currentTarget.id));
    };

    n.DragAndDrop.prototype._buildOrderDropZones = function($object, e) {
        this.removeDropAreas();

        //if ($object[0].id !== this.draggedId) {
            //if ($object.data("dnd-position") !== $("#" + this.draggedId).data("dnd-position") + 1) {
                var $dropArea = this.buildDropArea("before", $object.data("dnd-id"), $object.data("dnd-position"));
                //$dropArea.hide();
                $object.before($dropArea);
                //$dropArea.slideDown(500, 'swing');

                var $lastDrop = this.buildDropArea("after", $object.data("dnd-id"), $object.data("dnd-position"));
                //$lastDrop.hide();
                $object.after($lastDrop);
                //$lastDrop.slideDown(500, 'swing');
                //$lastDrop.appendTo(this.$container);
            //}

            /*if ($object[0].id === this.lastChild) {
                var $lastDrop = this.buildDropArea("after", $object.data("dnd-id"), $object.data("dnd-position"));
                $lastDrop.appendTo(this.$container);
            }*/
        //}
    };

    n.DragAndDrop.prototype.removeDropAreas = function() {
        if (this.$container.data("column")) {
            this.$container.children(".qfqTempTable").remove();
        }

        this.$container.children(".qfqDropTarget").remove();
    };

    n.DragAndDrop.prototype.removeDropTarget = function(e) {
        console.log(e);
    };

    n.DragAndDrop.prototype.makeSortable = function() {
        var that = this;
        var numberOfChildren = this.$container.children().length;
        var count = 0;

        this.$container.children().each( function() {
            count++;

            var child = $(this);
            if (numberOfChildren === count) {
                that.lastChild = child[0].id;
            }
            child.data("dnd-position", count);
            child.prop("draggable", true);
            child.on("dragstart", function(e) {
                e.originalEvent.dataTransfer.setData("text", child[0].id);
                that.draggedId = child[0].id;
                that.active = true;
                e.originalEvent.dataTransfer.effectAllowed = "move";
            });
            child.on("dragenter", function(e) {
                if (that.active) {
                    that._buildOrderDropZones($(this), e);
                }
            });
            child.on("dragend", function() {
                that.active = false;
                that.removeDropAreas();
            });
        });
    };

    n.DragAndDrop.prototype._moveObjectBefore = function(e, $hook) {
        var id = e.originalEvent.dataTransfer.getData("text");
        var $object = $("#" + id);
        var posTo = $hook.data("position");

        if (posTo === "after") {
            this.lastChild = $object[0].id;
        }

        $hook.before(document.getElementById(id));
        this._buildOrderUpdate($object, $hook.data("position"), $hook.data("related"), $hook.data("other-pos"));
        this.removeDropAreas();
    };

    n.DragAndDrop.prototype._buildOrderUpdate = function($object, position, otherId, otherPos) {
        var jObject = {};
        jObject.dragId = $object.data("dnd-id");
        jObject.dragPosition = $object.data("dnd-position");
        jObject.setTo = position;
        jObject.hoverId = otherId;
        jObject.hoverPosition = otherPos;
        this._sendToAPI(jObject);
    };

    n.DragAndDrop.prototype._sendToAPI = function(object) {
        var that = this;
        $.getJSON(this.api, object, function(data) {
            that._successHandler(data);
        });
    };

    n.DragAndDrop.prototype._successHandler = function(data) {
        if (data.status === "error") {
            var alert = new n.Alert({
                type: data.status,
                message: data.message,
                modal: true,
                buttons: [{
                    label: "Ok", eventName: "ok"
                }]
            });
            alert.show();
            console.error(data.message);
        } else {
            console.log("status:" + data.status + " message: " + data.message);
            if (data['element-update']) {
                var configuration = data['element-update'];
                n.ElementUpdate.updateAll(configuration);
            }
        }
    };

})(QfqNS);
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */
/* @depend Element/ElementBuilder.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {

    n.Droplet = function (url, color) {
        this._$trigger = {};
        this.position = {};
        this._$container = {};
        this.visible = false;
        this.eventEmitter = new EventEmitter();
        this.content = {};
        this.forms = [];
        this.url = url;
        this.color = color;
    };

    n.Droplet.prototype.on = n.EventEmitter.onMixin;

    n.Droplet.prototype.setTrigger = function($trigger) {
        this._$trigger = $trigger;
        var that = this;
        this._$trigger.click(function() {that.toggleDroplet();});
    };

    n.Droplet.prototype.setContainer = function($container) {
        this._$container = $container;
    };

    n.Droplet.prototype.setPosition = function(left, top) {
        this.position = {
            left: left,
            top: top
        };
    };

    n.Droplet.prototype.getContent = function() {
        var that = this;
        var response = jQuery.getJSON(this.url, function(data) {
            that._processResponse(data);
        });
        this._$container.text("Getting Data...");
    };

    n.Droplet.prototype._processResponse = function(data) {
        this._$container.text('');
        for(var i=0; i < data.elements.length; i++) {
            var element = data.elements[i];
            var $element = new n.ElementBuilder(element);
            this._$container.append($element.display());
            var that = this;
            if (element.type === "form") this.forms.push($element);
        }

        this.forms[0].on('form.submit.success',
            function() { that.toggleDroplet(); });
    };

    n.Droplet.prototype.createContainerBellowTrigger = function () {
        this.setPosition(
            this._$trigger.offset().left,
            this._$trigger.offset().top + this._$trigger.outerHeight()
        );
        var $container = $("<div />", {
            class: "qfq-droplet-container qfq-droplet-" + this.color
        });
        $container.css({
            position: 'absolute',
            zIndex: '100',
            top: this.position.top + 10 + "px",
            left: this.position.left + "px"
        });

        $(document.body).append($container);
        $container.addClass("qfq-droplet-container");
        $container.hide();
        return $container;
    };

    n.Droplet.prototype.getContainer = function() {
        if (this._$container) {
            console.error("No container has been created");
        } else {
            return this._$container;
        }
    };

    n.Droplet.prototype.toggleDroplet = function () {
        if (this.visible) {
            this._$container.hide();
            this.visible = false;
            this.forms = [];
        } else {
            this.eventEmitter.emitEvent('droplet.toggle',
                n.EventEmitter.makePayload(this, "toggle"));
            this._$container.show();
            this.visible = true;
            if (this.url) {
                this.getContent();
            }
        }
    };

    n.Droplet.prototype.hideDroplet = function() {
        if (this.visible) {
            this._$container.hide();
            this.visible = false;
        }
    };

})(QfqNS);

/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */
/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */
/* @depend Droplet.js */
/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
(function (n) {
    'use strict';

    n.DropletController = function() {
        this.droplets = [];
        this.eventEmitter = new EventEmitter();
    };

    n.DropletController.prototype.setUpDroplets = function() {
        var that = this;

        $(".qfq-droplet").each(function() {
            var url = false;
            var color = "grey";
            if ($(this).data("content")) {
                url = $(this).data("content");
            }
            if ($(this).data("color")) {
                color = $(this).data("color");
            }
            var droplet = new QfqNS.Droplet(url, color);
            droplet.setTrigger($(this));
            droplet.setContainer(droplet.createContainerBellowTrigger());

            that.droplets.push(droplet);
            droplet.on('droplet.toggle', function() { that.hideDroplets(); });
        });
    };

    n.DropletController.prototype.getDroplet = function(reference) {
        if (reference < this.droplets.length && reference >= 0) {
            return this.droplets[reference];
        }
    };

    n.DropletController.prototype.hideDroplets = function() {
        for(var i=0; i < this.droplets.length; i++) {
            this.droplets[i].hideDroplet();
        }
    };

})(QfqNS);
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */
/* @depend Comment.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Manages Text Editor for Comments
     *
     */
    n.Editor = function () {
        this.$container = {};
        this.$textArea = {};
        this.$submitButton = {};
        this.text = "";
        // Event Emitter is a Library qfq uses to emit custom Events.
        this.eventEmitter = new EventEmitter();
    };

    n.Editor.prototype.on = n.EventEmitter.onMixin;


    n.Editor.prototype.buildEditor = function(text) {
        var that = this;
        this.$container = $("<div />", {
            class: "qfqEditorContainer"
        });
        this.$textArea = $("<div />", {
            class: "qfqEditor",
            contenteditable: true,
            tabindex: 0
        });
        if (text) {
            this.$textArea.html(text);
        }
        this.$textArea.keydown(function() { that.activateSubmit(); });
        this._addEditorControls();
        var controls = $("<div />", {
            class: "qfqEditorControls"
        });
        var submitButton = $("<button />", {
            class: "btn btn-primary",
            disabled: true,
            text: "Send"
        });
        submitButton.on("click", function() { that._handleClick();});
        submitButton.appendTo(controls);
        this.$submitButton = submitButton;

        this.$textArea.appendTo(this.$container);
        controls.appendTo(this.$container);
        return this.$container;
    };

    n.Editor.prototype.activateSubmit = function() {
        this.$submitButton.attr("disabled", false);
    };

    n.Editor.prototype.destroy = function() {
          this.$container.remove();
    };

    n.Editor.prototype._handleClick = function() {
        var that = this;
        var text = this.$textArea.text().replace(/\s/g, '');
        if (text === "") {
            var alert = new n.Alert({
                message: "Please input text before sending.",
                type: "warning",
                modal: true,
                buttons: [
                    {label: "Ok", eventName: "ok"}
                ]
            });
            alert.show();
        } else {
            this.text = this.$textArea.html();
            this.eventEmitter.emitEvent('editor.submit',
                n.EventEmitter.makePayload(this, that));
        }
    };

    n.Editor.prototype._playingWithSelection = function() {
        var selection = window.getSelection();
        console.log(selection);
        if (!selection.isCollapsed) {
            var currentNode = selection.anchorNode;
            var count = 1;
            if (selection.anchorNode.nextSibling !== null) {
                if (currentNode.nextSibling.isSameNode(selection.focusNode)) {
                    console.log("Selected 2 nodes");
                } else {
                    while (!currentNode.isSameNode(selection.focusNode)) {
                        count++;
                        if (currentNode.nextSibling !== null) {
                            currentNode = currentNode.nextSibling;
                        } else {
                            console.error("whoops");
                            break;
                        }
                    }
                    console.log("Selected " + count + " nodes");
                }
            } else {
                if (selection.focusNode.nextSibling !== null) {
                    if (selection.focusNode.nextSibling.isSameNode(selection.anchorNode)) {
                        console.log("Selected 2 nodes");
                    } else {
                        currentNode = selection.focusNode;
                        while (!currentNode.isSameNode(selection.focusNode)) {
                            count++;
                            if (currentNode.previousSibling !== null) {
                                currentNode = currentNode.previousSibling;
                            } else {
                                console.error("whoops");
                                break;
                            }
                        }
                        console.log("Selected " + count + " nodes");
                    }
                } else {
                    console.log(selection.toString());
                    selection.deleteFromDocument();
                }
            }
        } else {
            console.log("Selected one node");
        }
    };

    n.Editor.prototype._addEditorControls = function() {
        var that = this;
        var $addCode = $("<span />", {
            class: "glyphicon glyphicon-console qfqEditorControl qfqCodeAdd"
        });
        $addCode.on("click", function() { that._addCodeElement();});
        $addCode.appendTo(this.$container);
        var $addList = $("<span />", {
            class: "glyphicon glyphicon-list qfqEditorControl qfqCodeList"
        });
        $addList.on("click", function() { that._addListElement();});
        $addList.appendTo(this.$container);
        this._addStandardTextElement();
    };

    n.Editor.prototype._addCodeElement = function() {
        var $code = $("<code />", {
            class: "qfqCodeElement",
            text: "Write your code here"
        });
        $code.appendTo(this.$textArea);
        this._addStandardTextElement();
    };

    n.Editor.prototype._addListElement = function() {
        var $unsortedList = $("<ul />");
        var selection = window.getSelection();
        console.log(selection);
        var $listElement = $("<li />", {
            text: "Write your list"
        });
        $listElement.appendTo($unsortedList);
        $unsortedList.appendTo(this.$textArea);
        this._addStandardTextElement();
    };

    n.Editor.prototype._addStandardTextElement = function() {
        var $regular = $("<p />", {
                text: "\xA0"
            }
        );
        $regular.appendTo(this.$textArea);
    };

}(QfqNS));
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Manages Text Editor for Comments
     */
    n.LocalStorage = function (key) {
        this.storage = {};
        this.key = key;
        this.storage[this.key] = {};

        // Event Emitter is a Library qfq uses to emit custom Events.
        this.eventEmitter = new EventEmitter();
        this._read();
        if (this.storage[this.key] !== "undefined") {
            this.storage[this.key] = {};
        }
    };

    n.LocalStorage.prototype.on = n.EventEmitter.onMixin;

    n.LocalStorage.prototype._read = function() {
        var o = JSON.parse(localStorage.getItem("qfq"));
        if(o) {
            this.storage = o;
        }
    };

    n.LocalStorage.prototype._write = function() {
        localStorage.setItem("qfq", JSON.stringify(this.storage));
        console.log(localStorage.getItem("qfq"));
    };

    n.LocalStorage.prototype.get = function(key) {
        if (this.storage[this.key][key] !== "undefined") {
            console.log(this.storage);
            return this.storage[this.key][key];
        } else {
            return false;
        }
    };

    n.LocalStorage.prototype.set = function(key, object) {
        if (this.storage[this.key][key] === "undefined") {
            this.storage[this.key][key] = {};
        }
        if(object) {
            this.storage[this.key][key] = object;
            this._write();
        }
    };

    n.LocalStorage.prototype.update = function() {
        this._read();
    };


}(QfqNS));
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */
/* @depend Comment.js */
/* @depend CommentController */
/* @depend SyntaxHighlighter */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Displays Code in a stylized fashion and allows
     * to write and display comments for each line of code.
     *
     * @param form Reference to the parent qfq Element
     * @param data Object containing the to be displayed data
     * @param $container Reference to the HTML Element that displays
     *                   the code correction
     * @param $target Reference to the HTML Element where the output (comment JSON)
     *                should be stored.
     */
    n.CodeCorrection = function () {
        this.page = {};
        this.data = {};
        this.eventEmitter = new EventEmitter();
        this.$parent = {};
        this.$target = {};
        this.$rows = [];
        this.annotations = [];
        this.users = [];
        this.currentUser = {};
        this.language = "";
        this.readOnly = false;
        this.syntaxHighlight = {};
    };

    /**
     * Initializes the Code Correction object, fetches data from URL or API if needed
     */
    n.CodeCorrection.prototype.initialize = function($container, page) {
        this.$parent = $container;
        this.$target = $("#" + $container.data("target"));
        this.data = {
            url: this.$parent.data("file"),
            text: this.$parent.data("text")

        };
        this.page = page;
        this.language = this.$parent.data("highlight") || "typo3conf/ext/qfq/Resources/Public/Json/javascript.json";
        this.readOnly = this.$parent.data("view-only") || false;
        this.currentUser = $container.data("uid");
        var that = this;
        if (this.readOnly) {
            if (this.$parent.data("annotations")) {
                var jsonAnnotations = this.$parent.data("annotations");
                this.annotations = jsonAnnotations.annotations;
                this.users = jsonAnnotations.users;
            } else {
                this._importFromTarget();
            }
        } else {
            this._importFromTarget();
        }

        if (this.data.url) {
            // Get data of a file and write it to data.text
            $.get(this.data.url, function(response) {
                that.data.text = response;
                that._prepareBuild();
            });
        } else if (this.data.text) {
            this._prepareBuild();
        } else {
            console.error("[CodeCorrection] No Code to correct passed to the object.");
        }
    };

    n.CodeCorrection.prototype._importFromTarget = function() {
        if (this.$target.val()) {
            var jImport = $.parseJSON(this.$target.val());
            if (jImport.annotations) {
                this.annotations = jImport.annotations;
                console.log("[CodeCorrection] Imported Annotations: " + this.annotations.length);
            }
            if (jImport.users) {
                this.users = jImport.users;
                console.log("[CodeCorrection] Imported Users: " + this.users.length);
            }
        }
    };

    n.CodeCorrection.prototype._prepareBuild = function() {
        var that = this;
        this.syntaxHighlight = new n.SyntaxHighlighter();
        this.syntaxHighlight.importInstructions(this.language, function() {
            that._buildEditor();
        });
    };

    /**
     * Breaks up the String by line and returns it as an Array
     * @param text Unix formatted text of the Code File
     * @returns {Array} Array with the Code broken up by Line
     */
    n.CodeCorrection.prototype.createLineByLineArray = function(text) {
        var textArray = [];
        if (typeof text === 'string' || text instanceof String) {
            textArray = text.split("\n");
        }
        return textArray;
    };

    /**
     * Builds the Code Correction HTML Element that should be displayed to the user.
     * @private
     */
    n.CodeCorrection.prototype._buildEditor = function() {
        var that = this;
        var $title = $('<div/>', {
            class: 'qfqCodeCorrectionTitle'
        });
        $title.appendTo(this.$parent);
        var container = $('<div/>', {
            class: 'codeCorrectionWrap'
        });
        var lineCount = 1;
        var textArray = this.createLineByLineArray(this.data.text);
        textArray.forEach(function(line) {
            that.$rows[lineCount] = that._buildLine(lineCount, line);
            that.$rows[lineCount].appendTo(container);
            lineCount++;
        });
        container.appendTo(this.$parent);
        this._buildAnnotations();

    };

    /**
     * Checks which codelines have annotations and initializes a CommentController
     * for each of them.
     * @private
     */
    n.CodeCorrection.prototype._buildAnnotations = function() {
        for (var i = 0; i < this.annotations.length; i++) {
            var annotation = this.annotations[i];
            var $hook = this.$rows[annotation.lineNumber];
            var commentController = this._buildCommentContainer($hook);
            commentController.importComments(annotation.comments, this.users);
            $hook.append(this._getCommentMarker(annotation.comments.length));
            this._setListeners(commentController);
            this._setCommentController(annotation.lineNumber, commentController);
        }
    };

    n.CodeCorrection.prototype._getCommentMarker = function(numberOfComments) {
        var container = $('<span/>', {
            class: "badge qfq-comment-marker",
            text: numberOfComments + ' '
        });
        container.append($('<span/>', {
            class: "glyphicon glyphicon-comment"
        }));
        return container;
    };

    /**
     * Builds a Line as a combination of HTML Elements. Binds the necessary Events.
     *
     * @param lineCount
     * @param line
     * @returns {jQuery|HTMLElement}
     * @private
     */
    n.CodeCorrection.prototype._buildLine = function(lineCount, line) {
        var that = this;
        var htmlRow = $('<div/>', {
            class: 'clearfix qfqCodeLine',
            id: 'qfqC' + lineCount
        });
        htmlRow.on("click", function() { that._handleClick(htmlRow, lineCount);});
        var htmlLineNumber = $('<div/>', {
            class: 'pull-left qfqLineCount',
            text: lineCount
        });
        var cLine = line.replace('&', '&amp;')
            .replace(';', '&semi;')
            .replace('<', '&lt;')
            .replace('>', '&gt;')
            .replace(/\s/g, '&nbsp;')
            .replace('"', '&quot;')
            .replace('\'', '&apos;')
            .replace('\\', '&bsol;');
        cLine = this.syntaxHighlight.highlightLine(cLine);
        var htmlCodeLine = $('<div/>', {
            class: 'pull-right qfqCode'
        });
        htmlCodeLine.html(cLine);
        htmlLineNumber.appendTo(htmlRow);
        htmlCodeLine.appendTo(htmlRow);
        return htmlRow;
    };

    /**
     * Initializes a CommentContainer at a given jQuery Hook and returns
     * the CommentContainer object
     * @param $hook
     * @returns {QfqNS.CommentController}
     * @private
     */
    n.CodeCorrection.prototype._buildCommentContainer = function($hook) {
        var options = {
            readOnly: this.readOnly
        };
        var commentController = new n.CommentController();
        commentController.buildContainer($hook, options);
        commentController.setCurrentUser(this.currentUser);
        return commentController;
    };

    /**
     * References the CommentController in this.annotations Array
     * for easy access later.
     * @param lineCount
     * @param commentController
     * @returns {boolean}
     * @private
     */
    n.CodeCorrection.prototype._setCommentController = function(lineCount, commentController) {
        for (var i=0; i < this.annotations.length; i++) {
            if (this.annotations[i].lineNumber === lineCount) {
                this.annotations[i].commentController = commentController;
                return true;
            }
        }
        return false;
    };

    /**
     * Sets listeners for events generated by the CommentController object
     * @param commentController
     * @private
     */
    n.CodeCorrection.prototype._setListeners = function(commentController) {
        var that = this;
        commentController.on('comment.added', function(argument) {
            console.log("Catch event: " + that.annotations.length);
            console.log("With data: " + argument.data);
            that._handleNew(argument.data);
        });
        commentController.on('comment.edited', function() {
            that._updateJSON();
        });
        commentController.on('comment.removed', function(e) {
            console.log(e);
            if(that._checkUserRemoval(e.data.uid)) {
                console.log("Removed User uid: " + e.data.uid);
            }
            that._checkLineRemoval();
            that._updateJSON();
        });
    };

    n.CodeCorrection.prototype._checkLineRemoval = function() {
        var removeLines = [];
        for (var i = 0; i < this.annotations.length; i++) {
            var comments = this.annotations[i].commentController.exportComments();

            if(comments.length == 0) {
                removeLines.push(i);
            }

        }

        for (var ii = 0; ii < removeLines.length; ii++) {
            this.annotations.splice(removeLines[ii], 1);
        }
    };

    n.CodeCorrection.prototype._handleNew = function(eventData) {
        this._addCurrentUser();
        this._updateJSON();
    };

    n.CodeCorrection.prototype._checkUserRemoval = function(uid) {
        var removeUser = true;
        for (var i = 0; i < this.annotations.length; i++) {
            var comments = this.annotations[i].commentController.exportComments();
            for (var ii = 0; ii < comments.length; ii++) {
                if(comments[ii].uid === uid) {
                    removeUser = false;
                    return false;
                }
            }
        }
        if (removeUser) {
            for (var iii = 0; iii < this.users.length; iii++) {
                if (this.users[iii].uid === uid) {
                    this.users.splice(i, 1);
                }
            }
        }
        return true;
    };

    n.CodeCorrection.prototype._addCurrentUser = function() {
        if (!this.checkUserExists(this.currentUser.uid)) {
            this.users.push(this.currentUser);
        }
    };

    n.CodeCorrection.prototype.checkUserExists = function(uid) {
        for (var i = 0; i < this.users.length; i++) {
            if (this.users[i].uid === uid) {
                return true;
            }
        }
        return false;
    };

    n.CodeCorrection.prototype._updateJSON = function() {
        var jexport = {};
        jexport.annotations = [];
        for (var i = 0; i < this.annotations.length; i++) {
            var annotation = {
                lineNumber: this.annotations[i].lineNumber,
                comments: this.annotations[i].commentController.exportComments()
            };
            jexport.annotations.push(annotation);
        }
        jexport.users = this.users;
        this.$target.val(JSON.stringify(jexport));
        var that = this;
        if (this.page.qfqForm) {
            this.page.qfqForm.eventEmitter.emitEvent('form.changed',
                n.EventEmitter.makePayload(that, null));
            this.page.qfqForm.changeHandler();
            this.page.qfqForm.form.formChanged = true;
        } else {
            console.log(this.page);
            throw("Error: Couldn't initialize qfqForm - not possible to send form.changed event");
        }
    };


    /**
     * Places a comment editor under the hook.
     * @param $hook
     * @param lineCount
     * @private
     */
    n.CodeCorrection.prototype._handleClick = function($hook, lineCount) {
        var comments = {};
        if (this._hasComments(lineCount)) {
            comments = this._getComments(lineCount);
            comments.commentController.toggle();
            comments.commentController.emitEvent("new");
        } else {
            if (!this.readOnly) {
                comments.lineNumber = lineCount;
                comments.commentController = new n.CommentController();
                comments.commentController.buildContainer($hook, {readOnly: this.readOnly});
                comments.commentController.setCurrentUser(this.currentUser);
                comments.commentController.displayEditor();
                this._setListeners(comments.commentController);
                this.annotations.push(comments);
            }
        }
    };

    /**
     * Checks if a line already has comments
     * @param lineCount
     * @returns {boolean}
     * @private
     */
    n.CodeCorrection.prototype._hasComments = function(lineCount) {
        for (var i=0; i < this.annotations.length; i++) {
            if (this.annotations[i].lineNumber === lineCount) {
                return true;
            }
        }
        return false;
    };

    /**
     * Gets comments for a specific line or returns false if said
     * comments can't be found.
     * @param lineCount
     * @returns {*}
     * @private
     */
    n.CodeCorrection.prototype._getComments = function(lineCount) {
        for (var i=0; i < this.annotations.length; i++) {
            if (this.annotations[i].lineNumber === lineCount) {
                return this.annotations[i];
            }
        }
        return false;
    };

})(QfqNS);