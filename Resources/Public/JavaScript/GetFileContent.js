var intervalId = 0;
var timer = 0;
var filePath = "";
var answerBlob = "";
var httpRequest = false;

onmessage = function(e) {
    timer = e.data[1] || 1000;
    filePath = e.data[0];
    intervalId = setInterval(getFileContent, timer);

};

function getFileContent() {
    if (!httpRequest) {
        httpRequest = new XMLHttpRequest();
        if (httpRequest.overrideMimeType) {
            httpRequest.overrideMimeType('text/plain');
        }

        httpRequest.onreadystatechange = postFileContent;
        httpRequest.open('GET', filePath, true);
        httpRequest.send(null);
        answerBlob = httpRequest.responseText;
        console.log("XML Request sent");
    }
}

function postFileContent() {
    if (httpRequest.readyState === 4) {
        console.log("XML Request received answer");
        postMessage(httpRequest.responseText);
        httpRequest = false;
    }
}