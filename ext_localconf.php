<?php
/**
 * @author Carsten Rose <carsten.rose@math.uzh.ch>
 */
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'IMATHUZH.' . $_EXTKEY,
    'Qfq',
    array('Qfq' => 'show'),
    array('Qfq' => 'show'), // put here as well, if controller output must not be cached
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:qfq/Configuration/PageTSconfig/PageTSconfig.ts">');

// By default, the 'tt_content' Header will be rendered: Avoid this,cause it's much nicer to use the header in the backend as a description title of what the record does.
$addLine = '
tt_content.qfq_qfq = COA
tt_content.qfq_qfq {
        10 >
}
';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'setup', '# Setting ' . $_EXTKEY . $addLine . '', 'defaultContentRendering');
