<?php

// QFQ configuration
//
// Save this file as: <site path>/typo3conf/config.qfq.php

return [
    'DB_1_USER' => '<DBUSER>',
    'DB_1_SERVER' => '<DBSERVER>',
    'DB_1_PASSWORD' => '<DBPW>',
    'DB_1_NAME' => '<DB>',

    //DB_2_USER => <DBUSER>
    //DB_2_SERVER => <DBSERVER>
    //DB_2_PASSWORD => <DBPW>
    //DB_2_NAME => <DB>

    // DB_n ...
    // ...

    // LDAP_1_RDN => 'ou=Admin,ou=example,dc=com'
    // LDAP_1_PASSWORD => 'mySecurePassword'
];
